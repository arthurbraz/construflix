# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-04-30 12:45
from __future__ import unicode_literals

from django.db import migrations, models
import utils.validators


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0004_auto_20180430_0929'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='bloqueado',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='profile',
            name='prazo_acesso',
            field=models.DateField(null=True, verbose_name='Prazo de Expiração de Acesso'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='cpf',
            field=models.CharField(max_length=11, null=True, validators=[utils.validators.validate_CPF]),
        ),
        migrations.AlterField(
            model_name='profile',
            name='funcao',
            field=models.CharField(max_length=100, null=True, verbose_name='Função/Cargo'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='user_type',
            field=models.IntegerField(choices=[(0, 'Normal'), (1, 'Assinante'), (2, 'Projetista')], default=0, verbose_name='Tipo'),
        ),
    ]
