# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-04-23 10:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='user_type',
            field=models.IntegerField(choices=[(0, 'Normal'), (1, 'Assinante'), (2, 'Avaliador')], default=0),
        ),
    ]
