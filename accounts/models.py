from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from utils.validators import validate_CPF

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    USER_TYPE_PRINCIPAL = 0
    USER_TYPE_ASSINANTE = 1
    USER_TYPE_AVALIADOR = 2
    USER_TYPES = (
        (USER_TYPE_PRINCIPAL, "Normal"),
        (USER_TYPE_ASSINANTE, "Assinante"),
        (USER_TYPE_AVALIADOR, "Avaliador"),
    )
    user_type = models.IntegerField("Tipo", choices=USER_TYPES, null=False, default=0)
    assinante = models.IntegerField(null=True)
    cpf = models.CharField(max_length=11, validators=[validate_CPF], null=True, unique=True) #unique=True,
    empresa = models.CharField(max_length=100 , null=True)
    funcao = models.CharField("Função/Cargo", max_length=100 , null=True)
    bloqueado = models.BooleanField(default=False)
    prazo_acesso = models.DateField("Prazo de Expiração de Acesso", null=True)
    pode_finalizar_analise = models.BooleanField(u"Pode finalizar análise incompleta?", help_text= u"Caso marcado, usuário poderá finalizar análises que não estão completas.", default=False)

    def acesso_expirado(self):
        return self.prazo_acesso and self.prazo_acesso < timezone.now().date()

    def username(self):
        return self.user.username

    def first_name(self):
        return self.user.first_name

    def email(self):
        return self.user.email

    def obter_assinante(self):
        if self.user_type == Profile.USER_TYPE_ASSINANTE:
            return self.user
        elif self.user_type == Profile.USER_TYPE_AVALIADOR:
            assinante = User.objects.get(pk=self.assinante)
            return assinante
        return self.user

"""
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    if hasattr(instance, "profile"):
        instance.profile.save()

"""