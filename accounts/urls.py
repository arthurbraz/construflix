from django.conf.urls import include, url
from django.contrib.auth import views as auth_views
from accounts import views as accounts_views
from .forms import CustomAuthenticationForm



#from django.conf.urls.i18n import i18n_patterns

urlpatterns = [
    #url(r'^entrar/$', 'django.contrib.auth.views.login',         {'template_name': 'accounts/login.html'}, name='login'),
    #url(r'^sair/$', 'django.contrib.auth.views.logout',            {'next_page': 'core:home'}, name='logout'),
    url(r'^cadastre-se/$', accounts_views.register,         name='register'),
    url(r'^entrar/$', auth_views.LoginView.as_view(template_name = 'accounts/login.html',
          authentication_form = CustomAuthenticationForm),        name='login'),
    url(r'^sair/$', auth_views.logout, {'next_page': '/'}, name='logout'),
    url(r'^password/$', accounts_views.change_password, name='change_password'),


    url(r'^profile/lista/$', accounts_views.ProfileListView.as_view(), name='profile_list'),
    url(r'^profile/add/$', accounts_views.add_profile, name='profile_add'),
    url(r'^profile/(?P<pk>[0-9]+)/update/$', accounts_views.update_profile, name='profile_update'),
    url(r'^profile/(?P<pk>[0-9]+)/delete/$', accounts_views.delete_profile, name='profile_delete'),
    url(r'^profile/$', accounts_views.ProfileView.as_view(), name='profile'),

    url(r'^password_change/done/$',
        auth_views.PasswordChangeDoneView.as_view(template_name='accounts/password_change_done.html'),
        name='password_change_done'),
    url(r'^password_reset/$', accounts_views.UserPasswordResetView.as_view(), name='password_reset'),
    url(r'^password_reset/done/$',
         auth_views.PasswordResetDoneView.as_view(template_name='accounts/password_reset_done.html'),
        name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.PasswordResetConfirmView.as_view(template_name='accounts/password_reset_confirm.html'),
        name='password_reset_confirm'),
    url(r'^reset/done/$',
        auth_views.PasswordResetCompleteView.as_view(template_name='accounts/password_reset_complete.html'),
        name='password_reset_complete'),



]