from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, UsernameField
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from accounts.models import Profile
from utils.pycpfcnpj.cpf import validate as cpf_validate
from checklist.models import Plano, Assinatura
from django.utils.timezone import now as timezone_now, timedelta

class UserForm(forms.ModelForm):
    # declare the fields you will show
    username = forms.CharField(label="Usuário", required=True, help_text=u"Utilizado para fazer login no sistema; apenas letras, números e os caracteres @/,/+/./_")
 
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError('Já existe usuário com este E-mail')
        return email



class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('cpf', 'funcao')
        
    def clean_cpf(self):
        
        cpf = self.cleaned_data['cpf']
        if Profile.objects.filter(cpf=cpf).exists():
            raise forms.ValidationError('Já existe usuário com este CPF')
        
        if  not  cpf_validate(cpf):
            raise forms.ValidationError('CPF inválido')
        return cpf



class UsuarioCreationForm(UserCreationForm):
    cpf = forms.CharField(label='CPF', required=True, max_length=11, widget=forms.TextInput(attrs={'type':'number' , 'autocomplete': 'off','pattern':'[0-9]+', 'title':'Informe seu CPF. Somente números.'}))
    first_name = forms.CharField(label="Nome completo", required=True)
    email = forms.EmailField(label = "E-mail", required=True)

    # declare the fields you will show
    username = forms.CharField(label="Usuário", required=True, help_text=u"Utilizado para fazer login no sistema; apenas letras, números e os caracteres @/,/+/./_")
    # first password field
    password1 = forms.CharField(label="Senha", widget=forms.PasswordInput, required=True)
    # confirm password field
    password2 = forms.CharField(label="Confirmação de senha", widget=forms.PasswordInput, required=True)
    funcao = forms.CharField(label='Função/Cargo', required=True)
    pode_finalizar_analise = forms.BooleanField(label=u'Este usuário poderá finalizar análise incompleta (sem que todos itens tenham sido analisados)? Análises finalizadas não podem ser editadas', required=False)


    # this sets the order of the fields
    class Meta:
        model = User
        fields = ( 'cpf', 'first_name', 'email', 'username', 'password1', 'password2', 'funcao', 'pode_finalizar_analise'  )


    def clean_cpf(self):

        cpf = self.cleaned_data['cpf']
        if Profile.objects.filter(cpf=cpf).exists():
            raise forms.ValidationError('Já existe usuário com este CPF')
        
        if  not  cpf_validate(cpf):
            raise forms.ValidationError('CPF inválido')
        return cpf

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError('Já existe usuário com este E-mail')
        return email

    def save(self, commit=True):
        user = super(UsuarioCreationForm, self).save(commit=False)


        if commit:
            #import pdb; pdb.set_trace()
            user.save()

            assinatura = Assinatura.objects.get(assinante = self.user)


            Profile.objects.create(user=user)
            user.profile.assinante = self.user.id
            user.profile.user_type = Profile.USER_TYPE_AVALIADOR
            user.profile.cpf = self.cleaned_data['cpf']
            #user.profile.empresa = self.cleaned_data['empresa']
            user.profile.funcao = self.cleaned_data['funcao']
            user.profile.prazo_acesso = assinatura.dt_fim_vigencia +  timedelta(days=30)  
            user.profile.pode_finalizar_analise = self.cleaned_data['pode_finalizar_analise']

            user.profile.save()

        return user




class UsuarioChangeForm(forms.ModelForm):
    
    cpf = forms.CharField(label='CPF', required=True, max_length=11, widget=forms.TextInput(attrs={'type':'number' , 'autocomplete': 'off','pattern':'[0-9]+', 'title':'Informe seu CPF. Somente números.'}))
    first_name = forms.CharField(label="Nome", required=True)

    # declare the fields you will show
    username = forms.CharField(label="Usuário", required=True, help_text= u"Login utilizado para acessar o sistema. Podem conter alfanuméricos, _, @, +,. e - caracteres.")
    email = forms.EmailField(label = "Email", required=True)
    funcao = forms.CharField(label='Função/Cargo', required=True)
    pode_finalizar_analise = forms.BooleanField(label=u'Este usuário poderá finalizar análise incompleta (sem que todos itens tenham sido analisados)? Análises finalizadas não podem ser editadas.', required=False)

    def __init__(self,  *args, **kwargs):
        super(UsuarioChangeForm, self).__init__(*args, **kwargs)
        self.fields['cpf'].initial = self.instance.profile.cpf
        self.fields['funcao'].initial = self.instance.profile.funcao
        self.fields['pode_finalizar_analise'].initial = self.instance.profile.pode_finalizar_analise


    # this sets the order of the fields
    class Meta:
        model = User
        field_classes = {'username': UsernameField}
        fields = ( 'cpf', 'first_name', 'email', 'username', 'funcao', 'pode_finalizar_analise'  )



    def clean_cpf(self):
        cpf = self.cleaned_data['cpf']
        if self.instance.profile.cpf != cpf:
            if Profile.objects.filter(cpf=cpf).exists():
                raise forms.ValidationError('Já existe usuário com este CPF')
            
            if  not  cpf_validate(cpf):
                raise forms.ValidationError('CPF inválido')
        return cpf

    def clean_email(self):
        email = self.cleaned_data['email']
        if self.instance.email != email:
            if User.objects.filter(email=email).exists():
                raise forms.ValidationError('Já existe usuário com este E-mail')
        return email

    def save(self, commit=True):
        
        user = super(UsuarioChangeForm, self).save(commit=False)

        if commit:

            user.save()
            user.profile.cpf = self.cleaned_data['cpf']
            user.profile.funcao = self.cleaned_data['funcao']  
            user.profile.pode_finalizar_analise = self.cleaned_data['pode_finalizar_analise'] 
            user.profile.save()

        return user







class RegisterForm(UserCreationForm):

    # declare the fields you will show
    username = forms.CharField(label="Usuário", required=True, help_text=u"Utilizado para fazer login no sistema; apenas letras, números e os caracteres @/,/+/./_")

    # first password field
    password1 = forms.CharField(label="Senha", widget=forms.PasswordInput)
    # confirm password field
    password2 = forms.CharField(label="Confirmação de senha", widget=forms.PasswordInput)
    email = forms.EmailField(label = "Email", required=True)

    cpf = forms.CharField(label='CPF', required=True, max_length=11, widget=forms.TextInput(attrs={'type':'number' , 'autocomplete': 'off','pattern':'[0-9]+', 'title':'Informe seu CPF. Somente números.'}))
    first_name = forms.CharField(label="Nome completo", required=True)

    empresa = forms.CharField(label='Empresa', required=True)
    funcao = forms.CharField(label='Função/Cargo', required=True)

    plano = forms.ChoiceField(label='Plano',  choices=[], required=True)
 
    # this sets the order of the fields
    class Meta:
        model = User
        #fields = (  "username", "password1", "password2", "email", )
        fields = ( 'cpf', 'first_name', 'email', 'username', 'password1', 'password2', 'funcao', 'empresa'  )
 

    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)
        planos = Plano.objects.filter(ic_ativo=True).order_by("valor")
        self.fields['plano'] = forms.ChoiceField(choices=[ (o.id, str(o)) for o in planos])

    def clean_cpf(self):

        cpf = self.cleaned_data['cpf']
        if Profile.objects.filter(cpf=cpf).exists():
            raise forms.ValidationError('Já existe usuário com este CPF')
        
        if  not  cpf_validate(cpf):
            raise forms.ValidationError('CPF inválido')
        return cpf

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError('Já existe usuário com este E-mail')
        return email

    def save(self, commit=True):
        user = super(RegisterForm, self).save(commit=False)


        if commit:
            #import pdb; pdb.set_trace()
            user.save()

            assinatura = Assinatura()
            plano = Plano.objects.get(pk = self.cleaned_data['plano'])

            assinatura.assinante = user
            assinatura.plano = plano
            assinatura.dt_inicio_vigencia = timezone_now()
            assinatura.dt_fim_vigencia = timezone_now() +   timedelta(days=plano.prazo)   
            assinatura.save()


            Profile.objects.create(user=user)
            user.profile.user_type = Profile.USER_TYPE_ASSINANTE
            user.profile.cpf = self.cleaned_data['cpf']
            user.profile.empresa = self.cleaned_data['empresa']
            user.profile.funcao = self.cleaned_data['funcao']
            user.profile.prazo_acesso = assinatura.dt_fim_vigencia +  timedelta(days=30)    
            user.profile.save()

        return user


class CustomAuthenticationForm(AuthenticationForm):

    error_messages = {
        'invalid_login':  "Por favor, informe  login/senha. Ambos são sensíveis à caixa das letras.",
        'inative_login':  "Cadastro inativo.",
        'expirated_login':  "Cadastro expirado.",
        'blocked_login':  "Cadastro bloqueado.",
        'inactive': "Conta inativa.",
    }

    def confirm_login_allowed(self, user):

        if not user.is_active:
            raise forms.ValidationError('Cadastro inativo.', code='inative_login')

        if hasattr(user, "profile"):
            if user.profile.bloqueado:
                raise forms.ValidationError('Cadastro bloqueado.', code='blocked_login')

            if user.profile.acesso_expirado():
                raise forms.ValidationError('Cadastro expirado.', code='expirated_login')