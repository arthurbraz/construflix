from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect, get_object_or_404

#from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from .forms import *
from portal import settings
from django.http import JsonResponse
from django.urls import reverse, reverse_lazy
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth import views as auth_views
from django.views import generic
from django_tables2 import MultiTableMixin, RequestConfig, SingleTableMixin, SingleTableView
from .tables import *
from django.views.generic.base import TemplateView
from utils.forms import PassRequestToFormViewMixin, AjaxableResponseMixin

#def update_profile(request, user_id):
#    user = User.objects.get(pk=user_id)
#    #user.profile.bio = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit...'
#    user.save()

@login_required
@transaction.atomic
def delete_profile(request,  pk):
    profile = get_object_or_404(Profile, pk=pk)

    try:
        #profile.user.delete()
        profile.user.is_active = False
        profile.user.save()
        messages.success(request, u"Usuário excluído com sucesso!")
        data = {
                'message': u"Usuário excluído com sucesso!"
            }
    except:
        messages.error(request,'Ocorreu um erro ao tentar excluir usuário')
        data = {
            'message': u"Ocorreu um erro ao tentar excluir usuário!"
        }
    

    return JsonResponse(data)



@login_required
@transaction.atomic
def update_profile(request,  pk):
    profile = get_object_or_404(Profile, pk=pk)
    if request.method == 'POST':       
        form = UsuarioChangeForm(request.POST, instance=profile.user)
        if form.is_valid() :
            form.save()
            messages.success(request, u"Usuário atualizado com sucesso!")
            data = {
                    'message': u"Usuário atualizado com sucesso!"
                }
            return JsonResponse(data)
        else:
            messages.error(request, u'Corrija os erros abaixo.')
            return JsonResponse(form.errors, status=400)
    else:
        form = UsuarioChangeForm(instance=profile.user)
    return render(request, 'accounts/profile.html', {
        'form': form
    })

@login_required
@transaction.atomic
def add_profile(request):

    if request.method == 'POST':
        form = UsuarioCreationForm( request.POST  )
        form.user = request.user
        if form.is_valid():
            form.save()
            messages.success(request, u"Usuário criado com sucesso!")
            data = {
                    'message': u"Usuário criado com sucesso!"
                }
            return JsonResponse(data)
        else:
            messages.error(request, u'Corrija os erros abaixo.')
            return JsonResponse(form.errors, status=400)

    else:
        form = UsuarioCreationForm()
        form.user = request.user
    return render(request, 'accounts/profile.html', {
        'form': form
    })


def register(request):
    template_name = 'accounts/register.html'
    
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            user = authenticate(
                username=user.username, password=form.cleaned_data['password1']
            )
            login(request, user)
            return redirect(settings.LOGIN_REDIRECT_URL)
    else:
        form = RegisterForm()

    context = {
        'form': form,
        'concorda' : request.POST.get("concorda")
    }
    return render(request, template_name, context)




def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Senha alterada com sucesso!')
            #return redirect('change_password')

            data = {
                'message': 'Senha alterada com sucesso!',
                'result': 'success'
            }
            return JsonResponse(data)

        else:
            messages.error(request, 'Por favor, corrija o erro abaixo.')
            data = {
                'message': 'Por favor, corrija os erros abaixo.',
                'result': 'error',
                'errors' : form.errors
            }
            return JsonResponse(data)
            
    else:
        form = PasswordChangeForm(request.user)
        return render(request, 'accounts/change_password.html', {'form': form  })


class UserPasswordResetView(auth_views.PasswordResetView):
    template_name           = 'accounts/password_reset_form.html'
    email_template_name     = 'accounts/password_reset_email.html'
    subject_template_name   = 'accounts/password_reset_subject.txt'
    success_url = reverse_lazy('accounts:password_reset_done')



class ProfileListView(generic.ListView):
    model = Profile
    template_name = 'accounts/profile_list.html'
    context_object_name = 'profile_list'


    def get_context_data(self, **kwargs):
        context = super(ProfileListView, self).get_context_data(**kwargs)
        table = ProfileTable(self.get_queryset())
        RequestConfig(self.request, paginate={'per_page': 30}).configure(table)
        context['table'] = table
        return context

    def get_queryset(self):
        results = Profile.objects.filter(assinante = self.request.user.id).filter(user__is_active=True)
        return results



class ProfileView(TemplateView):
    template_name = "accounts/profile_form.html"