#-*- coding: utf-8 -*-
import django_tables2 as tables
from django_tables2.utils import A
from .models import Profile

class ProfileTable(tables.Table):

    username = tables.LinkColumn('accounts:profile_update', args=[A('pk')], verbose_name="Usuário",
                 attrs={
                    'a': {"onclick": "return editarProfile(this);"}
             })

    first_name = tables.LinkColumn('accounts:profile_update', args=[A('pk')], verbose_name="Nome",
                 attrs={
                    'a': {"onclick": "return editarProfile(this);",
                    'td': {'class': 'text-center'}}
             })

    email = tables.LinkColumn('accounts:profile_update', args=[A('pk')], verbose_name="E-mail",
                 attrs={
                    'a': {"onclick": "return editarProfile(this);",
                    'td': {'class': 'text-center'}}
             })

    remover = tables.LinkColumn('accounts:profile_delete', args=[A('pk')],
                            text="      ",
                            attrs={'a': {'class': 'fa fa-trash', 'title': 'Remover' ,
                                        "onclick": "return removerProfile(this);", 
                                    'td': {'class': 'text-center'} }})


    class Meta:
        model = Profile
        template_name = 'django_tables2/bootstrap.html'
        fields = ('username', 'first_name' , 'email' ,'funcao',     'pode_finalizar_analise' )
        attrs = {"class": "table table-striped table-bordered"}
        empty_text = "Nenhum usuário cadastrado."
        orderable = False

    #def render_username(self, value):
    #    import pdb; pdb.set_trace()
    #    return '<%s>' % value
