from django.apps import AppConfig


class MuomConfig(AppConfig):
    name = 'muom'
