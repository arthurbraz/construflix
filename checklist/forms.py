#-*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.db.models import Q
from django.forms import inlineformset_factory, formset_factory
from django.forms.formsets import BaseFormSet
from django.forms.models import BaseModelFormSet
from django.forms import BaseInlineFormSet
from django.forms  import widgets
from django.core.exceptions import ObjectDoesNotExist
from municipios.widgets import SelectMunicipioWidget
from municipios.models import Municipio
from bootstrap3_datetime.widgets import DateTimePicker
from .models import *
from django.conf import settings


class ChecklistForm(forms.ModelForm):

    class Meta:
        model = Checklist
        fields = '__all__'

class EmpreendimentoForm( forms.ModelForm):

    #municipio = forms.IntegerField(label=u"UF - Município", widget=SelectMunicipioWidget)
    endereco = forms.CharField(label=u"Endereço")

    class Meta:
        model = Empreendimento
        fields = ['nome',  'municipio', 'endereco', 'descricao']

    def __init__(self,  *args, **kwargs):
        self.request = kwargs.pop("request")
        super().__init__(*args, **kwargs)

    def save(self):
        #data = self.cleaned_data
        user = self.request.user
        if not self.instance.pk: #novo
            if hasattr(user, "profile"):
                self.instance.assinante = user.profile.obter_assinante()
            else:
                self.instance.assinante = user
            self.instance.ic_ativo = True
        self.instance.save()

        return self.instance


class AvaliacaoForm( forms.ModelForm):       

    class Meta:
        model = Avaliacao
        fields = ['projeto', 'checklist', 'descricao']

    def __init__(self,  *args, **kwargs):
        self.request = kwargs.pop("request")
        super(AvaliacaoForm, self).__init__(*args, **kwargs)
        empreendimentos = Empreendimento.objects.assinante(self.request.user)

        
        self.fields['projeto'].queryset = Projeto.objects.filter(empreendimento__in=empreendimentos)
        self.fields['checklist'].queryset = Checklist.objects.none()

        if 'checklist' in self.data:
            try:
                projeto_id = int(self.data.get('projeto'))
                projeto = Projeto.objects.get(pk=projeto_id)
                self.fields['checklist'].queryset = projeto.checklists_disponiveis()
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['checklist'].queryset = self.instance.checklist

    def clean(self):
        """
        Adiciona validacao - verificar se existe projeto/checklist duplicado dentro de um determinado empreendimento, ou seja,
        mesma atividade técnica e etapa de projeto
        """
        if any(self.errors):
            return

        if self.instance.pk is None: #nova avaliacao


            #import pdb; pdb.set_trace()          
            avaliacoes = Avaliacao.objects.filter(projeto = self.cleaned_data['projeto'],
                                            checklist =  self.cleaned_data['checklist']).exclude(status=Avaliacao.CANCELADA)

            if len(avaliacoes):
                raise forms.ValidationError("Lista de verificações já está em uso para este projeto.", code="checklist",)
 
            return super().clean()



    def save(self):
        #user = self.request.user
        novo = self.instance.pk is None

        if not novo:
            self.instance.save()
        else:
            self.instance.avaliador = self.request.user# self.instance.projeto.avaliador #user
            self.instance.save()

            #Todo: filtrar somente checklist vinculado ao plano
            """
            checklist = Checklist.objects.get(
                            atividade_tecnica=self.instance.projeto.atividade_tecnica,
                            etapa_projeto=self.instance.projeto.etapa_projeto,
                            ic_ativo=True
                            )
            itens = checklist.itens()
            """
            
            itens = self.instance.checklist.itens()

            for requisito in itens:
                item = AvaliacaoItem()
                item.avaliacao = self.instance
                item.checklistItem = requisito
                item.save()

        return self.instance


class AvaliacaoItemForm(forms.Form):
    id = forms.IntegerField(widget = widgets.HiddenInput)
    checklistitem_id     = forms.IntegerField(widget = widgets.HiddenInput)
    requisito            = forms.CharField(required=False)
    requisito_geral      = forms.CharField(required=False)
    requisito_especifico = forms.CharField(required=False)
    norma                = forms.CharField(required=False)
    normativo            = forms.CharField(required=False)
    nivel_presenca_info  = forms.ChoiceField( label=u"Nível de presença da informação",  
                            choices = Recomendacao.NIVEL_INFO_CHOICES,
                            initial = Recomendacao.ND)
    info_analisada  = forms.CharField(required=False, widget = forms.Textarea(attrs={'cols': '100', 'rows': 3}))
    observacao          = forms.CharField(required=False, widget = forms.Textarea(attrs={'cols': '100', 'rows': 3}))
    recomendacao_avaliador  = forms.CharField(required=False, widget = forms.Textarea(attrs={'cols': '100', 'rows': 3}))
    recomendacao        = forms.CharField(required=False, widget = widgets.HiddenInput)

    
    

    def save(self, avaliacao):
        data = self.cleaned_data

        if avaliacao.status == Avaliacao.A_REALIZAR:
            avaliacao.status = Avaliacao.EM_ANDAMENTO
            avaliacao.save()

        item = AvaliacaoItem.objects.get(pk=data["id"])
        item.nivel_presenca_info  = data["nivel_presenca_info"]
        item.observacao = data["observacao"]
        item.recomendacao_avaliador = data["recomendacao_avaliador"]
        try:
            checklistitem = ChecklistItem.objects.get(pk=data["checklistitem_id"])
            recomendacao = checklistitem.obterRecomendacao( data["nivel_presenca_info"])
            item.recomendacao = recomendacao.recomendacao
            self.cleaned_data["recomendacao"] = item.recomendacao
        except ObjectDoesNotExist:
            pass
        

        item.save()
        return item


 
class ProjetoSimpleForm( forms.ModelForm):
    class Meta:
        model = Projeto
        fields = ['contratante', 'cnpj','contrato','empreendimento', 'atividade_tecnica', 'etapa_projeto' ,  'responsavel_analise', 'responsavel_validacao']
       
    def __init__(self,  *args, **kwargs):
        self.request = kwargs.pop("request")
        super(ProjetoSimpleForm, self).__init__(*args, **kwargs)
        self.fields['empreendimento'].queryset = Empreendimento.objects.assinante(self.request.user)
        self.fields['atividade_tecnica'].queryset = AtividadeTecnica.objects.all().order_by("titulo")
        self.fields['etapa_projeto'].queryset = EtapaProjeto.objects.all().order_by("nome")

        #verifica se já existe checklist associado. caso nao tenha, poderá alterar atividade/etapa
        if self.instance and self.instance.pk:
            avaliacao = self.instance.obter_avaliacao()
            if avaliacao: #existe avaliacao                         
                self.fields['empreendimento'].widget.attrs['readonly'] = True
                self.fields['atividade_tecnica'].widget.attrs['readonly'] = True
                self.fields['etapa_projeto'].widget.attrs['readonly'] = True

    def clean(self):
        """
        Adiciona validacao - verificar se existe projeto duplicado dentro de um determinado empreendimento, ou seja,
        mesma atividade técnica e etapa de projeto
        """
        if any(self.errors):
            return

        #import pdb; pdb.set_trace()          
        projetos = Projeto.objects.filter(empreendimento = self.cleaned_data['empreendimento'],
                                          atividade_tecnica =  self.cleaned_data['atividade_tecnica'],
                                          etapa_projeto =  self.cleaned_data['etapa_projeto']
                                        )

        if not self.instance.pk and len(projetos):
            # novo projeto 
            raise forms.ValidationError("Já existe projeto para essa atividade técnica e etapa.", code="atividade_tecnica",)
        elif len(projetos):
            # projeto ja cadastrado, verificar alteracao
            if self.instance.empreendimento != self.cleaned_data['empreendimento']      or \
             self.instance.atividade_tecnica != self.cleaned_data['atividade_tecnica']  or \
              self.instance.etapa_projeto != self.cleaned_data['etapa_projeto']:
                raise forms.ValidationError("Já existe projeto para essa atividade técnica e etapa.", code="atividade_tecnica",)

        return super().clean()
        
class ProjetoForm( forms.ModelForm):

    class Meta:
        model = Projeto
        fields = ['contratante', 'cnpj','contrato','empreendimento', 'atividade_tecnica', 'etapa_projeto' ,  'responsavel_analise', 'responsavel_validacao']

    def __init__(self,  *args, **kwargs):
        self.request = kwargs.pop("request")
        super(ProjetoForm, self).__init__(*args, **kwargs)
        self.fields['empreendimento'].queryset = Empreendimento.objects.assinante(self.request.user)
        self.fields['atividade_tecnica'].queryset = AtividadeTecnica.objects.all().order_by("titulo")
        self.fields['etapa_projeto'].queryset = EtapaProjeto.objects.all().order_by("nome")

        #verifica se já existe checklist associado. caso nao tenha, poderá alterar atividade/etapa
        if self.instance and self.instance.pk:
            avaliacao = self.instance.obter_avaliacao()
            if avaliacao: #existe avaliacao                         
                self.fields['empreendimento'].widget.attrs['readonly'] = True
                self.fields['atividade_tecnica'].widget.attrs['readonly'] = True
                self.fields['etapa_projeto'].widget.attrs['readonly'] = True




    def clean(self):
        """
        Adiciona validacao - verificar se existe projeto duplicado dentro de um determinado empreendimento, ou seja,
        mesma atividade técnica e etapa de projeto
        """
        if any(self.errors):
            return

        #import pdb; pdb.set_trace()          
        projetos = Projeto.objects.filter(empreendimento = self.cleaned_data['empreendimento'],
                                          atividade_tecnica =  self.cleaned_data['atividade_tecnica'],
                                          etapa_projeto =  self.cleaned_data['etapa_projeto']
                                        )

        if not self.instance.pk and len(projetos):
            # novo projeto 
            raise forms.ValidationError("Já existe projeto para essa atividade técnica e etapa.", code="atividade_tecnica",)
        elif len(projetos):
            # projeto ja cadastrado, verificar alteracao
            if self.instance.empreendimento != self.cleaned_data['empreendimento']      or \
             self.instance.atividade_tecnica != self.cleaned_data['atividade_tecnica']  or \
              self.instance.etapa_projeto != self.cleaned_data['etapa_projeto']:
                raise forms.ValidationError("Já existe projeto para essa atividade técnica e etapa.", code="atividade_tecnica",)

        return super().clean()


class ProjetoProjetistaForm( forms.ModelForm):
    class Meta:
        model = ProjetoProjetista
        fields = ['projetista', 'art_rrt' ]

class BaseProjetoProjetistaFormSet(BaseInlineFormSet):
    def __init__(self, *args, **kwargs):
        super(BaseProjetoProjetistaFormSet, self).__init__(*args, **kwargs)
        #self.projeto = kwargs["instance"]
        #self.queryset = ProjetoProjetista.objects.filter(projeto=projeto)

    def clean(self):
        """
        Validações:
        - projetista duplicado
        - ART/RRT duplicado
        """

        super(BaseProjetoProjetistaFormSet, self).clean()
        if any(self.errors):
            return


        projetistas = []
        arts = []
        #duplicates = False

        for form in self.forms:
            if form.cleaned_data:
                projetista = form.cleaned_data.get("projetista")
                art = form.cleaned_data.get("art_rrt")
                if projetista is None and art is None:
                    pass
                else:
                    if projetista is None and art is not None:
                        raise forms.ValidationError(
                            'Informe um projetista.',  code='projetista'
                        )  
                    if art is None and projetista is not None:
                        raise forms.ValidationError(
                            'Campo ART/RTT é obrigatório.',  code='art_rrt'
                        )  
                    
                    if projetista in  projetistas:
                        raise forms.ValidationError(
                            'Projetista duplicado',  code='projetista'
                        )
                    else:
                        projetistas.append(projetista)
                        

                    if art in  arts:
                        raise forms.ValidationError(
                            'ART/RRT duplicado',  code='art_rrt'
                        )
                    else:
                        arts.append(art)





# Create the formset, specifying the form and formset we want to use.
ProjetoProjetistaFormset = inlineformset_factory(Projeto, ProjetoProjetista,    form=ProjetoProjetistaForm,  
            formset=BaseProjetoProjetistaFormSet, extra=1)#, fields=('projetista', 'art_rrt'))


class InformacaoProjetoForm(forms.ModelForm):
    projeto = forms.IntegerField(widget = widgets.HiddenInput)
    data_revisao = forms.DateField(label="Data",
            widget=DateTimePicker(attrs={"class": "form-control"}, 
                            options={"format": "%d/%m/%Y",  "pickTime": False   }))

    #data_revisao = forms.DateField(input_formats=("%d/%m/%Y"))

    def __init__(self,  *args, **kwargs):
        self.request = kwargs.pop("request")
        super(InformacaoProjetoForm, self).__init__(*args, **kwargs)
        self.fields['projeto'].initial = self.request.GET["projeto_id"]


    class Meta:
        model = InformacaoProjeto
        fields = '__all__'
        exclude = ["projeto"]

    def save(self):
        data = self.cleaned_data
        self.instance.projeto = Projeto.objects.get(pk=data["projeto"])
        self.instance.save()
        return self.instance

class ProjetistaForm(forms.ModelForm):
    class Meta:
        model = Projetista
        fields = ['cpf', 'nome', 'crea_cau', 'email']

    def __init__(self,  *args, **kwargs):
        self.request = kwargs.pop("request")
        super(ProjetistaForm, self).__init__(*args, **kwargs)

        

