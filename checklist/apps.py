#-*- coding: utf-8 -*-
from django.apps import AppConfig


class ChecklistConfig(AppConfig):
    name = 'checklist'
    verbose_name = "Project Checklist"
