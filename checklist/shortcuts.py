"""
This module collects helper functions and classes that "span" multiple levels
of MVC. In other words, these functions/classes introduce controlled coupling
for convenience's sake.
"""
import warnings
from django.shortcuts import render, redirect, get_object_or_404
from django.http import (
    Http404, HttpResponse, HttpResponsePermanentRedirect, HttpResponseRedirect,
)
from .models import *

def get_avaliacao_or_404(user, *args, **kwargs):
    """
    retorna avaliacao/analise do usuario locado. 
    """
    try:
        queryset = Avaliacao.objects.assinante(user)
        return queryset.get(*args, **kwargs)
    except AttributeError:
        raise ValueError("Acesso negado.")
    except queryset.model.DoesNotExist:
        raise Http404('No %s matches the given query.' % queryset.model._meta.object_name)