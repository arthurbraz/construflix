#-*- coding: utf-8 -*-
import django_tables2 as tables
from django_tables2.utils import A
from .models import Checklist, Avaliacao, AvaliacaoItem, Empreendimento, Projeto, InformacaoProjeto, AvaliacaoEvolucao
from portal import settings 
class ChecklistsTable(tables.Table):
    class Meta:
        model = Checklist
        template_name = 'checklist/checklists_table.html'
        orderable = False

class AvaliacaoTable(tables.Table):

    empreendimento = tables.LinkColumn('Checklists:editar_avaliacao', args=[A('pk')], verbose_name="Empreendimento",
                 attrs={
                    'a': {"onclick": "return editarAnalise(this);"}
             })
    atividade_tecnica = tables.LinkColumn('Checklists:editar_avaliacao', args=[A('pk')], verbose_name=u"Atividade técnica",
                 attrs={
                    'a': {"onclick": "return editarAnalise(this);"}
             })
    progresso = tables.Column("Progresso", attrs={'td': {'class' : 'text-right' } })
    conformidade = tables.Column(u"Nível de Presença", attrs={'td': {'class' : 'text-right' } })
    modified = tables.DateTimeColumn(format = settings.DATETIME_FORMAT, verbose_name=u"Alterado em", attrs={'td': {'class' : 'text-center' } })
    #resultado = tables.LinkColumn('Checklists:results_avaliacao', args=[A('pk')],
    #                        text="      ",
    #                        attrs={'a': {'class': 'fa fa-eye', 'title': 'Visualizar relatório' ,
    #                                    "onclick": "return visualizarRelatorio(this);" }})
    resultado_simples = tables.LinkColumn('Checklists:avaliacao_pendencias', args=[A('pk')], 
                            verbose_name="Relatório Simplificado",
                            text="      ",
                            attrs={'a': {'class': 'fa fa-eye' , 'title': 'Visualizar relatório simplificado - pendências' ,
                                        "onclick": "return visualizarRelatorio(this);" }})
    
    class Meta:
        model = Avaliacao
        #template_name = 'checklist/avaliacao_table.html'
        template_name = 'django_tables2/bootstrap.html'
        fields = ('empreendimento', 'atividade_tecnica', 'etapa_projeto', "checklist",   'status', 'progresso' ,'conformidade', 'modified')
        attrs = {"class": "table table-striped table-bordered"}
        empty_text = u"Nenhuma análise disponível."
        orderable = False


class AvaliacaoItemTable(tables.Table):
    #requisito_geral  = tables.Column(attrs={'td': {'class' : 'nowrap' } })
    normativo  = tables.Column(u"Referência")
    norma  = tables.Column(u"Item")
    recomendacoes  = tables.Column(u"Recomendações")
    
    class Meta:
        model = AvaliacaoItem
        #template_name = 'checklist/avaliacao_table.html'
        template_name = 'django_tables2/bootstrap.html'
        fields = ( 'requisito_geral' , 'requisito_especifico' , 'normativo','norma', 'requisito', 'nivel_presenca_info' ,'recomendacoes', )
        attrs = {"class": "table table-striped table-bordered"}
        empty_text = u"Nenhum item de análise disponível."
        orderable = False

class AvaliacaoItemSimpleTable(tables.Table):
    #requisito_geral  = tables.Column(attrs={'td': {'class' : 'nowrap' } })
    normativo  = tables.Column(u"Referência")
    norma  = tables.Column(u"Item")
    recomendacao_avaliador  = tables.Column(u"Recomendações")
    
    class Meta:
        model = AvaliacaoItem
        #template_name = 'checklist/avaliacao_table.html'
        template_name = 'django_tables2/bootstrap.html'
        fields = ( 'normativo','norma',  'recomendacao_avaliador', )
        attrs = {"class": "table table-striped table-bordered"}
        empty_text = u"Nenhum item de análise disponível."
        orderable = False

class EmpreendimentoTable(tables.Table):
    nome = tables.LinkColumn('Checklists:empreendimento_update', args=[A('pk')],
            attrs={
                    'a': {"onclick": "return editarEmpreendimento(this);"}
             }

    )
    municipio  = tables.Column(u"Município")
    #foo = tables.TemplateColumn('{{ record.nome }}')
    quantidade_projetos = tables.Column("Projetos", attrs={'td': {'class' : 'text-right' } })

    class Meta:
        model = Empreendimento
        #template_name = 'checklist/avaliacao_table.html'
        template_name = 'django_tables2/bootstrap.html'
        fields = ( 'nome' , 'municipio', 'endereco',  'quantidade_projetos')
        attrs = {"class": "table table-striped table-bordered"}
        empty_text = u"Nenhum empreendimento cadastrado."
        orderable = False


class ProjetoTable(tables.Table):
    created = tables.DateTimeColumn(format = settings.DATETIME_FORMAT, verbose_name=u"Data de Criação", attrs={'td': {'class' : 'text-center' } })
    modified = tables.DateTimeColumn(format = settings.DATETIME_FORMAT, verbose_name=u"Última alteração", attrs={'td': {'class' : 'text-center' } })

    empreendimento = tables.LinkColumn('Checklists:projeto_update', args=[A('pk')],
            attrs={
                    'a': {"onclick": "return editarProjeto(this, event);"}
             }

    )

    class Meta:
        model = Projeto
        #template_name = 'checklist/avaliacao_table.html'
        template_name = 'django_tables2/bootstrap.html'
        fields = ( 'empreendimento' , 'atividade_tecnica' , 'etapa_projeto', 'responsavel_analise', 'created', 'modified' )
        attrs = {"class": "table table-striped table-bordered"}
        empty_text = u"Nenhum empreendimento cadastrado."
        orderable = False


class InformacaoProjetoTable(tables.Table):
    
    nome = tables.LinkColumn('Checklists:info_projeto_update', args=[A('pk')],
            attrs={
                    'a': {"onclick": "return editarInfoProjeto(this, event);"}
             }

    )

    data_revisao = tables.DateColumn(format = settings.DATE_FORMAT, verbose_name=u"Data", attrs={'td': {'class' : 'text-center' } })

    class Meta:
        model = InformacaoProjeto
        #template_name = 'checklist/avaliacao_table.html'
        template_name = 'django_tables2/bootstrap.html'
        fields = (  'nome' , 'prancha', 'conteudo' ,'revisao', 'data_revisao' )
        attrs = {"class": "table table-striped table-bordered"}
        empty_text = u"Nenhum item de análise disponível."
        orderable = False


class AvaliacaoEvolucaoTable(tables.Table):
    created = tables.DateTimeColumn(format = settings.DATETIME_FORMAT, attrs={'td': {'class' : 'text-center' } }, verbose_name="Data")
    conformidade = tables.Column(u"Nível de Presença", attrs={'td': {'class' : 'text-right' } })
    criado_por = tables.Column(u"Criado por", attrs={'td': {'class' : 'text-left' } })

    class Meta:
        model = AvaliacaoEvolucao
        template_name = 'django_tables2/bootstrap.html'
        fields = ( 'criado_por', 'created', 'progresso', 'conformidade', 'comentario')
        attrs = {"class": "table table-striped table-bordered"}
        empty_text = "Nenhum evolução cadastrado."
        orderable = False