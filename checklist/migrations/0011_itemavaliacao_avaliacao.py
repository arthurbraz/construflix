# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-03-05 01:52
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('checklist', '0010_auto_20180304_1628'),
    ]

    operations = [
        migrations.AddField(
            model_name='itemavaliacao',
            name='avaliacao',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='checklist.Avaliacao'),
            preserve_default=False,
        ),
    ]
