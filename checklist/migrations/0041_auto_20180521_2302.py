# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-05-21 23:02
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('checklist', '0040_auto_20180509_0852'),
    ]

    operations = [
        migrations.CreateModel(
            name='Assinatura',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(editable=False, verbose_name='creation date and time')),
                ('modified', models.DateTimeField(editable=False, null=True, verbose_name='modification date and time')),
                ('dt_inicio_vigencia', models.DateField(default=django.utils.timezone.now, verbose_name='Data de Início')),
                ('dt_fim_vigencia', models.DateField(blank=True, default=django.utils.timezone.now, verbose_name='Data Fim')),
                ('ic_ativo', models.BooleanField(default=True, verbose_name='Ativo')),
                ('assinante', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='plano',
            name='prazo',
            field=models.IntegerField(default=365, help_text='Quantidade de dias da vigência da assinatura', verbose_name='Fidelização'),
        ),
        migrations.AddField(
            model_name='plano',
            name='qtd_usuario',
            field=models.IntegerField(default=0, help_text='Quantidade de usuários que assinante poderá cadastrar', verbose_name='Quantidade de usuários'),
        ),
        migrations.AlterField(
            model_name='plano',
            name='descricao',
            field=models.TextField(blank=True, default='', max_length=1000, verbose_name='Descrição'),
        ),
        migrations.AlterField(
            model_name='plano',
            name='nome',
            field=models.CharField(max_length=200, verbose_name='Nome'),
        ),
        migrations.AlterField(
            model_name='plano',
            name='valor',
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=6, verbose_name='Valor (R$)'),
        ),
        migrations.AlterField(
            model_name='projeto',
            name='atividade_tecnica',
            field=models.ForeignKey(help_text='Atividades técnicas executadas no projeto', on_delete=django.db.models.deletion.CASCADE, to='checklist.AtividadeTecnica', verbose_name='Atividade Técnica'),
        ),
        migrations.AddField(
            model_name='assinatura',
            name='plano',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='checklist.Plano'),
        ),
    ]
