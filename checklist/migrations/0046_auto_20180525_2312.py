# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-05-25 23:12
from __future__ import unicode_literals

from django.db import migrations, models
import utils.validators


class Migration(migrations.Migration):

    dependencies = [
        ('checklist', '0045_auto_20180525_2236'),
    ]

    operations = [
        migrations.AlterField(
            model_name='projeto',
            name='cnpj',
            field=models.CharField(blank=True, max_length=14, null=True, validators=[utils.validators.validate_CNPJ], verbose_name='CNPJ'),
        ),
        migrations.AlterField(
            model_name='projeto',
            name='contrato',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='Contrato'),
        ),
        migrations.AlterField(
            model_name='projeto',
            name='responsavel_analise',
            field=models.CharField(blank=True, default='', max_length=200, null=True, verbose_name='Responsável pela análise'),
        ),
        migrations.AlterField(
            model_name='projeto',
            name='responsavel_validacao',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='Responsável pela validação'),
        ),
    ]
