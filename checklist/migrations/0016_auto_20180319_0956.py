# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-03-19 12:56
from __future__ import unicode_literals

import checklist.models
from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('checklist', '0015_auto_20180317_1848'),
    ]

    operations = [
        migrations.RenameField(
            model_name='produto',
            old_name='title',
            new_name='titulo',
        ),
        migrations.RemoveField(
            model_name='plano',
            name='descircao',
        ),
        migrations.RemoveField(
            model_name='projeto',
            name='dt_fim_publicacao',
        ),
        migrations.RemoveField(
            model_name='projeto',
            name='dt_inicio_publicacao',
        ),
        migrations.AddField(
            model_name='empreendimento',
            name='assinante',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='plano',
            name='descricao',
            field=models.TextField(blank=True, default='', max_length=1000, verbose_name='description'),
        ),
        migrations.AddField(
            model_name='plano',
            name='dt_fim_publicacao',
            field=models.DateField(blank=True, default=django.utils.timezone.now, verbose_name='Data Expiração'),
        ),
        migrations.AddField(
            model_name='plano',
            name='dt_inicio_publicacao',
            field=models.DateField(default=django.utils.timezone.now, verbose_name='Data de Publicação'),
        ),
        migrations.AddField(
            model_name='produto',
            name='dt_fim_publicacao',
            field=models.DateField(blank=True, default=django.utils.timezone.now, verbose_name='Data Expiração'),
        ),
        migrations.AddField(
            model_name='produto',
            name='dt_inicio_publicacao',
            field=models.DateField(default=django.utils.timezone.now, verbose_name='Data de Publicação'),
        ),
        migrations.AddField(
            model_name='produto',
            name='folder',
            field=models.ImageField(blank=True, upload_to='produtos'),
        ),
        migrations.AddField(
            model_name='produto',
            name='ic_ativo',
            field=models.BooleanField(default=True, verbose_name='active'),
        ),
        migrations.AddField(
            model_name='produto',
            name='ramo',
            field=models.ForeignKey(default=0, on_delete=checklist.models.Ramo, to='checklist.Ramo'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='projeto',
            name='ic_ativo',
            field=models.BooleanField(default=True, verbose_name='active'),
        ),
        migrations.AlterField(
            model_name='avaliacao',
            name='status',
            field=models.IntegerField(choices=[(0, 'A realizar'), (1, 'Em andamento'), (2, 'Finalizada'), (3, 'Cancelada')], default=0, verbose_name='Situação'),
        ),
        migrations.AlterField(
            model_name='checklist',
            name='requisito',
            field=models.CharField(max_length=200, verbose_name='Informação a constar'),
        ),
        migrations.AlterField(
            model_name='checklist',
            name='requisito_especifico',
            field=models.CharField(max_length=200, verbose_name='Requisito Específico'),
        ),
        migrations.AlterField(
            model_name='itemavaliacao',
            name='nivel_presenca_info',
            field=models.CharField(blank=True, choices=[('presente', 'Presente'), ('ausente', 'Ausente'), ('parcial', 'Parcial'), ('na', 'N/A')], default='na', max_length=15, verbose_name='Nível de presença da informação'),
        ),
        migrations.AlterField(
            model_name='itemavaliacao',
            name='recomendacao',
            field=models.TextField(blank=True, max_length=2000, verbose_name='Recomendação'),
        ),
        migrations.AlterField(
            model_name='itemchecklist',
            name='nivel_presenca_info',
            field=models.CharField(choices=[('presente', 'Presente'), ('ausente', 'Ausente'), ('parcial', 'Parcial'), ('na', 'N/A')], max_length=15, verbose_name='Nível de presença da informação'),
        ),
        migrations.AlterField(
            model_name='itemchecklist',
            name='recomendacao',
            field=models.TextField(max_length=2000, verbose_name='Recomendação'),
        ),
        migrations.AlterField(
            model_name='projeto',
            name='descricao',
            field=models.TextField(blank=True, default='', max_length=200, verbose_name='Descrição'),
        ),
    ]
