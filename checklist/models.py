#-*- coding: utf-8 -*-
from django.db import models
from django.utils.timezone import now as timezone_now
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse, NoReverseMatch
from django.utils.encoding import python_2_unicode_compatible
from utils.models import CreationModificationDateMixin
from django.contrib.auth.models import User
from accounts.models import Profile
from django.db.models import Q
from utils.validators import validate_CNPJ, validate_CPF
import ast
from  municipios.fields import MunicipioForeignKey
from municipios.models import Municipio
from auditlog.registry import auditlog
from django.utils import formats
from portal import settings

#################################################################################3
# Gerenciadores SQL
##################################################################################
class AssinanteQuerySet(models.QuerySet):
    def assinante(self, user): 		
        if hasattr(user, "profile"):
        	if user.profile.user_type == Profile.USER_TYPE_ASSINANTE:
        		#return self.filter(Q(assinante = user.id) | Q(avaliador = user))
        		return self.filter(assinante = user.id)
        	else:
        		return self.filter(assinante = user.profile.assinante)					
        else:
        	return self.filter(assinante = user.id)

    def status(self, status):
        return self.filter(status__in = status)

class AvaliacaoManager(models.Manager):
    def get_queryset(self):
        return AssinanteQuerySet(self.model, using=self._db)  # Important!

    def assinante(self, user):
        return self.get_queryset().assinante(user)

    def status(self, size):
        return self.get_queryset().status(size)

class EmpreendimentoManager(models.Manager):
    def get_queryset(self):
        return AssinanteQuerySet(self.model, using=self._db)  # Important!

    def assinante(self, user):
        return self.get_queryset().assinante(user)

    def status(self, size):
        return self.get_queryset().status(size)

class AssinaturaManager(models.Manager):
    def get_queryset(self):
        return AssinanteQuerySet(self.model, using=self._db)  # Important!

    def assinante(self, user):
        return self.get_queryset().assinante(user)

    def status(self, size):
        return self.get_queryset().status(size)



#################################################################################3
# Classes 
##################################################################################
@python_2_unicode_compatible
class Projetista(CreationModificationDateMixin):
	"""
	"""
	cpf = models.CharField("CPF", max_length=11, validators=[validate_CPF], null=True, unique=True) 
	nome = models.CharField("Nome", max_length=200)
	crea_cau = models.CharField("CREA/CAU", max_length=200, null=True)
    #identidade = models.CharField("Identidade Funcional", max_length=100 , null=True)
	email = models.EmailField("E-mail", null=True)

	ic_ativo = models.BooleanField("Ativo", default=True)

	def __str__(self):  
    		return '%s (%s)' % (self.nome, self.cpf)

	def get_absolute_url(self):
    		return reverse('Checklists:projetista_update', kwargs={'pk': self.pk})

@python_2_unicode_compatible
class Ramo(models.Model):
	"""
		Segmentos
	Ex.: Construção civil
	"""
	nome = models.CharField(_("title"), max_length=200)
	#slug = models.SlugField(_("slug"), max_length=40)
	descircao = models.TextField(_("descrition"), max_length=200 , blank=True, default="")
	ic_ativo = models.BooleanField(_("Ativo"), default=True)
	
	class Meta:
	   verbose_name = _("Ramo")
	   verbose_name_plural = _("Ramos")

	def __unicode__(self):  # Python 3: def __str__(self):
		return self.nome

	def __str__(self):  
    		return self.nome

@python_2_unicode_compatible
class Plano(models.Model):
	"""
		Planos
	"""
	nome = models.CharField("Nome", max_length=200)
	#slug = models.SlugField(_("slug"), max_length=40)
	descricao = models.TextField(u"Descrição", max_length=1000 , blank=True,  default="")
	valor = models.DecimalField("Valor (R$)", default=0.0, max_digits=6, decimal_places=2)
	dt_inicio_publicacao = models.DateField("Data de Publicação",  default=timezone_now)
	dt_fim_publicacao = models.DateField("Data Expiração", blank=True,  default=timezone_now)
	ic_ativo = models.BooleanField(_("Ativo"), default=True)

	prazo = models.IntegerField(u"Fidelização", help_text=u"Quantidade de dias da vigência da assinatura", default=365)
	qtd_usuario = models.IntegerField(u"Quantidade de usuários", help_text=u"Quantidade de usuários que assinante poderá cadastrar", default=0)

	def __unicode__(self):  # Python 3: def __str__(self):
		return self.nome

	def __str__(self):  
    		return self.nome


@python_2_unicode_compatible
class Produto(CreationModificationDateMixin):
	"""
	Classe abstrata
	"""
	class Meta:
		abstract = True

	titulo = models.CharField(u'Título', max_length=100)
	descricao = models.TextField(u"Descrição", max_length=200 , blank=True,  default="")
	planos = models.ManyToManyField(Plano)
	ramo = models.ForeignKey(Ramo, blank=True, null=True)
	folder = models.ImageField( upload_to='produtos', blank=True, verbose_name="Folder")
	dt_inicio_publicacao = models.DateField(u"Data de Publicação",  default=timezone_now)
	dt_fim_publicacao = models.DateField(u"Data Expiração", blank=True,  default=timezone_now)
	ic_ativo = models.BooleanField(_("Ativo"), default=True)

	def __str__(self):  
    		return self.titulo

@python_2_unicode_compatible
class AtividadeTecnica(CreationModificationDateMixin):
	"""
	"""
	titulo = models.CharField(_("title"), max_length=100)
	descricao = models.TextField(_("description"), max_length=200 , blank=True,  default="")
	ramo = models.ForeignKey("Ramo", Ramo)
	ic_ativo = models.BooleanField(_("Ativo"), default=True)

	class Meta:
		verbose_name = _("Atividade Técnica")
		verbose_name_plural = _("Atividades Técnica")

	def __str__(self):  
    		return self.titulo

@python_2_unicode_compatible
class Empreendimento(models.Model):
	"""
		Empreendimento
	"""
	objects = EmpreendimentoManager()

	nome = models.CharField("Empreendimento", max_length=200)
	#slug = models.SlugField(_("slug"), max_length=40)
	descricao = models.TextField(u"Descrição", max_length=2000 , blank=True,  default="")
	endereco = models.TextField(u"Endereço", max_length=2000 ,  blank=True, null=True,  default="")
	municipio = MunicipioForeignKey(Municipio,  verbose_name=u'Município', help_text="Primeiramente selecione a UF e depois informe o município de localização do empreendimento.") # models.IntegerField("Município")
	assinante = models.ForeignKey(User, on_delete=models.CASCADE)


	ic_ativo = models.BooleanField(_("Ativo"), default=True)

	def __unicode__(self):  # Python 3: def __str__(self):
		return self.nome

	def __str__(self):  
    		return self.nome

	def get_absolute_url(self):
    		return reverse('Checklists:empreendimento_update', kwargs={'pk': self.pk})

	def projetos(self):
		"""
		"""
		projetos = Projeto.objects.filter(empreendimento=self)
		return projetos

	def quantidade_projetos(self):
		"""
		Retorna a quantidade de projetos do empreendimento
		"""
		return len(self.projetos())

@python_2_unicode_compatible
class EtapaProjeto(models.Model):
	"""
		Etapa do projeto
	"""
	nome = models.CharField(_("name"), max_length=200)
	#slug = models.SlugField(_("slug"), max_length=40)
	descircao = models.TextField(_("description"), max_length=200 , blank=True, default="")
	ic_ativo = models.BooleanField(_("Ativo"), default=True)
	

	def __unicode__(self):  # Python 3: def __str__(self):
		return self.nome

	def __str__(self):  
    		return self.nome
	class Meta:
		verbose_name = _("Etapa do Projeto")
		verbose_name_plural = _("Etapas do Projeto")



@python_2_unicode_compatible
class Projeto(CreationModificationDateMixin):
	"""
	Atividade Técnica + Etapa do Projeto
	"""
	#slug = models.SlugField(_("slug"), max_length=40)
	#nome = models.CharField("Nome", max_length=200)
	contratante = models.CharField("Contratante", max_length=200, blank=True, null=True)
	cnpj = models.CharField("CNPJ",max_length=14, blank=True, null=True, validators=[validate_CNPJ]) #,unique=True
	contrato = models.CharField("Contrato", max_length=200, null=True,  blank=True)

	empreendimento = models.ForeignKey(Empreendimento)
	atividade_tecnica = models.ForeignKey(AtividadeTecnica, verbose_name=u'Atividade Técnica', 
			help_text=u'Atividades técnicas executadas no projeto')
	etapa_projeto = models.ForeignKey(EtapaProjeto, verbose_name=u'Etapa')

	descricao = models.TextField("Descrição", max_length=200, blank=True, default="")

	responsavel_analise = models.CharField(u'Responsável pela análise', max_length=200, default="", blank=True, null=True)
	responsavel_validacao = models.CharField(u'Responsável pela validação', blank=True, null=True, max_length=200)

	projetistas = models.ManyToManyField(Projetista, through='ProjetoProjetista')

	ic_ativo = models.BooleanField("Ativo", default=True)

	def __unicode__(self):  # Python 3: def __str__(self):
		return "%s | %s | %s" % (self.empreendimento, self.atividade_tecnica, self.etapa_projeto)

	def __str__(self):  
    		return "%s | %s | %s" % (self.empreendimento, self.atividade_tecnica, self.etapa_projeto)

	def get_absolute_url(self):
		return reverse('Checklists:projeto_update', kwargs={'pk': self.pk})


	def infoUtilizadas(self):
		infos = InformacaoProjeto.objects.filter(projeto = self).order_by('-data_revisao')
		return infos

	def obter_avaliacao(self):
		avaliacoes = Avaliacao.objects.filter(projeto = self)
		avaliacao = None
		if len(avaliacoes):
			avaliacao = avaliacoes[0]
		return avaliacao

	def checklists_disponiveis(self):
		#import pdb; pdb.set_trace()
		assinaturas = Assinatura.objects.assinante(self.empreendimento.assinante)
		if len(assinaturas):
    			assinatura = assinaturas[0]
		checklists = assinatura.plano.checklist_set.filter(atividade_tecnica=self.atividade_tecnica,
										etapa_projeto=self.etapa_projeto,
										ic_ativo=True).order_by('titulo')
		return checklists


class ProjetoProjetista(CreationModificationDateMixin):
	"""
	"""
	projeto = models.ForeignKey(Projeto)
	projetista = models.ForeignKey(Projetista, null=True)
	art_rrt = models.CharField(u"ART/RRT", max_length=100, null=True)




@python_2_unicode_compatible
class InformacaoProjeto(models.Model):
	"""
	INFORMAÇÕES UTILIZADAS PARA ANÁLISE
	"""

	class Meta:
		verbose_name = 'Informação utilizada no projeto'
		verbose_name_plural = 'Informações utilizadas no projeto'
		ordering = ['-data_revisao']

	#slug = models.SlugField(_("slug"), max_length=40)
	projeto = models.ForeignKey(Projeto)
	nome = models.CharField("Nome do arquivo/Projeto", max_length=200)
	prancha = models.CharField("Prancha", max_length=25, blank=True)	
	conteudo = models.TextField("Conteúdo", max_length=2000, blank=True, default="")
	revisao = models.CharField("Revisão", max_length=25, blank=True)
	data_revisao = models.DateField("Data" ,  default=timezone_now)
	observacao = models.TextField("Observação", max_length=2000, blank=True, default="")

	def __unicode__(self):  # Python 3: def __str__(self):
		return self.nome

	def __str__(self):  
    		return self.nome

	def get_absolute_url(self):
		return reverse('Checklists:info_projeto_update', kwargs={'pk': self.pk})

class Checklist(Produto):
	"""Lista de verificação de requisitos técnicos """
	class Meta:
		verbose_name = _(u"Requisitos analisados")
		verbose_name_plural = _(u"Listas de Requisitos analisados")

	atividade_tecnica = models.ManyToManyField(AtividadeTecnica)
	etapa_projeto = models.ManyToManyField(EtapaProjeto)

	def itens(self, somente_ativos=True):
		if somente_ativos:
			itens = ChecklistItem.objects.filter(checklist=self, ic_ativo=True).order_by('ordem')
		else:
			itens = ChecklistItem.objects.filter(checklist=self).order_by('ordem') 			
		return itens



class ChecklistItem(CreationModificationDateMixin):
	""" Requisito - item que compoe a lista de verificações """
	class Meta:
		verbose_name = _("Requisito")
		verbose_name_plural = _("Requisitos")

	checklist = models.ForeignKey(Checklist)
	requisito = models.CharField("Informação a constar", max_length=2000)
	#slug = models.SlugField(_("slug"), max_length=40)
	requisito_geral = models.CharField("Requisito Geral", max_length=2000)
	requisito_especifico = models.CharField("Requisito Específico", max_length=2000)
	normativo = models.CharField(u"Referência", max_length=50, blank=True , default="NBR 15575-1", null=True)
	norma = models.TextField("Item", max_length=10000, blank=True, default="", null=True)
	referenciais = models.CharField("Referências a outros requisitos", max_length=2000, blank=True, null=True)
	ordem = models.IntegerField("Ordem", default=0, null=True, blank=False)
	ic_ativo = models.BooleanField(_("Ativo"), default=True)

	def __str__(self):  # Python 3: def __str__(self):
    		return self.requisito

	def __unicode__(self):  # Python 3: def __str__(self):
		return self.requisito

	def obterRecomendacao(self, info):
		recomendacoes = Recomendacao.objects.filter(checklistItem = self, nivel_presenca_info=info)
		if recomendacoes:
    			return recomendacoes[0]
		return None

	def recomendacoes(self):
		return Recomendacao.objects.get(checklistItem = self)	

class Recomendacao(models.Model):
	PRESENTE = 'presente'
	AUSENTE = 'ausente'
	PARCIAL = 'parcial'
	ND = 'nd'
	NA = 'na'
	NIVEL_INFO_CHOICES = (
		(PRESENTE, 'Presente'),
		(AUSENTE, 'Ausente'),
		(PARCIAL, 'Parcial'),
		(NA, 'Não se aplica'),
		(ND, 'Não analisado'),
	)

	checklistItem = models.ForeignKey(ChecklistItem)

	nivel_presenca_info = models.CharField("Nível de presença da informação", choices = NIVEL_INFO_CHOICES, max_length=15)
	recomendacao = models.TextField("Recomendação",max_length=2000)

	def __unicode__(self):  # Python 3: def __str__(self):
		return self.nivel_presenca_info

	def __str__(self):  # Python 3: def __str__(self):
		return self.nivel_presenca_info


class Avaliacao(CreationModificationDateMixin):
	"""
	Análises de Projetos -  Lista de verificação do Projeto
	"""

	objects = AvaliacaoManager()

	A_REALIZAR = 0
	EM_ANDAMENTO = 1
	FINALIZADA = 2
	CANCELADA = 3
	STATUS_CHOICES = (
		(A_REALIZAR, 'A realizar'),
		(EM_ANDAMENTO, 'Em andamento'),
		(FINALIZADA, 'Finalizada'),
		(CANCELADA, 'Cancelada'),
	)
	titulo = models.CharField(u"Título", max_length=100, null=True, default="")
	assinante = models.IntegerField(null=True)
	checklist = models.ForeignKey(Checklist, null=True, verbose_name="Requisitos analisados")
	avaliador = models.ForeignKey(User, on_delete=models.CASCADE, related_name="avaliador", verbose_name="Avaliador")
	projeto = models.ForeignKey(Projeto)
	descricao = models.TextField(u"Descrição", max_length=2000 , blank=True,  default="")
	#ic_ativo = models.BooleanField(_("active"), default=True)
	status = models.IntegerField(u"Situação", choices = STATUS_CHOICES,  default=A_REALIZAR)

	#def __unicode__(self):  # Python 3: def __str__(self):
    #		return self.titulo

	#def __str__(self):  # Python 3: def __str__(self):
	#	return self.titulo

	class Meta:
		verbose_name = 'Análise'
		verbose_name_plural = 'Análises'
		ordering = ['-id']

	def atividade_tecnica(self):
    		return self.projeto.atividade_tecnica

	def etapa_projeto(self):
    		return self.projeto.etapa_projeto

	def empreendimento(self):
    		return self.projeto.empreendimento

	def finalizar(self, auto_save=True):
		"""Finalizar análise """
		self.status = Avaliacao.FINALIZADA
		if auto_save:
		    self.save()

	def cancelar(self, auto_save=True):
		"""Cancelar análise """
		self.status = Avaliacao.CANCELADA
		if auto_save:
			self.save()

	def evolucoes(self, auto_save=True):
		"""histórico de evolucao """
		return  AvaliacaoEvolucao.objects.filter(avaliacao = self)

	def salvar_evolucao(self, criador, comentario=None, auto_save=True):
		"""Salvar situacao atual """
		evolucao = AvaliacaoEvolucao()
		evolucao.avaliacao = self
		evolucao.criador = criador
		evolucao.comentario = comentario
		evolucao.situacao = str(self.estatisticas())
		evolucao.save()

	def finalizada(self):
		return self.status == Avaliacao.FINALIZADA

	def cancelada(self):
    		return self.status == Avaliacao.CANCELADA

	def itens(self, somente_recomendacoes=False):
		itens = AvaliacaoItem.objects.filter(avaliacao = self).order_by("pk")
		if somente_recomendacoes:
			itens = itens.exclude(recomendacao_avaliador__isnull=True).exclude(nivel_presenca_info = Recomendacao.NA).exclude(recomendacao_avaliador = "")

			
		return itens

	def get_absolute_url(self):
		return reverse('Checklists:editar_avaliacao', kwargs={'pk': self.pk})

	def getCountItens(self):
		itens = self.itens()
		return len(itens)

	def pontuacao(self):
		"""Pontuação"""
		itens = AvaliacaoItem.objects.filter(avaliacao = self).exclude(nivel_presenca_info=Recomendacao.NA)
		pontos = 0
		for i in itens:
			pontos += i.getPoint()
		return pontos

	def conformidade(self):
		"""Percentual de conclusão/resultado/presença de informação - Nível de Informação"""
		itens = AvaliacaoItem.objects.filter(avaliacao = self).exclude(nivel_presenca_info=Recomendacao.NA)
		total = len(itens) * AvaliacaoItem.MAX_PESO
		perc = 0
		if total > 0:
			perc = (self.pontuacao()  / total) * 100
		return int(perc)

	def progresso(self):
		"""Percentual de analisado"""
		itens = AvaliacaoItem.objects.filter(avaliacao = self)
		count = 0
		perc = 0
		for i in itens:
			if i.nivel_presenca_info != AvaliacaoItem.ND:
				count += 1
		if len(itens) > 0:
			perc = (count  / len(itens)) * 100
		return int(perc)


	def estatisticas(self):
		"""Estatístics"""
		modified = None
		if self.modified:
			modified = formats.date_format(self.modified, settings.DATETIME_FORMAT)

		dados = {
			'modified' :  modified,
			'progresso' : self.progresso(),
			'conformidade' : self.conformidade(),
			'pontuacao' : self.pontuacao(),
			'total_itens' : self.getCountItens(),
			'max_peso': AvaliacaoItem.MAX_PESO,
			AvaliacaoItem.PRESENTE : 0,
			AvaliacaoItem.PARCIAL : 0,
			AvaliacaoItem.AUSENTE : 0,
			AvaliacaoItem.ND : 0,
			AvaliacaoItem.NA : 0
		}


		itens = AvaliacaoItem.objects.filter(avaliacao = self)
		for i in itens:
			dados[i.nivel_presenca_info.lower()]  += 1

		#recuperar pesos
		for info, peso in AvaliacaoItem.NIVEL_INFO_PESOS:
			dados[info + '_peso'] = 0
			if info in dados.keys():
				dados[info + '_peso'] = peso * dados[info]

		return dados

	#custom save
	def save(self, *args, **kwargs):

		if hasattr(self.avaliador, "profile"):
			if self.avaliador.profile.user_type == Profile.USER_TYPE_ASSINANTE:
				self.assinante = self.avaliador.id
			else:
				self.assinante = self.avaliador.profile.assinante	
		else:
			self.assinante = self.avaliador.id						
    			
		super( Avaliacao, self ).save(*args, **kwargs)




class AvaliacaoItem(CreationModificationDateMixin):
	"""
		Requisitos analisas
	"""
	PRESENTE = 'presente'
	AUSENTE = 'ausente'
	PARCIAL = 'parcial'
	ND = 'nd'
	NA = 'na'
	NIVEL_INFO_CHOICES = (
		(PRESENTE, 'Presente'),
		(AUSENTE, 'Ausente'),
		(PARCIAL, 'Parcial'),
		(ND, 'Não analisado'),
		(NA, u'Não se aplica'),
	)
	NIVEL_INFO_PESOS =  (
		(PRESENTE, 2),
		(AUSENTE, 0),
		(PARCIAL, 1),
		(ND, 0),
		(NA, 2),
	)

	MAX_PESO = 2


	avaliacao = models.ForeignKey(Avaliacao)
	checklistItem = models.ForeignKey(ChecklistItem)
	nivel_presenca_info = models.CharField("Nível de presença da informação", choices = NIVEL_INFO_CHOICES, max_length=15, blank=True, default=ND)
	observacao = models.TextField("Observação/Evidências",max_length=2000, blank=True)
	recomendacao = models.TextField("Recomendação",max_length=2000, blank=True)
	recomendacao_avaliador = models.TextField("Recomendação",max_length=2000, blank=True)
	info_analisada = models.TextField(u"Informação analisada",max_length=2000, blank=True)

	def requisito(self):
    		return self.checklistItem.requisito

	def requisito_geral(self):
    		return self.checklistItem.requisito_geral

	def requisito_especifico(self):
    		return self.checklistItem.requisito_especifico

	def norma(self):
    		return self.checklistItem.norma

	def normativo(self):
    		return self.checklistItem.normativo

	def ordem(self):
    		return self.checklistItem.ordem

	def recomendacoes(self):
		recomendacoes = self.recomendacao
		if recomendacoes is not None:
    			recomendacoes = recomendacoes + " \n \n  " + self.recomendacao_avaliador	
		else:
    			recomendacoes = self.recomendacao_avaliador		
		return recomendacoes

	def __unicode__(self):  # Python 3: def __str__(self):
		return self.nivel_presenca_info

	def __str__(self):  # Python 3: def __str__(self):
		return self.nivel_presenca_info

	def getPoint(self):
		pesos = dict(AvaliacaoItem.NIVEL_INFO_PESOS)
		return pesos[self.nivel_presenca_info.lower()]


class AvaliacaoEvolucao(CreationModificationDateMixin):
	"""
		Eu, analista, quero poder salvar a situação atual da avaliação para acompanhamento da evolução
	"""
	criador = models.ForeignKey(User, on_delete=models.CASCADE, related_name="criador", verbose_name="Criado Por")
	avaliacao = models.ForeignKey(Avaliacao)
	situacao = models.TextField()
	comentario = models.TextField(u"Comentário", null=True, default="")

	def criado_por(self):
		return self.criador.first_name or self.criador 

	def progresso(self):
		if self.situacao:
			d = ast.literal_eval(self.situacao)
			return d.get("progresso")
		return 0

	def conformidade(self):
		if self.situacao:
			d = ast.literal_eval(self.situacao)
			return d.get("conformidade")
		return 0


@python_2_unicode_compatible
class Assinatura(CreationModificationDateMixin):
	"""
	"""
	objects = AssinaturaManager()

	MASTER = 'master'
	VISA = 'visa'
	ELO = 'elo'

	BANDEIRAS_CHOICES = (
		(MASTER, 'Master'),
		(VISA, 'Visa'),
		(ELO, 'Elo'),
	)
	

	assinante = models.ForeignKey(User)
	plano = models.ForeignKey(Plano)
	dt_inicio_vigencia = models.DateField(u"Data de Início",  default=timezone_now)
	dt_fim_vigencia = models.DateField(u"Data Fim", blank=True,  default=timezone_now)
	ic_ativo = models.BooleanField("Ativo", default=True)

	bandeira_cartao = models.CharField(u"Bandeira", max_length=20, null=True, choices = BANDEIRAS_CHOICES,)
	num_cartao = models.CharField(u"Cartão", max_length=100, null=True)


	def checklistsDisponiveis(self):
		checklists = self.plano.checklist_set.all()
		return checklists

	def produtosDisponiveis(self):
		tipos = []
		checklists = self.plano.checklist_set.all()
		if len(checklists):
				tipos.append("Análises")
					
		return tipos




"""
Auditoria - log
"""

auditlog.register(Avaliacao, exclude_fields=['created', 'modified'])
auditlog.register(AvaliacaoItem, exclude_fields=['created', 'modified'])
auditlog.register(Empreendimento, exclude_fields=['created', 'modified'])
auditlog.register(Projeto, exclude_fields=['created', 'modified'])
auditlog.register(InformacaoProjeto, exclude_fields=['created', 'modified'])