#-*- coding: utf-8 -*-
from django.contrib import admin

from .models import ChecklistItem, Checklist, Recomendacao, Ramo, Projeto, AtividadeTecnica, EtapaProjeto, Plano, Projetista


class RecomendacaoInline(admin.TabularInline):
    model = Recomendacao
    #readonly_fields = ('votes',)
    #extra = 3

class ChecklistItemAdmin(admin.ModelAdmin):
    #fields = ('checklist', 'requisito', 'requisito_geral','requisito_especifico' , 'normativo', 'norma', 'referenciais', 'ordem', 'ic_ativo' )
    """
    fieldsets = [
        (None, {'fields': ('descricao', 'produto'),}),
    ]
    """
    inlines = [RecomendacaoInline]
    list_display = [ 'checklist', 'requisito', 'requisito_geral','requisito_especifico']
    search_fields = [ 'checklist', 'requisito', 'requisito_geral','requisito_especifico']


class ChecklistAdmin(admin.ModelAdmin):
    fields = ('titulo',  'atividade_tecnica', 'etapa_projeto', 'descricao', 'planos', 'folder',  'dt_inicio_publicacao', 'dt_fim_publicacao', 'ic_ativo' )
    list_display = [ 'titulo',   'dt_inicio_publicacao', 'dt_fim_publicacao', 'ic_ativo']
    search_fields = ['titulo']



class RamoAdmin(admin.ModelAdmin):
    pass

class ProjetoAdmin(admin.ModelAdmin):
    ordering     = ('nome',)
    list_display = ('nome', 'descricao', 'ic_ativo')

class AtividadeTecnicaAdmin(admin.ModelAdmin):
    ordering     = ('titulo',)
    list_display = ('titulo', 'descricao',  'ic_ativo')

class PlanoAdmin(admin.ModelAdmin):
    pass

class ProjetistaAdmin(admin.ModelAdmin):
    ordering     = ('nome',)
    list_display = ('cpf', 'nome', 'crea_cau','ic_ativo')

admin.site.register(Checklist, ChecklistAdmin)
admin.site.register(ChecklistItem, ChecklistItemAdmin)
admin.site.register(Ramo, RamoAdmin)
admin.site.register(AtividadeTecnica, AtividadeTecnicaAdmin)
admin.site.register(Plano, PlanoAdmin)
admin.site.register(EtapaProjeto)

admin.site.register(Projetista, ProjetistaAdmin)