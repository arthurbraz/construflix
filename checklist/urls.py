#-*- coding: utf-8 -*-
from django.conf.urls import  url

from . import views

urlpatterns = [
    url(r'^$', views.ChecklistIndexView.as_view(), name='index'),
     url(r'^(?P<pk>\d+)/$', views.ChecklistDetailView.as_view(), name='detail'),
    url(r'^(?P<pk>\d+)/results/$', views.ChecklistResultsView.as_view(), name='results'),
    url(r'^checklist_new$', views.ChecklistEdit.as_view(), name='checklist_new'),

    url(r'^minha_pagina$', views.MinhaPaginaView.as_view(), name='minha_pagina'),
    url(r'^minhas_aplicacoes$', views.MinhasAplicacoesView.as_view(), name='minhas_aplicacoes'),

    url(r'^avaliacao$', views.MinhasAvaliacoesView.as_view(), name='avaliacao_index'),
    url(r'^avaliacao/lista$', views.AvaliacaoListView.as_view(), name='avaliacao_list'),

    url(r'^avaliacao/criar/$', views.AvaliacaoCreateView.as_view(), name='avaliacao_add'),
    #url(r'^avaliacao/(?P<pk>\d+)/$', views.AvaliacaoItemAvaliacaoUpdateView.as_view(), name='editar_avalicao'),
    url(r'^avaliacao/(?P<pk>\d+)/$', views.editar_avaliacao, name='editar_avaliacao'),    
     url(r'^avaliacao/(?P<pk>\d+)/resultado$', views.AvaliacaoResultsView.as_view(), name='results_avaliacao'),    
     url(r'^avaliacao/(?P<pk>\d+)/pendencias$', views.AvaliacaoPendenciasView.as_view(), name='avaliacao_pendencias'), 
    url(r'^avaliacao/itens_formset$', views.obter_itens_avaliacao_formset, name='itens_avaliacao_formset'),
    url(r'^avaliacao/(?P<pk>\d+)/evolucao$', views.AvaliacaoEvolucaolView.as_view(), name='avaliacao_evolucao'),

    url(r'^avaliacao/(?P<pk>\d+)/pendencias/print$', views.RelatorioSimplificaoPrintView.as_view(), name='avaliacao_pendencias_print'), 


    url(r'empreendimento/$', views.MeusEmpreendimentosView.as_view(), name='empreendimento_index'),
    url(r'empreendimento/criar/$', views.EmpreendimentoCreate.as_view(), name='empreendimento_add'),
    url(r'empreendimento/lista/$', views.EmpreendimentoListView.as_view(), name='empreendimento_list'),
    url(r'empreendimento/(?P<pk>[0-9]+)/$',  views.EmpreendimentoUpdate.as_view(), name='empreendimento_update'),
    url(r'empreendimento/(?P<pk>[0-9]+)/delete/$',  views.EmpreendimentoDelete.as_view(), name='empreendimento_delete'),


    url(r'equipe/$', views.MinhaEquipeView.as_view(), name='minha_equipe'),

    url(r'projeto/$', views.MeusProjetosView.as_view(), name='projeto_index'),
    url(r'projeto/criar/$', views.ProjetoCreateView.as_view(), name='projeto_add'),
    url(r'projeto/criar/popup$', views.ProjetoCreatePopup.as_view(), name='projeto_add_popup'),
    url(r'projeto/lista/$', views.ProjetoListView.as_view(), name='projeto_list'),
    url(r'projeto/(?P<pk>[0-9]+)/$',  views.ProjetoUpdate.as_view(), name='projeto_update'),
    url(r'projeto/(?P<pk>[0-9]+)/delete/$',  views.ProjetoDelete.as_view(), name='projeto_delete'),


    #url(r'projeto_info/$', views.MeusProjetosView.as_view(), name='projeto_index'),
    url(r'projeto_info/criar/$', views.InformacaoProjetoCreate.as_view(), name='info_projeto_add'),
    url(r'projeto_info/lista/$', views.InformacaoProjetoListView.as_view(), name='info_projeto_list'),
    url(r'projeto_info/(?P<pk>[0-9]+)/$',  views.InformacaoProjetoUpdate.as_view(), name='info_projeto_update'),
    url(r'projeto_info/(?P<pk>[0-9]+)/delete/$',  views.InformacaoProjetoDelete.as_view(), name='info_projeto_delete'),

    url(r'projetista/criar/$', views.ProjetistaCreate.as_view(), name='projetista_add'),
    url(r'projetista/(?P<pk>[0-9]+)/$',  views.ProjetistaUpdate.as_view(), name='projetista_update'),

#chamadas ajax
    url(r'ajax/obter_checklists/$', views.obter_checklists, name='obter_checklists'),
    url(r'ajax/obter_recomendacao/$', views.obter_recomendacao, name='obter_recomendacao'),
    url(r'ajax/editar_item_avaliacao/$', views.editar_item_avaliacao, name='editar_item_avaliacao'),
    url(r'ajax/obter_estatisticas_avaliacao/$', views.obter_estatisticas_avaliacao, name='obter_estatisticas_avaliacao'),
    url(r'ajax/avaliacao_finalizar/$', views.avaliacao_finalizar, name='avaliacao_finalizar'),
    url(r'ajax/avaliacao_cancelar/$', views.avaliacao_cancelar, name='avaliacao_cancelar'),
    url(r'ajax/avaliacao_salvar_evolucao/$', views.avaliacao_salvar_evolucao, name='avaliacao_salvar_evolucao'),   


 

]


