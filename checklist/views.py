#-*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
from django.forms.models import model_to_dict
from django.http import JsonResponse
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.views import generic
#from django.views.generic import FormView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views import View
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from django.core.files.storage import default_storage

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models.fields.files import FieldFile

from django.views.generic.base import TemplateView
from django.contrib import messages
from django.urls  import reverse_lazy

from django.forms import formset_factory
from django.forms.models import modelformset_factory
from django.forms import inlineformset_factory, formset_factory

from .models import *
from .forms import *
from .shortcuts import get_avaliacao_or_404
from utils.forms import PassRequestToFormViewMixin, AjaxableResponseMixin


from django_tables2 import MultiTableMixin, RequestConfig, SingleTableMixin, SingleTableView
from .tables import *
from django.utils import timezone
from django.core.files.storage import FileSystemStorage

from django.conf import settings



class ChecklistIndexView(generic.ListView):
    template_name = 'checklist/index.html'
    context_object_name = 'latest_checklist_list'

    def get_queryset(self):
        return Checklist.objects.all()[:5]


class ChecklistListView(generic.ListView):
    template_name = 'checklist/index.html'
    context_object_name = 'latest_checklist_list'

    def get_queryset(self):
        return Checklist.objects.all()

class ChecklistDetailView(generic.DetailView):
    model = Checklist
    template_name = 'checklist/detail.html'


class ChecklistResultsView(generic.DetailView):
    model = Checklist
    template_name = 'checklist/results.html'


class ChecklistEdit(generic.FormView):
    template_name = 'checklist/checklist_edit.html'
    form_class = ChecklistForm


class MinhasAvaliacoesView(generic.TemplateView):
    template_name = 'checklist/minhas_avaliacoes.html'


class AvaliacaoListView(generic.ListView):
    model = Avaliacao
    template_name = 'checklist/avaliacao_list.html'
    context_object_name = 'avaliacao_list'


    def get_context_data(self, **kwargs):
        context = super(AvaliacaoListView, self).get_context_data(**kwargs)
        table = AvaliacaoTable(self.get_queryset())
        RequestConfig(self.request, paginate={'per_page': 300}).configure(table)
        context['table'] = table
        return context

    def get_queryset(self):
        status = self.request.GET.getlist("status")
        results = Avaliacao.objects.assinante(self.request.user).status(status)
        return results.order_by('-modified')

class AvaliacaoResultsView(generic.DetailView):
    """
    Relatório completo - ficha da análise
    """
    model = Avaliacao
    template_name = 'checklist/avaliacao_results.html'

    def get_context_data(self, **kwargs):

        context = super(AvaliacaoResultsView, self).get_context_data(**kwargs)

        analise = self.get_object()
        table = AvaliacaoItemTable(analise.itens())
        RequestConfig(self.request, paginate={'per_page': 300}).configure(table)
        context['table_itens'] = table

        table2 = InformacaoProjetoTable(analise.projeto.infoUtilizadas())
        RequestConfig(self.request, paginate=False, ).configure(table2)
        context['table_info_utilizada'] = table2

        context['projetistas'] = analise.projeto.projetistas.filter()

        context['timezone_now'] = timezone_now()

        return context

class AvaliacaoPendenciasView(generic.DetailView):
    """
    Relatório simplificado 
    """
    model = Avaliacao
    template_name = 'checklist/avaliacao_pendencias.html'

    def get_context_data(self, **kwargs):
        context = super(AvaliacaoPendenciasView, self).get_context_data(**kwargs)

        analise = self.get_object()
        table = AvaliacaoItemSimpleTable(analise.itens(somente_recomendacoes=True) ,empty_text="Nenhuma recomendação")
        RequestConfig(self.request, paginate={'per_page': 1000}).configure(table)
        context['table'] = table

        context['projetistas'] = analise.projeto.projetistas.filter()

        context['timezone_now'] = timezone_now()

        return context


class AvaliacaoCreateView(AjaxableResponseMixin, PassRequestToFormViewMixin, CreateView):
    model = Avaliacao
    #fields = ['empreendimento', 'projeto', 'etapa_projeto', 'prancha']
    form_class = AvaliacaoForm
    #success_url = reverse_lazy('Checklists:editar_avaliacao' , kwargs={'pk': self.pk})

    def get_success_url(self):
        return reverse('Checklists:editar_avaliacao', kwargs={'pk': self.object.pk})
        

@login_required
def editar_avaliacao(request, pk):

    template_name='checklist/avaliacao_editar_form.html'
    avaliacao = get_avaliacao_or_404(request.user, pk=pk )

    if avaliacao.finalizada():
        return redirect("Checklists:results_avaliacao", pk=pk)

    if request.method == "POST":
        AvaliacaoItemFormset = formset_factory(AvaliacaoItemForm,  can_order=False, can_delete=False)
        form = AvaliacaoForm(instance=avaliacao, request=request, data=request.POST, initial=model_to_dict(avaliacao))
        form.instance = avaliacao

        if form.has_changed() and request.POST["ic_editar_avaliacao"] == "S":
            form.save()
            data = {
                'message': _("Dados do formulário enviados com sucesso."),
                'titulo': avaliacao.titulo ,
                'modified': avaliacao.modified ,
                'conformidade': avaliacao.conformidade(),
                'progresso': avaliacao.progresso() ,
                'itens': []
            }
            return JsonResponse(data)
        else:

            formset = AvaliacaoItemFormset(request.POST, request.FILES)

            ctx =  {"form": form, "formset" : formset, 'avaliacao' : avaliacao} 

            #ToDo: Verificar se teve alteracao - and formset.has_changed()
            
            if  formset.is_valid():
                itens = []
                for f in formset:
                    f.save(avaliacao)
                    itens.append(f.cleaned_data)

                data = {
                    'message': u"Dados do formulário enviados com sucesso." ,
                    'conformidade': avaliacao.conformidade(),
                    'progresso': avaliacao.progresso(),
                    'itens' : itens
                }

                if request.POST["finalizar"] == "1":
                    avaliacao.finalizar()
                    data["analise_finalizada"] = 1
                    data["message"] = "Análise finalizada com sucesso."
                    data["redirect"] = reverse_lazy("Checklists:results_avaliacao", kwargs={"pk":  avaliacao.pk} )
                else:
                    #persistir avaliacao/analise - data de modificacao
                    form.save()     

                data['modified'] =  avaliacao.modified 

                return JsonResponse(data)
            else:
                data = {
                'message': u"Ocorreu um erro ao gravar análise.",
                'errors' : form.errors
                }
                #return render(request, template_name,    ctx      )
                return JsonResponse(data)
    else:
        form = AvaliacaoForm(instance=avaliacao, request=request)
        
        actions = []
        pode_finalizar = request.user.profile and ( request.user.profile.user_type == Profile.USER_TYPE_ASSINANTE or \
                        request.user.profile.pode_finalizar_analise)

        pode_editar = not ( avaliacao.finalizada() or avaliacao.cancelada())


        if request.user.profile.user_type == Profile.USER_TYPE_ASSINANTE:
            #actions.append({"action": "refresh", "title":"Atualizar análise"})
            if pode_editar:
                actions.append({"action": "cancelar", "title":"Cancelar análise" })
            if avaliacao.finalizada():
                actions.append({"action": "reeditar", "title":"Reeditar análise"})

        if pode_editar:
            actions.append({"action": "salvar_progresso", "title":"Salvar evolução" })

        if pode_finalizar:
            actions.append({"action": "finalizar", "title":"Finalizar análise" })
        
        

        ctx =  {"form": form, 'avaliacao' : avaliacao, 'actions': actions, "pode_finalizar" : pode_finalizar, "pode_editar" : pode_editar} 

        ctx['projetistas'] = avaliacao.projeto.projetistas.filter()

        return render(request, template_name,    ctx      )





@login_required
def obter_itens_avaliacao_formset(request):
    #import pdb; pdb.set_trace()
    produto_id = request.GET.get('produto_id', None)
    avaliacao_id = request.GET.get('avaliacao_id', None)
    pode_editar = bool(request.GET.get('pode_editar', "false" ) == "true")


    template_name='checklist/item_avaliacao_form.html'
    AvaliacaoItemFormset = formset_factory(AvaliacaoItemForm,  can_order=False, can_delete=False, extra=0)
    #recuperar lista de requisitos
    if (avaliacao_id is not None):
        itens = AvaliacaoItem.objects.filter(avaliacao_id=avaliacao_id).order_by('pk')
        data = []
        for item in itens:
            item_dict = model_to_dict(item) #.__dict__
            item_dict["checklistitem_id"] = item.checklistItem.pk
            item_dict["requisito"] = item.requisito()
            item_dict["requisito_geral"] = item.requisito_geral()
            item_dict["requisito_especifico"] = item.requisito_especifico()
            item_dict["norma"] = item.norma()
            item_dict["normativo"] = item.normativo()
            data.append(item_dict)
    else:
        
        checklist = Checklist.objects.get(
                        atividade_tecnica=self.instance.projeto.atividade_tecnica,
                        etapa_projeto=self.instance.projeto.etapa_projeto,
                        )
        itens = checklist.itens()
        #itens = Checklist.objects.filter(produto_id=produto_id).order_by('ordem')
        data = itens.values()


    formset = AvaliacaoItemFormset(initial=data)
    ctx = {'formset': formset, "pode_editar" : pode_editar} 
    return render(request, template_name,     ctx )

@method_decorator(login_required, name='dispatch')
class MinhaEquipeView(generic.TemplateView):
    template_name = 'checklist/minha_equipe.html'

@method_decorator(login_required, name='dispatch')
class MeusEmpreendimentosView(generic.TemplateView):
    template_name = 'checklist/meus_empreendimentos.html'

@method_decorator(login_required, name='dispatch')
class EmpreendimentoListView(generic.ListView):
    model = Empreendimento
    template_name = 'checklist/generic_list.html'
    context_object_name = 'empreendimento_list'

    def get_context_data(self, **kwargs):
        context = super(EmpreendimentoListView, self).get_context_data(**kwargs)
        table = EmpreendimentoTable(self.get_queryset())
        RequestConfig(self.request, paginate={'per_page': 300}).configure(table)
        context['table'] = table
        return context

    def get_queryset(self):
        results = Empreendimento.objects.assinante(self.request.user)
        return results.order_by('-pk')

class EmpreendimentoCreate(AjaxableResponseMixin, PassRequestToFormViewMixin, CreateView):
    model = Empreendimento
    #fields = ['nome', 'descricao']
    form_class = EmpreendimentoForm
    #success_url = reverse_lazy("Checklists:empreendimento_update")

    def get_context_data(self, **kwargs):
        context = super(EmpreendimentoCreate, self).get_context_data(**kwargs)
        if self.request.GET.get("_popup",None):
            context['popup'] = True
        return context


class EmpreendimentoUpdate(AjaxableResponseMixin, PassRequestToFormViewMixin, UpdateView):
    model = Empreendimento
    form_class = EmpreendimentoForm

class EmpreendimentoDelete(AjaxableResponseMixin, PassRequestToFormViewMixin, DeleteView):
    model = Empreendimento
    form_class = EmpreendimentoForm

@method_decorator(login_required, name='dispatch')
class MinhasAplicacoesView(generic.TemplateView):
    template_name = 'checklist/minhas_aplicacoes.html'


@method_decorator(login_required, name='dispatch')
class MinhaPaginaView(generic.TemplateView):
    template_name = 'checklist/minha_pagina.html'

    def get_template_names(self):
        if self.request.user.is_superuser:
            template_name = 'checklist/minha_pagina.html'
        elif self.request.user.is_authenticated:
            template_name = 'checklist/minha_pagina.html'
        else:
            template_name = self.template_name
        return [template_name]


    def get(self, request, *args, **kwargs):
        """
        Redirecionar para páginas de planos caso nao tenha assinatura
        """
        try:
            #verificar se existe assinatura de algum plano
            assinatura = Assinatura.objects.assinante(request.user)[0]
        except:
            assinatura = None
            #return redirect("Checklists:planos")
            if not (request.user.has_perm('construflix_administrador') or request.user.is_superuser or request.user.is_staff):
                return redirect ("/planos/?msg=É necessário assinar algum plano")

        context = self.get_context_data(assinatura, **kwargs)
        return self.render_to_response(context)
        #return super(MinhaPaginaView, self).get(request, *args, **kwargs)

    def get_context_data(self, assinatura, **kwargs):
        context = super(MinhaPaginaView, self).get_context_data(**kwargs)
        user = self.request.user
        #lista de abas disponíveis
        abas = []
   
        if assinatura:
            abas.append({"title": "Aplicações"})
            abas.append({"title": "Empreendimentos"})  
            abas.append({"title": "Projetos"})

            #todo: utilizar sistema de permissoes - has_perm
            if hasattr(user, "profile"):
                if user.profile.user_type == Profile.USER_TYPE_ASSINANTE:
                    #abas.append({"title": "Planos"}) 
                    abas.append({"title": "Equipe"}) 
            #if user.is_superuser :
            #    abas.append({"title": "Planos"}) 
            #    abas.append({"title": "Equipe"}) 
            #abas.append({"title": "Conta"})       

            context["abas"] = abas
            context["aba_default"] = "Aplicações" #aba default

        if user.has_perm('construflix_administrador') or user.is_superuser or user.is_staff:
            abas.append("Manutenção de planos")
            abas.append("Manutenção de aplicações")
            abas.append("Relatórios")
            context["abas"] = abas
            context["aba_default"] = "Manutenção de planos" #aba default         

        return context



class MeusProjetosView(generic.TemplateView):
    template_name = 'checklist/meus_projetos.html'



class ProjetoListView(generic.ListView):
    model = Projeto
    template_name = 'checklist/generic_list.html'
    context_object_name = 'projeto_list'


    def get_context_data(self, **kwargs):
        context = super(ProjetoListView, self).get_context_data(**kwargs)
        table = ProjetoTable(self.get_queryset())
        RequestConfig(self.request, paginate={'per_page': 300}).configure(table)
        context['table'] = table
        return context

    def get_queryset(self):
        empreendimentos = Empreendimento.objects.assinante(self.request.user)
        results = Projeto.objects.filter(empreendimento__in=empreendimentos)
        return results.order_by('-pk')


class ProjetoCreatePopup(AjaxableResponseMixin, PassRequestToFormViewMixin, CreateView):
    model = Projeto
    form_class = ProjetoSimpleForm
    template_name_suffix = "_form_popup"


class ProjetoCreateView(AjaxableResponseMixin, PassRequestToFormViewMixin, CreateView):
    model = Projeto
    form_class = ProjetoForm

    def get(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        formset = ProjetoProjetistaFormset()
        return self.render_to_response(self.get_context_data(form=form, formset = formset  ))

    def post(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        formset = ProjetoProjetistaFormset(self.request.POST)
        if (form.is_valid() and formset .is_valid() and
            formset.is_valid()):
            return self.form_valid(form, formset  )
        else:
            return self.form_invalid(form, formset  )

    def form_valid(self, form, formset):
        self.object = form.save()

        formset.instance = self.object
        formset.save()

        #for form in formset.forms:
        #    form.instance.projeto = self.object
        #    form.save()

        data = {
                'message': u"Dados do formulário enviados com sucesso.",
                 'url' : self.get_success_url(),
                 'id' : self.object.pk
            }
        return JsonResponse(data)
        
    def form_invalid(self, form, formset):
        response = super().form_invalid(form)
        if self.request.is_ajax():
            errors = form.errors
            for error in formset.errors:
                errors.update(error)
            for error in formset.non_form_errors():
                errors.update({'__all__' : error})

            return JsonResponse(errors, status=400)
        else:
            return response

class ProjetoUpdate(AjaxableResponseMixin, PassRequestToFormViewMixin, UpdateView):
    model = Projeto
    form_class = ProjetoForm
    #template_name = 'projeto_form.html'

    def get_context_data(self, **kwargs):
        kwargs.update({'update':True})
        return kwargs

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        table = InformacaoProjetoTable(self.object.infoUtilizadas())
        RequestConfig(self.request, paginate={'per_page': 300}).configure(table)

        # Render formset
        initial_data = self.object.projetistas.filter().values()
        extra = 1
        if len(initial_data) > 0:
            extra = 0
        

        ProjetoProjetistaFormset = inlineformset_factory(Projeto, ProjetoProjetista,    form=ProjetoProjetistaForm,  
                    formset=BaseProjetoProjetistaFormSet, extra=extra)#, fields=('projetista', 'art_rrt'))

        formset = ProjetoProjetistaFormset(instance=self.object, initial=initial_data)
        return self.render_to_response(self.get_context_data(form=form,formset=formset, table=table, projeto=self.object))



    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        formset = ProjetoProjetistaFormset(self.request.POST, instance=self.object)#, instance=self.object)

        if (form.is_valid() and formset.is_valid()):
            return self.form_valid(form, formset)
        else:
            return self.form_invalid(form, formset)

    def form_valid(self, form, formset):
        #import pdb; pdb.set_trace()
        self.object = form.save()
        formset.instance = self.object
        formset.save()

        #for form in formset.forms:
        #    form.instance.projeto = self.object
        #    form.save()

        data = {
                'message': u"Dados do formulário enviados com sucesso.",
                 'url' : self.get_success_url(),
                 'id' : self.object.pk
            }
        return JsonResponse(data)



        
    def form_invalid(self, form, formset):
        response = super(ProjetoUpdate, self).form_invalid(form)
        if self.request.is_ajax():
            errors = form.errors
            for error in formset.errors:
                errors.update(error)
            for error in formset.non_form_errors():
                errors.update({'__all__' : error})

            return JsonResponse(errors, status=400)
        else:
            return response




class ProjetoDelete(AjaxableResponseMixin, PassRequestToFormViewMixin, DeleteView):
    model = Projeto
    form_class = ProjetoForm


class InformacaoProjetoListView(generic.ListView):
    model = InformacaoProjeto
    #template_name = 'checklist/generic_list.html'
    template_name = 'checklist/informacaoprojeto_list.html'
    context_object_name = 'info_list'


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        table = InformacaoProjetoTable(self.get_queryset())
        RequestConfig(self.request, paginate={'per_page': 300}).configure(table)
        context['table'] = table
        #context['projeto_id'] = self.request.GET["projeto_id"]
        return context

    def get_queryset(self):
        results = InformacaoProjeto.objects.filter(projeto=self.request.GET["projeto_id"])
        return results.order_by('-pk')


class InformacaoProjetoCreate(AjaxableResponseMixin, PassRequestToFormViewMixin, CreateView):
    model = InformacaoProjeto
    form_class = InformacaoProjetoForm

    def get_success_url(self):
        return reverse('Checklists:projeto_index')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        projeto = Projeto.objects.get(pk=self.request.GET["projeto_id"])
        context['projeto'] = projeto
        return context


class InformacaoProjetoUpdate(AjaxableResponseMixin, PassRequestToFormViewMixin, UpdateView):
    model = InformacaoProjeto
    form_class = InformacaoProjetoForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        projeto = Projeto.objects.get(pk=self.request.GET["projeto_id"])
        context['projeto'] = projeto
        return context

class InformacaoProjetoDelete(AjaxableResponseMixin, PassRequestToFormViewMixin, DeleteView):
    model = InformacaoProjeto
    form_class = InformacaoProjetoForm


class ProjetistaCreate(AjaxableResponseMixin, PassRequestToFormViewMixin, CreateView):
    model = Projetista
    form_class = ProjetistaForm

class ProjetistaUpdate(AjaxableResponseMixin, PassRequestToFormViewMixin, UpdateView):
    model = Projetista
    form_class = ProjetistaForm




"""
Chamadas AJAX
"""

@login_required
def obter_estatisticas_avaliacao(request):
    id = request.POST.get('id', None)
 
    avaliacao = get_object_or_404(Avaliacao, pk=id)
    
    data = {
        'estatisticas' :  avaliacao.estatisticas()
    }
    return JsonResponse(data)

@login_required
def obter_recomendacao(request):
    id = request.GET.get('id', None)
    info = request.GET.get('info', None)

    checklistitem = get_object_or_404(ChecklistItem, pk=id)

    recomendacao = checklistitem.obterRecomendacao(info)

    data = {
        'recomendacao' :  recomendacao.recomendacao
    }
    return JsonResponse(data)

@login_required
def editar_item_avaliacao(request):

    id = request.POST.get('id', None)
    info = request.POST.get('info', Recomendacao.ND).lower()
    observacao = request.POST.get('observacao', None)
    recomendacao_avaliador = request.POST.get('recomendacao_avaliador', None)
    info_analisada = request.POST.get('info_analisada', None)
    item_avaliacao = get_object_or_404(AvaliacaoItem, pk=id)


    #verificar se avaliacao está finaliza.
    if item_avaliacao.avaliacao.finalizada():
        return 

    recomendacao = item_avaliacao.checklistItem.obterRecomendacao(info)
    if recomendacao:
        recomendacao = recomendacao.recomendacao
    else:
        recomendacao = ""
        
    item_avaliacao.nivel_presenca_info = info
    item_avaliacao.recomendacao = recomendacao
    item_avaliacao.recomendacao_avaliador = recomendacao_avaliador
    item_avaliacao.observacao = observacao
    item_avaliacao.info_analisada = info_analisada
    
    item_avaliacao.save()


    #iniciar avaliacao
    if item_avaliacao.avaliacao.status == Avaliacao.A_REALIZAR:
        item_avaliacao.avaliacao.status = Avaliacao.EM_ANDAMENTO
    #alterar data de modificacao
    item_avaliacao.avaliacao.save() 

    if recomendacao:
        data = {
            'recomendacao' :  recomendacao
        }
    else:
        data = {
            'recomendacao' :  ""
        }
    return JsonResponse(data)


@login_required
def obter_checklists(request):

    id = request.GET.get('projeto', None)
    #import pdb; pdb.set_trace()

    projeto = get_object_or_404(Projeto, pk=id)
    checklists = projeto.checklists_disponiveis()

    data = []
    for chk in checklists:
        data.append({"key" : chk.pk, 'value': chk.titulo})


    return JsonResponse({'data' : data})

@login_required
def avaliacao_finalizar(request):
    id = request.POST.get('id', None)
    avaliacao = get_object_or_404(Avaliacao, pk=id)
    avaliacao.finalizar()
    return JsonResponse({'message' : u"Análise finalizada com sucesso"})


@login_required
def avaliacao_cancelar(request):

    id = request.POST.get('id', None)
    avaliacao = get_object_or_404(Avaliacao, pk=id)
    avaliacao.cancelar()
    return JsonResponse({'message' : u"Análise cancelada com sucesso"})


@login_required
def avaliacao_salvar_evolucao(request):
    id = request.POST.get('id', None)
    comentario = request.POST.get('comentario', None)
    avaliacao = get_object_or_404(Avaliacao, pk=id)
    avaliacao.salvar_evolucao(criador = request.user, comentario=comentario)
    return JsonResponse({'message' : u"Evolução salva com sucesso"})


""""
@login_required
def avaliacao_evolucao(request):
    id = request.POST.get('id', None)
    avaliacao = get_object_or_404(Avaliacao, pk=id)
    avaliacao.salvar_evolucao()
    return JsonResponse({'message' : u"Evolução salva com sucesso"})
"""

class AvaliacaoEvolucaolView(TemplateView):
    template_name = "checklist/generic_list.html"

    def get_context_data(self, **kwargs):
        context = super(AvaliacaoEvolucaolView, self).get_context_data(**kwargs)
        pk = self.kwargs['pk']
        avalaicao = get_avaliacao_or_404(self.request.user, pk=pk )

        context['table'] = AvaliacaoEvolucaoTable(avalaicao.evolucoes() )
        return context




 
"""
Relatórios  para impressao
"""

class RelatorioSimplificaoPrintView(generic.DetailView):
    """
    Relatório simplificado 
    """
    model = Avaliacao
    template_name = 'checklist/avaliacao_pendencias_print.html'

    def get_context_data(self, **kwargs):
        context = super(RelatorioSimplificaoPrintView, self).get_context_data(**kwargs)

        analise = self.get_object()
        table = AvaliacaoItemSimpleTable(analise.itens(somente_recomendacoes=True) ,empty_text="Nenhuma recomendação")
        RequestConfig(self.request, paginate={'per_page': 1000}).configure(table)
        context['table'] = table

        context['projetistas'] = analise.projeto.projetistas.filter()

        context['timezone_now'] = timezone_now()

        return context