from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from checklist_cms_integration.models import ChecklistsPluginModel
from django.utils.translation import ugettext as _



class ChecklistsPluginPublisher(CMSPluginBase):
    model = ChecklistsPluginModel  # model where plugin data are saved
    module = _("checklist")
    name = _("Checklists Plugin")  # name of the plugin in the interface
    render_template = "checklist_cms_integration/checklist_plugin.html"

    def render(self, context, instance, placeholder):
        """
        override the render() method which determines the template context variables that are used to render your plugin. 
        By default, this method only adds instance and placeholder objects to your context, 
        but plugins can override this to include any context that is required
        """
        context.update({'instance': instance})
        return context
