from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from cms.menu_bases import CMSAttachMenu
from menus.base import NavigationNode
from menus.menu_pool import menu_pool

from checklist.models import Checklist

"""
class ChecklistsMenu(CMSAttachMenu):
    name = _("Checklist Menu")  # give the menu a name this is required.

    def get_nodes(self, request):
        # This method is used to build the menu tree.
        nodes = []
        for checklist in Checklist.objects.all():
            node = NavigationNode(
                title=checklist.requisito,
                url=reverse('Checklist:detail', args=(checklist.pk,)),
                id=checklist.pk,  # unique id for this node within the menu
            )
            nodes.append(node)
        return nodes

menu_pool.register_menu(ChecklistsMenu)

"""