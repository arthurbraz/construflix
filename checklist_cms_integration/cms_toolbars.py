from django.utils.translation import ugettext_lazy as _
from cms.toolbar_pool import toolbar_pool
from cms.toolbar_base import CMSToolbar
from cms.utils.urlutils import admin_reverse
from checklist.models import Checklist

"""
class ChecklistToolbar(CMSToolbar):
    supported_apps = (
        'checklist',
        'checklist_cms_integration',
    )

    watch_models = [Checklist]

    def populate(self):
        if not self.is_current_app:
            return

        menu = self.toolbar.get_or_create_menu('checklist-app', _('Checklist'))

        menu.add_sideframe_item(
            name=_('Checklist list'),
            url=admin_reverse('checklist_checklists_changelist'),
        )

        menu.add_modal_item(
            name=_('Add new checklist'),
            url=admin_reverse('checklist_checklists_add'),
        )


toolbar_pool.register(ChecklistToolbar)  # register the toolbar

"""