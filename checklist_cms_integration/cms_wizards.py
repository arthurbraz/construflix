from cms.wizards.wizard_base import Wizard
from cms.wizards.wizard_pool import wizard_pool

from checklist_cms_integration.forms import ChecklistWizardForm


class ChecklistWizard(Wizard):
    pass

checklist_wizard = ChecklistWizard(
    title="Checklist",
    weight=200,  # determines the ordering of wizards in the Create dialog
    form=ChecklistWizardForm,
    description="Create a new Checklist",
)

wizard_pool.register(checklist_wizard)