from django.db import models
from cms.models import CMSPlugin
from django.utils.encoding import python_2_unicode_compatible
from checklist.models import Checklist, Avaliacao


class ChecklistsPluginModel(CMSPlugin):
    checklist = models.ForeignKey(Checklist)

    def __unicode__(self):
        return self.checklist.requisito


class AvaliacaoPluginModel(CMSPlugin):
    avaliacao = models.ForeignKey(Avaliacao)

        
