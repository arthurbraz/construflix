from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

from django.utils.translation import ugettext_lazy as _

from django.conf.urls import url
from checklist.views import ChecklistListView, ChecklistDetailView, AvaliacaoListView, MinhaPaginaView, MinhasAvaliacoesView, editar_avaliacao

"""
@apphook_pool.register  # register the application
class ChecklistsApphook(CMSApp):
    app_name = "Checklists"
    name = _("Analysis Application")

    #def get_urls(self, page=None, language=None, **kwargs):
    #    return ["Checklists.urls"]
    def get_urls(self, page=None, language=None, **kwargs):
    	#return ["checklist.urls"] 
        return [
            url(r'^$', ChecklistListView.as_view()),
            url(r'^(?P<slug>[\w-]+)/?$', ChecklistDetailView.as_view()),
        ]
 """
 
    
@apphook_pool.register  # register the application
class AvaliacaoApphook(CMSApp):
    app_name = "construflix"
    name = _("Construflix App")
    #urls = ["checklist.urls"]

    def get_urls(self, page=None, language=None, **kwargs):
        #return ["checklist.urls"]  
        return [
            url(r'^$', MinhaPaginaView.as_view(), name='minha_pagina')
        ]


#apphook_pool.register(AvaliacaoApphook)