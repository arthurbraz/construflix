from django.apps import AppConfig


class ChecklistCmsIntegrationConfig(AppConfig):
    name = 'checklist_cms_integration'
