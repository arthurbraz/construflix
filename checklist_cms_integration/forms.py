from django import forms

from checklist.models import Checklist


class ChecklistWizardForm(forms.ModelForm):
    class Meta:
        model = Checklist
        exclude = []