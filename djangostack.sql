--
-- PostgreSQL database dump
--

-- Dumped from database version 10.2
-- Dumped by pg_dump version 10.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: aldryn_bootstrap3_boostrap3alertplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_boostrap3alertplugin (
    cmsplugin_ptr_id integer NOT NULL,
    context character varying(255) NOT NULL,
    icon character varying(255) NOT NULL,
    classes text NOT NULL,
    attributes text NOT NULL
);


ALTER TABLE aldryn_bootstrap3_boostrap3alertplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_boostrap3blockquoteplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_boostrap3blockquoteplugin (
    cmsplugin_ptr_id integer NOT NULL,
    reverse boolean NOT NULL,
    classes text NOT NULL,
    attributes text NOT NULL
);


ALTER TABLE aldryn_bootstrap3_boostrap3blockquoteplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_boostrap3buttonplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_boostrap3buttonplugin (
    link_url character varying(200) NOT NULL,
    link_anchor character varying(255) NOT NULL,
    link_mailto character varying(255),
    link_phone character varying(255),
    link_target character varying(255) NOT NULL,
    cmsplugin_ptr_id integer NOT NULL,
    label character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    btn_context character varying(255) NOT NULL,
    btn_size character varying(255) NOT NULL,
    btn_block boolean NOT NULL,
    txt_context character varying(255) NOT NULL,
    icon_left character varying(255) NOT NULL,
    icon_right character varying(255) NOT NULL,
    classes text NOT NULL,
    link_file_id integer,
    link_page_id integer,
    link_attributes text NOT NULL
);


ALTER TABLE aldryn_bootstrap3_boostrap3buttonplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_boostrap3citeplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_boostrap3citeplugin (
    classes text NOT NULL,
    attributes text NOT NULL,
    cmsplugin_ptr_id integer NOT NULL
);


ALTER TABLE aldryn_bootstrap3_boostrap3citeplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_boostrap3iconplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_boostrap3iconplugin (
    cmsplugin_ptr_id integer NOT NULL,
    icon character varying(255) NOT NULL,
    classes text NOT NULL,
    attributes text NOT NULL
);


ALTER TABLE aldryn_bootstrap3_boostrap3iconplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_boostrap3imageplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_boostrap3imageplugin (
    cmsplugin_ptr_id integer NOT NULL,
    alt text NOT NULL,
    title text NOT NULL,
    aspect_ratio character varying(255) NOT NULL,
    thumbnail boolean NOT NULL,
    shape character varying(255) NOT NULL,
    classes text NOT NULL,
    img_responsive boolean NOT NULL,
    file_id integer,
    override_height integer,
    override_width integer,
    use_original_image boolean NOT NULL,
    attributes text NOT NULL
);


ALTER TABLE aldryn_bootstrap3_boostrap3imageplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_boostrap3jumbotronplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_boostrap3jumbotronplugin (
    label character varying(255) NOT NULL,
    grid boolean NOT NULL,
    classes text NOT NULL,
    attributes text NOT NULL,
    cmsplugin_ptr_id integer NOT NULL
);


ALTER TABLE aldryn_bootstrap3_boostrap3jumbotronplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_boostrap3labelplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_boostrap3labelplugin (
    cmsplugin_ptr_id integer NOT NULL,
    label character varying(255) NOT NULL,
    context character varying(255) NOT NULL,
    classes text NOT NULL,
    attributes text NOT NULL
);


ALTER TABLE aldryn_bootstrap3_boostrap3labelplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_boostrap3panelbodyplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_boostrap3panelbodyplugin (
    cmsplugin_ptr_id integer NOT NULL,
    classes text NOT NULL,
    attributes text NOT NULL
);


ALTER TABLE aldryn_bootstrap3_boostrap3panelbodyplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_boostrap3panelfooterplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_boostrap3panelfooterplugin (
    cmsplugin_ptr_id integer NOT NULL,
    classes text NOT NULL,
    attributes text NOT NULL
);


ALTER TABLE aldryn_bootstrap3_boostrap3panelfooterplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_boostrap3panelheadingplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_boostrap3panelheadingplugin (
    cmsplugin_ptr_id integer NOT NULL,
    title text NOT NULL,
    classes text NOT NULL,
    attributes text NOT NULL
);


ALTER TABLE aldryn_bootstrap3_boostrap3panelheadingplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_boostrap3panelplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_boostrap3panelplugin (
    cmsplugin_ptr_id integer NOT NULL,
    context character varying(255) NOT NULL,
    classes text NOT NULL,
    attributes text NOT NULL
);


ALTER TABLE aldryn_bootstrap3_boostrap3panelplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_boostrap3spacerplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_boostrap3spacerplugin (
    cmsplugin_ptr_id integer NOT NULL,
    size character varying(255) NOT NULL,
    classes text NOT NULL,
    attributes text NOT NULL
);


ALTER TABLE aldryn_bootstrap3_boostrap3spacerplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_boostrap3wellplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_boostrap3wellplugin (
    cmsplugin_ptr_id integer NOT NULL,
    size character varying(255) NOT NULL,
    classes text NOT NULL,
    attributes text NOT NULL
);


ALTER TABLE aldryn_bootstrap3_boostrap3wellplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_bootstrap3accordionitemplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_bootstrap3accordionitemplugin (
    cmsplugin_ptr_id integer NOT NULL,
    title text NOT NULL,
    context character varying(255) NOT NULL,
    classes text NOT NULL,
    attributes text NOT NULL
);


ALTER TABLE aldryn_bootstrap3_bootstrap3accordionitemplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_bootstrap3accordionplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_bootstrap3accordionplugin (
    cmsplugin_ptr_id integer NOT NULL,
    index integer,
    classes text NOT NULL,
    attributes text NOT NULL,
    CONSTRAINT aldryn_bootstrap3_bootstrap3accordionplugin_index_check CHECK ((index >= 0))
);


ALTER TABLE aldryn_bootstrap3_bootstrap3accordionplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_bootstrap3carouselplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_bootstrap3carouselplugin (
    cmsplugin_ptr_id integer NOT NULL,
    style character varying(255) NOT NULL,
    aspect_ratio character varying(255) NOT NULL,
    transition_effect character varying(255) NOT NULL,
    ride boolean NOT NULL,
    "interval" integer NOT NULL,
    wrap boolean NOT NULL,
    pause boolean NOT NULL,
    classes text NOT NULL,
    attributes text NOT NULL
);


ALTER TABLE aldryn_bootstrap3_bootstrap3carouselplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_bootstrap3carouselslidefolderplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_bootstrap3carouselslidefolderplugin (
    cmsplugin_ptr_id integer NOT NULL,
    classes text NOT NULL,
    folder_id integer NOT NULL
);


ALTER TABLE aldryn_bootstrap3_bootstrap3carouselslidefolderplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_bootstrap3carouselslideplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_bootstrap3carouselslideplugin (
    cmsplugin_ptr_id integer NOT NULL,
    link_url character varying(200) NOT NULL,
    link_anchor character varying(255) NOT NULL,
    link_mailto character varying(255),
    link_phone character varying(255),
    link_target character varying(255) NOT NULL,
    link_text character varying(255) NOT NULL,
    content text NOT NULL,
    classes text NOT NULL,
    image_id integer,
    link_file_id integer,
    link_page_id integer,
    link_attributes text NOT NULL
);


ALTER TABLE aldryn_bootstrap3_bootstrap3carouselslideplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_bootstrap3codeplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_bootstrap3codeplugin (
    code_type character varying(255) NOT NULL,
    code text NOT NULL,
    classes text NOT NULL,
    attributes text NOT NULL,
    cmsplugin_ptr_id integer NOT NULL
);


ALTER TABLE aldryn_bootstrap3_bootstrap3codeplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_bootstrap3columnplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_bootstrap3columnplugin (
    cmsplugin_ptr_id integer NOT NULL,
    classes text NOT NULL,
    tag character varying(50) NOT NULL,
    xs_col integer,
    xs_offset integer,
    xs_push integer,
    xs_pull integer,
    sm_col integer,
    sm_offset integer,
    sm_push integer,
    sm_pull integer,
    md_col integer,
    md_offset integer,
    md_push integer,
    md_pull integer,
    lg_col integer,
    lg_offset integer,
    lg_push integer,
    lg_pull integer,
    attributes text NOT NULL
);


ALTER TABLE aldryn_bootstrap3_bootstrap3columnplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_bootstrap3fileplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_bootstrap3fileplugin (
    cmsplugin_ptr_id integer NOT NULL,
    name text NOT NULL,
    open_new_window boolean NOT NULL,
    show_file_size boolean NOT NULL,
    icon_left character varying(255) NOT NULL,
    icon_right character varying(255) NOT NULL,
    classes text NOT NULL,
    file_id integer,
    attributes text NOT NULL
);


ALTER TABLE aldryn_bootstrap3_bootstrap3fileplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_bootstrap3listgroupitemplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_bootstrap3listgroupitemplugin (
    cmsplugin_ptr_id integer NOT NULL,
    title text NOT NULL,
    context character varying(255) NOT NULL,
    state character varying(255) NOT NULL,
    classes text NOT NULL,
    attributes text NOT NULL
);


ALTER TABLE aldryn_bootstrap3_bootstrap3listgroupitemplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_bootstrap3listgroupplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_bootstrap3listgroupplugin (
    cmsplugin_ptr_id integer NOT NULL,
    classes text NOT NULL,
    add_list_group_class boolean NOT NULL,
    attributes text NOT NULL
);


ALTER TABLE aldryn_bootstrap3_bootstrap3listgroupplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_bootstrap3responsiveplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_bootstrap3responsiveplugin (
    device_breakpoints text NOT NULL,
    print_breakpoints text NOT NULL,
    classes text NOT NULL,
    attributes text NOT NULL,
    cmsplugin_ptr_id integer NOT NULL
);


ALTER TABLE aldryn_bootstrap3_bootstrap3responsiveplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_bootstrap3rowplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_bootstrap3rowplugin (
    cmsplugin_ptr_id integer NOT NULL,
    classes text NOT NULL,
    attributes text NOT NULL
);


ALTER TABLE aldryn_bootstrap3_bootstrap3rowplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_bootstrap3tabitemplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_bootstrap3tabitemplugin (
    title character varying(255) NOT NULL,
    icon character varying(255) NOT NULL,
    classes text NOT NULL,
    attributes text NOT NULL,
    cmsplugin_ptr_id integer NOT NULL
);


ALTER TABLE aldryn_bootstrap3_bootstrap3tabitemplugin OWNER TO postgres;

--
-- Name: aldryn_bootstrap3_bootstrap3tabplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE aldryn_bootstrap3_bootstrap3tabplugin (
    index integer,
    style character varying(255) NOT NULL,
    effect character varying(255) NOT NULL,
    classes text NOT NULL,
    attributes text NOT NULL,
    cmsplugin_ptr_id integer NOT NULL,
    CONSTRAINT aldryn_bootstrap3_bootstrap3tabplugin_index_check CHECK ((index >= 0))
);


ALTER TABLE aldryn_bootstrap3_bootstrap3tabplugin OWNER TO postgres;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: bitnami
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE auth_group OWNER TO bitnami;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: bitnami
--

CREATE SEQUENCE auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_id_seq OWNER TO bitnami;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bitnami
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: bitnami
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_group_permissions OWNER TO bitnami;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: bitnami
--

CREATE SEQUENCE auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_permissions_id_seq OWNER TO bitnami;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bitnami
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: bitnami
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE auth_permission OWNER TO bitnami;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: bitnami
--

CREATE SEQUENCE auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_permission_id_seq OWNER TO bitnami;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bitnami
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: bitnami
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE auth_user OWNER TO bitnami;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: bitnami
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE auth_user_groups OWNER TO bitnami;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: bitnami
--

CREATE SEQUENCE auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_groups_id_seq OWNER TO bitnami;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bitnami
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: bitnami
--

CREATE SEQUENCE auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_id_seq OWNER TO bitnami;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bitnami
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: bitnami
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_user_user_permissions OWNER TO bitnami;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: bitnami
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_user_permissions_id_seq OWNER TO bitnami;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bitnami
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: checklist_avaliacao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE checklist_avaliacao (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone,
    descricao text NOT NULL,
    prancha character varying(25) NOT NULL,
    status integer NOT NULL,
    avaliador_id integer NOT NULL,
    empreendimento_id integer NOT NULL,
    etapa_projeto_id integer NOT NULL,
    produto_id integer NOT NULL,
    projeto_id integer NOT NULL
);


ALTER TABLE checklist_avaliacao OWNER TO postgres;

--
-- Name: checklist_avaliacao_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE checklist_avaliacao_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE checklist_avaliacao_id_seq OWNER TO postgres;

--
-- Name: checklist_avaliacao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE checklist_avaliacao_id_seq OWNED BY checklist_avaliacao.id;


--
-- Name: checklist_checklist; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE checklist_checklist (
    id integer NOT NULL,
    requisito character varying(2000) NOT NULL,
    requisito_geral character varying(2000) NOT NULL,
    requisito_especifico character varying(2000) NOT NULL,
    normativo character varying(50),
    norma text,
    produto_id integer NOT NULL,
    dt_fim_publicacao date,
    dt_inicio_publicacao date,
    ic_ativo boolean NOT NULL
);


ALTER TABLE checklist_checklist OWNER TO postgres;

--
-- Name: checklist_checklist_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE checklist_checklist_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE checklist_checklist_id_seq OWNER TO postgres;

--
-- Name: checklist_checklist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE checklist_checklist_id_seq OWNED BY checklist_checklist.id;


--
-- Name: checklist_cms_integration_avaliacaopluginmodel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE checklist_cms_integration_avaliacaopluginmodel (
    cmsplugin_ptr_id integer NOT NULL,
    avaliacao_id integer NOT NULL
);


ALTER TABLE checklist_cms_integration_avaliacaopluginmodel OWNER TO postgres;

--
-- Name: checklist_cms_integration_checklistspluginmodel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE checklist_cms_integration_checklistspluginmodel (
    cmsplugin_ptr_id integer NOT NULL,
    checklist_id integer NOT NULL
);


ALTER TABLE checklist_cms_integration_checklistspluginmodel OWNER TO postgres;

--
-- Name: checklist_empreendimento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE checklist_empreendimento (
    id integer NOT NULL,
    nome character varying(200) NOT NULL,
    descricao text NOT NULL,
    ic_ativo boolean NOT NULL,
    assinante_id integer NOT NULL
);


ALTER TABLE checklist_empreendimento OWNER TO postgres;

--
-- Name: checklist_empreendimento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE checklist_empreendimento_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE checklist_empreendimento_id_seq OWNER TO postgres;

--
-- Name: checklist_empreendimento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE checklist_empreendimento_id_seq OWNED BY checklist_empreendimento.id;


--
-- Name: checklist_etapaprojeto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE checklist_etapaprojeto (
    id integer NOT NULL,
    nome character varying(200) NOT NULL,
    descircao text NOT NULL,
    ic_ativo boolean NOT NULL
);


ALTER TABLE checklist_etapaprojeto OWNER TO postgres;

--
-- Name: checklist_etapaprojeto_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE checklist_etapaprojeto_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE checklist_etapaprojeto_id_seq OWNER TO postgres;

--
-- Name: checklist_etapaprojeto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE checklist_etapaprojeto_id_seq OWNED BY checklist_etapaprojeto.id;


--
-- Name: checklist_itemavaliacao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE checklist_itemavaliacao (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone,
    nivel_presenca_info character varying(15) NOT NULL,
    recomendacao text NOT NULL,
    avaliacao_id integer NOT NULL,
    checklist_id integer NOT NULL,
    observacao text NOT NULL
);


ALTER TABLE checklist_itemavaliacao OWNER TO postgres;

--
-- Name: checklist_itemavaliacao_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE checklist_itemavaliacao_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE checklist_itemavaliacao_id_seq OWNER TO postgres;

--
-- Name: checklist_itemavaliacao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE checklist_itemavaliacao_id_seq OWNED BY checklist_itemavaliacao.id;


--
-- Name: checklist_itemchecklist; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE checklist_itemchecklist (
    id integer NOT NULL,
    nivel_presenca_info character varying(15) NOT NULL,
    recomendacao text NOT NULL,
    checklist_id integer NOT NULL
);


ALTER TABLE checklist_itemchecklist OWNER TO postgres;

--
-- Name: checklist_itemchecklist_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE checklist_itemchecklist_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE checklist_itemchecklist_id_seq OWNER TO postgres;

--
-- Name: checklist_itemchecklist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE checklist_itemchecklist_id_seq OWNED BY checklist_itemchecklist.id;


--
-- Name: checklist_plano; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE checklist_plano (
    id integer NOT NULL,
    nome character varying(200) NOT NULL,
    descricao text NOT NULL,
    valor numeric(6,2) NOT NULL,
    dt_inicio_publicacao date NOT NULL,
    dt_fim_publicacao date NOT NULL,
    ic_ativo boolean NOT NULL
);


ALTER TABLE checklist_plano OWNER TO postgres;

--
-- Name: checklist_plano_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE checklist_plano_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE checklist_plano_id_seq OWNER TO postgres;

--
-- Name: checklist_plano_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE checklist_plano_id_seq OWNED BY checklist_plano.id;


--
-- Name: checklist_produto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE checklist_produto (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone,
    titulo character varying(100) NOT NULL,
    descricao text NOT NULL,
    folder character varying(100) NOT NULL,
    dt_inicio_publicacao date NOT NULL,
    dt_fim_publicacao date NOT NULL,
    ic_ativo boolean NOT NULL,
    ramo_id integer NOT NULL
);


ALTER TABLE checklist_produto OWNER TO postgres;

--
-- Name: checklist_produto_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE checklist_produto_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE checklist_produto_id_seq OWNER TO postgres;

--
-- Name: checklist_produto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE checklist_produto_id_seq OWNED BY checklist_produto.id;


--
-- Name: checklist_produto_planos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE checklist_produto_planos (
    id integer NOT NULL,
    produto_id integer NOT NULL,
    plano_id integer NOT NULL
);


ALTER TABLE checklist_produto_planos OWNER TO postgres;

--
-- Name: checklist_produto_planos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE checklist_produto_planos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE checklist_produto_planos_id_seq OWNER TO postgres;

--
-- Name: checklist_produto_planos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE checklist_produto_planos_id_seq OWNED BY checklist_produto_planos.id;


--
-- Name: checklist_projeto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE checklist_projeto (
    id integer NOT NULL,
    nome character varying(200) NOT NULL,
    descricao text NOT NULL,
    ic_ativo boolean NOT NULL,
    ramo_id integer NOT NULL
);


ALTER TABLE checklist_projeto OWNER TO postgres;

--
-- Name: checklist_projeto_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE checklist_projeto_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE checklist_projeto_id_seq OWNER TO postgres;

--
-- Name: checklist_projeto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE checklist_projeto_id_seq OWNED BY checklist_projeto.id;


--
-- Name: checklist_ramo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE checklist_ramo (
    id integer NOT NULL,
    nome character varying(200) NOT NULL,
    descircao text NOT NULL,
    ic_ativo boolean NOT NULL
);


ALTER TABLE checklist_ramo OWNER TO postgres;

--
-- Name: checklist_ramo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE checklist_ramo_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE checklist_ramo_id_seq OWNER TO postgres;

--
-- Name: checklist_ramo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE checklist_ramo_id_seq OWNED BY checklist_ramo.id;


--
-- Name: cms_aliaspluginmodel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cms_aliaspluginmodel (
    cmsplugin_ptr_id integer NOT NULL,
    plugin_id integer,
    alias_placeholder_id integer
);


ALTER TABLE cms_aliaspluginmodel OWNER TO postgres;

--
-- Name: cms_cmsplugin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cms_cmsplugin (
    id integer NOT NULL,
    "position" smallint NOT NULL,
    language character varying(15) NOT NULL,
    plugin_type character varying(50) NOT NULL,
    creation_date timestamp with time zone NOT NULL,
    changed_date timestamp with time zone NOT NULL,
    parent_id integer,
    placeholder_id integer,
    depth integer NOT NULL,
    numchild integer NOT NULL,
    path character varying(255) NOT NULL,
    CONSTRAINT cms_cmsplugin_depth_check CHECK ((depth >= 0)),
    CONSTRAINT cms_cmsplugin_numchild_check CHECK ((numchild >= 0)),
    CONSTRAINT cms_cmsplugin_position_check CHECK (("position" >= 0))
);


ALTER TABLE cms_cmsplugin OWNER TO postgres;

--
-- Name: cms_cmsplugin_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cms_cmsplugin_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cms_cmsplugin_id_seq OWNER TO postgres;

--
-- Name: cms_cmsplugin_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cms_cmsplugin_id_seq OWNED BY cms_cmsplugin.id;


--
-- Name: cms_globalpagepermission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cms_globalpagepermission (
    id integer NOT NULL,
    can_change boolean NOT NULL,
    can_add boolean NOT NULL,
    can_delete boolean NOT NULL,
    can_change_advanced_settings boolean NOT NULL,
    can_publish boolean NOT NULL,
    can_change_permissions boolean NOT NULL,
    can_move_page boolean NOT NULL,
    can_view boolean NOT NULL,
    can_recover_page boolean NOT NULL,
    group_id integer,
    user_id integer
);


ALTER TABLE cms_globalpagepermission OWNER TO postgres;

--
-- Name: cms_globalpagepermission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cms_globalpagepermission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cms_globalpagepermission_id_seq OWNER TO postgres;

--
-- Name: cms_globalpagepermission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cms_globalpagepermission_id_seq OWNED BY cms_globalpagepermission.id;


--
-- Name: cms_globalpagepermission_sites; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cms_globalpagepermission_sites (
    id integer NOT NULL,
    globalpagepermission_id integer NOT NULL,
    site_id integer NOT NULL
);


ALTER TABLE cms_globalpagepermission_sites OWNER TO postgres;

--
-- Name: cms_globalpagepermission_sites_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cms_globalpagepermission_sites_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cms_globalpagepermission_sites_id_seq OWNER TO postgres;

--
-- Name: cms_globalpagepermission_sites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cms_globalpagepermission_sites_id_seq OWNED BY cms_globalpagepermission_sites.id;


--
-- Name: cms_page; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cms_page (
    id integer NOT NULL,
    created_by character varying(255) NOT NULL,
    changed_by character varying(255) NOT NULL,
    creation_date timestamp with time zone NOT NULL,
    changed_date timestamp with time zone NOT NULL,
    publication_date timestamp with time zone,
    publication_end_date timestamp with time zone,
    in_navigation boolean NOT NULL,
    soft_root boolean NOT NULL,
    reverse_id character varying(40),
    navigation_extenders character varying(80),
    template character varying(100) NOT NULL,
    login_required boolean NOT NULL,
    limit_visibility_in_menu smallint,
    is_home boolean NOT NULL,
    application_urls character varying(200),
    application_namespace character varying(200),
    publisher_is_draft boolean NOT NULL,
    languages character varying(255),
    xframe_options integer NOT NULL,
    publisher_public_id integer,
    is_page_type boolean NOT NULL,
    node_id integer NOT NULL
);


ALTER TABLE cms_page OWNER TO postgres;

--
-- Name: cms_page_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cms_page_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cms_page_id_seq OWNER TO postgres;

--
-- Name: cms_page_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cms_page_id_seq OWNED BY cms_page.id;


--
-- Name: cms_page_placeholders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cms_page_placeholders (
    id integer NOT NULL,
    page_id integer NOT NULL,
    placeholder_id integer NOT NULL
);


ALTER TABLE cms_page_placeholders OWNER TO postgres;

--
-- Name: cms_page_placeholders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cms_page_placeholders_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cms_page_placeholders_id_seq OWNER TO postgres;

--
-- Name: cms_page_placeholders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cms_page_placeholders_id_seq OWNED BY cms_page_placeholders.id;


--
-- Name: cms_pagepermission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cms_pagepermission (
    id integer NOT NULL,
    can_change boolean NOT NULL,
    can_add boolean NOT NULL,
    can_delete boolean NOT NULL,
    can_change_advanced_settings boolean NOT NULL,
    can_publish boolean NOT NULL,
    can_change_permissions boolean NOT NULL,
    can_move_page boolean NOT NULL,
    can_view boolean NOT NULL,
    grant_on integer NOT NULL,
    group_id integer,
    page_id integer,
    user_id integer
);


ALTER TABLE cms_pagepermission OWNER TO postgres;

--
-- Name: cms_pagepermission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cms_pagepermission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cms_pagepermission_id_seq OWNER TO postgres;

--
-- Name: cms_pagepermission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cms_pagepermission_id_seq OWNED BY cms_pagepermission.id;


--
-- Name: cms_pageuser; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cms_pageuser (
    user_ptr_id integer NOT NULL,
    created_by_id integer NOT NULL
);


ALTER TABLE cms_pageuser OWNER TO postgres;

--
-- Name: cms_pageusergroup; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cms_pageusergroup (
    group_ptr_id integer NOT NULL,
    created_by_id integer NOT NULL
);


ALTER TABLE cms_pageusergroup OWNER TO postgres;

--
-- Name: cms_placeholder; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cms_placeholder (
    id integer NOT NULL,
    slot character varying(255) NOT NULL,
    default_width smallint,
    CONSTRAINT cms_placeholder_default_width_check CHECK ((default_width >= 0))
);


ALTER TABLE cms_placeholder OWNER TO postgres;

--
-- Name: cms_placeholder_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cms_placeholder_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cms_placeholder_id_seq OWNER TO postgres;

--
-- Name: cms_placeholder_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cms_placeholder_id_seq OWNED BY cms_placeholder.id;


--
-- Name: cms_placeholderreference; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cms_placeholderreference (
    cmsplugin_ptr_id integer NOT NULL,
    name character varying(255) NOT NULL,
    placeholder_ref_id integer
);


ALTER TABLE cms_placeholderreference OWNER TO postgres;

--
-- Name: cms_staticplaceholder; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cms_staticplaceholder (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    code character varying(255) NOT NULL,
    dirty boolean NOT NULL,
    creation_method character varying(20) NOT NULL,
    draft_id integer,
    public_id integer,
    site_id integer
);


ALTER TABLE cms_staticplaceholder OWNER TO postgres;

--
-- Name: cms_staticplaceholder_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cms_staticplaceholder_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cms_staticplaceholder_id_seq OWNER TO postgres;

--
-- Name: cms_staticplaceholder_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cms_staticplaceholder_id_seq OWNED BY cms_staticplaceholder.id;


--
-- Name: cms_title; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cms_title (
    id integer NOT NULL,
    language character varying(15) NOT NULL,
    title character varying(255) NOT NULL,
    page_title character varying(255),
    menu_title character varying(255),
    meta_description text,
    slug character varying(255) NOT NULL,
    path character varying(255) NOT NULL,
    has_url_overwrite boolean NOT NULL,
    redirect character varying(2048),
    creation_date timestamp with time zone NOT NULL,
    published boolean NOT NULL,
    publisher_is_draft boolean NOT NULL,
    publisher_state smallint NOT NULL,
    page_id integer NOT NULL,
    publisher_public_id integer
);


ALTER TABLE cms_title OWNER TO postgres;

--
-- Name: cms_title_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cms_title_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cms_title_id_seq OWNER TO postgres;

--
-- Name: cms_title_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cms_title_id_seq OWNED BY cms_title.id;


--
-- Name: cms_treenode; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cms_treenode (
    id integer NOT NULL,
    path character varying(255) NOT NULL,
    depth integer NOT NULL,
    numchild integer NOT NULL,
    parent_id integer,
    site_id integer NOT NULL,
    CONSTRAINT cms_treenode_depth_check CHECK ((depth >= 0)),
    CONSTRAINT cms_treenode_numchild_check CHECK ((numchild >= 0))
);


ALTER TABLE cms_treenode OWNER TO postgres;

--
-- Name: cms_treenode_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cms_treenode_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cms_treenode_id_seq OWNER TO postgres;

--
-- Name: cms_treenode_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cms_treenode_id_seq OWNED BY cms_treenode.id;


--
-- Name: cms_urlconfrevision; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cms_urlconfrevision (
    id integer NOT NULL,
    revision character varying(255) NOT NULL
);


ALTER TABLE cms_urlconfrevision OWNER TO postgres;

--
-- Name: cms_urlconfrevision_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cms_urlconfrevision_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cms_urlconfrevision_id_seq OWNER TO postgres;

--
-- Name: cms_urlconfrevision_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cms_urlconfrevision_id_seq OWNED BY cms_urlconfrevision.id;


--
-- Name: cms_usersettings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cms_usersettings (
    id integer NOT NULL,
    language character varying(10) NOT NULL,
    clipboard_id integer,
    user_id integer NOT NULL
);


ALTER TABLE cms_usersettings OWNER TO postgres;

--
-- Name: cms_usersettings_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cms_usersettings_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cms_usersettings_id_seq OWNER TO postgres;

--
-- Name: cms_usersettings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cms_usersettings_id_seq OWNED BY cms_usersettings.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: bitnami
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE django_admin_log OWNER TO bitnami;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: bitnami
--

CREATE SEQUENCE django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_admin_log_id_seq OWNER TO bitnami;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bitnami
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: bitnami
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE django_content_type OWNER TO bitnami;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: bitnami
--

CREATE SEQUENCE django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_content_type_id_seq OWNER TO bitnami;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bitnami
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: bitnami
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE django_migrations OWNER TO bitnami;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: bitnami
--

CREATE SEQUENCE django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_migrations_id_seq OWNER TO bitnami;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bitnami
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: bitnami
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE django_session OWNER TO bitnami;

--
-- Name: django_site; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE django_site OWNER TO postgres;

--
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_site_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_site_id_seq OWNER TO postgres;

--
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_site_id_seq OWNED BY django_site.id;


--
-- Name: djangocms_column_column; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE djangocms_column_column (
    cmsplugin_ptr_id integer NOT NULL,
    width character varying(50) NOT NULL
);


ALTER TABLE djangocms_column_column OWNER TO postgres;

--
-- Name: djangocms_column_multicolumns; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE djangocms_column_multicolumns (
    cmsplugin_ptr_id integer NOT NULL
);


ALTER TABLE djangocms_column_multicolumns OWNER TO postgres;

--
-- Name: djangocms_file_file; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE djangocms_file_file (
    cmsplugin_ptr_id integer NOT NULL,
    file_name character varying(255) NOT NULL,
    link_target character varying(255) NOT NULL,
    link_title character varying(255) NOT NULL,
    file_src_id integer,
    attributes text NOT NULL,
    template character varying(255) NOT NULL,
    show_file_size boolean NOT NULL
);


ALTER TABLE djangocms_file_file OWNER TO postgres;

--
-- Name: djangocms_file_folder; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE djangocms_file_folder (
    template character varying(255) NOT NULL,
    link_target character varying(255) NOT NULL,
    show_file_size boolean NOT NULL,
    attributes text NOT NULL,
    cmsplugin_ptr_id integer NOT NULL,
    folder_src_id integer
);


ALTER TABLE djangocms_file_folder OWNER TO postgres;

--
-- Name: djangocms_googlemap_googlemap; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE djangocms_googlemap_googlemap (
    cmsplugin_ptr_id integer NOT NULL,
    title character varying(255) NOT NULL,
    zoom smallint NOT NULL,
    lat double precision,
    lng double precision,
    width character varying(6) NOT NULL,
    height character varying(6) NOT NULL,
    scrollwheel boolean NOT NULL,
    double_click_zoom boolean NOT NULL,
    draggable boolean NOT NULL,
    keyboard_shortcuts boolean NOT NULL,
    pan_control boolean NOT NULL,
    zoom_control boolean NOT NULL,
    street_view_control boolean NOT NULL,
    style text NOT NULL,
    fullscreen_control boolean NOT NULL,
    map_type_control character varying(255) NOT NULL,
    rotate_control boolean NOT NULL,
    scale_control boolean NOT NULL,
    template character varying(255) NOT NULL,
    CONSTRAINT djangocms_googlemap_googlemap_zoom_check CHECK ((zoom >= 0))
);


ALTER TABLE djangocms_googlemap_googlemap OWNER TO postgres;

--
-- Name: djangocms_googlemap_googlemapmarker; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE djangocms_googlemap_googlemapmarker (
    cmsplugin_ptr_id integer NOT NULL,
    title character varying(255) NOT NULL,
    address character varying(255) NOT NULL,
    lat numeric(10,6),
    lng numeric(10,6),
    show_content boolean NOT NULL,
    info_content text NOT NULL,
    icon_id integer
);


ALTER TABLE djangocms_googlemap_googlemapmarker OWNER TO postgres;

--
-- Name: djangocms_googlemap_googlemaproute; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE djangocms_googlemap_googlemaproute (
    cmsplugin_ptr_id integer NOT NULL,
    title character varying(255) NOT NULL,
    origin character varying(255) NOT NULL,
    destination character varying(255) NOT NULL,
    travel_mode character varying(255) NOT NULL
);


ALTER TABLE djangocms_googlemap_googlemaproute OWNER TO postgres;

--
-- Name: djangocms_link_link; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE djangocms_link_link (
    cmsplugin_ptr_id integer NOT NULL,
    name character varying(255) NOT NULL,
    external_link character varying(2040) NOT NULL,
    anchor character varying(255) NOT NULL,
    mailto character varying(255) NOT NULL,
    phone character varying(255) NOT NULL,
    target character varying(255) NOT NULL,
    internal_link_id integer,
    attributes text NOT NULL,
    template character varying(255) NOT NULL
);


ALTER TABLE djangocms_link_link OWNER TO postgres;

--
-- Name: djangocms_picture_picture; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE djangocms_picture_picture (
    cmsplugin_ptr_id integer NOT NULL,
    link_url character varying(2040) NOT NULL,
    alignment character varying(255) NOT NULL,
    link_page_id integer,
    height integer,
    width integer,
    picture_id integer,
    attributes text NOT NULL,
    caption_text text NOT NULL,
    link_attributes text NOT NULL,
    link_target character varying(255) NOT NULL,
    use_automatic_scaling boolean NOT NULL,
    use_crop boolean NOT NULL,
    use_no_cropping boolean NOT NULL,
    use_upscale boolean NOT NULL,
    thumbnail_options_id integer,
    external_picture character varying(255) NOT NULL,
    template character varying(255) NOT NULL,
    CONSTRAINT djangocms_picture_picture_height_3db3e080_check CHECK ((height >= 0)),
    CONSTRAINT djangocms_picture_picture_width_5bba3699_check CHECK ((width >= 0))
);


ALTER TABLE djangocms_picture_picture OWNER TO postgres;

--
-- Name: djangocms_snippet_snippet; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE djangocms_snippet_snippet (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    html text NOT NULL,
    template character varying(255) NOT NULL,
    slug character varying(255) NOT NULL
);


ALTER TABLE djangocms_snippet_snippet OWNER TO postgres;

--
-- Name: djangocms_snippet_snippet_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE djangocms_snippet_snippet_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE djangocms_snippet_snippet_id_seq OWNER TO postgres;

--
-- Name: djangocms_snippet_snippet_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE djangocms_snippet_snippet_id_seq OWNED BY djangocms_snippet_snippet.id;


--
-- Name: djangocms_snippet_snippetptr; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE djangocms_snippet_snippetptr (
    cmsplugin_ptr_id integer NOT NULL,
    snippet_id integer NOT NULL
);


ALTER TABLE djangocms_snippet_snippetptr OWNER TO postgres;

--
-- Name: djangocms_style_style; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE djangocms_style_style (
    cmsplugin_ptr_id integer NOT NULL,
    class_name character varying(255) NOT NULL,
    tag_type character varying(255) NOT NULL,
    padding_left smallint,
    padding_right smallint,
    padding_top smallint,
    padding_bottom smallint,
    margin_left smallint,
    margin_right smallint,
    margin_top smallint,
    margin_bottom smallint,
    additional_classes character varying(255) NOT NULL,
    attributes text NOT NULL,
    id_name character varying(255) NOT NULL,
    label character varying(255) NOT NULL,
    template character varying(255) NOT NULL,
    CONSTRAINT djangocms_style_style_margin_bottom_08e7c851_check CHECK ((margin_bottom >= 0)),
    CONSTRAINT djangocms_style_style_margin_left_e51dea0a_check CHECK ((margin_left >= 0)),
    CONSTRAINT djangocms_style_style_margin_right_40a4928b_check CHECK ((margin_right >= 0)),
    CONSTRAINT djangocms_style_style_margin_top_929f0506_check CHECK ((margin_top >= 0)),
    CONSTRAINT djangocms_style_style_padding_bottom_566948a6_check CHECK ((padding_bottom >= 0)),
    CONSTRAINT djangocms_style_style_padding_left_2426413b_check CHECK ((padding_left >= 0)),
    CONSTRAINT djangocms_style_style_padding_right_90146b20_check CHECK ((padding_right >= 0)),
    CONSTRAINT djangocms_style_style_padding_top_fbca5ac1_check CHECK ((padding_top >= 0))
);


ALTER TABLE djangocms_style_style OWNER TO postgres;

--
-- Name: djangocms_text_ckeditor_text; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE djangocms_text_ckeditor_text (
    cmsplugin_ptr_id integer NOT NULL,
    body text NOT NULL
);


ALTER TABLE djangocms_text_ckeditor_text OWNER TO postgres;

--
-- Name: djangocms_video_videoplayer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE djangocms_video_videoplayer (
    cmsplugin_ptr_id integer NOT NULL,
    embed_link character varying(255) NOT NULL,
    poster_id integer,
    attributes text NOT NULL,
    label character varying(255) NOT NULL,
    template character varying(255) NOT NULL
);


ALTER TABLE djangocms_video_videoplayer OWNER TO postgres;

--
-- Name: djangocms_video_videosource; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE djangocms_video_videosource (
    cmsplugin_ptr_id integer NOT NULL,
    text_title character varying(255) NOT NULL,
    text_description text NOT NULL,
    attributes text NOT NULL,
    source_file_id integer
);


ALTER TABLE djangocms_video_videosource OWNER TO postgres;

--
-- Name: djangocms_video_videotrack; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE djangocms_video_videotrack (
    cmsplugin_ptr_id integer NOT NULL,
    kind character varying(255) NOT NULL,
    srclang character varying(255) NOT NULL,
    label character varying(255) NOT NULL,
    attributes text NOT NULL,
    src_id integer
);


ALTER TABLE djangocms_video_videotrack OWNER TO postgres;

--
-- Name: easy_thumbnails_source; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE easy_thumbnails_source (
    id integer NOT NULL,
    storage_hash character varying(40) NOT NULL,
    name character varying(255) NOT NULL,
    modified timestamp with time zone NOT NULL
);


ALTER TABLE easy_thumbnails_source OWNER TO postgres;

--
-- Name: easy_thumbnails_source_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE easy_thumbnails_source_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE easy_thumbnails_source_id_seq OWNER TO postgres;

--
-- Name: easy_thumbnails_source_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE easy_thumbnails_source_id_seq OWNED BY easy_thumbnails_source.id;


--
-- Name: easy_thumbnails_thumbnail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE easy_thumbnails_thumbnail (
    id integer NOT NULL,
    storage_hash character varying(40) NOT NULL,
    name character varying(255) NOT NULL,
    modified timestamp with time zone NOT NULL,
    source_id integer NOT NULL
);


ALTER TABLE easy_thumbnails_thumbnail OWNER TO postgres;

--
-- Name: easy_thumbnails_thumbnail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE easy_thumbnails_thumbnail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE easy_thumbnails_thumbnail_id_seq OWNER TO postgres;

--
-- Name: easy_thumbnails_thumbnail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE easy_thumbnails_thumbnail_id_seq OWNED BY easy_thumbnails_thumbnail.id;


--
-- Name: easy_thumbnails_thumbnaildimensions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE easy_thumbnails_thumbnaildimensions (
    id integer NOT NULL,
    thumbnail_id integer NOT NULL,
    width integer,
    height integer,
    CONSTRAINT easy_thumbnails_thumbnaildimensions_height_check CHECK ((height >= 0)),
    CONSTRAINT easy_thumbnails_thumbnaildimensions_width_check CHECK ((width >= 0))
);


ALTER TABLE easy_thumbnails_thumbnaildimensions OWNER TO postgres;

--
-- Name: easy_thumbnails_thumbnaildimensions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE easy_thumbnails_thumbnaildimensions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE easy_thumbnails_thumbnaildimensions_id_seq OWNER TO postgres;

--
-- Name: easy_thumbnails_thumbnaildimensions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE easy_thumbnails_thumbnaildimensions_id_seq OWNED BY easy_thumbnails_thumbnaildimensions.id;


--
-- Name: filer_clipboard; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE filer_clipboard (
    id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE filer_clipboard OWNER TO postgres;

--
-- Name: filer_clipboard_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE filer_clipboard_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE filer_clipboard_id_seq OWNER TO postgres;

--
-- Name: filer_clipboard_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE filer_clipboard_id_seq OWNED BY filer_clipboard.id;


--
-- Name: filer_clipboarditem; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE filer_clipboarditem (
    id integer NOT NULL,
    clipboard_id integer NOT NULL,
    file_id integer NOT NULL
);


ALTER TABLE filer_clipboarditem OWNER TO postgres;

--
-- Name: filer_clipboarditem_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE filer_clipboarditem_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE filer_clipboarditem_id_seq OWNER TO postgres;

--
-- Name: filer_clipboarditem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE filer_clipboarditem_id_seq OWNED BY filer_clipboarditem.id;


--
-- Name: filer_file; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE filer_file (
    id integer NOT NULL,
    file character varying(255),
    _file_size integer,
    sha1 character varying(40) NOT NULL,
    has_all_mandatory_data boolean NOT NULL,
    original_filename character varying(255),
    name character varying(255) NOT NULL,
    description text,
    uploaded_at timestamp with time zone NOT NULL,
    modified_at timestamp with time zone NOT NULL,
    is_public boolean NOT NULL,
    folder_id integer,
    owner_id integer,
    polymorphic_ctype_id integer
);


ALTER TABLE filer_file OWNER TO postgres;

--
-- Name: filer_file_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE filer_file_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE filer_file_id_seq OWNER TO postgres;

--
-- Name: filer_file_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE filer_file_id_seq OWNED BY filer_file.id;


--
-- Name: filer_folder; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE filer_folder (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    uploaded_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    modified_at timestamp with time zone NOT NULL,
    lft integer NOT NULL,
    rght integer NOT NULL,
    tree_id integer NOT NULL,
    level integer NOT NULL,
    owner_id integer,
    parent_id integer,
    CONSTRAINT filer_folder_level_check CHECK ((level >= 0)),
    CONSTRAINT filer_folder_lft_check CHECK ((lft >= 0)),
    CONSTRAINT filer_folder_rght_check CHECK ((rght >= 0)),
    CONSTRAINT filer_folder_tree_id_check CHECK ((tree_id >= 0))
);


ALTER TABLE filer_folder OWNER TO postgres;

--
-- Name: filer_folder_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE filer_folder_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE filer_folder_id_seq OWNER TO postgres;

--
-- Name: filer_folder_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE filer_folder_id_seq OWNED BY filer_folder.id;


--
-- Name: filer_folderpermission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE filer_folderpermission (
    id integer NOT NULL,
    type smallint NOT NULL,
    everybody boolean NOT NULL,
    can_edit smallint,
    can_read smallint,
    can_add_children smallint,
    folder_id integer,
    group_id integer,
    user_id integer
);


ALTER TABLE filer_folderpermission OWNER TO postgres;

--
-- Name: filer_folderpermission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE filer_folderpermission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE filer_folderpermission_id_seq OWNER TO postgres;

--
-- Name: filer_folderpermission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE filer_folderpermission_id_seq OWNED BY filer_folderpermission.id;


--
-- Name: filer_image; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE filer_image (
    file_ptr_id integer NOT NULL,
    _height integer,
    _width integer,
    date_taken timestamp with time zone,
    default_alt_text character varying(255),
    default_caption character varying(255),
    author character varying(255),
    must_always_publish_author_credit boolean NOT NULL,
    must_always_publish_copyright boolean NOT NULL,
    subject_location character varying(64) NOT NULL
);


ALTER TABLE filer_image OWNER TO postgres;

--
-- Name: filer_thumbnailoption; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE filer_thumbnailoption (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    width integer NOT NULL,
    height integer NOT NULL,
    crop boolean NOT NULL,
    upscale boolean NOT NULL
);


ALTER TABLE filer_thumbnailoption OWNER TO postgres;

--
-- Name: filer_thumbnailoption_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE filer_thumbnailoption_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE filer_thumbnailoption_id_seq OWNER TO postgres;

--
-- Name: filer_thumbnailoption_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE filer_thumbnailoption_id_seq OWNED BY filer_thumbnailoption.id;


--
-- Name: menus_cachekey; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE menus_cachekey (
    id integer NOT NULL,
    language character varying(255) NOT NULL,
    site integer NOT NULL,
    key character varying(255) NOT NULL,
    CONSTRAINT menus_cachekey_site_check CHECK ((site >= 0))
);


ALTER TABLE menus_cachekey OWNER TO postgres;

--
-- Name: menus_cachekey_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE menus_cachekey_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE menus_cachekey_id_seq OWNER TO postgres;

--
-- Name: menus_cachekey_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE menus_cachekey_id_seq OWNED BY menus_cachekey.id;


--
-- Name: polls_choice; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE polls_choice (
    id integer NOT NULL,
    choice_text character varying(200) NOT NULL,
    votes integer NOT NULL,
    poll_id integer NOT NULL
);


ALTER TABLE polls_choice OWNER TO postgres;

--
-- Name: polls_choice_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE polls_choice_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE polls_choice_id_seq OWNER TO postgres;

--
-- Name: polls_choice_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE polls_choice_id_seq OWNED BY polls_choice.id;


--
-- Name: polls_poll; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE polls_poll (
    id integer NOT NULL,
    question character varying(200) NOT NULL
);


ALTER TABLE polls_poll OWNER TO postgres;

--
-- Name: polls_poll_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE polls_poll_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE polls_poll_id_seq OWNER TO postgres;

--
-- Name: polls_poll_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE polls_poll_id_seq OWNED BY polls_poll.id;


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: checklist_avaliacao id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_avaliacao ALTER COLUMN id SET DEFAULT nextval('checklist_avaliacao_id_seq'::regclass);


--
-- Name: checklist_checklist id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_checklist ALTER COLUMN id SET DEFAULT nextval('checklist_checklist_id_seq'::regclass);


--
-- Name: checklist_empreendimento id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_empreendimento ALTER COLUMN id SET DEFAULT nextval('checklist_empreendimento_id_seq'::regclass);


--
-- Name: checklist_etapaprojeto id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_etapaprojeto ALTER COLUMN id SET DEFAULT nextval('checklist_etapaprojeto_id_seq'::regclass);


--
-- Name: checklist_itemavaliacao id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_itemavaliacao ALTER COLUMN id SET DEFAULT nextval('checklist_itemavaliacao_id_seq'::regclass);


--
-- Name: checklist_itemchecklist id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_itemchecklist ALTER COLUMN id SET DEFAULT nextval('checklist_itemchecklist_id_seq'::regclass);


--
-- Name: checklist_plano id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_plano ALTER COLUMN id SET DEFAULT nextval('checklist_plano_id_seq'::regclass);


--
-- Name: checklist_produto id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_produto ALTER COLUMN id SET DEFAULT nextval('checklist_produto_id_seq'::regclass);


--
-- Name: checklist_produto_planos id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_produto_planos ALTER COLUMN id SET DEFAULT nextval('checklist_produto_planos_id_seq'::regclass);


--
-- Name: checklist_projeto id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_projeto ALTER COLUMN id SET DEFAULT nextval('checklist_projeto_id_seq'::regclass);


--
-- Name: checklist_ramo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_ramo ALTER COLUMN id SET DEFAULT nextval('checklist_ramo_id_seq'::regclass);


--
-- Name: cms_cmsplugin id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_cmsplugin ALTER COLUMN id SET DEFAULT nextval('cms_cmsplugin_id_seq'::regclass);


--
-- Name: cms_globalpagepermission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_globalpagepermission ALTER COLUMN id SET DEFAULT nextval('cms_globalpagepermission_id_seq'::regclass);


--
-- Name: cms_globalpagepermission_sites id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_globalpagepermission_sites ALTER COLUMN id SET DEFAULT nextval('cms_globalpagepermission_sites_id_seq'::regclass);


--
-- Name: cms_page id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_page ALTER COLUMN id SET DEFAULT nextval('cms_page_id_seq'::regclass);


--
-- Name: cms_page_placeholders id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_page_placeholders ALTER COLUMN id SET DEFAULT nextval('cms_page_placeholders_id_seq'::regclass);


--
-- Name: cms_pagepermission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_pagepermission ALTER COLUMN id SET DEFAULT nextval('cms_pagepermission_id_seq'::regclass);


--
-- Name: cms_placeholder id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_placeholder ALTER COLUMN id SET DEFAULT nextval('cms_placeholder_id_seq'::regclass);


--
-- Name: cms_staticplaceholder id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_staticplaceholder ALTER COLUMN id SET DEFAULT nextval('cms_staticplaceholder_id_seq'::regclass);


--
-- Name: cms_title id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_title ALTER COLUMN id SET DEFAULT nextval('cms_title_id_seq'::regclass);


--
-- Name: cms_treenode id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_treenode ALTER COLUMN id SET DEFAULT nextval('cms_treenode_id_seq'::regclass);


--
-- Name: cms_urlconfrevision id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_urlconfrevision ALTER COLUMN id SET DEFAULT nextval('cms_urlconfrevision_id_seq'::regclass);


--
-- Name: cms_usersettings id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_usersettings ALTER COLUMN id SET DEFAULT nextval('cms_usersettings_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: django_site id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_site ALTER COLUMN id SET DEFAULT nextval('django_site_id_seq'::regclass);


--
-- Name: djangocms_snippet_snippet id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_snippet_snippet ALTER COLUMN id SET DEFAULT nextval('djangocms_snippet_snippet_id_seq'::regclass);


--
-- Name: easy_thumbnails_source id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY easy_thumbnails_source ALTER COLUMN id SET DEFAULT nextval('easy_thumbnails_source_id_seq'::regclass);


--
-- Name: easy_thumbnails_thumbnail id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY easy_thumbnails_thumbnail ALTER COLUMN id SET DEFAULT nextval('easy_thumbnails_thumbnail_id_seq'::regclass);


--
-- Name: easy_thumbnails_thumbnaildimensions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY easy_thumbnails_thumbnaildimensions ALTER COLUMN id SET DEFAULT nextval('easy_thumbnails_thumbnaildimensions_id_seq'::regclass);


--
-- Name: filer_clipboard id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_clipboard ALTER COLUMN id SET DEFAULT nextval('filer_clipboard_id_seq'::regclass);


--
-- Name: filer_clipboarditem id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_clipboarditem ALTER COLUMN id SET DEFAULT nextval('filer_clipboarditem_id_seq'::regclass);


--
-- Name: filer_file id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_file ALTER COLUMN id SET DEFAULT nextval('filer_file_id_seq'::regclass);


--
-- Name: filer_folder id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_folder ALTER COLUMN id SET DEFAULT nextval('filer_folder_id_seq'::regclass);


--
-- Name: filer_folderpermission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_folderpermission ALTER COLUMN id SET DEFAULT nextval('filer_folderpermission_id_seq'::regclass);


--
-- Name: filer_thumbnailoption id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_thumbnailoption ALTER COLUMN id SET DEFAULT nextval('filer_thumbnailoption_id_seq'::regclass);


--
-- Name: menus_cachekey id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY menus_cachekey ALTER COLUMN id SET DEFAULT nextval('menus_cachekey_id_seq'::regclass);


--
-- Name: polls_choice id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY polls_choice ALTER COLUMN id SET DEFAULT nextval('polls_choice_id_seq'::regclass);


--
-- Name: polls_poll id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY polls_poll ALTER COLUMN id SET DEFAULT nextval('polls_poll_id_seq'::regclass);


--
-- Data for Name: aldryn_bootstrap3_boostrap3alertplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_boostrap3alertplugin (cmsplugin_ptr_id, context, icon, classes, attributes) FROM stdin;
\.


--
-- Data for Name: aldryn_bootstrap3_boostrap3blockquoteplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_boostrap3blockquoteplugin (cmsplugin_ptr_id, reverse, classes, attributes) FROM stdin;
\.


--
-- Data for Name: aldryn_bootstrap3_boostrap3buttonplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_boostrap3buttonplugin (link_url, link_anchor, link_mailto, link_phone, link_target, cmsplugin_ptr_id, label, type, btn_context, btn_size, btn_block, txt_context, icon_left, icon_right, classes, link_file_id, link_page_id, link_attributes) FROM stdin;
\.


--
-- Data for Name: aldryn_bootstrap3_boostrap3citeplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_boostrap3citeplugin (classes, attributes, cmsplugin_ptr_id) FROM stdin;
\.


--
-- Data for Name: aldryn_bootstrap3_boostrap3iconplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_boostrap3iconplugin (cmsplugin_ptr_id, icon, classes, attributes) FROM stdin;
\.


--
-- Data for Name: aldryn_bootstrap3_boostrap3imageplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_boostrap3imageplugin (cmsplugin_ptr_id, alt, title, aspect_ratio, thumbnail, shape, classes, img_responsive, file_id, override_height, override_width, use_original_image, attributes) FROM stdin;
16				f			t	20	\N	\N	t	{}
176				f			t	21	\N	\N	t	{}
1014	Planos			f			t	30	\N	\N	t	{}
709				f			t	23	\N	\N	t	{}
1058				f			t	31	\N	\N	t	{}
1059				f			t	32	\N	\N	t	{}
754				f			t	24	\N	\N	t	{}
1067	Planos			f			t	30	\N	\N	t	{}
762				f			t	25	\N	\N	t	{}
818				f			t	25	\N	\N	t	{}
1112				f			t	24	\N	\N	t	{}
1116				f			t	23	\N	\N	t	{}
1117				f			t	25	\N	\N	t	{}
1118				f			t	24	\N	\N	t	{}
1124				f			t	27	\N	\N	f	{}
1157				f			t	20	\N	\N	t	{}
1161				f			t	21	\N	\N	t	{}
1164				f			t	23	\N	\N	t	{}
1167				f			t	24	\N	\N	t	{}
1170				f			t	25	\N	\N	t	{}
802				f			t	23	\N	\N	t	{}
806				f			t	24	\N	\N	t	{}
1009				f			t	27	\N	\N	f	{}
\.


--
-- Data for Name: aldryn_bootstrap3_boostrap3jumbotronplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_boostrap3jumbotronplugin (label, grid, classes, attributes, cmsplugin_ptr_id) FROM stdin;
	f		{}	1008
Jumb_Planos	f		{}	1013
Jumb_Planos	f		{}	1066
	f		{}	1123
\.


--
-- Data for Name: aldryn_bootstrap3_boostrap3labelplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_boostrap3labelplugin (cmsplugin_ptr_id, label, context, classes, attributes) FROM stdin;
\.


--
-- Data for Name: aldryn_bootstrap3_boostrap3panelbodyplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_boostrap3panelbodyplugin (cmsplugin_ptr_id, classes, attributes) FROM stdin;
\.


--
-- Data for Name: aldryn_bootstrap3_boostrap3panelfooterplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_boostrap3panelfooterplugin (cmsplugin_ptr_id, classes, attributes) FROM stdin;
\.


--
-- Data for Name: aldryn_bootstrap3_boostrap3panelheadingplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_boostrap3panelheadingplugin (cmsplugin_ptr_id, title, classes, attributes) FROM stdin;
\.


--
-- Data for Name: aldryn_bootstrap3_boostrap3panelplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_boostrap3panelplugin (cmsplugin_ptr_id, context, classes, attributes) FROM stdin;
\.


--
-- Data for Name: aldryn_bootstrap3_boostrap3spacerplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_boostrap3spacerplugin (cmsplugin_ptr_id, size, classes, attributes) FROM stdin;
\.


--
-- Data for Name: aldryn_bootstrap3_boostrap3wellplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_boostrap3wellplugin (cmsplugin_ptr_id, size, classes, attributes) FROM stdin;
\.


--
-- Data for Name: aldryn_bootstrap3_bootstrap3accordionitemplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_bootstrap3accordionitemplugin (cmsplugin_ptr_id, title, context, classes, attributes) FROM stdin;
43	Análise de Projeto	default		{}
1191	Análise do Manual de Uso, Operação e Manutenção.	default		{}
1194	Análise de Projeto	default		{}
1197	Selecione um dos produtos abaixo	default		{}
42	Análise do Manual de Uso, Operação e Manutenção.	default		{}
1069	Selecione um dos produtos abaixo	default		{}
\.


--
-- Data for Name: aldryn_bootstrap3_bootstrap3accordionplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_bootstrap3accordionplugin (cmsplugin_ptr_id, index, classes, attributes) FROM stdin;
41	1		{}
1190	1		{}
\.


--
-- Data for Name: aldryn_bootstrap3_bootstrap3carouselplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_bootstrap3carouselplugin (cmsplugin_ptr_id, style, aspect_ratio, transition_effect, ride, "interval", wrap, pause, classes, attributes) FROM stdin;
\.


--
-- Data for Name: aldryn_bootstrap3_bootstrap3carouselslidefolderplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_bootstrap3carouselslidefolderplugin (cmsplugin_ptr_id, classes, folder_id) FROM stdin;
\.


--
-- Data for Name: aldryn_bootstrap3_bootstrap3carouselslideplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_bootstrap3carouselslideplugin (cmsplugin_ptr_id, link_url, link_anchor, link_mailto, link_phone, link_target, link_text, content, classes, image_id, link_file_id, link_page_id, link_attributes) FROM stdin;
\.


--
-- Data for Name: aldryn_bootstrap3_bootstrap3codeplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_bootstrap3codeplugin (code_type, code, classes, attributes, cmsplugin_ptr_id) FROM stdin;
\.


--
-- Data for Name: aldryn_bootstrap3_bootstrap3columnplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_bootstrap3columnplugin (cmsplugin_ptr_id, classes, tag, xs_col, xs_offset, xs_push, xs_pull, sm_col, sm_offset, sm_push, sm_pull, md_col, md_offset, md_push, md_pull, lg_col, lg_offset, lg_push, lg_pull, attributes) FROM stdin;
798		div	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	{}
1114		div	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	{}
\.


--
-- Data for Name: aldryn_bootstrap3_bootstrap3fileplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_bootstrap3fileplugin (cmsplugin_ptr_id, name, open_new_window, show_file_size, icon_left, icon_right, classes, file_id, attributes) FROM stdin;
\.


--
-- Data for Name: aldryn_bootstrap3_bootstrap3listgroupitemplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_bootstrap3listgroupitemplugin (cmsplugin_ptr_id, title, context, state, classes, attributes) FROM stdin;
\.


--
-- Data for Name: aldryn_bootstrap3_bootstrap3listgroupplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_bootstrap3listgroupplugin (cmsplugin_ptr_id, classes, add_list_group_class, attributes) FROM stdin;
\.


--
-- Data for Name: aldryn_bootstrap3_bootstrap3responsiveplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_bootstrap3responsiveplugin (device_breakpoints, print_breakpoints, classes, attributes, cmsplugin_ptr_id) FROM stdin;
\.


--
-- Data for Name: aldryn_bootstrap3_bootstrap3rowplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_bootstrap3rowplugin (cmsplugin_ptr_id, classes, attributes) FROM stdin;
797		{}
1113		{}
\.


--
-- Data for Name: aldryn_bootstrap3_bootstrap3tabitemplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_bootstrap3tabitemplugin (title, icon, classes, attributes, cmsplugin_ptr_id) FROM stdin;
Minha Equipe			{}	395
Meus Produtos			{}	35
Meu Plano			{}	36
Administração de Produtos			{}	37
Meus Produtos			{}	1189
Meu Plano			{}	1198
Administração de Produtos			{}	1200
Minha Equipe			{}	1202
\.


--
-- Data for Name: aldryn_bootstrap3_bootstrap3tabplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY aldryn_bootstrap3_bootstrap3tabplugin (index, style, effect, classes, attributes, cmsplugin_ptr_id) FROM stdin;
1	nav-tabs	fade		{}	34
1	nav-tabs	fade		{}	1188
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: bitnami
--

COPY auth_group (id, name) FROM stdin;
1	Editor de Produtos
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: bitnami
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
1	1	258
2	1	259
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: bitnami
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add permission	1	add_permission
2	Can change permission	1	change_permission
3	Can delete permission	1	delete_permission
4	Can add group	2	add_group
5	Can change group	2	change_group
6	Can delete group	2	delete_group
7	Can add user	3	add_user
8	Can change user	3	change_user
9	Can delete user	3	delete_user
10	Can add content type	4	add_contenttype
11	Can change content type	4	change_contenttype
12	Can delete content type	4	delete_contenttype
13	Can add log entry	5	add_logentry
14	Can change log entry	5	change_logentry
15	Can delete log entry	5	delete_logentry
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
19	Can use Structure mode	7	use_structure
20	Can change page	8	change_page
21	Can add site	9	add_site
22	Can change site	9	change_site
23	Can delete site	9	delete_site
24	Can add cms plugin	10	add_cmsplugin
25	Can change cms plugin	10	change_cmsplugin
26	Can delete cms plugin	10	delete_cmsplugin
27	Can add alias plugin model	11	add_aliaspluginmodel
28	Can change alias plugin model	11	change_aliaspluginmodel
29	Can delete alias plugin model	11	delete_aliaspluginmodel
30	Can add Page global permission	12	add_globalpagepermission
31	Can change Page global permission	12	change_globalpagepermission
32	Can delete Page global permission	12	delete_globalpagepermission
33	Can add page	8	add_page
34	Can delete page	8	delete_page
35	Can view page	8	view_page
36	Can publish page	8	publish_page
37	Can edit static placeholders	8	edit_static_placeholder
38	Can add Page permission	13	add_pagepermission
39	Can change Page permission	13	change_pagepermission
40	Can delete Page permission	13	delete_pagepermission
41	Can add User (page)	14	add_pageuser
42	Can change User (page)	14	change_pageuser
43	Can delete User (page)	14	delete_pageuser
44	Can add User group (page)	15	add_pageusergroup
45	Can change User group (page)	15	change_pageusergroup
46	Can delete User group (page)	15	delete_pageusergroup
47	Can add placeholder	7	add_placeholder
48	Can change placeholder	7	change_placeholder
49	Can delete placeholder	7	delete_placeholder
50	Can add placeholder reference	16	add_placeholderreference
51	Can change placeholder reference	16	change_placeholderreference
52	Can delete placeholder reference	16	delete_placeholderreference
53	Can add static placeholder	17	add_staticplaceholder
54	Can change static placeholder	17	change_staticplaceholder
55	Can delete static placeholder	17	delete_staticplaceholder
56	Can add title	18	add_title
57	Can change title	18	change_title
58	Can delete title	18	delete_title
59	Can add user setting	19	add_usersettings
60	Can change user setting	19	change_usersettings
61	Can delete user setting	19	delete_usersettings
62	Can add urlconf revision	20	add_urlconfrevision
63	Can change urlconf revision	20	change_urlconfrevision
64	Can delete urlconf revision	20	delete_urlconfrevision
65	Can add cache key	23	add_cachekey
66	Can change cache key	23	change_cachekey
67	Can delete cache key	23	delete_cachekey
68	Can add text	24	add_text
69	Can change text	24	change_text
70	Can delete text	24	delete_text
71	Can add clipboard	25	add_clipboard
72	Can change clipboard	25	change_clipboard
73	Can delete clipboard	25	delete_clipboard
74	Can add clipboard item	26	add_clipboarditem
75	Can change clipboard item	26	change_clipboarditem
76	Can delete clipboard item	26	delete_clipboarditem
77	Can add file	27	add_file
78	Can change file	27	change_file
79	Can delete file	27	delete_file
80	Can add Folder	28	add_folder
81	Can change Folder	28	change_folder
82	Can delete Folder	28	delete_folder
83	Can use directory listing	28	can_use_directory_listing
84	Can add folder permission	29	add_folderpermission
85	Can change folder permission	29	change_folderpermission
86	Can delete folder permission	29	delete_folderpermission
87	Can add image	30	add_image
88	Can change image	30	change_image
89	Can delete image	30	delete_image
90	Can add thumbnail option	31	add_thumbnailoption
91	Can change thumbnail option	31	change_thumbnailoption
92	Can delete thumbnail option	31	delete_thumbnailoption
93	Can add source	32	add_source
94	Can change source	32	change_source
95	Can delete source	32	delete_source
96	Can add thumbnail	33	add_thumbnail
97	Can change thumbnail	33	change_thumbnail
98	Can delete thumbnail	33	delete_thumbnail
99	Can add thumbnail dimensions	34	add_thumbnaildimensions
100	Can change thumbnail dimensions	34	change_thumbnaildimensions
101	Can delete thumbnail dimensions	34	delete_thumbnaildimensions
102	Can add column	35	add_column
103	Can change column	35	change_column
104	Can delete column	35	delete_column
105	Can add multi columns	36	add_multicolumns
106	Can change multi columns	36	change_multicolumns
107	Can delete multi columns	36	delete_multicolumns
108	Can add file	37	add_file
109	Can change file	37	change_file
110	Can delete file	37	delete_file
111	Can add folder	38	add_folder
112	Can change folder	38	change_folder
113	Can delete folder	38	delete_folder
114	Can add link	39	add_link
115	Can change link	39	change_link
116	Can delete link	39	delete_link
117	Can add picture	40	add_picture
118	Can change picture	40	change_picture
119	Can delete picture	40	delete_picture
120	Can add style	41	add_style
121	Can change style	41	change_style
122	Can delete style	41	delete_style
123	Can add Snippet	42	add_snippet
124	Can change Snippet	42	change_snippet
125	Can delete Snippet	42	delete_snippet
126	Can add Snippet	43	add_snippetptr
127	Can change Snippet	43	change_snippetptr
128	Can delete Snippet	43	delete_snippetptr
129	Can add google map	44	add_googlemap
130	Can change google map	44	change_googlemap
131	Can delete google map	44	delete_googlemap
132	Can add google map marker	45	add_googlemapmarker
133	Can change google map marker	45	change_googlemapmarker
134	Can delete google map marker	45	delete_googlemapmarker
135	Can add google map route	46	add_googlemaproute
136	Can change google map route	46	change_googlemaproute
137	Can delete google map route	46	delete_googlemaproute
138	Can add video player	47	add_videoplayer
139	Can change video player	47	change_videoplayer
140	Can delete video player	47	delete_videoplayer
141	Can add video source	48	add_videosource
142	Can change video source	48	change_videosource
143	Can delete video source	48	delete_videosource
144	Can add video track	49	add_videotrack
145	Can change video track	49	change_videotrack
146	Can delete video track	49	delete_videotrack
147	Can add boostrap3 alert plugin	50	add_boostrap3alertplugin
148	Can change boostrap3 alert plugin	50	change_boostrap3alertplugin
149	Can delete boostrap3 alert plugin	50	delete_boostrap3alertplugin
150	Can add boostrap3 blockquote plugin	51	add_boostrap3blockquoteplugin
151	Can change boostrap3 blockquote plugin	51	change_boostrap3blockquoteplugin
152	Can delete boostrap3 blockquote plugin	51	delete_boostrap3blockquoteplugin
153	Can add boostrap3 button plugin	52	add_boostrap3buttonplugin
154	Can change boostrap3 button plugin	52	change_boostrap3buttonplugin
155	Can delete boostrap3 button plugin	52	delete_boostrap3buttonplugin
156	Can add boostrap3 icon plugin	53	add_boostrap3iconplugin
157	Can change boostrap3 icon plugin	53	change_boostrap3iconplugin
158	Can delete boostrap3 icon plugin	53	delete_boostrap3iconplugin
159	Can add boostrap3 image plugin	54	add_boostrap3imageplugin
160	Can change boostrap3 image plugin	54	change_boostrap3imageplugin
161	Can delete boostrap3 image plugin	54	delete_boostrap3imageplugin
162	Can add boostrap3 label plugin	55	add_boostrap3labelplugin
163	Can change boostrap3 label plugin	55	change_boostrap3labelplugin
164	Can delete boostrap3 label plugin	55	delete_boostrap3labelplugin
165	Can add boostrap3 panel body plugin	56	add_boostrap3panelbodyplugin
166	Can change boostrap3 panel body plugin	56	change_boostrap3panelbodyplugin
167	Can delete boostrap3 panel body plugin	56	delete_boostrap3panelbodyplugin
168	Can add boostrap3 panel footer plugin	57	add_boostrap3panelfooterplugin
169	Can change boostrap3 panel footer plugin	57	change_boostrap3panelfooterplugin
170	Can delete boostrap3 panel footer plugin	57	delete_boostrap3panelfooterplugin
171	Can add boostrap3 panel heading plugin	58	add_boostrap3panelheadingplugin
172	Can change boostrap3 panel heading plugin	58	change_boostrap3panelheadingplugin
173	Can delete boostrap3 panel heading plugin	58	delete_boostrap3panelheadingplugin
174	Can add boostrap3 panel plugin	59	add_boostrap3panelplugin
175	Can change boostrap3 panel plugin	59	change_boostrap3panelplugin
176	Can delete boostrap3 panel plugin	59	delete_boostrap3panelplugin
177	Can add boostrap3 spacer plugin	60	add_boostrap3spacerplugin
178	Can change boostrap3 spacer plugin	60	change_boostrap3spacerplugin
179	Can delete boostrap3 spacer plugin	60	delete_boostrap3spacerplugin
180	Can add boostrap3 well plugin	61	add_boostrap3wellplugin
181	Can change boostrap3 well plugin	61	change_boostrap3wellplugin
182	Can delete boostrap3 well plugin	61	delete_boostrap3wellplugin
183	Can add bootstrap3 accordion item plugin	62	add_bootstrap3accordionitemplugin
184	Can change bootstrap3 accordion item plugin	62	change_bootstrap3accordionitemplugin
185	Can delete bootstrap3 accordion item plugin	62	delete_bootstrap3accordionitemplugin
186	Can add bootstrap3 accordion plugin	63	add_bootstrap3accordionplugin
187	Can change bootstrap3 accordion plugin	63	change_bootstrap3accordionplugin
188	Can delete bootstrap3 accordion plugin	63	delete_bootstrap3accordionplugin
189	Can add bootstrap3 carousel plugin	64	add_bootstrap3carouselplugin
190	Can change bootstrap3 carousel plugin	64	change_bootstrap3carouselplugin
191	Can delete bootstrap3 carousel plugin	64	delete_bootstrap3carouselplugin
192	Can add bootstrap3 carousel slide folder plugin	65	add_bootstrap3carouselslidefolderplugin
193	Can change bootstrap3 carousel slide folder plugin	65	change_bootstrap3carouselslidefolderplugin
194	Can delete bootstrap3 carousel slide folder plugin	65	delete_bootstrap3carouselslidefolderplugin
195	Can add bootstrap3 carousel slide plugin	66	add_bootstrap3carouselslideplugin
196	Can change bootstrap3 carousel slide plugin	66	change_bootstrap3carouselslideplugin
197	Can delete bootstrap3 carousel slide plugin	66	delete_bootstrap3carouselslideplugin
198	Can add bootstrap3 column plugin	67	add_bootstrap3columnplugin
199	Can change bootstrap3 column plugin	67	change_bootstrap3columnplugin
200	Can delete bootstrap3 column plugin	67	delete_bootstrap3columnplugin
201	Can add bootstrap3 list group item plugin	68	add_bootstrap3listgroupitemplugin
202	Can change bootstrap3 list group item plugin	68	change_bootstrap3listgroupitemplugin
203	Can delete bootstrap3 list group item plugin	68	delete_bootstrap3listgroupitemplugin
204	Can add bootstrap3 list group plugin	69	add_bootstrap3listgroupplugin
205	Can change bootstrap3 list group plugin	69	change_bootstrap3listgroupplugin
206	Can delete bootstrap3 list group plugin	69	delete_bootstrap3listgroupplugin
207	Can add bootstrap3 row plugin	70	add_bootstrap3rowplugin
208	Can change bootstrap3 row plugin	70	change_bootstrap3rowplugin
209	Can delete bootstrap3 row plugin	70	delete_bootstrap3rowplugin
210	Can add bootstrap3 file plugin	71	add_bootstrap3fileplugin
211	Can change bootstrap3 file plugin	71	change_bootstrap3fileplugin
212	Can delete bootstrap3 file plugin	71	delete_bootstrap3fileplugin
213	Can add boostrap3 cite plugin	72	add_boostrap3citeplugin
214	Can change boostrap3 cite plugin	72	change_boostrap3citeplugin
215	Can delete boostrap3 cite plugin	72	delete_boostrap3citeplugin
216	Can add bootstrap3 code plugin	73	add_bootstrap3codeplugin
217	Can change bootstrap3 code plugin	73	change_bootstrap3codeplugin
218	Can delete bootstrap3 code plugin	73	delete_bootstrap3codeplugin
219	Can add bootstrap3 responsive plugin	74	add_bootstrap3responsiveplugin
220	Can change bootstrap3 responsive plugin	74	change_bootstrap3responsiveplugin
221	Can delete bootstrap3 responsive plugin	74	delete_bootstrap3responsiveplugin
222	Can add bootstrap3 tab item plugin	75	add_bootstrap3tabitemplugin
223	Can change bootstrap3 tab item plugin	75	change_bootstrap3tabitemplugin
224	Can delete bootstrap3 tab item plugin	75	delete_bootstrap3tabitemplugin
225	Can add bootstrap3 tab plugin	76	add_bootstrap3tabplugin
226	Can change bootstrap3 tab plugin	76	change_bootstrap3tabplugin
227	Can delete bootstrap3 tab plugin	76	delete_bootstrap3tabplugin
228	Can add boostrap3 jumbotron plugin	77	add_boostrap3jumbotronplugin
229	Can change boostrap3 jumbotron plugin	77	change_boostrap3jumbotronplugin
230	Can delete boostrap3 jumbotron plugin	77	delete_boostrap3jumbotronplugin
231	Can add checklist	78	add_checklist
232	Can change checklist	78	change_checklist
233	Can delete checklist	78	delete_checklist
234	Can add item checklist	79	add_itemchecklist
235	Can change item checklist	79	change_itemchecklist
236	Can delete item checklist	79	delete_itemchecklist
237	Can add projeto	80	add_projeto
238	Can change projeto	80	change_projeto
239	Can delete projeto	80	delete_projeto
240	Can add Ramo	81	add_ramo
241	Can change Ramo	81	change_ramo
242	Can delete Ramo	81	delete_ramo
243	Can add empreendimento	82	add_empreendimento
244	Can change empreendimento	82	change_empreendimento
245	Can delete empreendimento	82	delete_empreendimento
246	Can add Etapa do Projeto	83	add_etapaprojeto
247	Can change Etapa do Projeto	83	change_etapaprojeto
248	Can delete Etapa do Projeto	83	delete_etapaprojeto
249	Can add Análise	84	add_avaliacao
250	Can change Análise	84	change_avaliacao
251	Can delete Análise	84	delete_avaliacao
252	Can add item avaliacao	85	add_itemavaliacao
253	Can change item avaliacao	85	change_itemavaliacao
254	Can delete item avaliacao	85	delete_itemavaliacao
255	Can add plano	86	add_plano
256	Can change plano	86	change_plano
257	Can delete plano	86	delete_plano
258	Can add produto	87	add_produto
259	Can change produto	87	change_produto
260	Can delete produto	87	delete_produto
261	Can add checklists plugin model	88	add_checklistspluginmodel
262	Can change checklists plugin model	88	change_checklistspluginmodel
263	Can delete checklists plugin model	88	delete_checklistspluginmodel
264	Can add avaliacao plugin model	89	add_avaliacaopluginmodel
265	Can change avaliacao plugin model	89	change_avaliacaopluginmodel
266	Can delete avaliacao plugin model	89	delete_avaliacaopluginmodel
267	Can add choice	90	add_choice
268	Can change choice	90	change_choice
269	Can delete choice	90	delete_choice
270	Can add poll	91	add_poll
271	Can change poll	91	change_poll
272	Can delete poll	91	delete_poll
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: bitnami
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
2	pbkdf2_sha256$36000$ZS4ntx26Lbls$SKoYoJXkkCdwNAQYfiVRv6d9kp2XNHcFIaC63RazetY=	2018-04-14 22:35:05.542648+00	f	Fausto				f	t	2018-03-20 11:08:07+00
1	pbkdf2_sha256$36000$pNZQ3LTAyXmX$19Hzu//7cU4mOZVwWeSH8p+n4rE623Yr3+GQkpgXKBs=	2018-04-19 10:21:29.816962+00	t	admin			admin@gmail.com	t	t	2018-03-18 02:12:54.915376+00
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: bitnami
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
1	2	1
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: bitnami
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
1	2	69
2	2	37
3	2	11
4	2	75
5	2	268
6	2	271
7	2	57
8	2	127
\.


--
-- Data for Name: checklist_avaliacao; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY checklist_avaliacao (id, created, modified, descricao, prancha, status, avaliador_id, empreendimento_id, etapa_projeto_id, produto_id, projeto_id) FROM stdin;
2	2018-03-26 09:27:25.955884+00	2018-03-26 09:27:47.085006+00		Prancha 330202	1	1	2	1	1	1
3	2018-03-26 10:25:00.473394+00	2018-03-26 10:26:41.676287+00			1	2	3	1	1	3
1	2018-03-26 00:50:55.415337+00	2018-04-02 11:41:30.137463+00		7097890	2	1	1	1	1	1
4	2018-04-02 11:41:59.362255+00	2018-04-03 01:01:44.787084+00		445556666	2	1	1	1	1	3
5	2018-04-03 01:05:35.858821+00	2018-04-03 01:06:17.914282+00		wepoqweprawe	2	2	4	1	1	1
6	2018-04-07 15:04:53.078241+00	\N		trwerwer	0	1	2	1	2	3
7	2018-04-07 15:12:10.956348+00	2018-04-07 15:12:22.84267+00		asdfad	1	2	3	2	2	1
8	2018-04-07 17:48:01.807114+00	2018-04-07 17:57:46.370365+00		Desc prancha	2	2	6	2	2	1
\.


--
-- Data for Name: checklist_checklist; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY checklist_checklist (id, requisito, requisito_geral, requisito_especifico, normativo, norma, produto_id, dt_fim_publicacao, dt_inicio_publicacao, ic_ativo) FROM stdin;
1	Meio  destinado  a  impedir  contato  com  partes  vivas  perigosas  em  condições   normais	3.2 Proteção contra choques elétricos	3.2.1     elemento  condutivo  ou  parte  condutiva	NBR 15575-1	Meio  destinado  a  impedir  contato  com  partes  vivas perigosas  em  condições  normais.	1	2018-04-02	2018-04-02	t
2	Proteção contra choques elétricos	3.2 Proteção contra choques elétricos	3.2.1     Elemento  condutivo  ou  parte  condutiva	NBR 15575-1	Norma estabelece as condições a que devem satisfazer as instalações elétrica s de baixa tensão, a  fim  de  garantir  a  segurança  de  pessoas  e  animais,  o  funcionamento  adequado  da  instalação  e a  conservação dos bens	1	2018-04-02	2018-04-02	t
3	Há informações de que as bacias sanitárias são de volume de descarga de acordo com as especificações da ABNT NBR 15097-1?	Impacto ambiental	Consumo de água em bacias sanitárias	NBR 15575-6	18.1\t18.1.1\tRef.:\tNBR 15097-1	2	2018-04-05	2018-04-05	t
4	Foi informada a vida útil de projeto (VUP) do sistema e/ou dos elementos/componentes deste projeto?	Incumbência dos intervenientes	Incumbências do projetista	NBR 15575-1 5.53		2	2018-04-07	2018-04-07	t
5	Foram especificados materiais, produtos e processos que atendam ao desempenho estabelecido no PDE, com base nas normas prescritivas e desempenhos declarados pelos fabricantes dos produtos?	Incumbência dos intervenientes	Incumbências do projetista	NBR 15575-1 5.53		2	2018-04-07	2018-04-07	t
6	Foram especificados os métodos de controle necessários para comprovar o atendimento ao desempenho projetado (ex.: definição de ensaios, análises, verificações a fazer e suas respectivas frequências, a	Incumbência dos intervenientes	Incumbências do projetista	NBR 15575-1 5.53		2	2018-04-07	2018-04-07	t
7	Há informações sobre prazos de garantia dos componentes, elementos e/ou sistemas projetados?	Incumbência dos intervenientes	Incumbências do projetista	NBR 15575-1 5.53		2	2018-04-07	2018-04-07	t
10	- Há informação ou menção de que os projetos contemplem soluções para minimização do consumo de energia, como iluminação e ventilação natural e sistemas de aquecimento baseados em energia alternativa?	Impacto ambiental	Consumo de energia	NBR 15575-1 Item 18 Subitem 18.5		2	2018-04-07	2018-04-07	t
11	Para tubulações fixas no teto ou em outros elementos estruturais, há informações de que os fixadores ou suportes das tubulações, aparentes ou não, assim como as próprias tubulações, resistem, sem entr	Seg. estrut.	Tubulações suspensas	NBR 15575-6 Item 7 Subitem 7.1.1		2	2018-04-07	2018-04-07	t
12	Para as tubulações submetidas a esforços dinâmicos significativos, por exemplo, tubulações de recalque ou água quente, estes esforços foram levados em consideração?	Seg. estrut.	Tubulações suspensas	NBR 15575-6 Item 7 Subitem 7.11		2	2018-04-07	2018-04-07	t
13	Há informações sobre berços e envelopamentos, ou berços ou envelopamentos consubstanciados em memórias de cálculo constantes no projeto ou em literaturas especializadas, que garantem a preservação da	Seg. estrut.	Tubulações enterradas	NBR 15575-6 Item 7 Subitem 7.1.2		2	2018-04-07	2018-04-07	t
14	O projeto hidrossanitário especifica nos pontos de transição (parede x piso, parede x pilar etc.) dispositivos que assegurem a não transmissão dos esforços para a tubulação?	Seg. estrut.	Tubulações embutidas	NBR 15575-6 Item 7 Subitem 7.1.3		2	2018-04-07	2018-04-07	t
17	"Há informações sobre a velocidade do fluido no interior das tubulações quando da parada de bombas de recalques? - A velocidade deve ser inferior a 10 m/s. - O projeto pode estabelecer velocidades aci	Seg. estrut.	Sobrepressão máxima quando da parada de bombas de recalque	NBR 15575-6 Item 7 Subitem 7.2.3		2	2018-04-07	2018-04-07	t
21	Há informações de que as prumadas de esgoto sanitário e ventilação instaladas aparentes, em alvenaria ou no interior de dutos verticais (shafts), tenham sido fabricadas com material não propagante de	Seg. contra fogo	Evitar propagação de chamas entre pavimentos	NBR 15575-6 Item 8 Subitem 8.3.1	Referência à ISO 1182	2	2018-04-07	2018-04-07	t
20	Há informações sobre a classificação e o posicionamento do extintores?	Seg. contra fogo	Tipo e posicionamento dos extintores	NBR 15575-6 Item 8 Subitem 8.2.1	Referência à NBR 12693	2	2018-04-07	2018-04-07	t
19	Há informações sobre o volume de água reservado para combate a incêndio conforme legislação vigente ou, na sua ausência, conforme norma aplicável da ABNT?	Seg. contra fogo	Reserva de água para combate a incêndio	NBR 15575-6 Item 8 Subitem 8.1.1	Referência à legislação vigente ou, na sua ausência, NBR 10897, 13714	2	2018-04-07	2018-04-07	t
18	Há informações de que as tubulações aparentes fixadas até 1,5 m acima do piso resistem aos impactos que possa ocorrer durante a vida útil de projeto, sem sofrem perda de funcionalidade ou ruína?	Seg. estrut.	Resistência a impactos de tubulações aparentes	NBR 15575-6 Item 7 Subitem 7.2.4	Referência à outro documento Tabela 1	2	2018-04-07	2018-04-07	t
16	Há informações de que o sistema hidrossanitário atende à pressão estática máxima estabelecida na ABNT NB 5626?	Seg. estrut.	Pressão estática máxima	NBR 15575-6 Item 7 Subitem 7.2.2	Referência à NBR 5626	2	2018-04-07	2018-04-07	t
15	Há informações de que as válvulas de descarga, metais de fechamento rápido e do tipo monocomando não irão provocar sobrepressões no fechamento superiores a 0,2 Mpa?	Seg. estrut.	Sobrepressão máxima no fechamento de válvulas de descarga	NBR 15575-6 Item 7 Subitem 7.2.1	Referência à NBR 15857	2	2018-04-07	2018-04-07	t
9	- Há informação ou menção de que o Projeto hidrossanitário contemple soluções para uso racional e reuso de água?	Impacto ambiental	Utilização e reuso de água	NBR 15575-1 Item 18 Subitem 18.4	Referência à Tabela 8 - parâmetros de qualidade de água	2	2018-04-07	2018-04-07	t
8	Há informações de atendimento às normas de instalações?	Seg. no uso e oper.	Segurança na utilização das instalações (elétrica, SPDA, gás etc.)	NBR 15575-1 Item 9 - Subitem 9.3.1	Referência à NBR 5410, 5419, 13523, 15526, 15575-6 etc.	2	2018-04-07	2018-04-07	t
23	Há informações de que os equipamentos do sistema hidrossanitário atendem às especificações, quanto à corrente de fuga,  da ABNT NBR 12090 e ABNT NBR 14016, limitando-se à corrente de fuga para outros	Seg. no uso e oper.	Corrente de fuga em equipamentos	NBR 15575-6 Item 9 Subitem 9.1.2	Referência à NBR 12090 e NBR 14016. Corrente limitada a 15 mA para outros aparelhos.	2	2018-04-07	2018-04-07	t
22	Há informações de que todas as tubulações, equipamentos e acessórios do sistema hidrossanitário tenham sido direta ou indiretamente aterrados conforme ABNT 5410?	Seg. no uso e oper.	Aterramento das instalações, dos aparelhos aquecedores, dos eletrodomésticos e dos eletroeletrônicos	NBR 15575-6 Item 9 Subitem 9.1.1	Referência à NBR 5410	2	2018-04-07	2018-04-07	t
25	Há informações de que o funcionamento do equipamento a gás combustível, instalado em ambientes residenciais tenha sido feito de maneira que a concentração máxima de CO2 não ultrapasse o valor de 0,5 %	Seg. no uso e oper.	Instalação de equipamentos a gás combustível	NBR 15575-6 Item 9 Subitem 9.2.2	Referência à 23 normas listadas em 9.3.1.1 + inspeção visual.	2	2018-04-07	2018-04-07	t
24	Há informações de que os aparelhos elétricos de acumulação para o aquecimento de água sejam providos de dispositivo de alívio para o caso de sobrepressão e também de dispositivo de segurança que corte	Seg. no uso e oper.	Dispositivos de segurança em aquecedores de acumulação a gás	NBR 15575-6 Item 9 Subitem 9.2.1	Referência à NBR 13103, 14011 e legislação vigente	2	2018-04-07	2018-04-07	t
26	Há informações de que as peças de utilização e demais componentes dos sistemas hidrossanitários que são manipulados pelos usuários não possuem contos vivos ou superfícies ásperas conforme normas aplic	Seg. no uso e oper.	Prevenção de ferimentos	NBR 15575-6 Item 9 Subitem 9.3.1	Referência à 23 normas listadas em 9.3.1.1 + inspeção visual.	2	2018-04-07	2018-04-07	t
27	Há informações de que as peças e aparelhos sanitários possuem resistência mecânica aos esforços a que serão submetidos na sua utilização apresentando atendimento às normas aplicáveis?	Seg. no uso e oper.	Resistência mecânica de peças e aparelhos sanitários	NBR 15575-6 Item 9 Subitem 9.3.2	Referência à 22 normas listadas em 9.3.2.1.	2	2018-04-07	2018-04-07	t
28	Há informações de que as possibilidades de mistura de água fria, regulagem de vazão e outras técnicas existentes no sistema hidrossanitário, no limite de sua aplicação, permitem que a regulagem de tem	Seg. no uso e oper.	Temperatura de aquecimento	NBR 15575-6 Item 9 Subitem 9.4.1	Referência à NBR 12090, NBR 14011 e NBR 14016	2	2018-04-07	2018-04-07	t
29	"Há informações de que as tubulações do sistema predial de água não apresentem vazamento quando submetidas, durante 1 h, à pressão hidrostática de 1,5 vez o valor da pressão prevista em projeto? - Em	Estanqueidade	Estanqueidade à água do sistema de água	NBR 15575-6 Item 10 Subitem 10.1.1	Referência à NBR 5626, 7198 e 8160	2	2018-04-07	2018-04-07	t
30	Há informações sobre a estanqueidade das peças de utilização e dos metais sanitários conforme normas aplicáveis?	Estanqueidade	Estanqueidade à água de peças de utilização	NBR 15575-6 Item 10 Subitem 10.1.2	Referência a 18 normas listadas em 10.1.2.	2	2018-04-07	2018-04-07	t
31	Há informações sobre a estanqueidade dos reservatórios conforme normas aplicáveis?	Estanqueidade	Estanqueidade à água de peças de utilização	NBR 15575-6 Item 10 Subitem 10.1.2	Referência a 18 normas listadas em 10.1.2.	2	2018-04-07	2018-04-07	t
32	Há informações de que as tubulações dos sistemas prediais de esgoto sanitário e de águas pluviais não apresentam vazamento quando submetidas, durante 15 min, à pressão estática de 60 kPa, em caso de á	Estanqueidade	Estanqueidade das instalações de esgoto e de águas pluviais	NBR 15575-6 Item 10 Subitem 10.2.1	Referência a NBR 8160 e 10844	2	2018-04-07	2018-04-07	t
33	Há informações sobre a estanqueidade das calhas, com todos os seus componentes do sistema predial de águas pluviais?	Estanqueidade	Estanqueidade à agua das calhas	NBR 15575-6 Item 10 Subitem 10.2.2		2	2018-04-07	2018-04-07	t
34	Há informações sobre os limites de ruídos gerados durante o ciclo de utilização dos aparelhos hidrossanitário??	Desemp. acústico	Anexo B (não obrigatório)	NBR 15575-6 Item 12 Subitem 12		2	2018-04-07	2018-04-07	t
35	Foi estabelecida a vida útil de projeto?	Durabilidade	Critério para vida útil de projeto	NBR 15575-6 Item 14 Subitem 14.1.1	Referência à Tabela 7 da NBR 15575-1	2	2018-04-07	2018-04-07	t
36	As informações disponíveis neste projeto atendem ao Anexo A conforme NBR 13531, incluindo os conteúdos e produtos requeridos para esta etapa?	Durabilidade	Projeto e execução das instalações hidrossanitárias	NBR 15575-6 Item 14 Subitem 14.1.2	Referência à NBR 13531 e Anexo A	2	2018-04-07	2018-04-07	t
37	Há informações sobre a durabilidade dos elementos, componentes e instalações dos sistemas hidrossanitários, compatíveis com a vida útil de projeto?	Durabilidade	Durabilidade dos sistemas, elementos, componentes e instalação	NBR 15575-6 Item 14 Subitem 14.1.3	Referência ao Anexo C da NBR 15575-1	2	2018-04-07	2018-04-07	t
38	Há informações de que para as tubulações de esgoto e águas pluviais tenham sido previstos dispositivos de inspeção nas condições descritas nas normas aplicáveis?	Manutenibilidade	Inspeções em tubulações de esgoto e águas pluviais	NBR 15575-6 Item 14 Subitem 14.2.1	Referência à NBR 8160 e 10844	2	2018-04-07	2018-04-07	t
39	Há informações sobre as condições de uso, operação e manutenção, incluindo o "Como Construído", do sistema hidrossanitário, de seus elementos ou componente?	Manutenibilidade	Manual de uso, operação e manutenção das instalações hidrossanitárias	NBR 15575-6 Item 14 Subitem 14.2.2	Referência à NBR 5674 e 14037	2	2018-04-07	2018-04-07	t
41	Há informações de que a superfície interna dos materiais que fiquem em contato com a água seja lisa e de material lavável para evitar formação de biofilme (atenção para os reservatórios!)?	Saúde, hig. e qlde. do ar	Risco de contaminação biológica das tubulações	NBR 15575-6 Item 15 Subitem 15.2.2	Referência à NBR 15575-1	2	2018-04-07	2018-04-07	t
42	Há informações de que os componentes da instalação hidráulica não permitem o empoçamento de água e nem a sua estagnação  causada pela insuficiência de renovação?	Saúde, hig. e qlde. do ar	Risco de estagnação da água	NBR 15575-6 Item 15 Subitem 15.2.2	Referência a NBR 12450, 12451, 15097-1, 11778, 15423	2	2018-04-07	2018-04-07	t
43	Há informações sobre a proteção, dos componentes do sistema de instalação enterrados, contra a entrada de animais ou corpos estranhos, bem como de líquidos que possam contaminar a água potável, em con	Saúde, hig. e qlde. do ar	Tubulações e componentes de água potável enterrados	NBR 15575-6 Item 15 Subitem 15.3.1	Referência à NBR 5626 e 8160	2	2018-04-07	2018-04-07	t
44	Há informações sobre a separação atmosférica, física ou mediante equipamentos, em conformidade com ABNT NBR 5626?	Saúde, hig. e qlde. do ar	Separação atmosférica	NBR 15575-6 Item 15 Subitem 15.4.1	Referência à NBR 5626	2	2018-04-07	2018-04-07	t
45	Há informações de que o sistema de esgoto sanitário não permite a retrossifonagem ou quebra do fecho hídrico?	Saúde, hig. e qlde. do ar	Estanqueidade aos gases	NBR 15575-6 Item 15 Subitem 15.5.1	Referência à NBR 8160	2	2018-04-07	2018-04-07	t
40	Há informações de que o sistema de água potável seja separado fisicamente de qualquer outra instalação que conduza água não potável de qualidade insatisfatória, desconhecida ou questionável? - Há i	Saúde, hig. e qlde. do ar	Independência do sistema de água	NBR 15575-6 Item 15 Subitem 15.1.1	Referência a 17 normas listadas em 15.1.1.1.	2	2018-04-07	2018-04-07	t
46	Há informações sobre o teor de CO2 e de CO nos ambientes? - CO2 deve ser inferior a 0,5 %. - CO deve ser inferior a 30 ppm.	Saúde, hig. e qlde. do ar	Teor de poluentes	NBR 15575-6 Item 15 Subitem 15.6.1	Referência à NBR 13103	2	2018-04-07	2018-04-07	t
47	Há informações de que o sistema predial de água fria e quente fornece água na pressão, vazão e volume compatíveis com o uso, associado a cada ponto de utilização, consideram a possibilidade de uso sim	Funcional. e acessib.	Dimensionamento da instalação de água fria e quente	NBR 15575-6 Item 16 Subitem 16.1.1	Referência à NBR 5626 e 7198	2	2018-04-07	2018-04-07	t
48	Há informações de que as caixas e válvulas de descarga atendem ao disposto nas normas aplicáveis, no que se refere à vazão e volume de descarga?	Funcional. e acessib.	Funcionamento de dispositivos de descarga	NBR 15575-6 Item 16 Subitem 16.1.2	Referência à NBR 15491 e 15857	2	2018-04-07	2018-04-07	t
49	Há informações sobre a capacidade do sistema predial de esgoto de coletar e afastar os efluentes gerados pela edificação habitacional, nas vazões com que normalmente são descarregados os aparelhos sem	Funcional. e acessib.	Dimensionamento da instalação de esgoto	NBR 15575-6 Item 16 Subitem 16.2.1	Referência à NBR 8160, 7229 e 13969	2	2018-04-07	2018-04-07	t
50	Há informações de que as calhas e condutores suportam a vazão de projeto, calculada a partir da intensidade de chuva adotada para a localidade e para um certo período de retorno?	Funcional. e acessib.	Dimensionamento de calhas e condutores	NBR 15575-6 Item 16 Subitem 16.3.1	Referência à NBR 10844	2	2018-04-07	2018-04-07	t
51	- Há informação ou menção de que louças e metais tenham sido especificados conforme sua norma técnica?	Conf. tátil e antropodin.	Adaptação ergonômica dos equipamentos	NBR 15575-6 Item 17 Subitem 17.2	Referência a várias	2	2018-04-07	2018-04-07	t
52	Há informações de que as bacias sanitárias são de volume de descarga de acordo com as especificações da ABNT NBR 15097-1?	Impacto ambiental	Consumo de água em bacias sanitárias	NBR 15575-6 Subitens 18.1 e 18.1.1	Referência à NBR 15097-1	2	2018-04-07	2018-04-07	t
53	Há informações de que as peças de utilização possuem vazões que permitem tornar o mais eficiente possível o uso da água nele utilizada?	Impacto ambiental	Fluxo de água em peças de utilização	NBR 15575-6 Subitens 18.1 e 18.1.2	Referência a 10 normas listadas em 18.1.2.1.	2	2018-04-07	2018-04-07	t
54	Há informações sobre a ligação dos sistemas prediais de esgoto à rede pública de esgoto ou a um sistema localizado de tratamento e disposição de efluente, conforme as normas aplicáveis?	Impacto ambiental	Tratamento e disposição de efluentes	NBR 15575-6 Subitens 18.2 e 18.2.1	Referência a NBR 8160, 7229 e 13969	2	2018-04-07	2018-04-07	t
55	Projeto possui relação de requisitos regulamentares e estatutários atendidos, incluindo a versão? (normas, códigos etc.)	Requisito próprio	Normas atendidas em projeto	Requisito próprio geral		2	2018-04-07	2018-04-07	t
\.


--
-- Data for Name: checklist_cms_integration_avaliacaopluginmodel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY checklist_cms_integration_avaliacaopluginmodel (cmsplugin_ptr_id, avaliacao_id) FROM stdin;
\.


--
-- Data for Name: checklist_cms_integration_checklistspluginmodel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY checklist_cms_integration_checklistspluginmodel (cmsplugin_ptr_id, checklist_id) FROM stdin;
\.


--
-- Data for Name: checklist_empreendimento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY checklist_empreendimento (id, nome, descricao, ic_ativo, assinante_id) FROM stdin;
1	Condomínio ISLA		t	1
2	Portal do Sol - Área de Lazer	Ampliação da Área de Lazer do Portal do Sol 1.	t	1
3	Residencial Monet	Empreendimento luxo. Apartamento totolmente personalizado pelo cliente.	t	2
4	Centro de Convenções	Descrição do Centro de Convenções	t	2
5	Residencial das Palmeiras	Empreendimento residencial, torre única, 36 andares, 72 apartamentos, Setor Marista - Goiânia-GO	t	2
6	Shopping Bougainville	Análise do Shopping Bougainville	t	2
\.


--
-- Data for Name: checklist_etapaprojeto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY checklist_etapaprojeto (id, nome, descircao, ic_ativo) FROM stdin;
1	Pré-projeto		t
2	Projeto para execução		t
\.


--
-- Data for Name: checklist_itemavaliacao; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY checklist_itemavaliacao (id, created, modified, nivel_presenca_info, recomendacao, avaliacao_id, checklist_id, observacao) FROM stdin;
3	2018-03-26 09:27:25.961025+00	2018-03-27 02:54:56.110363+00	presente	Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.	2	2	
4	2018-03-26 09:27:25.963448+00	2018-03-27 02:54:56.114684+00	presente	Porque nós o usamos?\r\n\r\nÉ um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar Lorem Ipsum é que ele tem uma distribuição normal de letras, ao contrário de "Conteúdo aqui, conteúdo aqui", fazendo com que ele tenha uma aparência similar a de um texto legível. Muitos softwares de publicação e editores de páginas na internet agora usam Lorem Ipsum como texto-modelo padrão, e uma rápida busca por 'lorem ipsum' mostra vários websites ainda em sua fase de construção. Várias versões novas surgiram ao longo dos anos, eventualmente por acidente, e às vezes de propósito (injetando humor, e coisas do gênero).	2	1	
8	2018-04-02 11:41:59.371273+00	2018-04-03 01:01:44.782718+00	parcial	O trecho padrão original de Lorem Ipsum, usado desde o século XVI, está reproduzido abaixo para os interessados. Seções 1.10.32 e 1.10.33 de "de Finibus Bonorum et Malorum" de Cicero também foram reproduzidas abaixo em sua forma exata original, acompanhada das versões para o inglês da tradução feita por H. Rackham em 1914.	4	2	
1	2018-03-26 00:50:55.420759+00	2018-04-02 11:41:30.128133+00	presente	Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.	1	2	
2	2018-03-26 00:50:55.57662+00	2018-04-02 11:41:30.132488+00	parcial	Onde posso conseguí-lo?\r\n\r\nExistem muitas variações disponíveis de passagens de Lorem Ipsum, mas a maioria sofreu algum tipo de alteração, seja por inserção de passagens com humor, ou palavras aleatórias que não parecem nem um pouco convincentes. Se você pretende usar uma passagem de Lorem Ipsum, precisa ter certeza de que não há algo embaraçoso escrito escondido no meio do texto. Todos os geradores de Lorem Ipsum na internet tendem a repetir pedaços predefinidos conforme necessário, fazendo deste o primeiro gerador de Lorem Ipsum autêntico da internet. Ele usa um dicionário com mais de 200 palavras em Latim combinado com um punhado de modelos de estrutura de frases para gerar um Lorem Ipsum com aparência razoável, livre de repetições, inserções de humor, palavras não características, etc.	1	1	
9	2018-04-03 01:05:35.864079+00	2018-04-03 01:06:17.905598+00	ausente	Onde posso conseguí-lo?\r\n\r\nExistem muitas variações disponíveis de passagens de Lorem Ipsum, mas a maioria sofreu algum tipo de alteração, seja por inserção de passagens com humor, ou palavras aleatórias que não parecem nem um pouco convincentes. Se você pretende usar uma passagem de Lorem Ipsum, precisa ter certeza de que não há algo embaraçoso escrito escondido no meio do texto. Todos os geradores de Lorem Ipsum na internet tendem a repetir pedaços predefinidos conforme necessário, fazendo deste o primeiro gerador de Lorem Ipsum autêntico da internet. Ele usa um dicionário com mais de 200 palavras em Latim combinado com um punhado de modelos de estrutura de frases para gerar um Lorem Ipsum com aparência razoável, livre de repetições, inserções de humor, palavras não características, etc.	5	1	
10	2018-04-03 01:05:35.86669+00	2018-04-03 01:06:17.909824+00	parcial	O trecho padrão original de Lorem Ipsum, usado desde o século XVI, está reproduzido abaixo para os interessados. Seções 1.10.32 e 1.10.33 de "de Finibus Bonorum et Malorum" de Cicero também foram reproduzidas abaixo em sua forma exata original, acompanhada das versões para o inglês da tradução feita por H. Rackham em 1914.	5	2	
7	2018-04-02 11:41:59.368365+00	2018-04-03 01:01:44.778459+00	presente	Porque nós o usamos?\r\n\r\nÉ um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar Lorem Ipsum é que ele tem uma distribuição normal de letras, ao contrário de "Conteúdo aqui, conteúdo aqui", fazendo com que ele tenha uma aparência similar a de um texto legível. Muitos softwares de publicação e editores de páginas na internet agora usam Lorem Ipsum como texto-modelo padrão, e uma rápida busca por 'lorem ipsum' mostra vários websites ainda em sua fase de construção. Várias versões novas surgiram ao longo dos anos, eventualmente por acidente, e às vezes de propósito (injetando humor, e coisas do gênero).	4	1	
6	2018-03-26 10:25:00.480752+00	2018-04-03 07:42:48.344278+00	presente	Porque nós o usamos?\r\n\r\nÉ um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar Lorem Ipsum é que ele tem uma distribuição normal de letras, ao contrário de "Conteúdo aqui, conteúdo aqui", fazendo com que ele tenha uma aparência similar a de um texto legível. Muitos softwares de publicação e editores de páginas na internet agora usam Lorem Ipsum como texto-modelo padrão, e uma rápida busca por 'lorem ipsum' mostra vários websites ainda em sua fase de construção. Várias versões novas surgiram ao longo dos anos, eventualmente por acidente, e às vezes de propósito (injetando humor, e coisas do gênero).	3	1	Não informada a norma aplicável e sua edição
5	2018-03-26 10:25:00.478474+00	2018-04-03 07:42:48.348694+00	parcial	O trecho padrão original de Lorem Ipsum, usado desde o século XVI, está reproduzido abaixo para os interessados. Seções 1.10.32 e 1.10.33 de "de Finibus Bonorum et Malorum" de Cicero também foram reproduzidas abaixo em sua forma exata original, acompanhada das versões para o inglês da tradução feita por H. Rackham em 1914.	3	2	
11	2018-04-07 15:04:53.084155+00	\N	na		6	3	
12	2018-04-07 15:12:10.961709+00	2018-04-07 15:12:43.351232+00	na		7	3	
13	2018-04-07 15:12:10.967629+00	2018-04-07 15:12:43.355413+00	presente		7	4	
27	2018-04-07 17:48:01.83373+00	2018-04-07 17:57:46.237525+00	ausente		8	19	
28	2018-04-07 17:48:01.835094+00	2018-04-07 17:57:46.240739+00	presente		8	18	
18	2018-04-07 17:48:01.820642+00	2018-04-07 17:57:46.207625+00	ausente		8	7	
19	2018-04-07 17:48:01.82234+00	2018-04-07 17:57:46.211243+00	presente		8	10	
20	2018-04-07 17:48:01.823973+00	2018-04-07 17:57:46.214587+00	parcial		8	11	
21	2018-04-07 17:48:01.825415+00	2018-04-07 17:57:46.217951+00	ausente		8	12	
22	2018-04-07 17:48:01.826848+00	2018-04-07 17:57:46.2211+00	presente		8	13	
24	2018-04-07 17:48:01.829561+00	2018-04-07 17:57:46.227653+00	presente		8	17	
25	2018-04-07 17:48:01.83097+00	2018-04-07 17:57:46.230929+00	ausente		8	21	
26	2018-04-07 17:48:01.832297+00	2018-04-07 17:57:46.234132+00	parcial		8	20	
59	2018-04-07 17:48:01.877563+00	2018-04-07 17:57:46.341178+00	parcial		8	48	
60	2018-04-07 17:48:01.878814+00	2018-04-07 17:57:46.344477+00	presente		8	49	
61	2018-04-07 17:48:01.880141+00	2018-04-07 17:57:46.347721+00	presente		8	50	
62	2018-04-07 17:48:01.881532+00	2018-04-07 17:57:46.350909+00	presente		8	51	
63	2018-04-07 17:48:01.882858+00	2018-04-07 17:57:46.354107+00	presente		8	52	
64	2018-04-07 17:48:01.88435+00	2018-04-07 17:57:46.357273+00	presente		8	53	
65	2018-04-07 17:48:01.885638+00	2018-04-07 17:57:46.360509+00	presente		8	54	
66	2018-04-07 17:48:01.886941+00	2018-04-07 17:57:46.363802+00	presente		8	55	
14	2018-04-07 17:48:01.813083+00	2018-04-07 17:57:46.192628+00	parcial	Recomendação parcial	8	3	
15	2018-04-07 17:48:01.815711+00	2018-04-07 17:57:46.196831+00	ausente		8	4	
16	2018-04-07 17:48:01.817308+00	2018-04-07 17:57:46.200452+00	presente		8	5	
17	2018-04-07 17:48:01.818944+00	2018-04-07 17:57:46.203782+00	parcial		8	6	
23	2018-04-07 17:48:01.828181+00	2018-04-07 17:57:46.224407+00	parcial		8	14	
29	2018-04-07 17:48:01.836527+00	2018-04-07 17:57:46.243944+00	presente		8	16	
30	2018-04-07 17:48:01.838453+00	2018-04-07 17:57:46.247319+00	parcial		8	15	
31	2018-04-07 17:48:01.839739+00	2018-04-07 17:57:46.250597+00	presente		8	9	
32	2018-04-07 17:48:01.841044+00	2018-04-07 17:57:46.253919+00	ausente		8	8	
33	2018-04-07 17:48:01.842563+00	2018-04-07 17:57:46.257044+00	presente		8	23	
34	2018-04-07 17:48:01.843888+00	2018-04-07 17:57:46.260264+00	presente		8	22	
35	2018-04-07 17:48:01.845169+00	2018-04-07 17:57:46.263661+00	presente		8	25	
36	2018-04-07 17:48:01.846993+00	2018-04-07 17:57:46.266981+00	parcial		8	24	
37	2018-04-07 17:48:01.848265+00	2018-04-07 17:57:46.270208+00	presente		8	26	
38	2018-04-07 17:48:01.849538+00	2018-04-07 17:57:46.273459+00	ausente		8	27	
39	2018-04-07 17:48:01.850832+00	2018-04-07 17:57:46.276641+00	presente		8	28	
40	2018-04-07 17:48:01.852159+00	2018-04-07 17:57:46.279957+00	parcial		8	29	
41	2018-04-07 17:48:01.853554+00	2018-04-07 17:57:46.283269+00	presente		8	30	
42	2018-04-07 17:48:01.854843+00	2018-04-07 17:57:46.2865+00	presente		8	31	
43	2018-04-07 17:48:01.856247+00	2018-04-07 17:57:46.289682+00	presente		8	32	
44	2018-04-07 17:48:01.857532+00	2018-04-07 17:57:46.292888+00	ausente		8	33	
45	2018-04-07 17:48:01.858834+00	2018-04-07 17:57:46.296063+00	ausente		8	34	
46	2018-04-07 17:48:01.860139+00	2018-04-07 17:57:46.299239+00	presente		8	35	
47	2018-04-07 17:48:01.861449+00	2018-04-07 17:57:46.302512+00	parcial		8	36	
48	2018-04-07 17:48:01.862771+00	2018-04-07 17:57:46.305672+00	parcial		8	37	
49	2018-04-07 17:48:01.864106+00	2018-04-07 17:57:46.308856+00	presente		8	38	
50	2018-04-07 17:48:01.865452+00	2018-04-07 17:57:46.312067+00	ausente		8	39	
51	2018-04-07 17:48:01.866792+00	2018-04-07 17:57:46.315229+00	presente		8	41	
52	2018-04-07 17:48:01.868214+00	2018-04-07 17:57:46.318412+00	parcial		8	42	
53	2018-04-07 17:48:01.869733+00	2018-04-07 17:57:46.321666+00	ausente		8	43	
54	2018-04-07 17:48:01.871092+00	2018-04-07 17:57:46.324834+00	presente		8	44	
55	2018-04-07 17:48:01.872399+00	2018-04-07 17:57:46.328107+00	presente		8	45	
56	2018-04-07 17:48:01.873654+00	2018-04-07 17:57:46.331398+00	presente		8	40	
57	2018-04-07 17:48:01.874872+00	2018-04-07 17:57:46.334658+00	parcial		8	46	
58	2018-04-07 17:48:01.87624+00	2018-04-07 17:57:46.337946+00	ausente		8	47	
\.


--
-- Data for Name: checklist_itemchecklist; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY checklist_itemchecklist (id, nivel_presenca_info, recomendacao, checklist_id) FROM stdin;
1	presente	Porque nós o usamos?\r\n\r\nÉ um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar Lorem Ipsum é que ele tem uma distribuição normal de letras, ao contrário de "Conteúdo aqui, conteúdo aqui", fazendo com que ele tenha uma aparência similar a de um texto legível. Muitos softwares de publicação e editores de páginas na internet agora usam Lorem Ipsum como texto-modelo padrão, e uma rápida busca por 'lorem ipsum' mostra vários websites ainda em sua fase de construção. Várias versões novas surgiram ao longo dos anos, eventualmente por acidente, e às vezes de propósito (injetando humor, e coisas do gênero).	1
2	parcial	Onde posso conseguí-lo?\r\n\r\nExistem muitas variações disponíveis de passagens de Lorem Ipsum, mas a maioria sofreu algum tipo de alteração, seja por inserção de passagens com humor, ou palavras aleatórias que não parecem nem um pouco convincentes. Se você pretende usar uma passagem de Lorem Ipsum, precisa ter certeza de que não há algo embaraçoso escrito escondido no meio do texto. Todos os geradores de Lorem Ipsum na internet tendem a repetir pedaços predefinidos conforme necessário, fazendo deste o primeiro gerador de Lorem Ipsum autêntico da internet. Ele usa um dicionário com mais de 200 palavras em Latim combinado com um punhado de modelos de estrutura de frases para gerar um Lorem Ipsum com aparência razoável, livre de repetições, inserções de humor, palavras não características, etc.	1
3	ausente	Onde posso conseguí-lo?\r\n\r\nExistem muitas variações disponíveis de passagens de Lorem Ipsum, mas a maioria sofreu algum tipo de alteração, seja por inserção de passagens com humor, ou palavras aleatórias que não parecem nem um pouco convincentes. Se você pretende usar uma passagem de Lorem Ipsum, precisa ter certeza de que não há algo embaraçoso escrito escondido no meio do texto. Todos os geradores de Lorem Ipsum na internet tendem a repetir pedaços predefinidos conforme necessário, fazendo deste o primeiro gerador de Lorem Ipsum autêntico da internet. Ele usa um dicionário com mais de 200 palavras em Latim combinado com um punhado de modelos de estrutura de frases para gerar um Lorem Ipsum com aparência razoável, livre de repetições, inserções de humor, palavras não características, etc.	1
4	presente	Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.	2
5	ausente	De onde ele vem?\r\n\r\nAo contrário do que se acredita, Lorem Ipsum não é simplesmente um texto randômico. Com mais de 2000 anos, suas raízes podem ser encontradas em uma obra de literatura latina clássica datada de 45 AC. Richard McClintock, um professor de latim do Hampden-Sydney College na Virginia, pesquisou uma das mais obscuras palavras em latim, consectetur, oriunda de uma passagem de Lorem Ipsum, e, procurando por entre citações da palavra na literatura clássica, descobriu a sua indubitável origem. Lorem Ipsum vem das seções 1.10.32 e 1.10.33 do "de Finibus Bonorum et Malorum" (Os Extremos do Bem e do Mal), de Cícero, escrito em 45 AC. Este livro é um tratado de teoria da ética muito popular na época da Renascença. A primeira linha de Lorem Ipsum, "Lorem Ipsum dolor sit amet..." vem de uma linha na seção 1.10.32.\r\n\r\nO trecho padrão original de Lorem Ipsum, usado desde o sé	2
6	parcial	O trecho padrão original de Lorem Ipsum, usado desde o século XVI, está reproduzido abaixo para os interessados. Seções 1.10.32 e 1.10.33 de "de Finibus Bonorum et Malorum" de Cicero também foram reproduzidas abaixo em sua forma exata original, acompanhada das versões para o inglês da tradução feita por H. Rackham em 1914.	2
7	ausente	Recomendação cadastrada	3
8	parcial	Recomendação parcial	3
\.


--
-- Data for Name: checklist_plano; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY checklist_plano (id, nome, descricao, valor, dt_inicio_publicacao, dt_fim_publicacao, ic_ativo) FROM stdin;
1	Plano Básico		0.00	2018-03-26	2018-12-31	t
\.


--
-- Data for Name: checklist_produto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY checklist_produto (id, created, modified, titulo, descricao, folder, dt_inicio_publicacao, dt_fim_publicacao, ic_ativo, ramo_id) FROM stdin;
1	2018-03-26 00:23:03.161848+00	\N	Checklist básico para instalação elétrica			2018-03-26	2018-06-01	t	1
2	2018-04-05 14:22:16.939258+00	\N	Checklist Projeto	Descrição do Checklist Projeto		2018-04-05	2018-04-05	t	1
\.


--
-- Data for Name: checklist_produto_planos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY checklist_produto_planos (id, produto_id, plano_id) FROM stdin;
1	1	1
2	2	1
\.


--
-- Data for Name: checklist_projeto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY checklist_projeto (id, nome, descricao, ic_ativo, ramo_id) FROM stdin;
1	Instalação Hidrosanitária		t	1
3	Instalações elétricas de baixa tensão		t	1
\.


--
-- Data for Name: checklist_ramo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY checklist_ramo (id, nome, descircao, ic_ativo) FROM stdin;
1	Construção Civil		t
\.


--
-- Data for Name: cms_aliaspluginmodel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_aliaspluginmodel (cmsplugin_ptr_id, plugin_id, alias_placeholder_id) FROM stdin;
\.


--
-- Data for Name: cms_cmsplugin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_cmsplugin (id, "position", language, plugin_type, creation_date, changed_date, parent_id, placeholder_id, depth, numchild, path) FROM stdin;
43	1	pt	Bootstrap3AccordionItemCMSPlugin	2018-03-20 10:26:33.677602+00	2018-04-18 10:23:21.632622+00	41	25	4	2	001I000100020002
40	0	pt	TextPlugin	2018-03-20 10:20:58.031531+00	2018-04-18 20:19:12.761152+00	36	25	3	0	001I00020001
1196	1	pt	TextPlugin	2018-03-20 10:36:00.414853+00	2018-04-18 20:20:22.66128+00	1194	26	5	0	003V0001000100020002
818	1	pt	Bootstrap3ImageCMSPlugin	2018-04-03 13:19:35.831464+00	2018-04-18 09:59:09.888586+00	801	20	4	0	002J000100010002
1199	0	pt	TextPlugin	2018-03-20 10:20:58.031531+00	2018-04-18 20:20:22.726357+00	1198	26	3	0	003V00020001
396	0	pt	TextPlugin	2018-03-20 10:20:58.031531+00	2018-04-18 20:20:06.64045+00	395	25	3	0	001I00040001
1112	2	pt	Bootstrap3ImageCMSPlugin	2018-04-18 10:04:49.248382+00	2018-04-18 10:04:49.266093+00	801	20	4	0	002J000100010003
16	0	pt	Bootstrap3ImageCMSPlugin	2018-03-20 09:40:12.716958+00	2018-04-03 01:20:17.32986+00	\N	4	1	0	003B
964	0	pt	TextPlugin	2018-04-04 00:37:42.969407+00	2018-04-04 00:46:11.993349+00	\N	9	1	0	0031
1189	0	pt	Bootstrap3TabItemCMSPlugin	2018-03-20 10:14:34.663378+00	2018-04-18 20:20:22.309096+00	1188	26	2	1	003V0001
1193	0	pt	LinkPlugin	2018-03-26 09:43:27.782439+00	2018-04-18 20:20:22.425705+00	1191	26	5	0	003V0001000100010002
1191	2	pt	Bootstrap3AccordionItemCMSPlugin	2018-03-20 10:24:58.525816+00	2018-04-18 20:20:22.337888+00	1190	26	4	2	003V000100010001
1124	0	pt	Bootstrap3ImageCMSPlugin	2018-04-05 02:24:19.261319+00	2018-04-18 10:15:47.843865+00	1123	14	2	0	003Q0001
1195	0	pt	LinkPlugin	2018-03-26 09:43:27.782439+00	2018-04-18 20:20:22.439028+00	1194	26	5	0	003V0001000100020001
18	0	pt	TextPlugin	2018-03-20 09:43:15.567319+00	2018-04-03 13:41:55.325757+00	157	42	3	1	003800010002
758	3	pt	ColumnPlugin	2018-04-03 02:15:37.873369+00	2018-04-03 02:15:37.881306+00	156	42	2	1	00380004
41	0	pt	Bootstrap3AccordionCMSPlugin	2018-03-20 10:24:16.025725+00	2018-03-26 10:14:38.298378+00	35	25	3	3	001I00010002
37	3	pt	Bootstrap3TabItemCMSPlugin	2018-03-20 10:18:54.37612+00	2018-03-26 10:13:03.280254+00	34	25	2	1	001I0003
1123	0	pt	Bootstrap3JumbotronCMSPlugin	2018-04-05 02:23:27.289042+00	2018-04-18 10:15:47.839134+00	\N	14	1	1	003Q
1194	1	pt	Bootstrap3AccordionItemCMSPlugin	2018-03-20 10:26:33.677602+00	2018-04-18 20:20:22.433848+00	1190	26	4	2	003V000100010002
42	2	pt	Bootstrap3AccordionItemCMSPlugin	2018-03-20 10:24:58.525816+00	2018-04-13 09:33:39.974044+00	41	25	4	2	001I000100020001
1069	0	pt	Bootstrap3AccordionItemCMSPlugin	2018-04-13 09:55:59.359379+00	2018-04-13 09:55:59.379717+00	41	25	4	0	001I000100020003
1197	0	pt	Bootstrap3AccordionItemCMSPlugin	2018-04-13 09:55:59.359379+00	2018-04-18 20:20:22.493659+00	1190	26	4	0	003V000100010003
1008	0	pt	Bootstrap3JumbotronCMSPlugin	2018-04-05 02:23:27.289042+00	2018-04-05 02:23:27.296279+00	\N	13	1	1	003E
1190	0	pt	Bootstrap3AccordionCMSPlugin	2018-03-20 10:24:16.025725+00	2018-04-18 20:20:22.329932+00	1189	26	3	3	003V00010001
798	0	pt	Bootstrap3ColumnCMSPlugin	2018-04-03 12:58:28.260038+00	2018-04-03 13:07:10.045025+00	797	20	2	1	002J0001
1122	1	pt	TextPlugin	2018-04-05 02:24:44.102282+00	2018-04-18 10:15:47.853872+00	\N	14	1	0	003P
1198	1	pt	Bootstrap3TabItemCMSPlugin	2018-03-20 10:18:23.573069+00	2018-04-18 20:20:22.500528+00	1188	26	2	1	003V0002
1014	0	pt	Bootstrap3ImageCMSPlugin	2018-04-05 08:04:08.432758+00	2018-04-05 08:05:10.126574+00	1013	12	2	0	003H0001
35	0	pt	Bootstrap3TabItemCMSPlugin	2018-03-20 10:14:34.663378+00	2018-03-22 10:44:08.526804+00	34	25	2	1	001I0001
36	1	pt	Bootstrap3TabItemCMSPlugin	2018-03-20 10:18:23.573069+00	2018-03-22 10:46:57.287577+00	34	25	2	1	001I0002
1200	3	pt	Bootstrap3TabItemCMSPlugin	2018-03-20 10:18:54.37612+00	2018-04-18 20:20:22.523316+00	1188	26	2	1	003V0003
1188	0	pt	Bootstrap3TabCMSPlugin	2018-03-20 10:14:04.266597+00	2018-04-18 20:20:22.304765+00	\N	26	1	4	003V
797	0	pt	Bootstrap3RowCMSPlugin	2018-04-03 12:58:28.246075+00	2018-04-03 12:58:28.256039+00	\N	20	1	1	002J
1202	2	pt	Bootstrap3TabItemCMSPlugin	2018-03-20 10:18:23.573069+00	2018-04-18 20:20:22.543489+00	1188	26	2	1	003V0004
1192	1	pt	TextPlugin	2018-03-20 10:36:00.414853+00	2018-04-18 20:20:22.569722+00	1191	26	5	0	003V0001000100010001
639	0	pt	LinkPlugin	2018-03-26 09:43:27.782439+00	2018-03-26 10:15:40.235816+00	42	25	5	0	001I0001000200010002
1201	0	pt	TextPlugin	2018-03-20 11:36:10.425127+00	2018-04-18 20:20:22.748631+00	1200	26	3	0	003V00030001
1203	0	pt	TextPlugin	2018-03-20 10:20:58.031531+00	2018-04-18 20:20:22.766301+00	1202	26	3	0	003V00040001
34	0	pt	Bootstrap3TabCMSPlugin	2018-03-20 10:14:04.266597+00	2018-03-20 10:14:04.274777+00	\N	25	1	4	001I
395	2	pt	Bootstrap3TabItemCMSPlugin	2018-03-20 10:18:23.573069+00	2018-03-22 11:16:02.585111+00	34	25	2	1	001I0004
1157	0	pt	Bootstrap3ImageCMSPlugin	2018-03-20 09:40:12.716958+00	2018-04-18 20:15:12.961655+00	\N	15	1	0	003S
656	0	pt	TextPlugin	2018-03-22 04:00:51.814466+00	2018-03-26 12:06:54.137251+00	\N	37	1	0	001Y
1009	0	pt	Bootstrap3ImageCMSPlugin	2018-04-05 02:24:19.261319+00	2018-04-05 02:24:19.270234+00	1008	13	2	0	003E0001
1158	1	pt	MultiColumnPlugin	2018-03-22 01:13:11.331169+00	2018-04-18 20:15:12.989598+00	\N	43	1	4	003T
1115	0	pt	TextPlugin	2018-03-20 09:27:02.405292+00	2018-04-18 10:05:12.803228+00	1114	21	3	3	003O00010001
123	0	pt	TextPlugin	2018-03-20 11:36:10.425127+00	2018-04-18 20:20:18.384771+00	37	25	3	0	001I00030001
1159	0	pt	ColumnPlugin	2018-03-22 01:13:11.345459+00	2018-04-18 20:15:12.993776+00	1158	43	2	1	003T0001
1161	0	pt	Bootstrap3ImageCMSPlugin	2018-03-22 01:24:38.627603+00	2018-04-18 20:15:13.019533+00	1160	43	4	0	003T000100010001
1058	0	pt	Bootstrap3ImageCMSPlugin	2018-04-09 14:44:28.413949+00	2018-04-09 14:44:28.427313+00	277	36	2	0	001B0001
277	0	pt	TextPlugin	2018-03-22 04:00:51.814466+00	2018-04-09 14:45:39.596278+00	\N	36	1	2	001B
1006	1	pt	SnippetPlugin	2018-04-04 11:04:46.441757+00	2018-04-04 13:09:43.175043+00	\N	35	1	0	0037
1005	0	pt	TextPlugin	2018-03-22 03:02:01.849345+00	2018-04-04 13:09:43.180742+00	\N	35	1	0	0036
1162	1	pt	ColumnPlugin	2018-03-22 01:13:11.351205+00	2018-04-18 20:15:13.027561+00	1158	43	2	1	003T0002
1164	0	pt	Bootstrap3ImageCMSPlugin	2018-03-22 01:24:38.627603+00	2018-04-18 20:15:13.049769+00	1163	43	4	0	003T000200010001
1168	3	pt	ColumnPlugin	2018-04-03 02:15:37.873369+00	2018-04-18 20:15:13.088791+00	1158	43	2	1	003T0004
1165	2	pt	ColumnPlugin	2018-04-03 01:27:55.526926+00	2018-04-18 20:15:13.056822+00	1158	43	2	1	003T0003
708	0	pt	TextPlugin	2018-03-20 09:43:15.567319+00	2018-04-03 13:54:49.875722+00	158	42	3	1	003800020001
753	0	pt	TextPlugin	2018-03-20 09:43:15.567319+00	2018-04-03 13:55:20.10846+00	673	42	3	1	003800030001
754	0	pt	Bootstrap3ImageCMSPlugin	2018-03-22 01:24:38.627603+00	2018-04-03 02:14:44.228618+00	753	42	4	0	0038000300010001
1167	0	pt	Bootstrap3ImageCMSPlugin	2018-03-22 01:24:38.627603+00	2018-04-18 20:15:13.08144+00	1166	43	4	0	003T000300010001
176	0	pt	Bootstrap3ImageCMSPlugin	2018-03-22 01:24:38.627603+00	2018-04-03 01:30:16.08007+00	18	42	4	0	0038000100020001
1170	0	pt	Bootstrap3ImageCMSPlugin	2018-03-22 01:24:38.627603+00	2018-04-18 20:15:13.211366+00	1169	43	4	0	003T000400010001
158	1	pt	ColumnPlugin	2018-03-22 01:13:11.351205+00	2018-04-03 01:55:07.947081+00	156	42	2	1	00380002
709	0	pt	Bootstrap3ImageCMSPlugin	2018-03-22 01:24:38.627603+00	2018-04-03 01:56:50.578082+00	708	42	4	0	0038000200010001
1160	0	pt	TextPlugin	2018-03-20 09:43:15.567319+00	2018-04-18 20:15:13.240899+00	1159	43	3	1	003T00010001
1163	0	pt	TextPlugin	2018-03-20 09:43:15.567319+00	2018-04-18 20:15:13.265447+00	1162	43	3	1	003T00020001
157	0	pt	ColumnPlugin	2018-03-22 01:13:11.345459+00	2018-04-03 01:28:14.05366+00	156	42	2	1	00380001
806	0	pt	Bootstrap3ImageCMSPlugin	2018-04-03 12:29:33.060824+00	2018-04-03 12:59:54.340672+00	805	1	2	0	002M0001
805	0	pt	TextPlugin	2018-03-20 09:27:02.405292+00	2018-04-03 12:59:54.351243+00	\N	1	1	1	002M
1166	0	pt	TextPlugin	2018-03-20 09:43:15.567319+00	2018-04-18 20:15:13.291624+00	1165	43	3	1	003T00030001
156	1	pt	MultiColumnPlugin	2018-03-22 01:13:11.331169+00	2018-03-22 01:29:05.572034+00	\N	42	1	4	0038
1169	0	pt	TextPlugin	2018-03-20 09:43:15.567319+00	2018-04-18 20:15:13.318111+00	1168	43	3	1	003T00040001
1171	0	pt	TextPlugin	2018-04-03 01:57:50.428851+00	2018-04-18 20:15:13.342098+00	\N	43	1	0	003U
352	1	pt	TextPlugin	2018-03-20 10:36:00.414853+00	2018-04-18 20:16:13.522379+00	43	25	5	0	001I0001000200020003
597	1	pt	TextPlugin	2018-03-20 10:36:00.414853+00	2018-04-18 20:17:39.027473+00	42	25	5	0	001I0001000200010001
1113	0	pt	Bootstrap3RowCMSPlugin	2018-04-03 12:58:28.246075+00	2018-04-18 10:05:12.690703+00	\N	21	1	1	003O
1114	0	pt	Bootstrap3ColumnCMSPlugin	2018-04-03 12:58:28.260038+00	2018-04-18 10:05:12.694865+00	1113	21	2	1	003O0001
1116	0	pt	Bootstrap3ImageCMSPlugin	2018-04-03 12:29:33.060824+00	2018-04-18 10:05:12.768003+00	1115	21	4	0	003O000100010001
1117	1	pt	Bootstrap3ImageCMSPlugin	2018-04-03 13:19:35.831464+00	2018-04-18 10:05:12.775336+00	1115	21	4	0	003O000100010002
1118	2	pt	Bootstrap3ImageCMSPlugin	2018-04-18 10:04:49.248382+00	2018-04-18 10:05:12.782501+00	1115	21	4	0	003O000100010003
955	0	pt	TextPlugin	2018-04-04 00:37:42.969407+00	2018-04-04 00:45:42.580576+00	\N	8	1	0	002V
710	0	pt	TextPlugin	2018-04-03 01:57:50.428851+00	2018-04-03 13:46:13.061299+00	\N	42	1	0	0039
1068	1	pt	SnippetPlugin	2018-04-05 08:27:19.92032+00	2018-04-13 09:25:52.099935+00	1066	19	2	0	003M0002
1066	1	pt	Bootstrap3JumbotronCMSPlugin	2018-04-05 07:58:08.306002+00	2018-04-13 09:25:52.088862+00	\N	19	1	2	003M
238	0	pt	TextPlugin	2018-03-22 03:02:01.849345+00	2018-04-04 13:06:28.342991+00	\N	34	1	0	0033
801	0	pt	TextPlugin	2018-03-20 09:27:02.405292+00	2018-04-18 10:04:58.132401+00	798	20	3	3	002J00010001
1013	1	pt	Bootstrap3JumbotronCMSPlugin	2018-04-05 07:58:08.306002+00	2018-04-05 07:59:33.078424+00	\N	12	1	2	003H
1010	1	pt	TextPlugin	2018-04-05 02:24:44.102282+00	2018-04-18 10:15:42.926014+00	\N	13	1	0	003D
479	0	pt	LinkPlugin	2018-03-26 09:43:27.782439+00	2018-03-26 09:58:36.828897+00	43	25	5	0	001I0001000200020002
802	0	pt	Bootstrap3ImageCMSPlugin	2018-04-03 12:29:33.060824+00	2018-04-03 12:59:00.333623+00	801	20	4	0	002J000100010001
986	1	pt	SnippetPlugin	2018-04-04 11:04:46.441757+00	2018-04-04 13:09:21.984945+00	\N	34	1	0	0035
1059	1	pt	Bootstrap3ImageCMSPlugin	2018-04-09 14:45:32.740922+00	2018-04-09 14:45:32.755469+00	277	36	2	0	001B0002
673	2	pt	ColumnPlugin	2018-04-03 01:27:55.526926+00	2018-04-03 01:27:55.535246+00	156	42	2	1	00380003
761	0	pt	TextPlugin	2018-03-20 09:43:15.567319+00	2018-04-03 13:55:51.856745+00	758	42	3	1	003800040001
762	0	pt	Bootstrap3ImageCMSPlugin	2018-03-22 01:24:38.627603+00	2018-04-03 02:16:37.903396+00	761	42	4	0	0038000400010001
1017	1	pt	SnippetPlugin	2018-04-05 08:27:19.92032+00	2018-04-13 09:23:49.949709+00	1013	12	2	0	003H0002
1067	0	pt	Bootstrap3ImageCMSPlugin	2018-04-05 08:04:08.432758+00	2018-04-13 09:25:52.0929+00	1066	19	2	0	003M0001
\.


--
-- Data for Name: cms_globalpagepermission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_globalpagepermission (id, can_change, can_add, can_delete, can_change_advanced_settings, can_publish, can_change_permissions, can_move_page, can_view, can_recover_page, group_id, user_id) FROM stdin;
\.


--
-- Data for Name: cms_globalpagepermission_sites; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_globalpagepermission_sites (id, globalpagepermission_id, site_id) FROM stdin;
\.


--
-- Data for Name: cms_page; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_page (id, created_by, changed_by, creation_date, changed_date, publication_date, publication_end_date, in_navigation, soft_root, reverse_id, navigation_extenders, template, login_required, limit_visibility_in_menu, is_home, application_urls, application_namespace, publisher_is_draft, languages, xframe_options, publisher_public_id, is_page_type, node_id) FROM stdin;
13	admin	admin	2018-03-21 02:57:08.533992+00	2018-03-21 02:57:08.534027+00	2018-03-21 02:57:08.533666+00	\N	t	f	\N	\N	INHERIT	f	2	f		\N	f	pt	0	12	f	7
12	admin	admin	2018-03-21 02:55:11.244109+00	2018-03-21 02:55:40.234266+00	2018-03-21 02:57:08.533666+00	\N	t	f	\N	\N	INHERIT	f	2	f		\N	t	pt	0	13	f	7
26	admin	admin	2018-03-26 00:14:12.612745+00	2018-03-26 02:57:22.870437+00	2018-03-26 00:14:12.612477+00	\N	t	f	\N	\N	fullwidth.html	f	\N	f	AvaliacaoApphook	analises	f	pt	0	25	f	14
25	admin	admin	2018-03-26 00:13:19.182959+00	2018-03-26 00:31:53.112349+00	2018-03-26 00:14:12.612477+00	\N	t	f	\N	\N	fullwidth.html	f	\N	f	AvaliacaoApphook	analises	t	pt	0	26	f	14
15	admin	admin	2018-03-22 02:32:51.846258+00	2018-03-22 02:32:51.84628+00	2018-03-22 02:32:51.845864+00	\N	t	f	\N	\N	INHERIT	t	1	f		\N	f	pt	0	14	f	8
14	admin	admin	2018-03-22 01:11:08.755959+00	2018-03-22 01:12:29.595281+00	2018-03-22 02:32:51.845864+00	\N	t	f	\N	\N	INHERIT	t	1	f		\N	t	pt	0	15	f	8
6	admin	admin	2018-03-18 02:19:36.095634+00	2018-04-13 09:25:51.937606+00	2018-03-18 02:19:36.095239+00	\N	t	f	\N	\N	fullwidth.html	f	\N	f		\N	f	pt	0	3	f	2
3	admin	admin	2018-03-18 02:16:17.514638+00	2018-04-05 08:11:46.175187+00	2018-03-18 02:19:36.095239+00	\N	t	f	\N	\N	fullwidth.html	f	\N	f		\N	t	pt	0	6	f	2
8	admin	admin	2018-03-18 02:20:53.604455+00	2018-04-18 10:05:12.517643+00	2018-03-18 02:20:53.603969+00	\N	t	f	\N	\N	fullwidth.html	f	\N	f	\N	\N	f	pt	0	7	f	4
7	admin	admin	2018-03-18 02:20:22.496912+00	2018-03-18 02:20:22.49694+00	2018-03-18 02:20:53.603969+00	\N	t	f	\N	\N	fullwidth.html	f	\N	f	\N	\N	t	pt	0	8	f	4
20	admin	admin	2018-03-22 03:00:37.676108+00	2018-04-04 13:09:43.031373+00	2018-03-22 03:00:37.675792+00	\N	t	f	\N	\N	fullwidth.html	f	\N	f	\N	\N	f	pt	0	19	f	11
22	admin	admin	2018-03-22 03:59:45.842883+00	2018-03-26 12:06:54.015808+00	2018-03-22 03:59:45.842578+00	\N	t	f	\N	\N	fullwidth.html	f	2	f	\N	\N	f	pt	0	21	f	12
21	admin	admin	2018-03-22 03:59:27.376467+00	2018-03-22 04:00:06.414833+00	2018-03-22 03:59:45.842578+00	\N	t	f	\N	\N	fullwidth.html	f	2	f	\N	\N	t	pt	0	22	f	12
19	admin	admin	2018-03-22 02:59:42.051314+00	2018-03-22 02:59:42.236908+00	2018-03-22 03:00:37.675792+00	\N	t	f	\N	\N	fullwidth.html	f	\N	f	\N	\N	t	pt	0	20	f	11
5	admin	admin	2018-03-18 02:19:09.826418+00	2018-04-18 10:15:47.614718+00	2018-03-18 02:19:09.826079+00	\N	t	f	\N	\N	INHERIT	f	\N	f	\N	\N	f	pt	0	4	f	3
4	admin	admin	2018-03-18 02:16:33.01826+00	2018-03-18 02:16:33.018289+00	2018-03-18 02:19:09.826079+00	\N	t	f	\N	\N	INHERIT	f	\N	f	\N	\N	t	pt	0	5	f	3
2	admin	admin	2018-03-18 02:13:45.477873+00	2018-04-18 20:15:12.79659+00	2018-03-18 02:13:45.477623+00	\N	t	f	\N	\N	homepage.html	f	\N	t	\N	\N	f	pt	0	1	f	1
1	admin	admin	2018-03-18 02:13:45.271052+00	2018-03-22 03:20:18.613258+00	2018-03-18 02:13:45.477623+00	\N	t	f	\N	\N	homepage.html	f	\N	t	\N	\N	t	pt	0	2	f	1
11	admin	admin	2018-03-20 10:27:25.957951+00	2018-04-18 20:20:21.918006+00	2018-03-20 10:27:25.957631+00	\N	t	f	\N	\N	INHERIT	f	1	f		\N	f	pt	0	10	f	6
10	admin	admin	2018-03-20 10:09:21.97272+00	2018-03-26 01:11:52.589933+00	2018-03-20 10:27:25.957631+00	\N	t	f	\N	\N	INHERIT	f	1	f		\N	t	pt	0	11	f	6
\.


--
-- Data for Name: cms_page_placeholders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_page_placeholders (id, page_id, placeholder_id) FROM stdin;
1	1	2
2	2	3
3	1	4
4	1	5
5	1	6
6	1	7
7	3	12
8	4	13
9	5	14
10	2	15
11	2	16
12	2	17
13	2	18
14	6	19
15	7	20
16	8	21
18	3	23
19	6	24
20	10	25
21	11	26
22	12	27
23	13	28
24	14	29
25	15	30
29	19	34
30	20	35
31	21	36
32	22	37
35	25	40
36	26	41
37	1	42
38	2	43
\.


--
-- Data for Name: cms_pagepermission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_pagepermission (id, can_change, can_add, can_delete, can_change_advanced_settings, can_publish, can_change_permissions, can_move_page, can_view, grant_on, group_id, page_id, user_id) FROM stdin;
\.


--
-- Data for Name: cms_pageuser; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_pageuser (user_ptr_id, created_by_id) FROM stdin;
2	1
\.


--
-- Data for Name: cms_pageusergroup; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_pageusergroup (group_ptr_id, created_by_id) FROM stdin;
1	1
\.


--
-- Data for Name: cms_placeholder; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_placeholder (id, slot, default_width) FROM stdin;
1	clipboard	\N
2	content	\N
3	content	\N
4	jumbotron	\N
5	col_1	\N
6	col_2	\N
7	col_3	\N
8	footer	\N
9	footer	\N
10	footer-theme	\N
11	footer-theme	\N
12	content	\N
13	content	\N
14	content	\N
15	jumbotron	\N
16	col_1	\N
17	col_2	\N
18	col_3	\N
19	content	\N
20	content	\N
21	content	\N
23	sidebar	\N
24	sidebar	\N
25	content	\N
26	content	\N
27	content	\N
28	content	\N
29	content	\N
30	content	\N
34	content	\N
35	content	\N
36	content	\N
37	content	\N
40	content	\N
41	content	\N
42	conteudo	\N
43	conteudo	\N
\.


--
-- Data for Name: cms_placeholderreference; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_placeholderreference (cmsplugin_ptr_id, name, placeholder_ref_id) FROM stdin;
\.


--
-- Data for Name: cms_staticplaceholder; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_staticplaceholder (id, name, code, dirty, creation_method, draft_id, public_id, site_id) FROM stdin;
2		footer-theme	f	template	10	11	\N
1		footer	f	template	8	9	\N
\.


--
-- Data for Name: cms_title; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_title (id, language, title, page_title, menu_title, meta_description, slug, path, has_url_overwrite, redirect, creation_date, published, publisher_is_draft, publisher_state, page_id, publisher_public_id) FROM stdin;
11	pt	Minha Página	Minha Página	Minha Página		minha-pagina	minha-pagina	f	\N	2018-03-20 10:09:21.974096+00	t	f	1	11	10
26	pt	Análises				analises	minha-pagina/analises	f	\N	2018-03-26 00:13:19.184587+00	t	f	0	26	25
10	pt	Minha Página	Minha Página	Minha Página		minha-pagina	minha-pagina	f	\N	2018-03-20 10:09:21.974096+00	t	t	0	10	11
4	pt	Institucional	\N	\N	\N	institucional	institucional	f	\N	2018-03-18 02:16:33.019606+00	t	t	0	4	5
21	pt	Assine já!				assine-ja	assine-ja	f	\N	2018-03-22 03:59:27.377937+00	t	t	1	21	22
15	pt	Sair				sair	conta/sair	t	\N	2018-03-22 01:11:08.757351+00	t	f	0	15	14
14	pt	Sair				sair	conta/sair	t	\N	2018-03-22 01:11:08.757351+00	t	t	0	14	15
13	pt	Entrar				entrar	conta/entrar	t	\N	2018-03-21 02:55:11.245674+00	t	f	0	13	12
12	pt	Entrar				entrar	conta/entrar	t	\N	2018-03-21 02:55:11.245674+00	t	t	0	12	13
5	pt	Institucional	\N	\N	\N	institucional	institucional	f	\N	2018-03-18 02:16:33.019606+00	t	f	1	5	4
3	pt	Planos				planos	planos	f	\N	2018-03-18 02:16:17.516247+00	t	t	0	3	6
22	pt	Assine já!				assine-ja	assine-ja	f	\N	2018-03-22 03:59:27.377937+00	t	f	1	22	21
6	pt	Planos				planos	planos	f	\N	2018-03-18 02:16:17.516247+00	t	f	1	6	3
25	pt	Análises				analises	minha-pagina/analises	f	\N	2018-03-26 00:13:19.184587+00	t	t	0	25	26
1	pt	Início	Início	Início		pagina-inicial		f	\N	2018-03-18 02:13:45.272868+00	t	t	0	1	2
2	pt	Início	Início	Início		pagina-inicial		f	\N	2018-03-18 02:13:45.272868+00	t	f	1	2	1
7	pt	Produtos	\N	\N	\N	produtos	produtos	f	\N	2018-03-18 02:20:22.498279+00	t	t	0	7	8
19	pt	Contato				contato	contato	f	\N	2018-03-22 02:59:42.05283+00	t	t	0	19	20
8	pt	Produtos	\N	\N	\N	produtos	produtos	f	\N	2018-03-18 02:20:22.498279+00	t	f	1	8	7
20	pt	Contato				contato	contato	f	\N	2018-03-22 02:59:42.05283+00	t	f	1	20	19
\.


--
-- Data for Name: cms_treenode; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_treenode (id, path, depth, numchild, parent_id, site_id) FROM stdin;
1	0001	1	0	\N	1
8	0009	1	0	\N	1
7	0008	1	0	\N	1
11	0007	1	0	\N	1
2	0003	1	0	\N	1
4	0002	1	0	\N	1
3	0005	1	0	\N	1
12	0004	1	0	\N	1
14	00060002	2	0	6	1
6	0006	1	1	\N	1
\.


--
-- Data for Name: cms_urlconfrevision; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_urlconfrevision (id, revision) FROM stdin;
1	17708e29-dc45-4012-b38e-c2e4860b1482
\.


--
-- Data for Name: cms_usersettings; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_usersettings (id, language, clipboard_id, user_id) FROM stdin;
1	pt	1	1
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: bitnami
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2018-03-18 02:19:09.856401+00	4	Institucional	2		8	1
2	2018-03-18 02:19:26.505552+00	1	Página Inicial	2		8	1
3	2018-03-18 02:19:36.167027+00	3	Planos	2		8	1
4	2018-03-18 02:20:53.638721+00	7	Produtos	2		8	1
5	2018-03-18 02:24:27.983196+00	1	admin	2	[{"changed": {"fields": ["password"]}}]	3	1
6	2018-03-20 00:56:17.486667+00	1	Página Inicial Menu	2	[{"changed": {"fields": ["menu_title", "page_title"]}}]	8	1
7	2018-03-20 01:00:35.981223+00	3	Planos	2	[]	8	1
8	2018-03-20 08:16:58.497405+00	9	teste	1	[{"added": {}}]	8	1
9	2018-03-20 08:21:56.228943+00	1	Página Inicial	2	[{"changed": {"fields": ["menu_title"]}}]	8	1
10	2018-03-20 08:37:55.971733+00	3	Planos	2	[{"changed": {"fields": ["redirect", "template", "navigation_extenders", "xframe_options"]}}]	8	1
11	2018-03-20 08:38:06.493049+00	3	Planos	2		8	1
12	2018-03-20 08:46:42.222371+00	3	Planos	2	[{"changed": {"fields": ["redirect", "xframe_options"]}}]	8	1
13	2018-03-20 09:21:24.725862+00	4	checklist.jpg	3		30	1
14	2018-03-20 09:31:30.835851+00	7	Produtos	2		8	1
15	2018-03-20 09:56:09.336234+00	3	Planos	2		8	1
16	2018-03-20 09:58:23.001301+00	7	Produtos	2		8	1
17	2018-03-20 10:09:22.005019+00	10	Minha Página	1	[{"added": {}}]	8	1
18	2018-03-20 10:10:11.82843+00	9	teste	3		8	1
19	2018-03-20 10:26:05.354081+00	38	Meu Plano	3		58	1
20	2018-03-20 10:27:10.822172+00	30	default	3		59	1
21	2018-03-20 10:27:26.058834+00	10	Minha Página	2		8	1
22	2018-03-20 10:45:30.223572+00	10	Minha Página	2		8	1
23	2018-03-20 10:52:42.572687+00	74	site_icon_construf.png	3		54	1
24	2018-03-20 10:58:59.970558+00	4	Institucional	2		8	1
25	2018-03-20 11:00:17.15416+00	4	Institucional	2		8	1
26	2018-03-20 11:01:34.399726+00	4	Institucional	2		8	1
27	2018-03-20 11:03:07.388813+00	4	Institucional	2		8	1
28	2018-03-20 11:08:07.472566+00	2	Fausto	1	[{"added": {}}]	3	1
29	2018-03-20 11:11:30.409796+00	1	Editor de Produtos	1	[{"added": {}}]	2	1
30	2018-03-20 11:11:48.077742+00	2	Fausto	2	[]	3	1
31	2018-03-20 11:14:42.777281+00	1	Página Inicial	2		8	1
32	2018-03-20 11:17:20.423517+00	4	Institucional	2		8	1
33	2018-03-20 11:36:36.263725+00	10	Minha Página	2		8	1
34	2018-03-21 02:55:11.281497+00	12	Entrar	1	[{"added": {}}]	8	1
35	2018-03-21 02:55:22.471626+00	12	Entrar	2	[{"changed": {"fields": ["overwrite_url", "xframe_options"]}}]	8	1
36	2018-03-21 02:55:40.237767+00	12	Entrar	2	[{"changed": {"fields": ["limit_visibility_in_menu"]}}]	8	1
37	2018-03-21 02:57:08.718669+00	12	Entrar	2		8	1
38	2018-03-21 15:45:24.709703+00	1	Página Inicial	2		8	1
39	2018-03-21 15:47:48.85035+00	1	Página Inicial	2		8	1
40	2018-03-22 00:56:27.343925+00	152	nav-tabs 	3		76	1
41	2018-03-22 01:01:13.573544+00	153		3		61	1
42	2018-03-22 01:02:15.106498+00	154		3		51	1
43	2018-03-22 01:11:08.787414+00	14	Sair	1	[{"added": {}}]	8	1
44	2018-03-22 01:11:26.833631+00	14	Sair	2	[{"changed": {"fields": ["login_required", "limit_visibility_in_menu"]}}]	8	1
45	2018-03-22 01:12:29.597041+00	14	Sair	2	[{"changed": {"fields": ["overwrite_url", "xframe_options"]}}]	8	1
46	2018-03-22 01:17:16.503212+00	1	Página Inicial	2		8	1
47	2018-03-22 01:18:00.08161+00	151	size-sm 	3		60	1
48	2018-03-22 01:25:12.880943+00	20	checklist.jpg	3		54	1
49	2018-03-22 01:27:47.340718+00	155	Texto coluna 3...	3		24	1
50	2018-03-22 01:27:54.847688+00	162	Texto coluna 4...	3		24	1
51	2018-03-22 01:28:58.434013+00	159	50%	3		35	1
52	2018-03-22 01:29:05.681904+00	160	50%	3		35	1
53	2018-03-22 01:30:53.786123+00	1	Página Inicial	2		8	1
54	2018-03-22 01:33:50.391104+00	1	Página Inicial	2		8	1
55	2018-03-22 02:00:53.668692+00	1	Construção Civil	1	[{"added": {}}]	81	1
56	2018-03-22 02:02:40.742694+00	2	Projeto Hidráulico	1	[{"added": {}}]	80	1
57	2018-03-22 02:04:11.550551+00	1	Teste da informação. adfasdk adaça	1	[{"added": {}}, {"added": {"name": "item checklist", "object": "presente"}}]	78	1
58	2018-03-22 02:06:37.816395+00	1	Levantamento Preliminar	1	[{"added": {}}]	83	1
59	2018-03-22 02:06:59.316611+00	2	Execução	1	[{"added": {}}]	83	1
60	2018-03-22 02:15:54.356538+00	10	Minha Página	2		8	1
61	2018-03-22 02:19:35.966051+00	1	admin	2	[]	19	1
62	2018-03-22 02:27:09.069232+00	1	Editor de Produtos	2	[]	2	1
63	2018-03-22 02:27:43.160769+00	1	Editor de Produtos	2	[]	2	1
64	2018-03-22 02:30:07.47356+00	1	Editor de Produtos	2	[]	2	1
65	2018-03-22 02:32:51.889895+00	14	Sair	2		8	1
66	2018-03-22 02:36:46.855476+00	3	Planos	2		8	1
67	2018-03-22 02:40:00.119469+00	16	Teste	1	[{"added": {}}]	8	1
68	2018-03-22 02:40:26.711485+00	17	Teste	1	[{"added": {}}]	8	1
69	2018-03-22 02:41:04.237107+00	16	Teste	3		8	1
70	2018-03-22 02:42:11.242658+00	17	Teste	2	[{"changed": {"fields": ["navigation_extenders", "application_urls", "application_namespace", "xframe_options"]}}]	8	1
71	2018-03-22 02:42:33.472579+00	17	Teste	2		8	1
72	2018-03-22 02:46:14.147218+00	17	Teste	3		8	1
73	2018-03-22 02:46:56.031323+00	10	Minha Página	2	[{"changed": {"fields": ["limit_visibility_in_menu"]}}]	8	1
74	2018-03-22 02:47:12.285091+00	10	Minha Página	2		8	1
75	2018-03-22 02:55:02.631681+00	1	Página Inicial	2		8	1
76	2018-03-22 02:59:42.239989+00	19	Contato	1	[{"added": {}}]	8	1
77	2018-03-22 03:00:37.703206+00	19	Contato	2		8	1
78	2018-03-22 03:12:02.026595+00	19	Contato	2		8	1
79	2018-03-22 03:14:04.366437+00	19	Contato	2		8	1
80	2018-03-22 03:19:09.58972+00	1	Página Inicial	2	[{"changed": {"fields": ["title"]}}]	8	1
81	2018-03-22 03:20:18.616009+00	1	Início	2	[{"changed": {"fields": ["menu_title", "page_title"]}}]	8	1
82	2018-03-22 03:20:47.969757+00	1	Início	2		8	1
83	2018-03-22 03:47:03.194252+00	1	Início	2		8	1
84	2018-03-22 03:56:58.242562+00	1	Início	2		8	1
85	2018-03-22 03:57:47.208454+00	1	Início	2		8	1
86	2018-03-22 03:59:27.513075+00	21	Assine já!	1	[{"added": {}}]	8	1
87	2018-03-22 03:59:45.980944+00	21	Assine já!	2		8	1
88	2018-03-22 04:00:06.418272+00	21	Assine já!	2	[{"changed": {"fields": ["limit_visibility_in_menu"]}}]	8	1
89	2018-03-22 04:01:59.260526+00	21	Assine já!	2		8	1
90	2018-03-22 04:05:45.881598+00	3	Planos	2		8	1
91	2018-03-22 04:06:29.512813+00	1	Início	2		8	1
92	2018-03-22 04:09:25.428768+00	7	Produtos	2		8	1
93	2018-03-22 10:27:45.545341+00	2	footer-theme	2	[]	17	1
94	2018-03-22 10:51:56.026184+00	10	Minha Página	2		8	1
95	2018-03-22 10:57:30.315683+00	10	Minha Página	2		8	1
96	2018-03-22 10:58:26.974877+00	10	Minha Página	2		8	1
97	2018-03-22 11:04:16.383714+00	10	Minha Página	2		8	1
98	2018-03-22 11:06:03.392854+00	10	Minha Página	2		8	1
99	2018-03-22 11:19:22.000947+00	10	Minha Página	2		8	1
100	2018-03-22 11:20:33.088593+00	10	Minha Página	2		8	1
101	2018-03-22 11:21:36.881968+00	10	Minha Página	2		8	1
102	2018-03-22 23:49:22.793035+00	1	Início	2		8	1
103	2018-03-22 23:51:26.725335+00	1	Início	2		8	1
104	2018-03-23 00:11:31.242385+00	23	Lista de Análises	1	[{"added": {}}]	8	1
105	2018-03-23 00:12:40.585798+00	23	Lista de Análises	2	[{"changed": {"fields": ["application_urls", "application_namespace", "xframe_options"]}}]	8	1
106	2018-03-23 00:23:34.51098+00	23	Lista de Análises	2		8	1
107	2018-03-26 00:13:19.217251+00	25	Análises	1	[{"added": {}}]	8	1
108	2018-03-26 00:14:04.530877+00	25	Análises	2	[{"changed": {"fields": ["application_urls", "application_namespace", "xframe_options"]}}]	8	1
109	2018-03-26 00:14:12.640041+00	25	Análises	2		8	1
110	2018-03-26 00:15:18.208058+00	1	Construção Civil	1	[{"added": {}}]	81	1
111	2018-03-26 00:15:35.555025+00	1	Pré-projeto	1	[{"added": {}}]	80	1
112	2018-03-26 00:15:44.830111+00	2	Projeto para execução	1	[{"added": {}}]	80	1
113	2018-03-26 00:16:19.357643+00	1	Plano Básico	1	[{"added": {}}]	86	1
114	2018-03-26 00:19:32.284767+00	2	Projeto para execução	3		80	1
115	2018-03-26 00:19:57.101+00	1	Instalação Hidrosanitária	2	[{"changed": {"fields": ["nome"]}}]	80	1
116	2018-03-26 00:20:09.155333+00	3	Instalações elétricas de baixa tensão	1	[{"added": {}}]	80	1
117	2018-03-26 00:20:22.833476+00	1	Pré-projeto	1	[{"added": {}}]	83	1
118	2018-03-26 00:20:48.013161+00	2	Projeto para execução	1	[{"added": {}}]	83	1
119	2018-03-26 00:23:03.170515+00	1	Checklist básico para instalação elétrica	1	[{"added": {}}]	87	1
120	2018-03-26 00:24:51.139291+00	1	Meio  destinado  a  impedir  contato  com  partes  vivas  perigosas  em  condições   normais	1	[{"added": {}}, {"added": {"name": "item checklist", "object": "presente"}}, {"added": {"name": "item checklist", "object": "parcial"}}, {"added": {"name": "item checklist", "object": "ausente"}}]	78	1
121	2018-03-26 00:26:39.918488+00	2	Proteção contra choques elétricos	1	[{"added": {}}, {"added": {"name": "item checklist", "object": "presente"}}, {"added": {"name": "item checklist", "object": "ausente"}}, {"added": {"name": "item checklist", "object": "parcial"}}]	78	1
122	2018-03-26 00:31:18.462587+00	23	Lista de Análises	3		8	1
123	2018-03-26 00:31:53.11364+00	25	Análises	2	[{"changed": {"fields": ["template", "application_namespace", "xframe_options"]}}]	8	1
124	2018-03-26 01:11:52.591571+00	10	Minha Página	2	[{"changed": {"fields": ["xframe_options"]}}]	8	1
125	2018-03-26 02:57:23.097047+00	25	Análises	2		8	1
126	2018-03-26 09:43:41.762052+00	10	Minha Página	2		8	1
127	2018-03-26 09:48:13.769454+00	10	Minha Página	2		8	1
128	2018-03-26 09:49:17.566954+00	10	Minha Página	2		8	1
129	2018-03-26 09:51:28.010777+00	54	Análises em Andamento	3		75	1
130	2018-03-26 09:51:35.515547+00	10	Minha Página	2		8	1
131	2018-03-26 09:54:34.110479+00	53	Nova Análise	3		75	1
132	2018-03-26 09:56:14.034194+00	52	nav-tabs 	3		76	1
133	2018-03-26 09:58:41.277267+00	10	Minha Página	2		8	1
134	2018-03-26 10:01:44.836574+00	10	Minha Página	2		8	1
135	2018-03-26 10:06:58.388216+00	3	Planos	2		8	1
136	2018-03-26 10:08:04.947986+00	3	Planos	2		8	1
137	2018-03-26 10:11:48.952624+00	10	Minha Página	2		8	1
138	2018-03-26 10:13:03.39071+00	604	Sobre a Análise...	3		24	1
139	2018-03-26 10:13:08.375514+00	10	Minha Página	2		8	1
140	2018-03-26 10:14:38.450033+00	122	Administração de Produtos	3		62	1
141	2018-03-26 10:15:44.208294+00	10	Minha Página	2		8	1
142	2018-03-26 12:06:23.369173+00	21	Assine já!	2		8	1
143	2018-03-26 12:06:54.150061+00	21	Assine já!	2		8	1
144	2018-03-26 12:09:44.309986+00	4	Institucional	2		8	1
145	2018-03-26 12:12:16.959554+00	19	Contato	2		8	1
146	2018-04-03 01:20:24.082386+00	1	Início	2		8	1
147	2018-04-03 01:33:22.545771+00	1	Início	2		8	1
148	2018-04-03 01:35:25.033155+00	1	Início	2		8	1
149	2018-04-03 01:37:36.816564+00	1	Início	2		8	1
150	2018-04-03 01:50:45.780631+00	1	Início	2		8	1
151	2018-04-03 01:55:08.185276+00	19	Teste col2 Texto...	3		24	1
152	2018-04-03 02:01:16.088888+00	1	Início	2		8	1
153	2018-04-03 02:04:06.105129+00	1	Início	2		8	1
154	2018-04-03 02:05:02.250278+00	1	Início	2		8	1
155	2018-04-03 02:12:22.368181+00	1	Início	2		8	1
156	2018-04-03 02:16:55.168181+00	1	Início	2		8	1
157	2018-04-03 12:24:23.858967+00	2	Fausto	2	[]	3	1
158	2018-04-03 12:27:41.96709+00	7	checklist1.png	3		54	1
159	2018-04-03 12:45:50.761872+00	7	Produtos	2		8	1
160	2018-04-03 12:52:23.704566+00	8	anlise-de-produtos.jpg	3		54	1
161	2018-04-03 12:55:09.041704+00	7	Produtos	2		8	1
162	2018-04-03 13:00:19.834521+00	4	1	3		63	1
163	2018-04-03 13:01:12.57258+00	7	Produtos	2		8	1
164	2018-04-03 13:07:10.156121+00	817		3		24	1
165	2018-04-03 13:17:46.000676+00	803	803	3		70	1
166	2018-04-03 13:19:58.272453+00	7	Produtos	2		8	1
167	2018-04-03 13:21:43.323563+00	7	Produtos	2		8	1
168	2018-04-03 13:27:21.787854+00	7	Produtos	2		8	1
169	2018-04-03 13:30:24.745857+00	7	Produtos	2		8	1
170	2018-04-03 13:44:00.953022+00	1	Início	2		8	1
171	2018-04-03 13:46:43.44703+00	1	Início	2		8	1
172	2018-04-03 13:48:55.180207+00	1	Início	2		8	1
173	2018-04-03 13:56:01.183093+00	1	Início	2		8	1
174	2018-04-03 13:59:23.531051+00	7	Produtos	2		8	1
175	2018-04-03 14:16:48.78672+00	4	Institucional	2		8	1
176	2018-04-03 14:18:36.54976+00	4	Institucional	2		8	1
177	2018-04-03 14:19:19.616759+00	75	1	3		63	1
178	2018-04-03 14:19:29.788769+00	4	Institucional	2		8	1
179	2018-04-03 14:29:40.507617+00	4	Institucional	2		8	1
180	2018-04-03 14:33:25.301913+00	4	Institucional	2		8	1
181	2018-04-03 14:35:21.488268+00	4	Institucional	2		8	1
182	2018-04-03 14:37:06.245599+00	4	Institucional	2		8	1
183	2018-04-03 14:40:19.715974+00	4	Institucional	2		8	1
184	2018-04-04 00:10:32.71676+00	19	Contato	2		8	1
185	2018-04-04 00:27:45.501161+00	19	Contato	2		8	1
186	2018-04-04 00:40:12.695736+00	19	Contato	2		8	1
187	2018-04-04 00:44:33.51454+00	19	Contato	2		8	1
188	2018-04-04 00:46:12.000065+00	19	Contato	2		8	1
189	2018-04-04 01:08:11.018051+00	19	Contato	2		8	1
190	2018-04-04 01:09:19.233732+00	967	mapa_contato.jpg	3		54	1
191	2018-04-04 01:09:34.287772+00	965	TESTE	3		41	1
192	2018-04-04 01:09:53.178833+00	19	Contato	2		8	1
193	2018-04-04 01:19:57.22314+00	19	Contato	2		8	1
194	2018-04-04 01:28:58.468058+00	19	Contato	2		8	1
195	2018-04-04 09:27:26.989511+00	19	Contato	2		8	1
196	2018-04-04 09:31:11.241165+00	19	Contato	2		8	1
197	2018-04-04 10:36:34.899849+00	19	Contato	2		8	1
198	2018-04-04 10:37:30.726552+00	19	Contato	2		8	1
199	2018-04-04 11:04:40.766865+00	1	Basico	1	[{"added": {}}]	42	1
200	2018-04-04 11:05:14.998658+00	19	Contato	2		8	1
201	2018-04-04 11:08:41.569764+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
202	2018-04-04 11:09:36.271464+00	19	Contato	2		8	1
203	2018-04-04 11:12:20.32398+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
204	2018-04-04 11:13:43.090687+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
205	2018-04-04 11:14:12.386619+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
206	2018-04-04 11:15:31.489127+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
207	2018-04-04 11:15:42.276525+00	19	Contato	2		8	1
208	2018-04-04 11:18:04.054887+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
209	2018-04-04 11:19:37.978826+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
210	2018-04-04 11:23:28.347453+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
211	2018-04-04 11:24:29.731648+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
212	2018-04-04 11:26:43.639275+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
213	2018-04-04 11:28:02.759012+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
214	2018-04-04 11:30:00.897852+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
215	2018-04-04 11:33:27.803364+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
216	2018-04-04 11:35:13.729553+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
217	2018-04-04 11:38:02.79704+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
218	2018-04-04 11:41:54.248311+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
219	2018-04-04 11:46:26.416451+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
220	2018-04-04 11:48:43.332789+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
221	2018-04-04 11:50:05.560621+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
222	2018-04-04 11:51:08.857929+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
223	2018-04-04 11:52:23.458449+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
224	2018-04-04 11:53:58.608979+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
225	2018-04-04 11:55:03.744039+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
226	2018-04-04 11:56:36.260363+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
227	2018-04-04 11:59:17.469814+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
228	2018-04-04 12:00:02.170751+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
229	2018-04-04 12:01:24.734758+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
230	2018-04-04 12:01:36.109075+00	19	Contato	2		8	1
231	2018-04-04 12:04:25.186988+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
232	2018-04-04 12:07:05.325278+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
233	2018-04-04 12:09:42.576855+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
234	2018-04-04 12:11:20.140645+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
235	2018-04-04 12:13:52.360483+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
236	2018-04-04 12:14:51.447389+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
237	2018-04-04 12:16:56.21981+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
238	2018-04-04 12:19:14.487127+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
239	2018-04-04 12:21:10.709979+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
240	2018-04-04 12:21:21.732347+00	19	Contato	2		8	1
241	2018-04-04 12:22:53.050394+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
242	2018-04-04 12:26:24.81637+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
243	2018-04-04 12:27:52.270879+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
244	2018-04-04 12:29:44.131986+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
245	2018-04-04 12:31:52.384848+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
246	2018-04-04 12:33:41.197288+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
247	2018-04-04 12:34:54.376383+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
248	2018-04-04 12:36:09.482209+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
249	2018-04-04 12:38:54.556633+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
250	2018-04-04 12:41:14.002997+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
251	2018-04-04 12:42:05.801757+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
252	2018-04-04 12:42:37.576183+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
253	2018-04-04 12:43:42.590424+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
254	2018-04-04 12:45:34.217845+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
255	2018-04-04 12:47:37.983342+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
256	2018-04-04 12:48:44.734567+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
257	2018-04-04 12:49:26.407001+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
258	2018-04-04 12:49:36.456489+00	19	Contato	2		8	1
259	2018-04-04 12:50:37.613872+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
260	2018-04-04 12:50:51.993561+00	19	Contato	2		8	1
261	2018-04-04 12:51:30.321196+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
262	2018-04-04 12:51:39.708901+00	19	Contato	2		8	1
263	2018-04-04 13:05:19.21883+00	1003	fone.png	3		54	1
264	2018-04-04 13:08:09.610614+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
265	2018-04-04 13:09:19.8782+00	1	Basico	2	[{"changed": {"fields": ["html"]}}]	42	1
266	2018-04-04 13:09:43.195613+00	19	Contato	2		8	1
267	2018-04-05 02:18:14.179401+00	1007	1007	3		77	1
268	2018-04-05 02:25:57.738212+00	904	904	3		70	1
269	2018-04-05 07:50:28.336899+00	1	Style: standard, Transition effect: slide, Ride: True, Interval: 4000, Aspect ratio: 16x10	3		64	1
270	2018-04-05 07:56:23.45085+00	3	Planos	2		8	1
271	2018-04-05 07:59:33.193297+00	1011	topo_planos.jpg	3		54	1
272	2018-04-05 08:05:36.350613+00	3	Planos	2		8	1
273	2018-04-05 08:09:12.101533+00	23	sidebar	3		7	1
274	2018-04-05 08:11:46.327299+00	3	Planos	2	[{"changed": {"fields": ["template", "navigation_extenders", "xframe_options"]}}]	8	1
275	2018-04-05 08:27:16.297693+00	2	tabela planos	1	[{"added": {}}]	42	1
276	2018-04-05 08:37:29.106598+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
277	2018-04-05 08:37:57.95297+00	3	Planos	2		8	1
278	2018-04-05 08:39:11.703421+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
279	2018-04-05 08:39:24.202652+00	3	Planos	2		8	1
280	2018-04-05 08:44:12.563483+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
281	2018-04-05 08:51:32.473793+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
282	2018-04-05 09:00:10.216238+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
283	2018-04-05 09:02:09.889011+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
284	2018-04-05 09:05:32.673407+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
285	2018-04-05 09:07:33.20631+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
286	2018-04-05 09:09:23.743985+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
287	2018-04-05 09:10:52.551917+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
288	2018-04-05 09:18:50.858962+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
289	2018-04-05 09:20:14.709056+00	3	Planos	2		8	1
290	2018-04-05 14:22:16.944825+00	2	Checklist Projeto	1	[{"added": {}}]	87	1
291	2018-04-05 14:34:36.439969+00	3	Há informações de que as bacias sanitárias são de volume de descarga de acordo com as especificações da ABNT NBR 15097-1?	1	[{"added": {}}, {"added": {"name": "item checklist", "object": "ausente"}}, {"added": {"name": "item checklist", "object": "parcial"}}]	78	1
292	2018-04-07 15:10:59.920434+00	4	Foi informada a vida útil de projeto (VUP) do sistema e/ou dos elementos/componentes deste projeto?	1	[{"added": {}}]	78	1
293	2018-04-07 15:16:29.463537+00	5	Foram especificados materiais, produtos e processos que atendam ao desempenho estabelecido no PDE, com base nas normas prescritivas e desempenhos declarados pelos fabricantes dos produtos?	1	[{"added": {}}]	78	1
294	2018-04-07 15:58:20.418001+00	6	Foram especificados os métodos de controle necessários para comprovar o atendimento ao desempenho projetado (ex.: definição de ensaios, análises, verificações a fazer e suas respectivas frequências, a	1	[{"added": {}}]	78	1
295	2018-04-07 15:59:55.923997+00	7	Há informações sobre prazos de garantia dos componentes, elementos e/ou sistemas projetados?	1	[{"added": {}}]	78	1
296	2018-04-07 16:02:00.478954+00	8	Há informações de atendimento às normas de instalações?	1	[{"added": {}}]	78	1
297	2018-04-07 16:03:15.193028+00	9	- Há informação ou menção de que o Projeto hidrossanitário contemple soluções para uso racional e reuso de água?	1	[{"added": {}}]	78	1
298	2018-04-07 16:04:44.93703+00	10	- Há informação ou menção de que os projetos contemplem soluções para minimização do consumo de energia, como iluminação e ventilação natural e sistemas de aquecimento baseados em energia alternativa?	1	[{"added": {}}]	78	1
299	2018-04-07 16:06:37.027602+00	11	Para tubulações fixas no teto ou em outros elementos estruturais, há informações de que os fixadores ou suportes das tubulações, aparentes ou não, assim como as próprias tubulações, resistem, sem entr	1	[{"added": {}}]	78	1
300	2018-04-07 16:08:02.355968+00	12	Para as tubulações submetidas a esforços dinâmicos significativos, por exemplo, tubulações de recalque ou água quente, estes esforços foram levados em consideração?	1	[{"added": {}}]	78	1
301	2018-04-07 16:10:26.901557+00	13	Há informações sobre berços e envelopamentos, ou berços ou envelopamentos consubstanciados em memórias de cálculo constantes no projeto ou em literaturas especializadas, que garantem a preservação da	1	[{"added": {}}]	78	1
302	2018-04-07 16:11:45.283795+00	14	O projeto hidrossanitário especifica nos pontos de transição (parede x piso, parede x pilar etc.) dispositivos que assegurem a não transmissão dos esforços para a tubulação?	1	[{"added": {}}]	78	1
303	2018-04-07 16:12:51.622005+00	15	Há informações de que as válvulas de descarga, metais de fechamento rápido e do tipo monocomando não irão provocar sobrepressões no fechamento superiores a 0,2 Mpa?	1	[{"added": {}}]	78	1
304	2018-04-07 16:14:21.383895+00	16	Há informações de que o sistema hidrossanitário atende à pressão estática máxima estabelecida na ABNT NB 5626?	1	[{"added": {}}]	78	1
305	2018-04-07 16:15:21.366724+00	17	"Há informações sobre a velocidade do fluido no interior das tubulações quando da parada de bombas de recalques? - A velocidade deve ser inferior a 10 m/s. - O projeto pode estabelecer velocidades aci	1	[{"added": {}}]	78	1
306	2018-04-07 16:16:20.553782+00	18	Há informações de que as tubulações aparentes fixadas até 1,5 m acima do piso resistem aos impactos que possa ocorrer durante a vida útil de projeto, sem sofrem perda de funcionalidade ou ruína?	1	[{"added": {}}]	78	1
307	2018-04-07 16:17:30.654032+00	19	Há informações sobre o volume de água reservado para combate a incêndio conforme legislação vigente ou, na sua ausência, conforme norma aplicável da ABNT?	1	[{"added": {}}]	78	1
308	2018-04-07 16:19:33.950241+00	20	Há informações sobre a classificação e o posicionamento do extintores?	1	[{"added": {}}]	78	1
309	2018-04-07 16:21:28.144261+00	21	Há informações de que as prumadas de esgoto sanitário e ventilação instaladas aparentes, em alvenaria ou no interior de dutos verticais (shafts), tenham sido fabricadas com material não propagante de	1	[{"added": {}}]	78	1
310	2018-04-07 16:22:15.990098+00	20	Há informações sobre a classificação e o posicionamento do extintores?	2	[{"changed": {"fields": ["norma"]}}]	78	1
311	2018-04-07 16:23:13.988057+00	19	Há informações sobre o volume de água reservado para combate a incêndio conforme legislação vigente ou, na sua ausência, conforme norma aplicável da ABNT?	2	[{"changed": {"fields": ["norma"]}}]	78	1
312	2018-04-07 16:24:19.555995+00	18	Há informações de que as tubulações aparentes fixadas até 1,5 m acima do piso resistem aos impactos que possa ocorrer durante a vida útil de projeto, sem sofrem perda de funcionalidade ou ruína?	2	[{"changed": {"fields": ["norma"]}}]	78	1
313	2018-04-07 16:24:57.82348+00	16	Há informações de que o sistema hidrossanitário atende à pressão estática máxima estabelecida na ABNT NB 5626?	2	[{"changed": {"fields": ["norma"]}}]	78	1
314	2018-04-07 16:26:00.158824+00	15	Há informações de que as válvulas de descarga, metais de fechamento rápido e do tipo monocomando não irão provocar sobrepressões no fechamento superiores a 0,2 Mpa?	2	[{"changed": {"fields": ["norma"]}}]	78	1
315	2018-04-07 16:26:58.364447+00	9	- Há informação ou menção de que o Projeto hidrossanitário contemple soluções para uso racional e reuso de água?	2	[{"changed": {"fields": ["norma"]}}]	78	1
316	2018-04-07 16:27:41.467082+00	8	Há informações de atendimento às normas de instalações?	2	[{"changed": {"fields": ["norma"]}}]	78	1
317	2018-04-07 16:29:39.922691+00	22	Há informações de que todas as tubulações, equipamentos e acessórios do sistema hidrossanitário tenham sido direta ou indiretamente aterrados conforme ABNT 5410?	1	[{"added": {}}]	78	1
318	2018-04-07 16:31:12.114544+00	23	Há informações de que os equipamentos do sistema hidrossanitário atendem às especificações, quanto à corrente de fuga,  da ABNT NBR 12090 e ABNT NBR 14016, limitando-se à corrente de fuga para outros	1	[{"added": {}}]	78	1
319	2018-04-07 16:31:48.471203+00	22	Há informações de que todas as tubulações, equipamentos e acessórios do sistema hidrossanitário tenham sido direta ou indiretamente aterrados conforme ABNT 5410?	2	[{"changed": {"fields": ["requisito_geral", "requisito_especifico"]}}]	78	1
320	2018-04-07 16:33:33.460556+00	24	Há informações de que os aparelhos elétricos de acumulação para o aquecimento de água sejam providos de dispositivo de alívio para o caso de sobrepressão e também de dispositivo de segurança que corte	1	[{"added": {}}]	78	1
321	2018-04-07 16:35:24.873286+00	25	Há informações de que o funcionamento do equipamento a gás combustível, instalado em ambientes residenciais tenha sido feito de maneira que a concentração máxima de CO2 não ultrapasse o valor de 0,5 %	1	[{"added": {}}]	78	1
322	2018-04-07 16:36:10.030968+00	24	Há informações de que os aparelhos elétricos de acumulação para o aquecimento de água sejam providos de dispositivo de alívio para o caso de sobrepressão e também de dispositivo de segurança que corte	2	[{"changed": {"fields": ["norma"]}}]	78	1
323	2018-04-07 16:37:38.473475+00	26	Há informações de que as peças de utilização e demais componentes dos sistemas hidrossanitários que são manipulados pelos usuários não possuem contos vivos ou superfícies ásperas conforme normas aplic	1	[{"added": {}}]	78	1
324	2018-04-07 16:39:20.257302+00	27	Há informações de que as peças e aparelhos sanitários possuem resistência mecânica aos esforços a que serão submetidos na sua utilização apresentando atendimento às normas aplicáveis?	1	[{"added": {}}]	78	1
325	2018-04-07 16:41:14.502203+00	28	Há informações de que as possibilidades de mistura de água fria, regulagem de vazão e outras técnicas existentes no sistema hidrossanitário, no limite de sua aplicação, permitem que a regulagem de tem	1	[{"added": {}}]	78	1
326	2018-04-07 16:42:36.812363+00	29	"Há informações de que as tubulações do sistema predial de água não apresentem vazamento quando submetidas, durante 1 h, à pressão hidrostática de 1,5 vez o valor da pressão prevista em projeto? - Em	1	[{"added": {}}]	78	1
327	2018-04-07 16:44:22.504794+00	30	Há informações sobre a estanqueidade das peças de utilização e dos metais sanitários conforme normas aplicáveis?	1	[{"added": {}}]	78	1
328	2018-04-07 16:45:46.869163+00	31	Há informações sobre a estanqueidade dos reservatórios conforme normas aplicáveis?	1	[{"added": {}}]	78	1
329	2018-04-07 16:47:07.221894+00	32	Há informações de que as tubulações dos sistemas prediais de esgoto sanitário e de águas pluviais não apresentam vazamento quando submetidas, durante 15 min, à pressão estática de 60 kPa, em caso de á	1	[{"added": {}}]	78	1
330	2018-04-07 16:48:08.179411+00	33	Há informações sobre a estanqueidade das calhas, com todos os seus componentes do sistema predial de águas pluviais?	1	[{"added": {}}]	78	1
331	2018-04-07 17:05:52.142705+00	34	Há informações sobre os limites de ruídos gerados durante o ciclo de utilização dos aparelhos hidrossanitário??	1	[{"added": {}}]	78	1
332	2018-04-07 17:07:02.804924+00	35	Foi estabelecida a vida útil de projeto?	1	[{"added": {}}]	78	1
333	2018-04-07 17:12:14.856917+00	36	As informações disponíveis neste projeto atendem ao Anexo A conforme NBR 13531, incluindo os conteúdos e produtos requeridos para esta etapa?	1	[{"added": {}}]	78	1
334	2018-04-07 17:14:11.250913+00	37	Há informações sobre a durabilidade dos elementos, componentes e instalações dos sistemas hidrossanitários, compatíveis com a vida útil de projeto?	1	[{"added": {}}]	78	1
335	2018-04-07 17:17:00.165251+00	38	Há informações de que para as tubulações de esgoto e águas pluviais tenham sido previstos dispositivos de inspeção nas condições descritas nas normas aplicáveis?	1	[{"added": {}}]	78	1
336	2018-04-07 17:18:02.671612+00	39	Há informações sobre as condições de uso, operação e manutenção, incluindo o "Como Construído", do sistema hidrossanitário, de seus elementos ou componente?	1	[{"added": {}}]	78	1
337	2018-04-07 17:19:26.922195+00	40	"- Há informações de que o sistema de água potável seja separado fisicamente de qualquer outra instalação que conduza água não potável de qualidade insatisfatória, desconhecida ou questionável? - Há i	1	[{"added": {}}]	78	1
338	2018-04-07 17:20:45.260446+00	41	Há informações de que a superfície interna dos materiais que fiquem em contato com a água seja lisa e de material lavável para evitar formação de biofilme (atenção para os reservatórios!)?	1	[{"added": {}}]	78	1
339	2018-04-07 17:22:07.689712+00	42	Há informações de que os componentes da instalação hidráulica não permitem o empoçamento de água e nem a sua estagnação  causada pela insuficiência de renovação?	1	[{"added": {}}]	78	1
340	2018-04-07 17:23:51.9231+00	43	Há informações sobre a proteção, dos componentes do sistema de instalação enterrados, contra a entrada de animais ou corpos estranhos, bem como de líquidos que possam contaminar a água potável, em con	1	[{"added": {}}]	78	1
341	2018-04-07 17:25:12.12532+00	44	Há informações sobre a separação atmosférica, física ou mediante equipamentos, em conformidade com ABNT NBR 5626?	1	[{"added": {}}]	78	1
342	2018-04-07 17:26:18.114936+00	45	Há informações de que o sistema de esgoto sanitário não permite a retrossifonagem ou quebra do fecho hídrico?	1	[{"added": {}}]	78	1
343	2018-04-07 17:26:43.635558+00	40	Há informações de que o sistema de água potável seja separado fisicamente de qualquer outra instalação que conduza água não potável de qualidade insatisfatória, desconhecida ou questionável? - Há i	2	[{"changed": {"fields": ["requisito"]}}]	78	1
344	2018-04-07 17:29:23.447692+00	46	Há informações sobre o teor de CO2 e de CO nos ambientes? - CO2 deve ser inferior a 0,5 %. - CO deve ser inferior a 30 ppm.	1	[{"added": {}}]	78	1
345	2018-04-07 17:32:31.574227+00	47	Há informações de que o sistema predial de água fria e quente fornece água na pressão, vazão e volume compatíveis com o uso, associado a cada ponto de utilização, consideram a possibilidade de uso sim	1	[{"added": {}}]	78	1
346	2018-04-07 17:33:41.836104+00	48	Há informações de que as caixas e válvulas de descarga atendem ao disposto nas normas aplicáveis, no que se refere à vazão e volume de descarga?	1	[{"added": {}}]	78	1
347	2018-04-07 17:35:07.966622+00	49	Há informações sobre a capacidade do sistema predial de esgoto de coletar e afastar os efluentes gerados pela edificação habitacional, nas vazões com que normalmente são descarregados os aparelhos sem	1	[{"added": {}}]	78	1
348	2018-04-07 17:36:33.973664+00	50	Há informações de que as calhas e condutores suportam a vazão de projeto, calculada a partir da intensidade de chuva adotada para a localidade e para um certo período de retorno?	1	[{"added": {}}]	78	1
349	2018-04-07 17:38:13.338481+00	51	- Há informação ou menção de que louças e metais tenham sido especificados conforme sua norma técnica?	1	[{"added": {}}]	78	1
350	2018-04-07 17:43:10.350008+00	52	Há informações de que as bacias sanitárias são de volume de descarga de acordo com as especificações da ABNT NBR 15097-1?	1	[{"added": {}}]	78	1
351	2018-04-07 17:44:09.568709+00	53	Há informações de que as peças de utilização possuem vazões que permitem tornar o mais eficiente possível o uso da água nele utilizada?	1	[{"added": {}}]	78	1
352	2018-04-07 17:45:14.94019+00	54	Há informações sobre a ligação dos sistemas prediais de esgoto à rede pública de esgoto ou a um sistema localizado de tratamento e disposição de efluente, conforme as normas aplicáveis?	1	[{"added": {}}]	78	1
353	2018-04-07 17:46:47.039041+00	55	Projeto possui relação de requisitos regulamentares e estatutários atendidos, incluindo a versão? (normas, códigos etc.)	1	[{"added": {}}]	78	1
354	2018-04-09 13:20:45.649313+00	7	Produtos	2		8	1
355	2018-04-09 13:39:58.141688+00	7	Produtos	2		8	1
356	2018-04-09 13:42:58.238776+00	7	Produtos	2		8	1
357	2018-04-09 13:57:57.684783+00	7	Produtos	2		8	1
358	2018-04-09 14:03:49.418088+00	7	Produtos	2		8	1
359	2018-04-09 14:27:33.823104+00	4	Institucional	2		8	1
360	2018-04-09 14:28:44.352639+00	4	Institucional	2		8	1
361	2018-04-13 00:08:04.461444+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
362	2018-04-13 00:08:58.247785+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
363	2018-04-13 00:10:28.031044+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
364	2018-04-13 00:12:31.802147+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
365	2018-04-13 00:13:11.866102+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
366	2018-04-13 00:15:14.318069+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
367	2018-04-13 00:17:54.423863+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
368	2018-04-13 00:18:12.658711+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
369	2018-04-13 00:19:56.193815+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
370	2018-04-13 00:20:31.361302+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
371	2018-04-13 00:23:00.193403+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
372	2018-04-13 00:29:52.806357+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
373	2018-04-13 00:35:52.092806+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
374	2018-04-13 00:37:50.238744+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
375	2018-04-13 00:39:26.286179+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
376	2018-04-13 00:40:31.137118+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
377	2018-04-13 00:42:06.200903+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
378	2018-04-13 00:42:41.247913+00	3	Planos	2		8	1
379	2018-04-13 08:46:08.592967+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
380	2018-04-13 08:48:01.143282+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
381	2018-04-13 08:50:10.271666+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
382	2018-04-13 08:55:56.404939+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
383	2018-04-13 08:58:50.974285+00	3	Planos	2		8	1
384	2018-04-13 09:07:04.846244+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
385	2018-04-13 09:08:56.996872+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
386	2018-04-13 09:12:05.89584+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
387	2018-04-13 09:17:36.496291+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
388	2018-04-13 09:20:48.765823+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
389	2018-04-13 09:23:00.04267+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
390	2018-04-13 09:23:47.671859+00	2	tabela planos	2	[{"changed": {"fields": ["html"]}}]	42	1
391	2018-04-13 09:25:52.108497+00	3	Planos	2		8	1
392	2018-04-13 09:56:32.973244+00	10	Minha Página	2		8	1
393	2018-04-13 09:58:23.202627+00	10	Minha Página	2		8	1
394	2018-04-18 09:47:12.915932+00	7	Produtos	2		8	1
395	2018-04-18 10:02:22.226801+00	7	Produtos	2		8	1
396	2018-04-18 10:05:12.87579+00	7	Produtos	2		8	1
397	2018-04-18 10:11:50.981146+00	4	Institucional	2		8	1
398	2018-04-18 10:15:47.917676+00	4	Institucional	2		8	1
399	2018-04-18 10:19:47.546964+00	10	Minha Página	2		8	1
400	2018-04-18 10:23:25.780293+00	10	Minha Página	2		8	1
401	2018-04-18 20:15:13.36252+00	1	Início	2		8	1
402	2018-04-18 20:17:44.253285+00	10	Minha Página	2		8	1
403	2018-04-18 20:20:22.823932+00	10	Minha Página	2		8	1
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: bitnami
--

COPY django_content_type (id, app_label, model) FROM stdin;
1	auth	permission
2	auth	group
3	auth	user
4	contenttypes	contenttype
5	admin	logentry
6	sessions	session
7	cms	placeholder
8	cms	page
9	sites	site
10	cms	cmsplugin
11	cms	aliaspluginmodel
12	cms	globalpagepermission
13	cms	pagepermission
14	cms	pageuser
15	cms	pageusergroup
16	cms	placeholderreference
17	cms	staticplaceholder
18	cms	title
19	cms	usersettings
20	cms	urlconfrevision
21	cms	treenode
22	cms	pagetype
23	menus	cachekey
24	djangocms_text_ckeditor	text
25	filer	clipboard
26	filer	clipboarditem
27	filer	file
28	filer	folder
29	filer	folderpermission
30	filer	image
31	filer	thumbnailoption
32	easy_thumbnails	source
33	easy_thumbnails	thumbnail
34	easy_thumbnails	thumbnaildimensions
35	djangocms_column	column
36	djangocms_column	multicolumns
37	djangocms_file	file
38	djangocms_file	folder
39	djangocms_link	link
40	djangocms_picture	picture
41	djangocms_style	style
42	djangocms_snippet	snippet
43	djangocms_snippet	snippetptr
44	djangocms_googlemap	googlemap
45	djangocms_googlemap	googlemapmarker
46	djangocms_googlemap	googlemaproute
47	djangocms_video	videoplayer
48	djangocms_video	videosource
49	djangocms_video	videotrack
50	aldryn_bootstrap3	boostrap3alertplugin
51	aldryn_bootstrap3	boostrap3blockquoteplugin
52	aldryn_bootstrap3	boostrap3buttonplugin
53	aldryn_bootstrap3	boostrap3iconplugin
54	aldryn_bootstrap3	boostrap3imageplugin
55	aldryn_bootstrap3	boostrap3labelplugin
56	aldryn_bootstrap3	boostrap3panelbodyplugin
57	aldryn_bootstrap3	boostrap3panelfooterplugin
58	aldryn_bootstrap3	boostrap3panelheadingplugin
59	aldryn_bootstrap3	boostrap3panelplugin
60	aldryn_bootstrap3	boostrap3spacerplugin
61	aldryn_bootstrap3	boostrap3wellplugin
62	aldryn_bootstrap3	bootstrap3accordionitemplugin
63	aldryn_bootstrap3	bootstrap3accordionplugin
64	aldryn_bootstrap3	bootstrap3carouselplugin
65	aldryn_bootstrap3	bootstrap3carouselslidefolderplugin
66	aldryn_bootstrap3	bootstrap3carouselslideplugin
67	aldryn_bootstrap3	bootstrap3columnplugin
68	aldryn_bootstrap3	bootstrap3listgroupitemplugin
69	aldryn_bootstrap3	bootstrap3listgroupplugin
70	aldryn_bootstrap3	bootstrap3rowplugin
71	aldryn_bootstrap3	bootstrap3fileplugin
72	aldryn_bootstrap3	boostrap3citeplugin
73	aldryn_bootstrap3	bootstrap3codeplugin
74	aldryn_bootstrap3	bootstrap3responsiveplugin
75	aldryn_bootstrap3	bootstrap3tabitemplugin
76	aldryn_bootstrap3	bootstrap3tabplugin
77	aldryn_bootstrap3	boostrap3jumbotronplugin
78	checklist	checklist
79	checklist	itemchecklist
80	checklist	projeto
81	checklist	ramo
82	checklist	empreendimento
83	checklist	etapaprojeto
84	checklist	avaliacao
85	checklist	itemavaliacao
86	checklist	plano
87	checklist	produto
88	checklist_cms_integration	checklistspluginmodel
89	checklist_cms_integration	avaliacaopluginmodel
90	polls	choice
91	polls	poll
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: bitnami
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2018-03-12 13:01:04.897642+00
2	auth	0001_initial	2018-03-12 13:01:05.014058+00
3	contenttypes	0002_remove_content_type_name	2018-03-12 13:01:05.034197+00
4	auth	0002_alter_permission_name_max_length	2018-03-12 13:01:05.040566+00
5	auth	0003_alter_user_email_max_length	2018-03-12 13:01:05.052181+00
6	auth	0004_alter_user_username_opts	2018-03-12 13:01:05.062096+00
7	auth	0005_alter_user_last_login_null	2018-03-12 13:01:05.074039+00
8	auth	0006_require_contenttypes_0002	2018-03-12 13:01:05.076849+00
9	auth	0007_alter_validators_add_error_messages	2018-03-12 13:01:05.090466+00
10	auth	0008_alter_user_username_max_length	2018-03-12 13:01:05.109142+00
11	auth	0009_alter_user_last_name_max_length	2018-03-12 13:01:05.121641+00
12	admin	0001_initial	2018-03-12 13:01:06.490435+00
13	admin	0002_logentry_remove_auto_add	2018-03-12 13:01:06.509425+00
14	sessions	0001_initial	2018-03-12 13:01:06.541248+00
15	sites	0001_initial	2018-03-18 02:10:20.256567+00
16	cms	0001_initial	2018-03-18 02:10:20.607556+00
17	cms	0002_auto_20140816_1918	2018-03-18 02:10:21.069489+00
18	cms	0003_auto_20140926_2347	2018-03-18 02:10:21.157096+00
19	cms	0004_auto_20140924_1038	2018-03-18 02:10:21.581551+00
20	cms	0005_auto_20140924_1039	2018-03-18 02:10:21.622916+00
21	cms	0006_auto_20140924_1110	2018-03-18 02:10:21.825652+00
22	cms	0007_auto_20141028_1559	2018-03-18 02:10:21.855563+00
23	cms	0008_auto_20150208_2149	2018-03-18 02:10:21.94881+00
24	cms	0008_auto_20150121_0059	2018-03-18 02:10:21.99199+00
25	cms	0009_merge	2018-03-18 02:10:21.995428+00
26	cms	0010_migrate_use_structure	2018-03-18 02:10:22.062279+00
27	cms	0011_auto_20150419_1006	2018-03-18 02:10:22.085725+00
28	cms	0012_auto_20150607_2207	2018-03-18 02:10:22.177282+00
29	cms	0013_urlconfrevision	2018-03-18 02:10:22.190246+00
30	cms	0014_auto_20160404_1908	2018-03-18 02:10:22.223426+00
31	cms	0015_auto_20160421_0000	2018-03-18 02:10:22.24299+00
32	cms	0016_auto_20160608_1535	2018-03-18 02:10:22.318688+00
33	filer	0001_initial	2018-03-18 02:10:22.779116+00
34	filer	0002_auto_20150606_2003	2018-03-18 02:10:22.83229+00
35	aldryn_bootstrap3	0001_initial	2018-03-18 02:10:24.455115+00
36	aldryn_bootstrap3	0002_bootstrap3fileplugin	2018-03-18 02:10:24.536775+00
37	aldryn_bootstrap3	0003_auto_20151113_1604	2018-03-18 02:10:24.930449+00
38	aldryn_bootstrap3	0004_auto_20151211_1333	2018-03-18 02:10:25.375586+00
39	aldryn_bootstrap3	0005_boostrap3imageplugin_use_original_image	2018-03-18 02:10:25.438032+00
40	aldryn_bootstrap3	0006_auto_20160615_1740	2018-03-18 02:10:25.57316+00
41	aldryn_bootstrap3	0007_auto_20160705_1155	2018-03-18 02:10:25.682898+00
42	aldryn_bootstrap3	0008_auto_20160820_2332	2018-03-18 02:10:26.452453+00
43	aldryn_bootstrap3	0009_auto_20161219_1530	2018-03-18 02:10:34.142925+00
44	aldryn_bootstrap3	0010_bootstrap3codeplugin	2018-03-18 02:10:34.223487+00
45	aldryn_bootstrap3	0011_bootstrap3responsiveplugin	2018-03-18 02:10:34.343987+00
46	aldryn_bootstrap3	0012_bootstrap3tabplugin	2018-03-18 02:10:34.501869+00
47	aldryn_bootstrap3	0013_boostrap3jumbotronplugin	2018-03-18 02:10:34.589631+00
48	aldryn_bootstrap3	0014_translations_update	2018-03-18 02:10:35.486846+00
50	checklist	0002_auto_20180227_2235	2018-03-18 02:10:35.616628+00
51	checklist	0003_auto_20180227_2242	2018-03-18 02:10:35.662494+00
52	checklist	0004_auto_20180228_2204	2018-03-18 02:10:35.822961+00
53	checklist	0005_auto_20180228_2218	2018-03-18 02:10:35.871711+00
54	checklist	0006_auto_20180228_2337	2018-03-18 02:10:36.134808+00
55	checklist	0007_analise_descircao	2018-03-18 02:10:36.172106+00
56	checklist	0008_auto_20180301_2309	2018-03-18 02:10:36.235637+00
57	checklist	0009_analise_prancha	2018-03-18 02:10:36.26827+00
58	checklist	0010_auto_20180304_1628	2018-03-18 02:10:36.562634+00
59	checklist	0011_itemavaliacao_avaliacao	2018-03-18 02:10:36.585425+00
60	checklist	0012_auto_20180304_2332	2018-03-18 02:10:36.637724+00
61	checklist	0013_auto_20180311_2334	2018-03-18 02:10:36.709117+00
62	checklist	0014_auto_20180312_1118	2018-03-18 02:10:36.751233+00
63	checklist	0015_auto_20180317_1848	2018-03-18 02:10:36.852724+00
64	checklist	0016_auto_20180317_2310	2018-03-18 02:10:36.959104+00
65	cms	0017_pagetype	2018-03-18 02:10:37.136871+00
66	cms	0018_pagenode	2018-03-18 02:10:37.956704+00
68	checklist_cms_integration	0002_avaliacaopluginmodel	2018-03-18 02:10:38.127387+00
69	djangocms_column	0001_initial	2018-03-18 02:10:38.261116+00
70	djangocms_column	0002_auto_20160915_0818	2018-03-18 02:10:38.394962+00
71	filer	0003_thumbnailoption	2018-03-18 02:10:38.414642+00
72	filer	0004_auto_20160328_1434	2018-03-18 02:10:38.444139+00
73	filer	0005_auto_20160623_1425	2018-03-18 02:10:38.636606+00
74	filer	0006_auto_20160623_1627	2018-03-18 02:10:38.848013+00
75	djangocms_file	0001_initial	2018-03-18 02:10:38.915887+00
76	djangocms_file	0002_auto_20151202_1551	2018-03-18 02:10:39.009001+00
77	djangocms_file	0003_remove_related_name_for_cmsplugin_ptr	2018-03-18 02:10:39.083207+00
78	djangocms_file	0004_set_related_name_for_cmsplugin_ptr	2018-03-18 02:10:39.158282+00
79	djangocms_file	0005_auto_20160119_1534	2018-03-18 02:10:39.18547+00
80	djangocms_file	0006_migrate_to_filer	2018-03-18 02:10:39.375789+00
81	djangocms_file	0007_adapted_fields	2018-03-18 02:10:39.889727+00
82	djangocms_file	0008_add_folder	2018-03-18 02:10:39.970808+00
83	djangocms_file	0009_fixed_null_fields	2018-03-18 02:10:40.067474+00
84	djangocms_file	0010_removed_null_fields	2018-03-18 02:10:40.115338+00
85	filer	0007_auto_20161016_1055	2018-03-18 02:10:40.14403+00
86	djangocms_googlemap	0001_initial	2018-03-18 02:10:40.220247+00
87	djangocms_googlemap	0002_auto_20160622_1031	2018-03-18 02:10:40.391485+00
88	djangocms_googlemap	0003_auto_20160825_1829	2018-03-18 02:10:40.419856+00
89	djangocms_googlemap	0004_adapted_fields	2018-03-18 02:10:41.59441+00
90	djangocms_googlemap	0005_create_nested_plugins	2018-03-18 02:10:41.689122+00
91	djangocms_googlemap	0006_remove_fields	2018-03-18 02:10:42.038508+00
92	djangocms_googlemap	0007_reset_null_values	2018-03-18 02:10:42.158826+00
93	djangocms_googlemap	0008_removed_null_fields	2018-03-18 02:10:42.199717+00
94	djangocms_googlemap	0009_googlemapmarker_icon	2018-03-18 02:10:42.285048+00
95	djangocms_link	0001_initial	2018-03-18 02:10:42.383163+00
96	djangocms_link	0002_auto_20140929_1705	2018-03-18 02:10:42.427289+00
97	djangocms_link	0003_auto_20150212_1310	2018-03-18 02:10:42.468716+00
98	djangocms_link	0004_auto_20150708_1133	2018-03-18 02:10:42.548313+00
99	djangocms_link	0005_auto_20151003_1710	2018-03-18 02:10:42.591202+00
100	djangocms_link	0006_remove_related_name_for_cmsplugin_ptr	2018-03-18 02:10:42.667774+00
101	djangocms_link	0007_set_related_name_for_cmsplugin_ptr	2018-03-18 02:10:42.747222+00
102	djangocms_link	0008_link_attributes	2018-03-18 02:10:42.801889+00
103	djangocms_link	0009_auto_20160705_1344	2018-03-18 02:10:42.856441+00
104	djangocms_link	0010_adapted_fields	2018-03-18 02:10:43.401218+00
105	djangocms_link	0011_fixed_null_values	2018-03-18 02:10:43.50697+00
106	djangocms_link	0012_removed_null	2018-03-18 02:10:43.636823+00
107	djangocms_link	0013_fix_hostname	2018-03-18 02:10:43.67494+00
108	djangocms_picture	0001_initial	2018-03-18 02:10:43.758219+00
109	djangocms_picture	0002_auto_20151018_1927	2018-03-18 02:10:43.849614+00
110	djangocms_picture	0003_migrate_to_filer	2018-03-18 02:10:44.527417+00
111	djangocms_picture	0004_adapt_fields	2018-03-18 02:10:46.003254+00
112	djangocms_picture	0005_reset_null_values	2018-03-18 02:10:46.255495+00
113	djangocms_picture	0006_remove_null_values	2018-03-18 02:10:46.362106+00
114	djangocms_picture	0007_fix_alignment	2018-03-18 02:10:46.414165+00
115	djangocms_snippet	0001_initial	2018-03-18 02:10:46.524676+00
116	djangocms_snippet	0002_snippet_slug	2018-03-18 02:10:46.560215+00
117	djangocms_snippet	0003_auto_data_fill_slug	2018-03-18 02:10:46.693877+00
118	djangocms_snippet	0004_auto_alter_slug_unique	2018-03-18 02:10:46.723049+00
119	djangocms_snippet	0005_set_related_name_for_cmsplugin_ptr	2018-03-18 02:10:46.806306+00
120	djangocms_snippet	0006_auto_20160831_0729	2018-03-18 02:10:46.896486+00
121	djangocms_snippet	0007_auto_alter_template_helptext	2018-03-18 02:10:46.909627+00
122	djangocms_style	0001_initial	2018-03-18 02:10:46.993098+00
123	djangocms_style	0002_set_related_name_for_cmsplugin_ptr	2018-03-18 02:10:47.07786+00
124	djangocms_style	0003_adapted_fields	2018-03-18 02:10:47.737668+00
125	djangocms_style	0004_use_positive_small_integer_field	2018-03-18 02:10:48.108511+00
126	djangocms_style	0005_reset_null_values	2018-03-18 02:10:48.216474+00
127	djangocms_style	0006_removed_null_fields	2018-03-18 02:10:48.259229+00
128	djangocms_style	0007_style_template	2018-03-18 02:10:48.459881+00
129	djangocms_text_ckeditor	0001_initial	2018-03-18 02:10:48.542117+00
130	djangocms_text_ckeditor	0002_remove_related_name_for_cmsplugin_ptr	2018-03-18 02:10:48.640979+00
131	djangocms_text_ckeditor	0003_set_related_name_for_cmsplugin_ptr	2018-03-18 02:10:48.723204+00
132	djangocms_text_ckeditor	0004_auto_20160706_1339	2018-03-18 02:10:48.827262+00
133	djangocms_video	0001_initial	2018-03-18 02:10:48.91647+00
134	djangocms_video	0002_set_related_name_for_cmsplugin_ptr	2018-03-18 02:10:49.008081+00
135	djangocms_video	0003_field_adaptions	2018-03-18 02:10:49.601894+00
136	djangocms_video	0004_move_to_attributes	2018-03-18 02:10:50.541904+00
137	djangocms_video	0005_migrate_to_filer	2018-03-18 02:10:50.65161+00
138	djangocms_video	0006_field_adaptions	2018-03-18 02:10:51.015835+00
139	djangocms_video	0007_create_nested_plugin	2018-03-18 02:10:51.3563+00
140	djangocms_video	0008_reset_null_values	2018-03-18 02:10:51.465835+00
141	djangocms_video	0009_removed_null_values	2018-03-18 02:10:51.524619+00
142	easy_thumbnails	0001_initial	2018-03-18 02:10:51.60914+00
143	easy_thumbnails	0002_thumbnaildimensions	2018-03-18 02:10:51.635855+00
144	menus	0001_initial	2018-03-18 02:10:51.657878+00
145	sites	0002_alter_domain_unique	2018-03-18 02:10:51.681639+00
146	polls	0001_initial	2018-03-25 13:27:39.883853+00
147	checklist	0001_initial	2018-03-26 00:11:27.936589+00
148	checklist_cms_integration	0001_initial	2018-03-26 00:11:35.862148+00
149	checklist	0002_itemavaliacao_observacao	2018-03-27 02:53:20.683491+00
150	checklist	0003_auto_20180402_0839	2018-04-02 11:40:20.58579+00
151	checklist	0004_auto_20180413_0815	2018-04-13 11:15:46.704073+00
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: bitnami
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
c61e8y4f5knqu1zm338ihg4aq6daoigr	NjM3ODU2OGU2MzBjNzljN2U0M2I3ODU0NmYwM2NlNWI5NzA3ODBkYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJmOTNlMzA5NWYwN2U1ODg1OGQ4MDYyMTUzMWZkZjNmMmE0MWViZDc2IiwiY21zX3Rvb2xiYXJfZGlzYWJsZWQiOmZhbHNlLCJjbXNfZWRpdCI6ZmFsc2UsImNtc19wcmV2aWV3Ijp0cnVlLCJmaWxlcl9sYXN0X2ZvbGRlcl9pZCI6bnVsbH0=	2018-05-02 20:20:25.091145+00
7hs8wuckubc2e7euar0jcedvm640t4b4	MGY5NGU2NTQ1NWIwZWIwMTA2ODgyMjU3OTJhMjFlNDk4ZTMxNzE4Njp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJmOTNlMzA5NWYwN2U1ODg1OGQ4MDYyMTUzMWZkZjNmMmE0MWViZDc2In0=	2018-05-03 10:21:29.821825+00
ir41cz9r1tvhsyq0du2ydgxawdwgeqxo	MGY5NGU2NTQ1NWIwZWIwMTA2ODgyMjU3OTJhMjFlNDk4ZTMxNzE4Njp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJmOTNlMzA5NWYwN2U1ODg1OGQ4MDYyMTUzMWZkZjNmMmE0MWViZDc2In0=	2018-04-10 09:57:03.079534+00
7tdym0561ln84o7blqeb2kwov0eeqzoy	OGYyMzRhMjM4NzgyMTkzYTgyOTQwYjA2NmZiNDYxMzhhNzMxOTU1Mjp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIzMWRjY2NkNWEzOGViYjQzZjJhZGQwMDljMDkzMzdhZmUwODk2M2EwIn0=	2018-04-21 17:03:21.071994+00
55vugrdvwrufmomtj9fo6nsq5vplo1js	OGYyMzRhMjM4NzgyMTkzYTgyOTQwYjA2NmZiNDYxMzhhNzMxOTU1Mjp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIzMWRjY2NkNWEzOGViYjQzZjJhZGQwMDljMDkzMzdhZmUwODk2M2EwIn0=	2018-04-19 14:28:56.939176+00
u9od9kfqlt64gf1u5jnveye7ocwwa3c3	OGYyMzRhMjM4NzgyMTkzYTgyOTQwYjA2NmZiNDYxMzhhNzMxOTU1Mjp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIzMWRjY2NkNWEzOGViYjQzZjJhZGQwMDljMDkzMzdhZmUwODk2M2EwIn0=	2018-04-17 12:25:43.20009+00
j5s4qvp988z130y12atejbap3y95ewrl	MTEyZTdkNGM2MTNkMGIyMWY4MTYxMmJlN2M3ZDAyZWEzNGQ4NzJjNjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJmOTNlMzA5NWYwN2U1ODg1OGQ4MDYyMTUzMWZkZjNmMmE0MWViZDc2IiwiY21zX3Rvb2xiYXJfZGlzYWJsZWQiOmZhbHNlLCJjbXNfZWRpdCI6dHJ1ZSwiY21zX3ByZXZpZXciOmZhbHNlLCJmaWxlcl9sYXN0X2ZvbGRlcl9pZCI6bnVsbH0=	2018-04-27 13:10:10.406744+00
aehvmkmvyndq3sz743cbkatokue07nmj	OGYyMzRhMjM4NzgyMTkzYTgyOTQwYjA2NmZiNDYxMzhhNzMxOTU1Mjp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIzMWRjY2NkNWEzOGViYjQzZjJhZGQwMDljMDkzMzdhZmUwODk2M2EwIn0=	2018-04-28 22:35:05.546724+00
zvldzdz20am8xmi65g7r5yp7m1ivab2m	YTg1OTkxMmRhNTE0N2E0YjQyYTlmMzIyYjNjMmY4NzBmZDU2ZTkwMDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJmOTNlMzA5NWYwN2U1ODg1OGQ4MDYyMTUzMWZkZjNmMmE0MWViZDc2IiwiY21zX3Rvb2xiYXJfZGlzYWJsZWQiOmZhbHNlLCJjbXNfZWRpdCI6dHJ1ZSwiY21zX3ByZXZpZXciOmZhbHNlfQ==	2018-04-30 17:43:04.783103+00
wyicsjxxyssjzde5r71250vav3f4ec0s	NzkyYzc3OGExZGU4ZGZiYzkyYjFkYzNkYjE3ZDQzYjYxOTcwOWIxZTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3MzkxNTVhMmI0MGQ2YzNmOWFjMzhjMGUzZDFlOTc4NDk3MzUwZWQ5In0=	2018-04-05 01:56:27.967373+00
\.


--
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_site (id, domain, name) FROM stdin;
1	example.com	example.com
\.


--
-- Data for Name: djangocms_column_column; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY djangocms_column_column (cmsplugin_ptr_id, width) FROM stdin;
758	25%
1159	25%
1162	25%
1165	25%
1168	25%
673	25%
157	25%
158	25%
\.


--
-- Data for Name: djangocms_column_multicolumns; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY djangocms_column_multicolumns (cmsplugin_ptr_id) FROM stdin;
156
1158
\.


--
-- Data for Name: djangocms_file_file; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY djangocms_file_file (cmsplugin_ptr_id, file_name, link_target, link_title, file_src_id, attributes, template, show_file_size) FROM stdin;
\.


--
-- Data for Name: djangocms_file_folder; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY djangocms_file_folder (template, link_target, show_file_size, attributes, cmsplugin_ptr_id, folder_src_id) FROM stdin;
\.


--
-- Data for Name: djangocms_googlemap_googlemap; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY djangocms_googlemap_googlemap (cmsplugin_ptr_id, title, zoom, lat, lng, width, height, scrollwheel, double_click_zoom, draggable, keyboard_shortcuts, pan_control, zoom_control, street_view_control, style, fullscreen_control, map_type_control, rotate_control, scale_control, template) FROM stdin;
\.


--
-- Data for Name: djangocms_googlemap_googlemapmarker; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY djangocms_googlemap_googlemapmarker (cmsplugin_ptr_id, title, address, lat, lng, show_content, info_content, icon_id) FROM stdin;
\.


--
-- Data for Name: djangocms_googlemap_googlemaproute; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY djangocms_googlemap_googlemaproute (cmsplugin_ptr_id, title, origin, destination, travel_mode) FROM stdin;
\.


--
-- Data for Name: djangocms_link_link; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY djangocms_link_link (cmsplugin_ptr_id, name, external_link, anchor, mailto, phone, target, internal_link_id, attributes, template) FROM stdin;
479	Acessar Análises					_self	25	{}	default
639	Acessar Análises					_self	25	{}	default
1193	Acessar Análises					_self	25	{}	default
1195	Acessar Análises					_self	25	{}	default
\.


--
-- Data for Name: djangocms_picture_picture; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY djangocms_picture_picture (cmsplugin_ptr_id, link_url, alignment, link_page_id, height, width, picture_id, attributes, caption_text, link_attributes, link_target, use_automatic_scaling, use_crop, use_no_cropping, use_upscale, thumbnail_options_id, external_picture, template) FROM stdin;
\.


--
-- Data for Name: djangocms_snippet_snippet; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY djangocms_snippet_snippet (id, name, html, template, slug) FROM stdin;
1	Basico	<style>\r\n    .padded tr.first td { padding-top:10px; padding-bottom:10px; }\r\n    .padded tr.last td { padding-bottom:12px; }\r\n    .padded td.first { padding-left:16px; padding-right:16px; }\r\n    .padded td.last { padding-right:10px; }\r\n</style>\r\n\r\n\r\n<table class='padded'>\r\n\t<tbody>\r\n\t\t<tr class='first'>\r\n\t\t\t<td class='first'>\r\n\t\t\t<table style="position: absolute; margin: 15px; left: 150px; top: 285px; background-color: rgba(0, 0, 0, 0.6) !important;">\r\n\t\t\t\t<tbody>\r\n\t\t\t\t\t<tr valign="middle" class='first'>\r\n\t\t\t\t\t\t<td width="22px" class='first'>\r\n\t\t\t\t\t\t<table style=" background-color: rgba(255, 255, 255, 1.0) !important;" width="40px" height="40px">\r\n\t\t\t\t\t\t\t<tbody>\r\n\t\t\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t\t\t<td align="center"  class='first'><img alt="Endereço" data-type="image" id="comp-j2677def1imgimage"  height="18" width="22" src="https://static.wixstatic.com/media/46c83e_8e35331bac98477d9f326b4af5f135a7.png/v1/fill/w_45,h_44,al_c,usm_0.66_1.00_0.01/46c83e_8e35331bac98477d9f326b4af5f135a7.png" /></td>\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t</tbody>\r\n\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t<td>\r\n\t\t\t\t\t\t<font color="white" size="3">Rua 88 n. 443, Sala 07 - Setor Sul<br />\r\n\t\t\t\t\t\tCEP 74085-115 - Goi&acirc;nia - GO\r\n\t\t\t\t\t\t</font>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t<td class='first'>&nbsp;</td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t\t\r\n\t\t\t\t\t<tr valign="middle" class='last'>\r\n\t\t\t\t\t\t<td width="32px"  class='first'>\r\n\t\t\t\t\t\t<table style="background-color: rgba(255, 255, 255, 1.0) !important;"  width="40px" height="40px">\r\n\t\t\t\t\t\t\t<tbody>\r\n\t\t\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t\t\t<td align="center" class='first'><img alt="Fone" data-type="image" id="comp-j2677dedimgimage"  height="18" width="22" src="/media/filer_public_thumbnails/filer_public/d1/6c/d16ccf92-c3ed-47e3-a04b-072b2405bf70/fone.png__1170x0_q85_subsampling-2_upscale.png" /></td>\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t</tbody>\r\n\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t<td height="60px" valign="middle">\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t<font color="white" size="3">Fone: (62) 3941-8910</font>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t\t<tr class='last'>\r\n\t\t\t\t\t\t<td width="38px" class='first'>\r\n\t\t\t\t\t\t<table style="background-color: rgba(255, 255, 255, 1.0) !important;"  width="54px" height="40px">\r\n\t\t\t\t\t\t\t<tbody>\r\n\t\t\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t\t\t<td align="center"><img alt="eMail" data-type="image" id="comp-j2677defimgimage"  height="18" width="22" src="https://static.wixstatic.com/media/46c83e_fd85724d4cb440978f9338e890525429.png/v1/fill/w_48,h_50,al_c,usm_0.66_1.00_0.01/46c83e_fd85724d4cb440978f9338e890525429.png" /></td>\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t</tbody>\r\n\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t<td height="65px" valign="middle">\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t<a data-auto-recognition="true" data-content="atendimento@construflix.com.br" data-type="mail" href="mailto:atendimento@concretaconsultoria.com.br"><font color="white" size="3">atendimento@construflix.com.br</font></a><br />\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</tbody>\r\n\t\t\t</table>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>		basico
2	tabela planos	<style type="text/css">\r\n\r\n.planos{\r\n    \r\n    width:100%;\r\n\r\n    \r\n    \r\n}\r\n\r\n.planos th, .planos td { \r\n    padding: 5px; \r\n    border-color:#333333;\r\n    border-style:solid;\r\n    box-sizing: 30px;\r\n    border-width: medium medium thick medium;\r\n    \r\n          color: #FFFFFF;\r\n            text-align: center;\r\n            font-size: medium;\r\n} \r\n\r\n.planos th { \r\n    background-color: #000000;\r\n} \r\n\r\n.planos td { \r\n    background-color: #444444;\r\n} \r\n\r\n.btnAssine {\r\n     background-color: #FF3300 !important;\r\n     width: 185px;\r\n}\r\n\r\n\r\n.style6\r\n        {\r\n\r\n\r\n            background-color: #333333 !important; \r\n\r\n        }\r\n    \r\n    .style1\r\n        {\r\n\r\n            text-align: left !important;\r\n \r\n\r\n        }    \r\n\r\n    .style5\r\n        {\r\n\r\n            text-align: left !important;\r\n \r\n\r\n        }    \r\n        .style7\r\n        {\r\n\r\n            width: 185px;\r\n \r\n\r\n        }\r\n        .style8\r\n        {\r\n        \r\n            width: 185px;\r\n            \r\n        }\r\n        .style9\r\n        {\r\n         \r\n            width: 185px;\r\n          \r\n        }\r\n        \r\n\r\n    </style>\r\n<br>\r\n<table  class="planos" >\r\n        <tr>\r\n            <th class="style5" align="left">\r\n                Características</th>\r\n            <th class="style4">\r\n                Mínimo</th>\r\n            <th class="style7">\r\n                Intermediário</th>\r\n            <th class="style4">\r\n                Superior</th>\r\n        </tr>\r\n        <tr>\r\n            <td class="style1">\r\n                Preço mensal</td>\r\n            <td class="style3">\r\n                R$ 150,00</td>\r\n            <td class="style8">\r\n                R$ 250,00</td>\r\n            <td class="style3">\r\n                R$ 350,00</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="style1">\r\n                Quantidade de aplicações permitidas por mês (exceto modelos avulsos)</td>\r\n            <td class="style3">\r\n                2</td>\r\n            <td class="style8">\r\n                4</td>\r\n            <td class="style3">\r\n                ilimitada</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="style1">\r\n                Quantidade de usuários</td>\r\n            <td class="style3">\r\n                1</td>\r\n            <td class="style8">\r\n                2</td>\r\n            <td class="style3">\r\n                4</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="style1">\r\n                Desconto para venda avulsa e usuário adicional</td>\r\n            <td class="style3">\r\n                0%</td>\r\n            <td class="style8">\r\n                10%</td>\r\n            <td class="style3">\r\n                20%</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="style1">\r\n                Fidelização</td>\r\n            <td class="style3">\r\n                12 meses</td>\r\n            <td class="style8">\r\n                9 meses</td>\r\n            <td class="style3">\r\n                6 meses</td>\r\n        </tr>\r\n        <tr>\r\n            <td class="style6">\r\n                &nbsp;</td>\r\n            <td class="btnAssine">Assine</td>\r\n            <td class="btnAssine">Assine</td>\r\n            <td class="btnAssine">Assine</td>\r\n        </tr>\r\n    </table>\r\n    <br>		tabela-planos
\.


--
-- Data for Name: djangocms_snippet_snippetptr; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY djangocms_snippet_snippetptr (cmsplugin_ptr_id, snippet_id) FROM stdin;
1017	2
1068	2
986	1
1006	1
\.


--
-- Data for Name: djangocms_style_style; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY djangocms_style_style (cmsplugin_ptr_id, class_name, tag_type, padding_left, padding_right, padding_top, padding_bottom, margin_left, margin_right, margin_top, margin_bottom, additional_classes, attributes, id_name, label, template) FROM stdin;
\.


--
-- Data for Name: djangocms_text_ckeditor_text; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY djangocms_text_ckeditor_text (cmsplugin_ptr_id, body) FROM stdin;
18	<table border="0" cellpadding="1" cellspacing="1" style="width: 250px;">\n\t<tbody>\n\t\t<tr>\n\t\t\t<td style="background-color: rgb(102, 102, 102); text-align: center; height: 20px;">\n\t\t\t<div><span style="color: #ffffff;"><span style="background-color: null;"><small>Olha quem já está usando...</small></span></span></div>\n\t\t\t</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style="background-color: rgb(170, 170, 170);"><span style="color: #ffffff;"><span style="background-color: #7f8c8d;"><cms-plugin alt="Image - clientes-logo.jpg " title="Image - clientes-logo.jpg" id="176"></cms-plugin></span></span></td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p style="margin-left: 40px;"> </p>
955	<p><small> © 2018 Construflix Ltda   |    Ajuda    |    Termos de Uso    |    Política de privacidade</small></p>
964	<p><small> © 2018 Construflix Ltda   |    Ajuda    |    Termos de Uso    |    Política de privacidade</small></p>
1010	<table border="0" cellpadding="1" cellspacing="1" style="width: 1000px;">\n\t<tbody>\n\t\t<tr>\n\t\t\t<td>\n\t\t\t<h3 style="margin: 0cm 0cm 8pt 80px;"> </h3>\n\n\t\t\t<h3 style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>O que é?</b></span></span></big></span></h3>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Construflix é uma plataforma web de ferramentas (também chamadas aplicações) que ajudam o usuário – profissionais e empresas que atuam na construção civil - a atender à norma de desempenho (ABNT NBR 15575) e normas de gestão da qualidade (SiAC/PBQP-H, ISO 9001) de forma fácil e intuitiva.</span></span></span></span><br>\n\t\t\t </p>\n\n\t\t\t<h3 style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Como surgiu?</b></span></span></big></span></h3>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">O atendimento à norma de desempenho é obrigatório para todas as empresas e profissionais que trabalham com construção de edificações habitacionais, enquanto o atendimento ao PBQP-H/SiAC é obrigatório para muitas empresas, especialmente as que necessitam de órgãos financiadores.</span></span></span></span></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Grandes <i>players</i> do mercado possuem departamentos de P&amp;D, contratam consultores renomados e têm acesso a tecnologia de ponta. Mas, e a esmagadora maioria de profissionais e empresas que não podem contar com as mesmas facilidades? É onde entra o Construflix.</span></span></span></big></span></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Nossa plataforma “traduz” as normas citadas de uma forma mais fácil de entender e aplicar. Sabemos que há no Brasil milhares de profissionais e empresas de arquitetura e construção que conhecem suas responsabilidades e gostariam de aplicar as normas em seus trabalhos, mas não sabem por onde começar ou não tem o recurso financeiro para isto. Foi daí que surgimos.</span></span></span></big></span></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big> </big></span></p>\n\n\t\t\t<h3 style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Qual o objetivo do Construflix?</b></span></span></big></span></h3>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Nós queremos: </span></span></span></big></span></p>\n\n\t\t\t<ul>\n\t\t\t\t<li style="margin-left: 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">revolucionar o mercado da construção civil quanto ao atendimento à norma de desempenho e às normas de Gestão da Qualidade;</span></span></span></big></span></li>\n\t\t\t\t<li style="margin-left: 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">traduzir normas complicadas em algo mais fácil para os profissionais de projeto e construção. Sabemos que a formação em Arquitetura e Engenharia Civil no Brasil  está longe de fornecer as ferramentas necessárias para os alunos conhecerem e aplicarem normas de qualidade e desempenho na construção civil. Viemos para facilitar esse processo;</span></span></span></big></span></li>\n\t\t\t\t<li style="margin-left: 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">diminuir drasticamente os custos com consultorias, permitindo acesso a ferramentas fáceis de utilizar;</span></span></span></big></span></li>\n\t\t\t\t<li style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">por fim, queremos ver a norma de desempenho sendo efetivamente aplicada e, com isto, contribuir para que a construção civil ofereça a seus consumidores edificações mais seguras, confortáveis e duráveis.</span></span></span></big></span><br>\n\t\t\t\t </li>\n\t\t\t</ul>\n\n\t\t\t<h3 style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Posso comparar o Construflix com o quê?</b></span></span></big></span></h3>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Na construção civil, com nada! Nossa plataforma é inovadora e não possui concorrência. Ao contrário, nossas aplicações são complementares a outras aplicações <i>online</i> para construção civil disponíveis no mercado.</span></span></span></big></span></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Para efeito didático, usando uma outra área de atuação, podemos comparar com a educação física. Uma pessoa pode contratar um <i>personal trainer</i> para lhe orientar específica e individualmente. Isto é ótimo, mas, obviamente, possui um custo muito alto. Porém, há uma alternativa muito mais barata que pode ser quase tão boa quanto: usar os serviços de uma academia. Nela você tem acesso a todos os aparelhos, possui alguma orientação técnica e, com seu próprio esforço, pode atingir resultados altamente satisfatórios, a um custo muito mais baixo. O Construflix é parecido: você paga um plano mensal conforme sua necessidade, tem acesso a ferramentas técnicas cuidadosamente preparadas por profissionais de alto gabarito e dispõe de instruções para uso em tutoriais e/ou vídeos. O resto só depende de você!</span></span></span></big></span></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">O uso sistemático do Construflix pode ajudar muito para que seus projetos e obras estejam cada vez mais aderentes à norma de desempenho, e também a normas de Gestão da Qualidade, melhorando o trabalho profissional e minimizando problemas jurídicos.</span></span></span></big></span><br>\n\t\t\t </p>\n\n\t\t\t<h3 style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Ok, achei interessante, mas como funciona?</b></span></span></big></span></h3>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Primeiro, você se cadastra e faz um teste grátis por 3 dias. Se gostar, assina um dos 3 planos mensais. Eles foram batizados, não por acaso, de MÍNIMO, INTERMEDIÁRIO e SUPERIOR. A partir daí, você pode usar a quantidade de ferramentas permitidas no seu plano quantas vezes quiser por mês. </span></span></span></big></span></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Construflix se encarrega de guardar seus resultados e manter seu banco de dados, de modo que você possa acessar quando quiser.</span></span></span></big></span></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Para cada aplicação temos uma sessão de explicação escrita e/ou em vídeo, e contamos com pessoal pronto para tirar suas dúvidas.</span></span></span></big></span></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"> </p>\n\n\t\t\t<h3 style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Se eu mandar o projeto o Construflix fornece os resultados da minha obra?</b></span></span></big></span></h3>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Não, isso seria consultoria! O que queremos é que você tenha autonomia para fazer ou analisar vários assuntos relacionados ao desempenho das edificações. </span></span></span></big></span></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Lembre-se da comparação com a academia: nós fornecemos “equipamentos” super atuais e as orientações de uso, mas quem tem que “subir no aparelho” e fazer o esforço é você. O que fizemos foi deixar essa tarefa muito mais fácil a um custo quase inacreditavelmente acessível.</span></span></span></big></span><br>\n\t\t\t </p>\n\n\t\t\t<h3 style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Vi que hoje o Construflix tem uma certa quantidade de ferramentas. Será sempre assim? </b></span></span></big></span></h3>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Claro que não! Estamos sempre desenvolvendo e incluindo novas aplicações na plataforma. Esse é o segredo do nosso negócio! À medida que aumentamos nossa base de clientes, estamos em constante evolução. E o melhor? Você não pagará um centavo a mais por isto! Incrível, não?</span></span></span><br>\n\t\t\t </big></span></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Bom, agora que você conheceu mais sobre a gente, nós te convidamos a explorar todo o nosso site e se cadastrar para começar seu período de teste. Se quiser começar já, é só clicar <u>aqui</u> .</span></span></span></big></span><br>\n\t\t\t </p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Bem-vindo ao Construflix!</span></span></span></big></span></p>\n\t\t\t</td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p style="margin: 0cm 0cm 8pt 40px;"> </p>
277	<p style="margin-left: 40px;"><cms-plugin alt="Image - construflix-logomarca-use-full.png " title="Image - construflix-logomarca-use-full.png" id="1058"></cms-plugin></p>\n\n<p style="margin-left: 80px;">Clique <strong><u>aqui</u></strong> para escolher o plano e fazer sua assinatura Construflix.</p>\n\n<p style="margin-left: 80px;"> </p>\n\n<p style="margin-left: 80px;"><cms-plugin alt="Image - construflix-logomarca-use.png " title="Image - construflix-logomarca-use.png" id="1059"></cms-plugin></p>
1192	<p><strong>Sobre a Análise do Manual de Uso, Operação e Manutenção</strong></p>\n\n<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>O que é?</b></span></span></span></span></p>\n\n<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">É uma avaliação completa, baseada na norma ABNT NBR 14037 e nos itens aplicáveis a manuais de uso, operação e manutenção diretamente textualmente citados na ABNT NBR 15575.</span></span></span></span></p>\n\n<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Premissa</b></span></span></span></span></p>\n\n<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">A premissa da análise é fornecer uma lista de verificação tecnicamente robusta, que permita ao usuário avaliar criticamente seu manual de uso, operação e manutenção, independente de ter sido elaborado pela própria construtora ou por provedor externo.</span></span></span></span></p>\n\n<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Manual de uso, operação e manutenção</b></span></span></span></span></p>\n\n<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">De acordo com a ABNT NBR 14037:2012, é o “documento que reúne as informações necessárias para orientar as atividades de conservação, uso e manutenção da edificação e operação dos equipamentos”.</span></span></span></span></p>\n\n<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Geralmente, divide-se em: manual das áreas comuns, também conhecido como “manual do síndico”, e manual da unidade habitacional, também conhecido como “manual do proprietário”.</span></span></span></span></p>\n\n<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Metodologia</b></span></span></span></span></p>\n\n<ul>\n\t<li style="margin-top: 6.0pt; margin-right: 0cm; margin-bottom: 6.0pt; margin: 0cm 0cm 8pt 36pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Há dois tipos de lista de verificação: uma para o manual do síndico e outra para o manual do proprietário.</span></span></span></span></li>\n\t<li style="margin-top: 6.0pt; margin-right: 0cm; margin-bottom: 6.0pt; margin: 0cm 0cm 8pt 36pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">As normas aplicáveis à matéria, especialmente a ABNT NBR 14037, contêm alguns termos que podem suscitar interpretações distintas, subjetivas. Por isto, para ajudar na padronização da interpretação, desenvolvemos alguns critérios de interpretação na Tabela 1.</span></span></span></span></li>\n</ul>\n\n<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Tabela 1 – Critérios de interpretação de termos da ABNT NBR 14037</b></span></span></span></span></p>\n\n<table align="left" class="MsoTableGrid" style="border-collapse: collapse; margin-left: 5.25pt; margin-right: 5.25pt;" width="708">\n\t<tbody>\n\t\t<tr style="height: 12.75pt;">\n\t\t\t<td nowrap="nowrap" style="width: 42.3pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 12.75pt;" valign="top" width="71">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;"><b>Item</b></span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 70.85pt; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 12.75pt;" valign="top" width="118">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;"><b>Termo</b></span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td nowrap="nowrap" style="width: 11.0cm; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 12.75pt;" valign="top" width="520">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;"><b>Critério de interpretação</b></span></span></span></span></p>\n\t\t\t</td>\n\t\t</tr>\n\t\t<tr style="height: 54.75pt;">\n\t\t\t<td nowrap="nowrap" style="width: 42.3pt; border-top: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 54.75pt;" valign="top" width="71">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">4.1.1</span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 70.85pt; border-top: none; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 54.75pt;" valign="top" width="118">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Linguagem simples</span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 11.0cm; border-top: none; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 54.75pt;" valign="top" width="520">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Entende-se como o texto escrito de modo a permitir a compreensão por um leigo; no caso de expressões/termos tipicamente técnicos, cuja compreensão pelo leigo não seja possível pela mera leitura, espera-se que a informação seja passada ao usuário através de recursos que permitam a compreensão como, por exemplo, ilustrações ou definição do termo em item específico.</span></span></span></span></p>\n\t\t\t</td>\n\t\t</tr>\n\t\t<tr style="height: 30.0pt;">\n\t\t\t<td nowrap="nowrap" style="width: 42.3pt; border-top: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 30.0pt;" valign="top" width="71">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">4.1.1</span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 70.85pt; border-top: none; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 30.0pt;" valign="top" width="118">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Linguagem direta</span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 11.0cm; border-top: none; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 30.0pt;" valign="top" width="520">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Entende-se como o texto escrito de modo que a informação seja transmitida sem floreios, redundâncias, pleonasmos, adjetivos desnecessários ou informações irrelevantes. </span></span></span></span></p>\n\t\t\t</td>\n\t\t</tr>\n\t\t<tr style="height: 38.25pt;">\n\t\t\t<td nowrap="nowrap" style="width: 42.3pt; border-top: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 38.25pt;" valign="top" width="71">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">4.1.2</span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 70.85pt; border-top: none; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 38.25pt;" valign="top" width="118">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Forma didática</span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 11.0cm; border-top: none; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 38.25pt;" valign="top" width="520">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Entende-se como o texto escrito de maneira a elucidar os procedimentos, explicações, instruções, recomendações de modo auto explicativo, que não exija conhecimento prévio por parte do leitor e este não precise buscar tal conhecimento fora do manual.</span></span></span></span></p>\n\t\t\t</td>\n\t\t</tr>\n\t\t<tr style="height: 38.25pt;">\n\t\t\t<td style="width: 42.3pt; border-top: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 38.25pt;" valign="top" width="71">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">4.2.1</span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 70.85pt; border-top: none; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 38.25pt;" valign="top" width="118">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Orientação na forma de obtenção de informações</span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 11.0cm; border-top: none; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 38.25pt;" valign="top" width="520">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Entende-se como tudo o que proporciona ao usuário facilidade em encontrar informações úteis ao uso, operação e manutenção da edificação.</span></span></span></span></p>\n\t\t\t</td>\n\t\t</tr>\n\t\t<tr style="height: 33.0pt;">\n\t\t\t<td nowrap="nowrap" style="width: 42.3pt; border-top: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 33.0pt;" valign="top" width="71">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">5.7.4.5</span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 70.85pt; border-top: none; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 33.0pt;" valign="top" width="118">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Explicitamente </span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 11.0cm; border-top: none; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 33.0pt;" valign="top" width="520">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Entende-se como tudo que seja textualmente escrito, apresentado de forma categórica, sem dúvidas ou ambiguidades.</span></span></span></span></p>\n\t\t\t</td>\n\t\t</tr>\n\t\t<tr style="height: 104.25pt;">\n\t\t\t<td style="width: 42.3pt; border-top: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 104.25pt;" valign="top" width="71">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">5.5.b</span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 70.85pt; border-top: none; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 104.25pt;" valign="top" width="118">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Equipamentos previstos em projeto</span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 11.0cm; border-top: none; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 104.25pt;" valign="top" width="520">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Há dúvidas sobre o que seriam "equipamentos previstos em projeto para serem fornecidos<br>\n\t\t\te instalados pelos usuários". Nos exemplos mais simples, chuveiro e ar condicionado certamente se enquadram neste quesito, pois há uma preparação específica (local, circuito elétrico, rede frigorígena etc.). Nestes casos, o 'como instalar' possui duas situações: uma, relativa à infra-estrutura, de responsabilidade da construtora (peso máximo dos equipamentos, local para acomodação etc.) e outra, de responsabilidade do fabricante, relativo à operação de instalação, incluindo etapas, ferramentas e procedimentos adequados para pôr o equipamento em funcionamento. Entende-se que a norma se refere à 1ª situação e é sobre ela que se faz a verificação.</span></span></span></span></p>\n\t\t\t</td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p style="margin: 0cm 0cm 8pt;"><br clear="all">\n<span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Perguntas frequentes /  Exemplos</b></span></span></span></span></p>\n\n<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Nosso empreendimento possui mais de uma tipologia de unidade habitacional. É necessário fazer a análise para cada uma?</b></span></span></span></span></p>\n\n<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Em nossa visão, sim, embora seja provável que a maioria dos itens sejam iguais. Mas há diversas situações onde a diferença nas tipologias pode ser significativa. Exemplo: empreendimento de uma torre com unidades normais e unidades do tipo duplex. Nestas, haverá diferenças substanciais como, por exemplo, a existência de escada, guarda-corpo, bem como características que podem ser diferenciadas em relação à limpeza e manutenção de elementos construtivos onde há pé-direito duplo. Portanto, recomendamos que se faça a análise para cada tipologia.</span></span></span></span></p>
1196	<p><strong>Sobre a Análise de Projetos</strong></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>O que é?</b></span></span></span></span></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">É uma avaliação expedita de projeto com o objetivo de revelar seu potencial de atendimento em relação a referenciais conhecidos e ao escopo da análise. </span></span></span></span></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Premissa</b></span></span></span></span></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">A principal premissa da análise se baseia na seguinte pergunta: "Considerando a fase e o escopo do projeto em análise, a informação apresentada permitirá que o produto final atenda aos referenciais/requisitos indicados, se estiver corretamente especificada e for efetivamente aplicada no empreendimento?"</span></span></span></span></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Também é premissa que a avaliação possa ser feita em pouco tempo. Entendemos que as normas técnicas são diversas e muitas vezes complexas, e desejamos que as análises sejam compatíveis com o tempo geralmente disponível em uma construtora. Por isto, a seleção dos itens dos check-lists é feita por cuidadosa amostragem. Assim, o resultado da análise não é absoluto, mas uma boa referência sobre se o projeto está “mais para atender” ou “menos para atender” aos requisitos. Em outras palavras, se o resultado de uma análise indicar 100% de atendimento, é mais provável que o projeto analisado esteja próximo de atender a todos os requisitos do que uma análise que aponte um resultado de 15%. Mas, sob qualquer hipótese, pode-se considerar o resultado de 100% como um certificado de projeto perfeito ou coisa parecida.</span></span></span></span></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Projeto</b></span></span></span></span></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Entende-se como projeto o conjunto de informações documentadas compostas pelas representações gráfica e escrita (plantas, desenhos, memoriais, cadernos de detalhes etc.) que especifique materiais, produtos e processos capazes de atender ao desempenho estabelecido, conforme sua fase, especialidade e escopo.</span></span></span></span></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Metodologia / Diretrizes e critérios de análise</b></span></span></span></span></p>\n\n<ul>\n\t<li style="margin-top: 6.0pt; margin-right: 0cm; margin-bottom: 6.0pt; margin: 0cm 0cm 8pt 36pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Por se tratar de uma avaliação expedita, que analisa a presença de uma informação e não o seu conteúdo, o resultado da análise não conclui sobre o <b>atendimento</b> ao requisito em análise, mas sobre seu <b>potencial de atendimento</b>. Se as informações contidas no projeto estiverem tecnicamente corretas (responsabilidade do projetista), poder-se-á, então, afirmar que a porcentagem encontrada corresponde à porcentagem de atendimento ao requisito avaliado. Em resumo: a análise não conclui se o projeto atende ao requisito, mas qual seu potencial para tal.</span></span></span></span></li>\n\t<li style="margin-top: 6.0pt; margin-right: 0cm; margin-bottom: 6.0pt; margin: 0cm 0cm 8pt 36pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Consideração à fase do projeto. O nível de detalhamento requerido para realizar a análise dependerá da fase em que o projeto se encontra. Não é esperado, por exemplo, que um projeto legal (para aprovação em prefeitura) contenha detalhes de execução ou manutenção dos sistemas projetados. Não obstante, é esperado que ele possua (ou mencione) as informações necessárias para que tais detalhes sejam elaborados na etapa seguinte em conformidade com requisitos. Seguem dois exemplos que ilustram como essa diretriz pode ser atendida.</span></span></span></span>\n\t<ol>\n\t\t<li style=""><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Menção a uma norma.</b> Situação: em um Projeto Legal de Arquitetura há a representação gráfica de um guarda-corpo. Espera-se que exista informação (lista, quadro, nota etc.) indicando que o guarda-corpo deve ser projetado conforme ABNT NBR 14718. Neste caso, o item seria considerado atendido, pois, por se tratar de Projeto Legal, a informação existente remete à necessidade de uma informação completa em etapa posterior de projeto. Neste sentido, permite ao cliente do projeto saber que há uma informação a ser considerada.</span></span></span></span></li>\n\t\t<li style=""><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Delimitação do escopo.</b> Situação: em um Projeto Legal de Arquitetura há a representação de um guarda-corpo e uma nota esclarecendo que o projeto do guarda-corpo não faz parte do escopo deste projeto, devendo ser elaborado à parte, por um profissional habilitado, conforme ABNT NBR 14718. Como no caso anterior, há a consideração de que aquela informação específica deve ser tratada, mas não neste projeto. Para fins de análise, esta informação atende aos propósitos, cabendo ao cliente do projeto, neste caso, verificar se tal combinação (fazer projeto de guarda-corpo com outro profissional) foi previamente acordada com o projetista.</span></span></span></span></li>\n\t</ol>\n\t</li>\n\t<li style="margin-top: 6.0pt; margin-right: 0cm; margin-bottom: 6.0pt; margin: 0cm 0cm 8pt 36pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Recomendações normativas foram consideradas requisitos; portanto, entram na composição dos cálculos.</span></span></span></span></li>\n\t<li style="margin-top: 6.0pt; margin-right: 0cm; margin-bottom: 6.0pt; margin: 0cm 0cm 8pt 36pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">As informações avaliadas referem-se àquelas que conduzem a um desempenho intencional, ou seja, aquele planejado pela empresa; que não seja fruto de coincidência ou de componentes e elementos que façam parte da cultura construtiva local e que atendam naturalmente aos requisitos normativos. Por isto, independente de eventuais resultados satisfatórios dos métodos de avaliação (ensaios), procura-se por evidências documentadas, intencionais e mencionadas nos documentos de projeto.</span></span></span></span></li>\n\t<li style="margin-top: 6.0pt; margin-right: 0cm; margin-bottom: 6.0pt; margin: 0cm 0cm 8pt 36pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Variáveis como PROJETO e ETAPA não são padronizadas (rígidas), pois não há como garantir que o projeto avaliado ou mesmo o avaliador estejam alinhados com as definições da ABNT NBR 13.531. Portanto, esses itens são de preenchimento opcional, considerando e respeitando a cultura da empresa.</span></span></span></span></li>\n\t<li style="margin-top: 6.0pt; margin-right: 0cm; margin-bottom: 6.0pt; margin: 0cm 0cm 8pt 36pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Diretriz: caso um item seja NA (não aplicável) ao projeto em análise, mas importante para a empresa, recomenda-se manter o item NA (sem pontuação), mas fazer observações ou recomendações.</span></span></span></span></li>\n</ul>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"> </p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Perguntas frequentes /  Exemplos</b></span></span></span></span></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b><i>Se o projeto em análise contiver um elemento construtivo, mas remeter a responsabilidade técnica deste elemento a outro projeto/profissional, como se procede com a pontuação: "presente", "parcial", "ausente" ou "NA"?</i></b></span></span></span></span></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Deve-se ponderar qual seria o nível necessário de informação no projeto em análise para que o resultado final possa ser atingido. </span></span></span></span></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Exemplo: se a análise for de um Projeto Legal de Arquitetura no qual há a representação gráfica de um guarda-corpo na sacada, e este projeto informar que o projeto específico do guarda-corpo deve ser feito por outro profissional, deverá ser analisado se há alguma informação indicando que o guarda-corpo deve atender à NBR 14718 (norma de guarda-corpo). Se sim, o requisito será considerado atendido ("presente"). A intenção é não permitir "terceirização" pura e simples de responsabilidades, exceto se acordado de outra forma entre as partes (no contrato, por exemplo). O projetista deve delimitar a abrangência e limites do escopo de seu projeto, porém, demonstrando conhecimento de causa.</span></span></span></span></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Este outro exemplo, da indústria alimentícia, serve como parâmetro. Leites longa-vida possuem a seguinte inscrição na embalagem: "AVISO IMPORTANTE: ESTE PRODUTO NÃO DEVE SER USADO PARA ALIMENTAR CRIANÇAS, A NÃO SER POR INDICAÇÃO EXPRESSA DE MÉDICO OU NUTRICIONISTA. O ALEITAMENTO MATERNO EVITA INFECÇÕES E ALERGIAS E É RECOMENDADO ATÉ OS 2 (DOIS) ANOS DE IDADE OU MAIS." Note que o texto não se limitou a informar o que não se deve fazer, mas, além disto, orientou quem deve ser consultado sobre o assunto (médico ou nutricionista), informou que a indicação destes profissionais deve ser expressa (ou seja, formal), e apresentou o motivo para a recomendação do não uso por crianças (aleitamento materno); em suma: apesar de se isentar, indicou o próximo passo a seguir. É este nível de informação que se espera nos projetos avaliados, qual seja, permitir que o usuário do projeto tenha condições de trilhar o caminho que o leve ao pleno atendimento dos requisitos relacionados ao produto em questão.</span></span></span></span></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">DIRETRIZ: caso um item seja considerado não aplicável (NA) ao projeto em análise, mas importante à empresa na visão do avaliador, recomenda-se registrar o item como "NA" (não entrará na pontuação), e registrar observações ou recomendações. Apesar do método incluir uma parte de cálculo, sua essência é dispor ao cliente da avaliação informações que permitam melhorias de qualquer tipo nos projetos.</span></span></span></span></p>
1199	<h3 style="margin-left: 40px;"><span style="color: #ffffff;">Plano Básico</span></h3>\n\n<p style="margin-left: 40px;"><span style="color: #ffffff;">1 produto utilizado</span></p>\n\n<p style="margin-left: 40px;"><span style="color: #ffffff;">1 produto disponível.</span></p>\n\n<p style="margin-left: 40px;"><span style="color: #ffffff;">Vencimento do plano: 99/99/9999</span></p>\n\n<p style="margin-left: 40px;"><span style="color: #ffffff;"><u>Upgrade</u>   <u>Cancelar</u></span></p>
1201	<p> </p>\n\n<p style="margin-left: 40px;"><span style="color: #ffffff;">Para uso exclusivo do Administrador de Produtos da Construflix</span></p>
1203	<p style="margin-left: 40px;"> </p>\n\n<p style="margin-left: 40px;"><span style="color: #ffffff;">Juliana Silva      <u>Excluir</u></span></p>\n\n<p style="margin-left: 40px;"><span style="color: #ffffff;">Roberto Júnior  <u>Excluir</u></span></p>\n\n<p style="margin-left: 40px;"> </p>\n\n<p style="margin-left: 40px;"><span style="color: #ffffff;"><u>Novo Projetista</u></span></p>
352	<p><strong>Sobre a Análise de Projetos</strong></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>O que é?</b></span></span></span></span></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">É uma avaliação expedita de projeto com o objetivo de revelar seu potencial de atendimento em relação a referenciais conhecidos e ao escopo da análise. </span></span></span></span></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Premissa</b></span></span></span></span></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">A principal premissa da análise se baseia na seguinte pergunta: "Considerando a fase e o escopo do projeto em análise, a informação apresentada permitirá que o produto final atenda aos referenciais/requisitos indicados, se estiver corretamente especificada e for efetivamente aplicada no empreendimento?"</span></span></span></span></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Também é premissa que a avaliação possa ser feita em pouco tempo. Entendemos que as normas técnicas são diversas e muitas vezes complexas, e desejamos que as análises sejam compatíveis com o tempo geralmente disponível em uma construtora. Por isto, a seleção dos itens dos check-lists é feita por cuidadosa amostragem. Assim, o resultado da análise não é absoluto, mas uma boa referência sobre se o projeto está “mais para atender” ou “menos para atender” aos requisitos. Em outras palavras, se o resultado de uma análise indicar 100% de atendimento, é mais provável que o projeto analisado esteja próximo de atender a todos os requisitos do que uma análise que aponte um resultado de 15%. Mas, sob qualquer hipótese, pode-se considerar o resultado de 100% como um certificado de projeto perfeito ou coisa parecida.</span></span></span></span></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Projeto</b></span></span></span></span></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Entende-se como projeto o conjunto de informações documentadas compostas pelas representações gráfica e escrita (plantas, desenhos, memoriais, cadernos de detalhes etc.) que especifique materiais, produtos e processos capazes de atender ao desempenho estabelecido, conforme sua fase, especialidade e escopo.</span></span></span></span></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Metodologia / Diretrizes e critérios de análise</b></span></span></span></span></p>\n\n<ul>\n\t<li style="margin-top: 6.0pt; margin-right: 0cm; margin-bottom: 6.0pt; margin: 0cm 0cm 8pt 36pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Por se tratar de uma avaliação expedita, que analisa a presença de uma informação e não o seu conteúdo, o resultado da análise não conclui sobre o <b>atendimento</b> ao requisito em análise, mas sobre seu <b>potencial de atendimento</b>. Se as informações contidas no projeto estiverem tecnicamente corretas (responsabilidade do projetista), poder-se-á, então, afirmar que a porcentagem encontrada corresponde à porcentagem de atendimento ao requisito avaliado. Em resumo: a análise não conclui se o projeto atende ao requisito, mas qual seu potencial para tal.</span></span></span></span></li>\n\t<li style="margin-top: 6.0pt; margin-right: 0cm; margin-bottom: 6.0pt; margin: 0cm 0cm 8pt 36pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Consideração à fase do projeto. O nível de detalhamento requerido para realizar a análise dependerá da fase em que o projeto se encontra. Não é esperado, por exemplo, que um projeto legal (para aprovação em prefeitura) contenha detalhes de execução ou manutenção dos sistemas projetados. Não obstante, é esperado que ele possua (ou mencione) as informações necessárias para que tais detalhes sejam elaborados na etapa seguinte em conformidade com requisitos. Seguem dois exemplos que ilustram como essa diretriz pode ser atendida.</span></span></span></span>\n\t<ol>\n\t\t<li style=""><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Menção a uma norma.</b> Situação: em um Projeto Legal de Arquitetura há a representação gráfica de um guarda-corpo. Espera-se que exista informação (lista, quadro, nota etc.) indicando que o guarda-corpo deve ser projetado conforme ABNT NBR 14718. Neste caso, o item seria considerado atendido, pois, por se tratar de Projeto Legal, a informação existente remete à necessidade de uma informação completa em etapa posterior de projeto. Neste sentido, permite ao cliente do projeto saber que há uma informação a ser considerada.</span></span></span></span></li>\n\t\t<li style=""><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Delimitação do escopo.</b> Situação: em um Projeto Legal de Arquitetura há a representação de um guarda-corpo e uma nota esclarecendo que o projeto do guarda-corpo não faz parte do escopo deste projeto, devendo ser elaborado à parte, por um profissional habilitado, conforme ABNT NBR 14718. Como no caso anterior, há a consideração de que aquela informação específica deve ser tratada, mas não neste projeto. Para fins de análise, esta informação atende aos propósitos, cabendo ao cliente do projeto, neste caso, verificar se tal combinação (fazer projeto de guarda-corpo com outro profissional) foi previamente acordada com o projetista.</span></span></span></span></li>\n\t</ol>\n\t</li>\n\t<li style="margin-top: 6.0pt; margin-right: 0cm; margin-bottom: 6.0pt; margin: 0cm 0cm 8pt 36pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Recomendações normativas foram consideradas requisitos; portanto, entram na composição dos cálculos.</span></span></span></span></li>\n\t<li style="margin-top: 6.0pt; margin-right: 0cm; margin-bottom: 6.0pt; margin: 0cm 0cm 8pt 36pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">As informações avaliadas referem-se àquelas que conduzem a um desempenho intencional, ou seja, aquele planejado pela empresa; que não seja fruto de coincidência ou de componentes e elementos que façam parte da cultura construtiva local e que atendam naturalmente aos requisitos normativos. Por isto, independente de eventuais resultados satisfatórios dos métodos de avaliação (ensaios), procura-se por evidências documentadas, intencionais e mencionadas nos documentos de projeto.</span></span></span></span></li>\n\t<li style="margin-top: 6.0pt; margin-right: 0cm; margin-bottom: 6.0pt; margin: 0cm 0cm 8pt 36pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Variáveis como PROJETO e ETAPA não são padronizadas (rígidas), pois não há como garantir que o projeto avaliado ou mesmo o avaliador estejam alinhados com as definições da ABNT NBR 13.531. Portanto, esses itens são de preenchimento opcional, considerando e respeitando a cultura da empresa.</span></span></span></span></li>\n\t<li style="margin-top: 6.0pt; margin-right: 0cm; margin-bottom: 6.0pt; margin: 0cm 0cm 8pt 36pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Diretriz: caso um item seja NA (não aplicável) ao projeto em análise, mas importante para a empresa, recomenda-se manter o item NA (sem pontuação), mas fazer observações ou recomendações.</span></span></span></span></li>\n</ul>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"> </p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Perguntas frequentes /  Exemplos</b></span></span></span></span></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b><i>Se o projeto em análise contiver um elemento construtivo, mas remeter a responsabilidade técnica deste elemento a outro projeto/profissional, como se procede com a pontuação: "presente", "parcial", "ausente" ou "NA"?</i></b></span></span></span></span></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Deve-se ponderar qual seria o nível necessário de informação no projeto em análise para que o resultado final possa ser atingido. </span></span></span></span></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Exemplo: se a análise for de um Projeto Legal de Arquitetura no qual há a representação gráfica de um guarda-corpo na sacada, e este projeto informar que o projeto específico do guarda-corpo deve ser feito por outro profissional, deverá ser analisado se há alguma informação indicando que o guarda-corpo deve atender à NBR 14718 (norma de guarda-corpo). Se sim, o requisito será considerado atendido ("presente"). A intenção é não permitir "terceirização" pura e simples de responsabilidades, exceto se acordado de outra forma entre as partes (no contrato, por exemplo). O projetista deve delimitar a abrangência e limites do escopo de seu projeto, porém, demonstrando conhecimento de causa.</span></span></span></span></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Este outro exemplo, da indústria alimentícia, serve como parâmetro. Leites longa-vida possuem a seguinte inscrição na embalagem: "AVISO IMPORTANTE: ESTE PRODUTO NÃO DEVE SER USADO PARA ALIMENTAR CRIANÇAS, A NÃO SER POR INDICAÇÃO EXPRESSA DE MÉDICO OU NUTRICIONISTA. O ALEITAMENTO MATERNO EVITA INFECÇÕES E ALERGIAS E É RECOMENDADO ATÉ OS 2 (DOIS) ANOS DE IDADE OU MAIS." Note que o texto não se limitou a informar o que não se deve fazer, mas, além disto, orientou quem deve ser consultado sobre o assunto (médico ou nutricionista), informou que a indicação destes profissionais deve ser expressa (ou seja, formal), e apresentou o motivo para a recomendação do não uso por crianças (aleitamento materno); em suma: apesar de se isentar, indicou o próximo passo a seguir. É este nível de informação que se espera nos projetos avaliados, qual seja, permitir que o usuário do projeto tenha condições de trilhar o caminho que o leve ao pleno atendimento dos requisitos relacionados ao produto em questão.</span></span></span></span></p>\n\n<p style="margin-left: 18.0pt; margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">DIRETRIZ: caso um item seja considerado não aplicável (NA) ao projeto em análise, mas importante à empresa na visão do avaliador, recomenda-se registrar o item como "NA" (não entrará na pontuação), e registrar observações ou recomendações. Apesar do método incluir uma parte de cálculo, sua essência é dispor ao cliente da avaliação informações que permitam melhorias de qualquer tipo nos projetos.</span></span></span></span></p>
40	<h3 style="margin-left: 40px;"><span style="color: #ffffff;">Plano Básico</span></h3>\n\n<p style="margin-left: 40px;"><span style="color: #ffffff;">1 produto utilizado</span></p>\n\n<p style="margin-left: 40px;"><span style="color: #ffffff;">1 produto disponível.</span></p>\n\n<p style="margin-left: 40px;"><span style="color: #ffffff;">Vencimento do plano: 99/99/9999</span></p>\n\n<p style="margin-left: 40px;"><span style="color: #ffffff;"><u>Upgrade</u>   <u>Cancelar</u></span></p>
656	<p style="margin-left: 40px;"> </p>\n\n<p style="margin-left: 80px;">Clique <strong><u>aqui</u></strong> para escolher o plano e fazer sua assinatura Construflix.</p>
396	<p style="margin-left: 40px;"> </p>\n\n<p style="margin-left: 40px;"><span style="color: #ffffff;">Juliana Silva      <u>Excluir</u></span></p>\n\n<p style="margin-left: 40px;"><span style="color: #ffffff;">Roberto Júnior  <u>Excluir</u></span></p>\n\n<p style="margin-left: 40px;"> </p>\n\n<p style="margin-left: 40px;"><span style="color: #ffffff;"><u>Novo Projetista</u></span></p>
123	<p> </p>\n\n<p style="margin-left: 40px;"><span style="color: #ffffff;">Para uso exclusivo do Administrador de Produtos da Construflix</span></p>
1005	<table background="/media/filer_public_thumbnails/filer_public/ae/de/aede9d94-accb-44d2-8340-c6965c276347/mapa_contato.jpg__1170x0_q85_subsampling-2_upscale.jpg" border="0" cellpadding="1" cellspacing="1" style="width: 1200px; height: 800px;">\n</table>\n\n<p> </p>\n\n<p> </p>\n\n<p> </p>
708	<table border="0" cellpadding="1" cellspacing="1" style="width: 250px;">\n\t<tbody>\n\t\t<tr>\n\t\t\t<td style="background-color: rgb(102, 102, 102); text-align: center;">\n\t\t\t<div><small><span style="color: #ffffff;"><span style="background-color: null;">Guia de produtos e fornecedores</span></span></small></div>\n\t\t\t</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style="background-color: rgb(170, 170, 170);"><cms-plugin alt="Image - guia_de_produtos_e_fornecedores.jpg " title="Image - guia_de_produtos_e_fornecedores.jpg" id="709"></cms-plugin></td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p style="margin-left: 40px;"> </p>
1160	<table border="0" cellpadding="1" cellspacing="1" style="width: 250px;">\n\t<tbody>\n\t\t<tr>\n\t\t\t<td style="background-color: rgb(102, 102, 102); text-align: center; height: 20px;">\n\t\t\t<div><span style="color: #ffffff;"><span style="background-color: null;"><small>Olha quem já está usando...</small></span></span></div>\n\t\t\t</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style="background-color: rgb(170, 170, 170);"><span style="color: #ffffff;"><span style="background-color: #7f8c8d;"><cms-plugin alt="Image - clientes-logo.jpg " title="Image - clientes-logo.jpg" id="1161"></cms-plugin></span></span></td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p style="margin-left: 40px;"> </p>
1163	<table border="0" cellpadding="1" cellspacing="1" style="width: 250px;">\n\t<tbody>\n\t\t<tr>\n\t\t\t<td style="background-color: rgb(102, 102, 102); text-align: center;">\n\t\t\t<div><small><span style="color: #ffffff;"><span style="background-color: null;">Guia de produtos e fornecedores</span></span></small></div>\n\t\t\t</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style="background-color: rgb(170, 170, 170);"><cms-plugin alt="Image - guia_de_produtos_e_fornecedores.jpg " title="Image - guia_de_produtos_e_fornecedores.jpg" id="1164"></cms-plugin></td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p style="margin-left: 40px;"> </p>
761	<table border="0" cellpadding="1" cellspacing="1" style="width: 250px;">\n\t<tbody>\n\t\t<tr>\n\t\t\t<td style="background-color: rgb(102, 102, 102); text-align: center; height: 20px;">\n\t\t\t<div><span style="color: #ffffff;"><span style="background-color: null;"><small>Análise de manual do proprietário</small></span></span></div>\n\t\t\t</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style="background-color: rgb(170, 170, 170);"><cms-plugin alt="Image - analise_de_manual_do_proprietario.jpg " title="Image - analise_de_manual_do_proprietario.jpg" id="762"></cms-plugin></td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p style="margin-left: 40px;"> </p>
1166	<table border="0" cellpadding="1" cellspacing="1" style="width: 250px;">\n\t<tbody>\n\t\t<tr>\n\t\t\t<td style="background-color: rgb(102, 102, 102); text-align: center; height: 20px;">\n\t\t\t<div><span style="color: #ffffff;"><span style="background-color: null;"><small>Análise de projetos</small></span></span></div>\n\t\t\t</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style="background-color: rgb(170, 170, 170);"><cms-plugin alt="Image - analise_de_projetos.jpg " title="Image - analise_de_projetos.jpg" id="1167"></cms-plugin></td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p style="margin-left: 40px;"> </p>
1169	<table border="0" cellpadding="1" cellspacing="1" style="width: 250px;">\n\t<tbody>\n\t\t<tr>\n\t\t\t<td style="background-color: rgb(102, 102, 102); text-align: center; height: 20px;">\n\t\t\t<div><span style="color: #ffffff;"><span style="background-color: null;"><small>Análise de manual do proprietário</small></span></span></div>\n\t\t\t</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style="background-color: rgb(170, 170, 170);"><cms-plugin alt="Image - analise_de_manual_do_proprietario.jpg " title="Image - analise_de_manual_do_proprietario.jpg" id="1170"></cms-plugin></td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p style="margin-left: 40px;"> </p>
1171	<table border="0" cellpadding="0" cellspacing="0" style="width: 1240px;">\n\t<tbody>\n\t\t<tr>\n\t\t\t<td style="background-color: rgb(51, 51, 51); width: 1200px;"> </td>\n\t\t</tr>\n\t</tbody>\n</table>
753	<table border="0" cellpadding="1" cellspacing="1" style="width: 250px;">\n\t<tbody>\n\t\t<tr>\n\t\t\t<td style="background-color: rgb(102, 102, 102); text-align: center; height: 20px;">\n\t\t\t<div><span style="color: #ffffff;"><span style="background-color: null;"><small>Análise de projetos</small></span></span></div>\n\t\t\t</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style="background-color: rgb(170, 170, 170);"><cms-plugin alt="Image - analise_de_projetos.jpg " title="Image - analise_de_projetos.jpg" id="754"></cms-plugin></td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p style="margin-left: 40px;"> </p>
597	<p><strong>Sobre a Análise do Manual de Uso, Operação e Manutenção</strong></p>\n\n<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>O que é?</b></span></span></span></span></p>\n\n<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">É uma avaliação completa, baseada na norma ABNT NBR 14037 e nos itens aplicáveis a manuais de uso, operação e manutenção diretamente textualmente citados na ABNT NBR 15575.</span></span></span></span></p>\n\n<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Premissa</b></span></span></span></span></p>\n\n<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">A premissa da análise é fornecer uma lista de verificação tecnicamente robusta, que permita ao usuário avaliar criticamente seu manual de uso, operação e manutenção, independente de ter sido elaborado pela própria construtora ou por provedor externo.</span></span></span></span></p>\n\n<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Manual de uso, operação e manutenção</b></span></span></span></span></p>\n\n<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">De acordo com a ABNT NBR 14037:2012, é o “documento que reúne as informações necessárias para orientar as atividades de conservação, uso e manutenção da edificação e operação dos equipamentos”.</span></span></span></span></p>\n\n<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Geralmente, divide-se em: manual das áreas comuns, também conhecido como “manual do síndico”, e manual da unidade habitacional, também conhecido como “manual do proprietário”.</span></span></span></span></p>\n\n<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Metodologia</b></span></span></span></span></p>\n\n<ul>\n\t<li style="margin-top: 6.0pt; margin-right: 0cm; margin-bottom: 6.0pt; margin: 0cm 0cm 8pt 36pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Há dois tipos de lista de verificação: uma para o manual do síndico e outra para o manual do proprietário.</span></span></span></span></li>\n\t<li style="margin-top: 6.0pt; margin-right: 0cm; margin-bottom: 6.0pt; margin: 0cm 0cm 8pt 36pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">As normas aplicáveis à matéria, especialmente a ABNT NBR 14037, contêm alguns termos que podem suscitar interpretações distintas, subjetivas. Por isto, para ajudar na padronização da interpretação, desenvolvemos alguns critérios de interpretação na Tabela 1.</span></span></span></span></li>\n</ul>\n\n<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Tabela 1 – Critérios de interpretação de termos da ABNT NBR 14037</b></span></span></span></span></p>\n\n<table align="left" class="MsoTableGrid" style="border-collapse: collapse; margin-left: 5.25pt; margin-right: 5.25pt;" width="708">\n\t<tbody>\n\t\t<tr style="height: 12.75pt;">\n\t\t\t<td nowrap="nowrap" style="width: 42.3pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 12.75pt;" valign="top" width="71">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;"><b>Item</b></span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 70.85pt; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 12.75pt;" valign="top" width="118">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;"><b>Termo</b></span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td nowrap="nowrap" style="width: 11.0cm; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 12.75pt;" valign="top" width="520">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;"><b>Critério de interpretação</b></span></span></span></span></p>\n\t\t\t</td>\n\t\t</tr>\n\t\t<tr style="height: 54.75pt;">\n\t\t\t<td nowrap="nowrap" style="width: 42.3pt; border-top: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 54.75pt;" valign="top" width="71">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">4.1.1</span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 70.85pt; border-top: none; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 54.75pt;" valign="top" width="118">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Linguagem simples</span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 11.0cm; border-top: none; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 54.75pt;" valign="top" width="520">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Entende-se como o texto escrito de modo a permitir a compreensão por um leigo; no caso de expressões/termos tipicamente técnicos, cuja compreensão pelo leigo não seja possível pela mera leitura, espera-se que a informação seja passada ao usuário através de recursos que permitam a compreensão como, por exemplo, ilustrações ou definição do termo em item específico.</span></span></span></span></p>\n\t\t\t</td>\n\t\t</tr>\n\t\t<tr style="height: 30.0pt;">\n\t\t\t<td nowrap="nowrap" style="width: 42.3pt; border-top: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 30.0pt;" valign="top" width="71">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">4.1.1</span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 70.85pt; border-top: none; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 30.0pt;" valign="top" width="118">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Linguagem direta</span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 11.0cm; border-top: none; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 30.0pt;" valign="top" width="520">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Entende-se como o texto escrito de modo que a informação seja transmitida sem floreios, redundâncias, pleonasmos, adjetivos desnecessários ou informações irrelevantes. </span></span></span></span></p>\n\t\t\t</td>\n\t\t</tr>\n\t\t<tr style="height: 38.25pt;">\n\t\t\t<td nowrap="nowrap" style="width: 42.3pt; border-top: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 38.25pt;" valign="top" width="71">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">4.1.2</span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 70.85pt; border-top: none; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 38.25pt;" valign="top" width="118">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Forma didática</span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 11.0cm; border-top: none; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 38.25pt;" valign="top" width="520">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Entende-se como o texto escrito de maneira a elucidar os procedimentos, explicações, instruções, recomendações de modo auto explicativo, que não exija conhecimento prévio por parte do leitor e este não precise buscar tal conhecimento fora do manual.</span></span></span></span></p>\n\t\t\t</td>\n\t\t</tr>\n\t\t<tr style="height: 38.25pt;">\n\t\t\t<td style="width: 42.3pt; border-top: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 38.25pt;" valign="top" width="71">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">4.2.1</span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 70.85pt; border-top: none; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 38.25pt;" valign="top" width="118">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Orientação na forma de obtenção de informações</span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 11.0cm; border-top: none; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 38.25pt;" valign="top" width="520">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Entende-se como tudo o que proporciona ao usuário facilidade em encontrar informações úteis ao uso, operação e manutenção da edificação.</span></span></span></span></p>\n\t\t\t</td>\n\t\t</tr>\n\t\t<tr style="height: 33.0pt;">\n\t\t\t<td nowrap="nowrap" style="width: 42.3pt; border-top: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 33.0pt;" valign="top" width="71">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">5.7.4.5</span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 70.85pt; border-top: none; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 33.0pt;" valign="top" width="118">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Explicitamente </span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 11.0cm; border-top: none; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 33.0pt;" valign="top" width="520">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Entende-se como tudo que seja textualmente escrito, apresentado de forma categórica, sem dúvidas ou ambiguidades.</span></span></span></span></p>\n\t\t\t</td>\n\t\t</tr>\n\t\t<tr style="height: 104.25pt;">\n\t\t\t<td style="width: 42.3pt; border-top: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 104.25pt;" valign="top" width="71">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">5.5.b</span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 70.85pt; border-top: none; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 104.25pt;" valign="top" width="118">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Equipamentos previstos em projeto</span></span></span></span></p>\n\t\t\t</td>\n\t\t\t<td style="width: 11.0cm; border-top: none; border-left: none; padding: 0cm 5.4pt 0cm 5.4pt; height: 104.25pt;" valign="top" width="520">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;">Há dúvidas sobre o que seriam "equipamentos previstos em projeto para serem fornecidos<br>\n\t\t\te instalados pelos usuários". Nos exemplos mais simples, chuveiro e ar condicionado certamente se enquadram neste quesito, pois há uma preparação específica (local, circuito elétrico, rede frigorígena etc.). Nestes casos, o 'como instalar' possui duas situações: uma, relativa à infra-estrutura, de responsabilidade da construtora (peso máximo dos equipamentos, local para acomodação etc.) e outra, de responsabilidade do fabricante, relativo à operação de instalação, incluindo etapas, ferramentas e procedimentos adequados para pôr o equipamento em funcionamento. Entende-se que a norma se refere à 1ª situação e é sobre ela que se faz a verificação.</span></span></span></span></p>\n\t\t\t</td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p style="margin: 0cm 0cm 8pt;"><br clear="all">\n<span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Perguntas frequentes /  Exemplos</b></span></span></span></span></p>\n\n<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Nosso empreendimento possui mais de uma tipologia de unidade habitacional. É necessário fazer a análise para cada uma?</b></span></span></span></span></p>\n\n<p style="margin: 0cm 0cm 8pt;"><span style="color: null;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Em nossa visão, sim, embora seja provável que a maioria dos itens sejam iguais. Mas há diversas situações onde a diferença nas tipologias pode ser significativa. Exemplo: empreendimento de uma torre com unidades normais e unidades do tipo duplex. Nestas, haverá diferenças substanciais como, por exemplo, a existência de escada, guarda-corpo, bem como características que podem ser diferenciadas em relação à limpeza e manutenção de elementos construtivos onde há pé-direito duplo. Portanto, recomendamos que se faça a análise para cada tipologia.</span></span></span></span></p>
710	<table border="0" cellpadding="0" cellspacing="0" style="width: 1240px;">\n\t<tbody>\n\t\t<tr>\n\t\t\t<td style="background-color: rgb(51, 51, 51); width: 1200px;"> </td>\n\t\t</tr>\n\t</tbody>\n</table>
1115	<table border="0" cellpadding="0" cellspacing="0" style="width: 1170px;">\n\t<tbody>\n\t\t<tr>\n\t\t\t<td style="width: 20px; text-align: center;"> </td>\n\t\t\t<td style="width: 319px; text-align: center; background-color: rgb(102, 102, 102);"><small><span style="color: #ffffff;"><span style="font-size: 12pt;"><span style="line-height: 100%;"><span style="font-family: Calibri,sans-serif;"><span style="background-color: null;">Guia de produtos e fornecedores</span></span></span></span></span></small></td>\n\t\t\t<td style="width: 13px;"> </td>\n\t\t\t<td style="background-color: rgb(102, 102, 102);"><small><span style="color: #ffffff;"><span style="font-size: 10pt;"><span style="line-height: 90%;"><span style="font-family: Calibri,sans-serif;"> É uma poderosa ferramenta de busca  e consulta que reúne informações dos principais programas de avaliação de conformidade </span></span></span></span></small></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style="width: 20px;"> </td>\n\t\t\t<td style="width: 319px; height: 235px;"><cms-plugin alt="Image - guia_de_produtos_e_fornecedores.jpg " title="Image - guia_de_produtos_e_fornecedores.jpg" id="1116"></cms-plugin></td>\n\t\t\t<td style="width: 13px;"> </td>\n\t\t\t<td style="width: 850px; background-color: rgb(102, 102, 102);">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: #ffffff;"><span style="font-size: 10pt;"><span style="line-height: 80%;"><span style="font-family: Calibri,sans-serif;"> </span></span></span></span><small><span style="color: #ffffff;"><span style="font-size: 10pt;"><span style="line-height: 90%;"><span style="font-family: Calibri,sans-serif;">brasileiros (PSQ, </span></span></span></span></small><small><span style="color: #ffffff;"><span style="font-size: 10pt;"><span style="line-height: 90%;"><span style="font-family: Calibri,sans-serif;">SBAC),</span></span></span></span></small><span style="color: #ffffff;"><span style="font-size: 10pt;"><span style="line-height: 80%;"><span style="font-family: Calibri,sans-serif;"> facilitando incrivelmente a busca por fornecedores e materiais que atendam às normas técnicas aplicáveis. Com o guia é possível:</span></span></span></span></p>\n\n\t\t\t<p style="margin-left: 40px;"><span style="color: #ffffff;"><span style="font-size: 10pt;"><span style="line-height: 80%;"><span style="font-family: Calibri,sans-serif;">. saber quem são os fornecedores e produtos "qualificados" e "não conformes" no respectivo PSQ do PBQP-H*;</span></span></span></span><br>\n\t\t\t<span style="color: #ffffff;"><span style="font-size: 10pt;"><span style="line-height: 80%;"><span style="font-family: Calibri,sans-serif;">. acessar a página do INMETRO para buscar produtos certificados;<br>\n\t\t\t. </span></span></span></span><span style="color: #ffffff;"><span style="font-size: 10pt;"><span style="line-height: 80%;"><span style="font-family: Calibri,sans-serif;">qualificar fornecedores no atendimento às normas (requisito de qualificação de fornecedores do SiAC e da ISO 9001);</span></span></span></span><br>\n\t\t\t<span style="color: #ffffff;"><span style="font-size: 10pt;"><span style="line-height: 80%;"><span style="font-family: Calibri,sans-serif;">. baixar relatórios, certificados** e documentos de programas de avaliação de conformidade de produtos da construção civil;<br>\n\t\t\t. </span></span></span></span><span style="color: #ffffff;"><span style="font-size: 10pt;"><span style="line-height: 80%;"><span style="font-family: Calibri,sans-serif;">comprovar a conformidade da compra de um produto em uma determinada data </span></span></span></span><span style="color: #ffffff;"><span style="font-size: 11pt;"><span style="line-height: 80%;"><span style="font-family: Calibri,sans-serif;">e muito mais.</span></span></span></span></p>\n\n\t\t\t<p align="right" style="text-align: right; margin: 0cm 0cm 8pt;"><span style="color: #ffffff;"><span style="font-size: 9pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;"><i><span style="font-size: 9pt;">*A partir de nov/2017.          **Ver Termos de uso. </span></i></span></span></span></span></p>\n\t\t\t</td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p> </p>\n\n<table border="0" cellpadding="0" cellspacing="0" style="width: 1170px;">\n\t<tbody>\n\t\t<tr>\n\t\t\t<td style="text-align: center; width: 20px;"> </td>\n\t\t\t<td style="background-color: rgb(102, 102, 102); text-align: center; width: 319px;"><small><span style="color: #ffffff;"><span style="font-size: 12pt;"><span style="line-height: 100%;"><span style="font-family: Calibri,sans-serif;"><span style="background-color: null;">Análise de projetos</span></span></span></span></span></small></td>\n\t\t\t<td style="width: 13px;"> </td>\n\t\t\t<td style="width: 850px; background-color: rgb(102, 102, 102);"> </td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style="width: 20px;"> </td>\n\t\t\t<td style="width: 319px;"><cms-plugin alt="Image - analise_de_projetos.jpg " title="Image - analise_de_projetos.jpg" id="1118"></cms-plugin></td>\n\t\t\t<td style="width: 13px;"> </td>\n\t\t\t<td style="background-color: rgb(102, 102, 102); width: 319px;">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: #ffffff;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><span style="font-size: 10.0pt;"><span style="line-height: 107%;">Trata-se de um conjunto de listas de verificação, cada uma com escopo específico, que permitem uma análise expedita do potencial de  atendimento de um projeto a um determinado requisito/norma. Com estas listas é possível:</span></span></span></span></span></span></p>\n\n\t\t\t<p style="margin-left: 40px;"><span style="color: #ffffff;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><span style="font-size: 10.0pt;"><span style="line-height: 107%;">   . analisar rapidamente se um projeto atende ou tem potencial para atender a um determinado requisito ou norma técnica;</span></span></span></span></span></span></p>\n\n\t\t\t<p style="margin-left: 40px;"><span style="color: #ffffff;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><span style="font-size: 10.0pt;"><span style="line-height: 107%;">   . padronizar um critério técnico para análise e recebimento de projetos;</span></span></span></span></span></span></p>\n\n\t\t\t<p style="margin-left: 40px;"><span style="color: #ffffff;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><span style="font-size: 10.0pt;"><span style="line-height: 107%;">   . acompanhar a evolução do atendimento do projeto e informá-la ao projetista por meio de gráficos;</span></span></span></span></span></span></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 36pt;"><span style="color: #ffffff;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><span style="font-size: 10.0pt;"><span style="line-height: 107%;">. informar diretamente ao projetista os principais itens que estão faltando no projeto, com base em normas técnicas.</span></span></span></span></span></span></p>\n\t\t\t</td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p> </p>\n\n<table border="0" cellpadding="0" cellspacing="0" style="width: 1170px;">\n\t<tbody>\n\t\t<tr>\n\t\t\t<td style="text-align: center; width: 20px;"> </td>\n\t\t\t<td style="background-color: rgb(102, 102, 102); text-align: center; width: 319px;"><small><span style="color: #ffffff;"><span style="font-size: 12pt;"><span style="line-height: 100%;"><span style="font-family: Calibri,sans-serif;"><span style="background-color: null;">Análise do Manual de Uso, Operação e Manutenção</span></span></span></span></span></small></td>\n\t\t\t<td style="width: 13px;"> </td>\n\t\t\t<td style="width: 850px; background-color: rgb(102, 102, 102);"><span style="color: #ffffff;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><span style="font-size: 10.0pt;"><span style="line-height: 107%;"> O manual do síndico ou do proprietário está de acordo com as normas? É o que esta ferramenta responde.</span></span></span></span></span></span></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style="width: 20px;"> </td>\n\t\t\t<td style="width: 319px;"><cms-plugin alt="Image - analise_de_manual_do_proprietario.jpg " title="Image - analise_de_manual_do_proprietario.jpg" id="1117"></cms-plugin></td>\n\t\t\t<td style="width: 13px;"> </td>\n\t\t\t<td style="background-color: rgb(102, 102, 102); width: 319px;">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: #ffffff;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><span style="font-size: 10.0pt;"><span style="line-height: 107%;">Trata-se de duas listas de verificação de manuais de uso, operação e manutenção, sendo uma para o manual do síndico (áreas comuns) e outra para o manual do proprietário (unidades habitacionais), criadas com 100% dos requisitos da NBR 14037 e da NBR 15575 (norma de desempenho*), que permitem uma análise completa do atendimento do manual às normas. </span></span></span></span></span></span><span style="color: #ffffff;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><span style="font-size: 10.0pt;"><span style="line-height: 107%;">Com estas listas é possível:</span></span></span></span></span></span></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 40px;"><span style="color: #ffffff;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><span style="font-size: 10.0pt;"><span style="line-height: 107%;">. realizar uma análise completa do manual de uso, operação e manutenção com base nas normas aplicáveis;</span></span></span></span></span></span><br>\n\t\t\t<font color="#ffffff" face="Calibri, sans-serif"><span style="font-size: 13.3333px;">. avaliar tecnicamente o trabalho do provedor externo contratado para elaboração do manual;</span></font><br>\n\t\t\t<font color="#ffffff" face="Calibri, sans-serif"><span style="font-size: 13.3333px;">. acompanhar a evolução do atendimento do manual e informá-lo ao profissional contratado para confeccionar o manual;</span></font><br>\n\t\t\t<font color="#ffffff" face="Calibri, sans-serif"><span style="font-size: 13.3333px;">. informar os itens que estão faltando no manual com base em normas técnicas.</span></font></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 40px; text-align: right;"><em><font color="#ffffff" face="Calibri, sans-serif"><span style="font-size: 9px;">* Itens do NBR 15575 aplicáveis aos manuais. Ver termos de Uso para maiores detalhes. </span></font></em></p>\n\t\t\t</td>\n\t\t</tr>\n\t</tbody>\n</table>
805	<table border="0" cellpadding="0" cellspacing="0" style="width: 1200px;">\n\t<tbody>\n\t\t<tr>\n\t\t\t<td style="width: 319px; text-align: center; background-color: rgb(170, 170, 170);"><small><span style="color: #ffffff;"><span style="background-color: null;">Análise de Projetos</span></span></small></td>\n\t\t\t<td style="width: 20px;"> </td>\n\t\t\t<td style="background-color: rgb(170, 170, 170);"> </td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style="width: 319px;"><cms-plugin alt="Image - analise_de_projetos.jpg " title="Image - analise_de_projetos.jpg" id="806"></cms-plugin></td>\n\t\t\t<td style="width: 20px;"> </td>\n\t\t\t<td style="width: 900px; background-color: rgb(170, 170, 170);">\n\t\t\t<p style="margin-left: 40px;"><span style="color: #ffffff;">A<small>NÁLISE EXPRESSA DAS INFORMAÇÕES CONTIDAS NO PROJETO</small></span></p>\n\n\t\t\t<p style="margin-left: 40px;"><small><span style="color: #ffffff;">Avaliação amostral cujo objetivo é analisar, por amostragem, a porcentagem das informações presentes no projeto, em comparação a referenciais conhecidos. Trata-se de uma avaliação qualitativa capaz de inferir o potencial de atendimento do projeto aos referenciais estabelecidos, especialmente à norma de desempenho.</span></small></p>\n\t\t\t</td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p style="margin-left: 40px;"> </p>
801	<table border="0" cellpadding="0" cellspacing="0" style="width: 1170px;">\n\t<tbody>\n\t\t<tr>\n\t\t\t<td style="width: 20px; text-align: center;"> </td>\n\t\t\t<td style="width: 319px; text-align: center; background-color: rgb(102, 102, 102);"><small><span style="color: #ffffff;"><span style="font-size: 12pt;"><span style="line-height: 100%;"><span style="font-family: Calibri,sans-serif;"><span style="background-color: null;">Guia de produtos e fornecedores</span></span></span></span></span></small></td>\n\t\t\t<td style="width: 13px;"> </td>\n\t\t\t<td style="background-color: rgb(102, 102, 102);"><small><span style="color: #ffffff;"><span style="font-size: 10pt;"><span style="line-height: 90%;"><span style="font-family: Calibri,sans-serif;"> É uma poderosa ferramenta de busca  e consulta que reúne informações dos principais programas de avaliação de conformidade </span></span></span></span></small></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style="width: 20px;"> </td>\n\t\t\t<td style="width: 319px; height: 235px;"><cms-plugin alt="Image - guia_de_produtos_e_fornecedores.jpg " title="Image - guia_de_produtos_e_fornecedores.jpg" id="802"></cms-plugin></td>\n\t\t\t<td style="width: 13px;"> </td>\n\t\t\t<td style="width: 850px; background-color: rgb(102, 102, 102);">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: #ffffff;"><span style="font-size: 10pt;"><span style="line-height: 80%;"><span style="font-family: Calibri,sans-serif;"> </span></span></span></span><small><span style="color: #ffffff;"><span style="font-size: 10pt;"><span style="line-height: 90%;"><span style="font-family: Calibri,sans-serif;">brasileiros (PSQ, </span></span></span></span></small><small><span style="color: #ffffff;"><span style="font-size: 10pt;"><span style="line-height: 90%;"><span style="font-family: Calibri,sans-serif;">SBAC),</span></span></span></span></small><span style="color: #ffffff;"><span style="font-size: 10pt;"><span style="line-height: 80%;"><span style="font-family: Calibri,sans-serif;"> facilitando incrivelmente a busca por fornecedores e materiais que atendam às normas técnicas aplicáveis. Com o guia é possível:</span></span></span></span></p>\n\n\t\t\t<p style="margin-left: 40px;"><span style="color: #ffffff;"><span style="font-size: 10pt;"><span style="line-height: 80%;"><span style="font-family: Calibri,sans-serif;">. saber quem são os fornecedores e produtos "qualificados" e "não conformes" no respectivo PSQ do PBQP-H*;</span></span></span></span><br>\n\t\t\t<span style="color: #ffffff;"><span style="font-size: 10pt;"><span style="line-height: 80%;"><span style="font-family: Calibri,sans-serif;">. acessar a página do INMETRO para buscar produtos certificados;<br>\n\t\t\t. </span></span></span></span><span style="color: #ffffff;"><span style="font-size: 10pt;"><span style="line-height: 80%;"><span style="font-family: Calibri,sans-serif;">qualificar fornecedores no atendimento às normas (requisito de qualificação de fornecedores do SiAC e da ISO 9001);</span></span></span></span><br>\n\t\t\t<span style="color: #ffffff;"><span style="font-size: 10pt;"><span style="line-height: 80%;"><span style="font-family: Calibri,sans-serif;">. baixar relatórios, certificados** e documentos de programas de avaliação de conformidade de produtos da construção civil;<br>\n\t\t\t. </span></span></span></span><span style="color: #ffffff;"><span style="font-size: 10pt;"><span style="line-height: 80%;"><span style="font-family: Calibri,sans-serif;">comprovar a conformidade da compra de um produto em uma determinada data </span></span></span></span><span style="color: #ffffff;"><span style="font-size: 11pt;"><span style="line-height: 80%;"><span style="font-family: Calibri,sans-serif;">e muito mais.</span></span></span></span></p>\n\n\t\t\t<p align="right" style="text-align: right; margin: 0cm 0cm 8pt;"><span style="color: #ffffff;"><span style="font-size: 9pt;"><span style="line-height: normal;"><span style="font-family: Calibri,sans-serif;"><i><span style="font-size: 9pt;">*A partir de nov/2017.          **Ver Termos de uso. </span></i></span></span></span></span></p>\n\t\t\t</td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p> </p>\n\n<table border="0" cellpadding="0" cellspacing="0" style="width: 1170px;">\n\t<tbody>\n\t\t<tr>\n\t\t\t<td style="text-align: center; width: 20px;"> </td>\n\t\t\t<td style="background-color: rgb(102, 102, 102); text-align: center; width: 319px;"><small><span style="color: #ffffff;"><span style="font-size: 12pt;"><span style="line-height: 100%;"><span style="font-family: Calibri,sans-serif;"><span style="background-color: null;">Análise de projetos</span></span></span></span></span></small></td>\n\t\t\t<td style="width: 13px;"> </td>\n\t\t\t<td style="width: 850px; background-color: rgb(102, 102, 102);"> </td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style="width: 20px;"> </td>\n\t\t\t<td style="width: 319px;"><cms-plugin alt="Image - analise_de_projetos.jpg " title="Image - analise_de_projetos.jpg" id="1112"></cms-plugin></td>\n\t\t\t<td style="width: 13px;"> </td>\n\t\t\t<td style="background-color: rgb(102, 102, 102); width: 319px;">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: #ffffff;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><span style="font-size: 10.0pt;"><span style="line-height: 107%;">Trata-se de um conjunto de listas de verificação, cada uma com escopo específico, que permitem uma análise expedita do potencial de  atendimento de um projeto a um determinado requisito/norma. Com estas listas é possível:</span></span></span></span></span></span></p>\n\n\t\t\t<p style="margin-left: 40px;"><span style="color: #ffffff;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><span style="font-size: 10.0pt;"><span style="line-height: 107%;">   . analisar rapidamente se um projeto atende ou tem potencial para atender a um determinado requisito ou norma técnica;</span></span></span></span></span></span></p>\n\n\t\t\t<p style="margin-left: 40px;"><span style="color: #ffffff;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><span style="font-size: 10.0pt;"><span style="line-height: 107%;">   . padronizar um critério técnico para análise e recebimento de projetos;</span></span></span></span></span></span></p>\n\n\t\t\t<p style="margin-left: 40px;"><span style="color: #ffffff;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><span style="font-size: 10.0pt;"><span style="line-height: 107%;">   . acompanhar a evolução do atendimento do projeto e informá-la ao projetista por meio de gráficos;</span></span></span></span></span></span></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 36pt;"><span style="color: #ffffff;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><span style="font-size: 10.0pt;"><span style="line-height: 107%;">. informar diretamente ao projetista os principais itens que estão faltando no projeto, com base em normas técnicas.</span></span></span></span></span></span></p>\n\t\t\t</td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p> </p>\n\n<table border="0" cellpadding="0" cellspacing="0" style="width: 1170px;">\n\t<tbody>\n\t\t<tr>\n\t\t\t<td style="text-align: center; width: 20px;"> </td>\n\t\t\t<td style="background-color: rgb(102, 102, 102); text-align: center; width: 319px;"><small><span style="color: #ffffff;"><span style="font-size: 12pt;"><span style="line-height: 100%;"><span style="font-family: Calibri,sans-serif;"><span style="background-color: null;">Análise do Manual de Uso, Operação e Manutenção</span></span></span></span></span></small></td>\n\t\t\t<td style="width: 13px;"> </td>\n\t\t\t<td style="width: 850px; background-color: rgb(102, 102, 102);"><span style="color: #ffffff;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><span style="font-size: 10.0pt;"><span style="line-height: 107%;"> O manual do síndico ou do proprietário está de acordo com as normas? É o que esta ferramenta responde.</span></span></span></span></span></span></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style="width: 20px;"> </td>\n\t\t\t<td style="width: 319px;"><cms-plugin alt="Image - analise_de_manual_do_proprietario.jpg " title="Image - analise_de_manual_do_proprietario.jpg" id="818"></cms-plugin></td>\n\t\t\t<td style="width: 13px;"> </td>\n\t\t\t<td style="background-color: rgb(102, 102, 102); width: 319px;">\n\t\t\t<p style="margin: 0cm 0cm 8pt;"><span style="color: #ffffff;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><span style="font-size: 10.0pt;"><span style="line-height: 107%;">Trata-se de duas listas de verificação de manuais de uso, operação e manutenção, sendo uma para o manual do síndico (áreas comuns) e outra para o manual do proprietário (unidades habitacionais), criadas com 100% dos requisitos da NBR 14037 e da NBR 15575 (norma de desempenho*), que permitem uma análise completa do atendimento do manual às normas. </span></span></span></span></span></span><span style="color: #ffffff;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><span style="font-size: 10.0pt;"><span style="line-height: 107%;">Com estas listas é possível:</span></span></span></span></span></span></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 40px;"><span style="color: #ffffff;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><span style="font-size: 10.0pt;"><span style="line-height: 107%;">. realizar uma análise completa do manual de uso, operação e manutenção com base nas normas aplicáveis;</span></span></span></span></span></span><br>\n\t\t\t<font color="#ffffff" face="Calibri, sans-serif"><span style="font-size: 13.3333px;">. avaliar tecnicamente o trabalho do provedor externo contratado para elaboração do manual;</span></font><br>\n\t\t\t<font color="#ffffff" face="Calibri, sans-serif"><span style="font-size: 13.3333px;">. acompanhar a evolução do atendimento do manual e informá-lo ao profissional contratado para confeccionar o manual;</span></font><br>\n\t\t\t<font color="#ffffff" face="Calibri, sans-serif"><span style="font-size: 13.3333px;">. informar os itens que estão faltando no manual com base em normas técnicas.</span></font></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 40px; text-align: right;"><em><font color="#ffffff" face="Calibri, sans-serif"><span style="font-size: 9px;">* Itens do NBR 15575 aplicáveis aos manuais. Ver termos de Uso para maiores detalhes. </span></font></em></p>\n\t\t\t</td>\n\t\t</tr>\n\t</tbody>\n</table>
1122	<table border="0" cellpadding="1" cellspacing="1" style="width: 1000px;">\n\t<tbody>\n\t\t<tr>\n\t\t\t<td>\n\t\t\t<h3 style="margin: 0cm 0cm 8pt 80px;"> </h3>\n\n\t\t\t<h3 style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>O que é?</b></span></span></big></span></h3>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Construflix é uma plataforma web de ferramentas (também chamadas aplicações) que ajudam o usuário – profissionais e empresas que atuam na construção civil - a atender à norma de desempenho (ABNT NBR 15575) e normas de gestão da qualidade (SiAC/PBQP-H, ISO 9001) de forma fácil e intuitiva.</span></span></span></span><br>\n\t\t\t </p>\n\n\t\t\t<h3 style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Como surgiu?</b></span></span></big></span></h3>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">O atendimento à norma de desempenho é obrigatório para todas as empresas e profissionais que trabalham com construção de edificações habitacionais, enquanto o atendimento ao PBQP-H/SiAC é obrigatório para muitas empresas, especialmente as que necessitam de órgãos financiadores.</span></span></span></span></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Grandes <i>players</i> do mercado possuem departamentos de P&amp;D, contratam consultores renomados e têm acesso a tecnologia de ponta. Mas, e a esmagadora maioria de profissionais e empresas que não podem contar com as mesmas facilidades? É onde entra o Construflix.</span></span></span></big></span></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Nossa plataforma “traduz” as normas citadas de uma forma mais fácil de entender e aplicar. Sabemos que há no Brasil milhares de profissionais e empresas de arquitetura e construção que conhecem suas responsabilidades e gostariam de aplicar as normas em seus trabalhos, mas não sabem por onde começar ou não tem o recurso financeiro para isto. Foi daí que surgimos.</span></span></span></big></span></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big> </big></span></p>\n\n\t\t\t<h3 style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Qual o objetivo do Construflix?</b></span></span></big></span></h3>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Nós queremos: </span></span></span></big></span></p>\n\n\t\t\t<ul>\n\t\t\t\t<li style="margin-left: 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">revolucionar o mercado da construção civil quanto ao atendimento à norma de desempenho e às normas de Gestão da Qualidade;</span></span></span></big></span></li>\n\t\t\t\t<li style="margin-left: 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">traduzir normas complicadas em algo mais fácil para os profissionais de projeto e construção. Sabemos que a formação em Arquitetura e Engenharia Civil no Brasil  está longe de fornecer as ferramentas necessárias para os alunos conhecerem e aplicarem normas de qualidade e desempenho na construção civil. Viemos para facilitar esse processo;</span></span></span></big></span></li>\n\t\t\t\t<li style="margin-left: 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">diminuir drasticamente os custos com consultorias, permitindo acesso a ferramentas fáceis de utilizar;</span></span></span></big></span></li>\n\t\t\t\t<li style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">por fim, queremos ver a norma de desempenho sendo efetivamente aplicada e, com isto, contribuir para que a construção civil ofereça a seus consumidores edificações mais seguras, confortáveis e duráveis.</span></span></span></big></span><br>\n\t\t\t\t </li>\n\t\t\t</ul>\n\n\t\t\t<h3 style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Posso comparar o Construflix com o quê?</b></span></span></big></span></h3>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Na construção civil, com nada! Nossa plataforma é inovadora e não possui concorrência. Ao contrário, nossas aplicações são complementares a outras aplicações <i>online</i> para construção civil disponíveis no mercado.</span></span></span></big></span></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Para efeito didático, usando uma outra área de atuação, podemos comparar com a educação física. Uma pessoa pode contratar um <i>personal trainer</i> para lhe orientar específica e individualmente. Isto é ótimo, mas, obviamente, possui um custo muito alto. Porém, há uma alternativa muito mais barata que pode ser quase tão boa quanto: usar os serviços de uma academia. Nela você tem acesso a todos os aparelhos, possui alguma orientação técnica e, com seu próprio esforço, pode atingir resultados altamente satisfatórios, a um custo muito mais baixo. O Construflix é parecido: você paga um plano mensal conforme sua necessidade, tem acesso a ferramentas técnicas cuidadosamente preparadas por profissionais de alto gabarito e dispõe de instruções para uso em tutoriais e/ou vídeos. O resto só depende de você!</span></span></span></big></span></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">O uso sistemático do Construflix pode ajudar muito para que seus projetos e obras estejam cada vez mais aderentes à norma de desempenho, e também a normas de Gestão da Qualidade, melhorando o trabalho profissional e minimizando problemas jurídicos.</span></span></span></big></span><br>\n\t\t\t </p>\n\n\t\t\t<h3 style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Ok, achei interessante, mas como funciona?</b></span></span></big></span></h3>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Primeiro, você se cadastra e faz um teste grátis por 3 dias. Se gostar, assina um dos 3 planos mensais. Eles foram batizados, não por acaso, de MÍNIMO, INTERMEDIÁRIO e SUPERIOR. A partir daí, você pode usar a quantidade de ferramentas permitidas no seu plano quantas vezes quiser por mês. </span></span></span></big></span></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Construflix se encarrega de guardar seus resultados e manter seu banco de dados, de modo que você possa acessar quando quiser.</span></span></span></big></span></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Para cada aplicação temos uma sessão de explicação escrita e/ou em vídeo, e contamos com pessoal pronto para tirar suas dúvidas.</span></span></span></big></span></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"> </p>\n\n\t\t\t<h3 style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Se eu mandar o projeto o Construflix fornece os resultados da minha obra?</b></span></span></big></span></h3>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Não, isso seria consultoria! O que queremos é que você tenha autonomia para fazer ou analisar vários assuntos relacionados ao desempenho das edificações. </span></span></span></big></span></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Lembre-se da comparação com a academia: nós fornecemos “equipamentos” super atuais e as orientações de uso, mas quem tem que “subir no aparelho” e fazer o esforço é você. O que fizemos foi deixar essa tarefa muito mais fácil a um custo quase inacreditavelmente acessível.</span></span></span></big></span><br>\n\t\t\t </p>\n\n\t\t\t<h3 style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;"><b>Vi que hoje o Construflix tem uma certa quantidade de ferramentas. Será sempre assim? </b></span></span></big></span></h3>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Claro que não! Estamos sempre desenvolvendo e incluindo novas aplicações na plataforma. Esse é o segredo do nosso negócio! À medida que aumentamos nossa base de clientes, estamos em constante evolução. E o melhor? Você não pagará um centavo a mais por isto! Incrível, não?</span></span></span><br>\n\t\t\t </big></span></p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Bom, agora que você conheceu mais sobre a gente, nós te convidamos a explorar todo o nosso site e se cadastrar para começar seu período de teste. Se quiser começar já, é só clicar <u>aqui</u> .</span></span></span></big></span><br>\n\t\t\t </p>\n\n\t\t\t<p style="margin: 0cm 0cm 8pt 80px;"><span style="color: #ffffff;"><big><span style="font-size: 11pt;"><span style="line-height: 107%;"><span style="font-family: Calibri,sans-serif;">Bem-vindo ao Construflix!</span></span></span></big></span></p>\n\t\t\t</td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p style="margin: 0cm 0cm 8pt 40px;"> </p>
238	<table background="/media/filer_public_thumbnails/filer_public/ae/de/aede9d94-accb-44d2-8340-c6965c276347/mapa_contato.jpg__1170x0_q85_subsampling-2_upscale.jpg" border="0" cellpadding="1" cellspacing="1" style="width: 1200px; height: 800px;">\n</table>\n\n<p> </p>\n\n<p> </p>\n\n<p> </p>
\.


--
-- Data for Name: djangocms_video_videoplayer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY djangocms_video_videoplayer (cmsplugin_ptr_id, embed_link, poster_id, attributes, label, template) FROM stdin;
\.


--
-- Data for Name: djangocms_video_videosource; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY djangocms_video_videosource (cmsplugin_ptr_id, text_title, text_description, attributes, source_file_id) FROM stdin;
\.


--
-- Data for Name: djangocms_video_videotrack; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY djangocms_video_videotrack (cmsplugin_ptr_id, kind, srclang, label, attributes, src_id) FROM stdin;
\.


--
-- Data for Name: easy_thumbnails_source; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY easy_thumbnails_source (id, storage_hash, name, modified) FROM stdin;
1	f9bde26a1556cd667f742bd34ec7c55e	filer_public/ab/9f/ab9f605e-c3cf-4842-af76-9cd1ad41dd9e/plano_basico.jpg	2018-03-20 08:47:20.504683+00
18	f9bde26a1556cd667f742bd34ec7c55e	filer_public/27/bb/27bbea40-5e0a-4608-b4f6-9380a87341ee/plano_basico1.png	2018-03-22 04:04:52.657332+00
20	f9bde26a1556cd667f742bd34ec7c55e	filer_public/05/0b/050bce90-6304-4646-b696-c95b0ed2ea11/topo_home.jpg	2018-04-03 01:20:13.38384+00
21	f9bde26a1556cd667f742bd34ec7c55e	filer_public/37/67/3767d6d0-aae0-447e-a23f-27f5e4c5ffdf/clientes-logo.jpg	2018-04-03 01:30:12.072858+00
22	f9bde26a1556cd667f742bd34ec7c55e	filer_public/c2/31/c231f7f2-ed3e-4eb1-bd07-33f625fca1c9/guia_de_produtos_e_fornecedores.jpg	2018-04-03 01:56:27.107346+00
5	f9bde26a1556cd667f742bd34ec7c55e	filer_public/af/07/af078814-5cbe-4b41-a137-07c136069c34/checklist1.png	2018-03-20 09:22:17.667409+00
23	f9bde26a1556cd667f742bd34ec7c55e	filer_public/a5/10/a510969e-9632-43c9-a7b3-f4dfa6756109/guia_de_produtos_e_fornecedores.jpg	2018-04-03 01:56:48.772752+00
6	f9bde26a1556cd667f742bd34ec7c55e	filer_public/3f/e4/3fe4fca1-86d2-4b0c-98f6-47503a51eee7/anlise-de-produtos.jpg	2018-03-20 09:24:51.496503+00
7	f9bde26a1556cd667f742bd34ec7c55e	filer_public/5e/6a/5e6a8beb-4e32-46be-8e91-b7d1964e4e1f/default.png	2018-03-20 09:40:14.080375+00
9	f9bde26a1556cd667f742bd34ec7c55e	filer_public/d5/61/d56196c1-f1bc-499a-bb17-84284651b004/construflix.jpg	2018-03-20 09:47:18.132005+00
8	f9bde26a1556cd667f742bd34ec7c55e	filer_public/8c/ab/8cab0ee8-622c-4669-842a-d44c957ee9b8/checklist.jpg	2018-03-20 09:57:09.92147+00
10	f9bde26a1556cd667f742bd34ec7c55e	filer_public/ea/f6/eaf609a7-6962-4388-a3fe-e4588f1ab0cc/site_icon_construf.png	2018-03-20 10:48:53.680224+00
11	f9bde26a1556cd667f742bd34ec7c55e	filer_public/98/45/98452f1c-0f4c-4778-af81-f92c2f10ed49/site_icon_construf.png	2018-03-20 10:53:08.723246+00
2	f9bde26a1556cd667f742bd34ec7c55e	filer_public/39/70/3970b287-6231-4118-b135-e2365b17347b/plano_basico1.png	2018-03-21 15:38:10.750604+00
12	f9bde26a1556cd667f742bd34ec7c55e	filer_public/75/7d/757d7ce6-ef8c-4451-a833-415d0572c7ad/construflix2.png	2018-03-21 15:43:26.79849+00
13	f9bde26a1556cd667f742bd34ec7c55e	filer_public/f7/83/f7832c7c-849d-4b14-8476-af2a1574c74b/checklist1.png	2018-03-22 01:24:37.205518+00
14	f9bde26a1556cd667f742bd34ec7c55e	filer_public/9d/22/9d227c6f-773d-47b3-addd-b4af9c20e207/comece.jpg	2018-03-22 03:40:24.880578+00
15	f9bde26a1556cd667f742bd34ec7c55e	filer_public/28/82/2882c2d5-0540-4468-86e2-308d0baa2200/comece2.jpg	2018-03-22 03:43:33.495624+00
16	f9bde26a1556cd667f742bd34ec7c55e	filer_public/44/7e/447e24ab-4469-43f5-bcb3-83fab071ea03/comece2.jpg	2018-03-22 03:45:13.367277+00
17	f9bde26a1556cd667f742bd34ec7c55e	filer_public/e9/6f/e96f9f50-cae8-4b21-b1a5-cfd55eab6b2b/comece2.jpg	2018-03-22 03:46:50.185998+00
25	f9bde26a1556cd667f742bd34ec7c55e	filer_public/e1/27/e12700d3-1b45-4efb-850c-3bb8d89952a5/analise_de_manual_do_proprietario.jpg	2018-04-03 02:16:36.84813+00
26	f9bde26a1556cd667f742bd34ec7c55e	filer_public/5b/14/5b14616a-3a56-4464-a7fb-b17d29a5581d/contato_imagem.jpg	2018-04-03 02:28:24.647303+00
24	f9bde26a1556cd667f742bd34ec7c55e	filer_public/ff/73/ff736e36-c8fd-44a5-9e72-7a0e8b30df39/analise_de_projetos.jpg	2018-04-03 12:29:34.429077+00
19	f9bde26a1556cd667f742bd34ec7c55e	filer_public/fa/ae/faaed877-ec1e-4b1a-9dbc-c2db04beb280/plano_basico.jpg	2018-04-05 02:21:42.480152+00
27	f9bde26a1556cd667f742bd34ec7c55e	filer_public/2b/96/2b961732-722c-4e1c-84d6-213f09e99c6b/topo_institucional.jpg	2018-04-03 14:14:58.398877+00
3	f9bde26a1556cd667f742bd34ec7c55e	filer_public/06/90/06903ff0-bc6a-4b9f-b77a-45cf5f31bfbd/plano-intermediario.jpg	2018-04-05 02:21:42.590721+00
28	f9bde26a1556cd667f742bd34ec7c55e	filer_public/ae/de/aede9d94-accb-44d2-8340-c6965c276347/mapa_contato.jpg	2018-04-04 00:18:12.925652+00
29	f9bde26a1556cd667f742bd34ec7c55e	filer_public/d1/6c/d16ccf92-c3ed-47e3-a04b-072b2405bf70/fone.png	2018-04-04 13:04:35.459747+00
30	f9bde26a1556cd667f742bd34ec7c55e	filer_public/ea/3c/ea3cdde2-5ffa-4b26-8b26-5dab0dacb7d0/topo_planos.jpg	2018-04-05 07:54:57.490029+00
31	f9bde26a1556cd667f742bd34ec7c55e	filer_public/ac/d6/acd6ae08-41a5-4446-8956-a09e57803145/construflix-logomarca-use-full.png	2018-04-09 14:44:22.548783+00
32	f9bde26a1556cd667f742bd34ec7c55e	filer_public/69/0a/690a178f-6f3f-4e86-9a05-f10e8de3c10c/construflix-logomarca-use.png	2018-04-09 14:45:27.587301+00
\.


--
-- Data for Name: easy_thumbnails_thumbnail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY easy_thumbnails_thumbnail (id, storage_hash, name, modified, source_id) FROM stdin;
1	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ab/9f/ab9f605e-c3cf-4842-af76-9cd1ad41dd9e/plano_basico.jpg__16x16_q85_crop_subsampling-2_upscale.jpg	2018-03-20 08:44:57.990138+00	1
2	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ab/9f/ab9f605e-c3cf-4842-af76-9cd1ad41dd9e/plano_basico.jpg__32x32_q85_crop_subsampling-2_upscale.jpg	2018-03-20 08:44:58.000142+00	1
3	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ab/9f/ab9f605e-c3cf-4842-af76-9cd1ad41dd9e/plano_basico.jpg__48x48_q85_crop_subsampling-2_upscale.jpg	2018-03-20 08:44:58.010402+00	1
4	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ab/9f/ab9f605e-c3cf-4842-af76-9cd1ad41dd9e/plano_basico.jpg__64x64_q85_crop_subsampling-2_upscale.jpg	2018-03-20 08:44:58.018804+00	1
5	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ab/9f/ab9f605e-c3cf-4842-af76-9cd1ad41dd9e/plano_basico.jpg__180x180_q85_crop_subsampling-2_upscale.jpg	2018-03-20 08:44:58.033458+00	1
6	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ab/9f/ab9f605e-c3cf-4842-af76-9cd1ad41dd9e/plano_basico.jpg__1170x658_q85_crop_subsampling-2_upscale.jpg	2018-03-20 08:45:04.689389+00	1
7	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ab/9f/ab9f605e-c3cf-4842-af76-9cd1ad41dd9e/plano_basico.jpg__210x10000_q85_subsampling-2_upscale.jpg	2018-03-20 08:47:20.50861+00	1
8	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/39/70/3970b287-6231-4118-b135-e2365b17347b/plano_basico1.png__16x16_q85_crop_subsampling-2_upscale.png	2018-03-20 08:50:24.747689+00	2
9	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/39/70/3970b287-6231-4118-b135-e2365b17347b/plano_basico1.png__32x32_q85_crop_subsampling-2_upscale.png	2018-03-20 08:50:24.755543+00	2
10	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/39/70/3970b287-6231-4118-b135-e2365b17347b/plano_basico1.png__48x48_q85_crop_subsampling-2_upscale.png	2018-03-20 08:50:24.763683+00	2
11	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/39/70/3970b287-6231-4118-b135-e2365b17347b/plano_basico1.png__64x64_q85_crop_subsampling-2_upscale.png	2018-03-20 08:50:24.773082+00	2
12	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/39/70/3970b287-6231-4118-b135-e2365b17347b/plano_basico1.png__180x180_q85_crop_subsampling-2_upscale.png	2018-03-20 08:50:24.811778+00	2
13	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/39/70/3970b287-6231-4118-b135-e2365b17347b/plano_basico1.png__1170x658_q85_crop_subsampling-2_upscale.png	2018-03-20 08:50:29.326828+00	2
14	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/39/70/3970b287-6231-4118-b135-e2365b17347b/plano_basico1.png__1170x877_q85_crop_subsampling-2_upscale.png	2018-03-20 08:52:04.179132+00	2
15	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/06/90/06903ff0-bc6a-4b9f-b77a-45cf5f31bfbd/plano-intermediario.jpg__16x16_q85_crop_subsampling-2_upscale.jpg	2018-03-20 09:01:05.270155+00	3
16	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/06/90/06903ff0-bc6a-4b9f-b77a-45cf5f31bfbd/plano-intermediario.jpg__32x32_q85_crop_subsampling-2_upscale.jpg	2018-03-20 09:01:05.282453+00	3
17	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/06/90/06903ff0-bc6a-4b9f-b77a-45cf5f31bfbd/plano-intermediario.jpg__48x48_q85_crop_subsampling-2_upscale.jpg	2018-03-20 09:01:05.294027+00	3
18	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/06/90/06903ff0-bc6a-4b9f-b77a-45cf5f31bfbd/plano-intermediario.jpg__64x64_q85_crop_subsampling-2_upscale.jpg	2018-03-20 09:01:05.304898+00	3
19	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/06/90/06903ff0-bc6a-4b9f-b77a-45cf5f31bfbd/plano-intermediario.jpg__180x180_q85_crop_subsampling-2_upscale.jpg	2018-03-20 09:01:05.324385+00	3
20	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/06/90/06903ff0-bc6a-4b9f-b77a-45cf5f31bfbd/plano-intermediario.jpg__1170x877_q85_crop_subsampling-2_upscale.jpg	2018-03-20 09:01:16.509884+00	3
21	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/39/70/3970b287-6231-4118-b135-e2365b17347b/plano_basico1.png__1170x501_q85_crop_subsampling-2_upscale.png	2018-03-20 09:07:47.629421+00	2
22	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/06/90/06903ff0-bc6a-4b9f-b77a-45cf5f31bfbd/plano-intermediario.jpg__1170x501_q85_crop_subsampling-2_upscale.jpg	2018-03-20 09:07:47.722666+00	3
29	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/af/07/af078814-5cbe-4b41-a137-07c136069c34/checklist1.png__16x16_q85_crop_subsampling-2_upscale.png	2018-03-20 09:20:37.973744+00	5
30	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/af/07/af078814-5cbe-4b41-a137-07c136069c34/checklist1.png__32x32_q85_crop_subsampling-2_upscale.png	2018-03-20 09:20:37.981333+00	5
31	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/af/07/af078814-5cbe-4b41-a137-07c136069c34/checklist1.png__48x48_q85_crop_subsampling-2_upscale.png	2018-03-20 09:20:37.988871+00	5
32	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/af/07/af078814-5cbe-4b41-a137-07c136069c34/checklist1.png__64x64_q85_crop_subsampling-2_upscale.png	2018-03-20 09:20:37.998028+00	5
33	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/af/07/af078814-5cbe-4b41-a137-07c136069c34/checklist1.png__180x180_q85_crop_subsampling-2_upscale.png	2018-03-20 09:20:38.040688+00	5
34	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/af/07/af078814-5cbe-4b41-a137-07c136069c34/checklist1.png__1170x0_q85_subsampling-2_upscale.png	2018-03-20 09:20:41.996447+00	5
35	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/af/07/af078814-5cbe-4b41-a137-07c136069c34/checklist1.png__210x10000_q85_subsampling-2_upscale.png	2018-03-20 09:21:30.019583+00	5
36	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/af/07/af078814-5cbe-4b41-a137-07c136069c34/checklist1.png__1170x658_q85_crop_subsampling-2_upscale.png	2018-03-20 09:22:17.672329+00	5
37	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/3f/e4/3fe4fca1-86d2-4b0c-98f6-47503a51eee7/anlise-de-produtos.jpg__16x16_q85_crop_subsampling-2_upscale.jpg	2018-03-20 09:24:48.948679+00	6
38	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/3f/e4/3fe4fca1-86d2-4b0c-98f6-47503a51eee7/anlise-de-produtos.jpg__32x32_q85_crop_subsampling-2_upscale.jpg	2018-03-20 09:24:48.976963+00	6
39	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/3f/e4/3fe4fca1-86d2-4b0c-98f6-47503a51eee7/anlise-de-produtos.jpg__48x48_q85_crop_subsampling-2_upscale.jpg	2018-03-20 09:24:49.004177+00	6
40	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/3f/e4/3fe4fca1-86d2-4b0c-98f6-47503a51eee7/anlise-de-produtos.jpg__64x64_q85_crop_subsampling-2_upscale.jpg	2018-03-20 09:24:49.031686+00	6
41	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/3f/e4/3fe4fca1-86d2-4b0c-98f6-47503a51eee7/anlise-de-produtos.jpg__180x180_q85_crop_subsampling-2_upscale.jpg	2018-03-20 09:24:49.065959+00	6
42	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/3f/e4/3fe4fca1-86d2-4b0c-98f6-47503a51eee7/anlise-de-produtos.jpg__1170x0_q85_subsampling-2_upscale.jpg	2018-03-20 09:24:51.501495+00	6
43	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/5e/6a/5e6a8beb-4e32-46be-8e91-b7d1964e4e1f/default.png__16x16_q85_crop_subsampling-2_upscale.png	2018-03-20 09:40:11.11634+00	7
44	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/5e/6a/5e6a8beb-4e32-46be-8e91-b7d1964e4e1f/default.png__32x32_q85_crop_subsampling-2_upscale.png	2018-03-20 09:40:11.159725+00	7
45	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/5e/6a/5e6a8beb-4e32-46be-8e91-b7d1964e4e1f/default.png__48x48_q85_crop_subsampling-2_upscale.png	2018-03-20 09:40:11.202711+00	7
46	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/5e/6a/5e6a8beb-4e32-46be-8e91-b7d1964e4e1f/default.png__64x64_q85_crop_subsampling-2_upscale.png	2018-03-20 09:40:11.248252+00	7
47	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/5e/6a/5e6a8beb-4e32-46be-8e91-b7d1964e4e1f/default.png__180x180_q85_crop_subsampling-2_upscale.png	2018-03-20 09:40:11.301791+00	7
48	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/5e/6a/5e6a8beb-4e32-46be-8e91-b7d1964e4e1f/default.png__1170x0_q85_subsampling-2_upscale.png	2018-03-20 09:40:14.085058+00	7
49	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/8c/ab/8cab0ee8-622c-4669-842a-d44c957ee9b8/checklist.jpg__16x16_q85_crop_subsampling-2_upscale.jpg	2018-03-20 09:44:34.967066+00	8
50	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/8c/ab/8cab0ee8-622c-4669-842a-d44c957ee9b8/checklist.jpg__32x32_q85_crop_subsampling-2_upscale.jpg	2018-03-20 09:44:34.978576+00	8
51	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/8c/ab/8cab0ee8-622c-4669-842a-d44c957ee9b8/checklist.jpg__48x48_q85_crop_subsampling-2_upscale.jpg	2018-03-20 09:44:34.989584+00	8
52	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/8c/ab/8cab0ee8-622c-4669-842a-d44c957ee9b8/checklist.jpg__64x64_q85_crop_subsampling-2_upscale.jpg	2018-03-20 09:44:34.998603+00	8
53	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/8c/ab/8cab0ee8-622c-4669-842a-d44c957ee9b8/checklist.jpg__180x180_q85_crop_subsampling-2_upscale.jpg	2018-03-20 09:44:35.015323+00	8
54	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/d5/61/d56196c1-f1bc-499a-bb17-84284651b004/construflix.jpg__16x16_q85_crop_subsampling-2_upscale.jpg	2018-03-20 09:47:18.135467+00	9
55	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/d5/61/d56196c1-f1bc-499a-bb17-84284651b004/construflix.jpg__32x32_q85_crop_subsampling-2_upscale.jpg	2018-03-20 09:47:18.223203+00	9
56	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/d5/61/d56196c1-f1bc-499a-bb17-84284651b004/construflix.jpg__48x48_q85_crop_subsampling-2_upscale.jpg	2018-03-20 09:47:18.24068+00	9
57	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/d5/61/d56196c1-f1bc-499a-bb17-84284651b004/construflix.jpg__64x64_q85_crop_subsampling-2_upscale.jpg	2018-03-20 09:47:18.258389+00	9
58	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/d5/61/d56196c1-f1bc-499a-bb17-84284651b004/construflix.jpg__180x180_q85_crop_subsampling-2_upscale.jpg	2018-03-20 09:47:18.282941+00	9
59	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/8c/ab/8cab0ee8-622c-4669-842a-d44c957ee9b8/checklist.jpg__210x10000_q85_subsampling-2_upscale.jpg	2018-03-20 09:57:09.926482+00	8
60	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ea/f6/eaf609a7-6962-4388-a3fe-e4588f1ab0cc/site_icon_construf.png__16x16_q85_crop_subsampling-2_upscale.jpg	2018-03-20 10:48:53.683497+00	10
61	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ea/f6/eaf609a7-6962-4388-a3fe-e4588f1ab0cc/site_icon_construf.png__32x32_q85_crop_subsampling-2_upscale.jpg	2018-03-20 10:48:53.691507+00	10
62	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ea/f6/eaf609a7-6962-4388-a3fe-e4588f1ab0cc/site_icon_construf.png__48x48_q85_crop_subsampling-2_upscale.jpg	2018-03-20 10:48:53.696534+00	10
63	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ea/f6/eaf609a7-6962-4388-a3fe-e4588f1ab0cc/site_icon_construf.png__64x64_q85_crop_subsampling-2_upscale.jpg	2018-03-20 10:48:53.701636+00	10
64	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ea/f6/eaf609a7-6962-4388-a3fe-e4588f1ab0cc/site_icon_construf.png__180x180_q85_crop_subsampling-2_upscale.jpg	2018-03-20 10:48:53.711979+00	10
65	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/98/45/98452f1c-0f4c-4778-af81-f92c2f10ed49/site_icon_construf.png__16x16_q85_crop_subsampling-2_upscale.jpg	2018-03-20 10:53:02.596124+00	11
66	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/98/45/98452f1c-0f4c-4778-af81-f92c2f10ed49/site_icon_construf.png__32x32_q85_crop_subsampling-2_upscale.jpg	2018-03-20 10:53:02.716363+00	11
67	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/98/45/98452f1c-0f4c-4778-af81-f92c2f10ed49/site_icon_construf.png__48x48_q85_crop_subsampling-2_upscale.jpg	2018-03-20 10:53:02.72196+00	11
68	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/98/45/98452f1c-0f4c-4778-af81-f92c2f10ed49/site_icon_construf.png__64x64_q85_crop_subsampling-2_upscale.jpg	2018-03-20 10:53:02.72691+00	11
69	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/98/45/98452f1c-0f4c-4778-af81-f92c2f10ed49/site_icon_construf.png__180x180_q85_crop_subsampling-2_upscale.jpg	2018-03-20 10:53:02.737926+00	11
70	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/98/45/98452f1c-0f4c-4778-af81-f92c2f10ed49/site_icon_construf.png__1170x0_q85_subsampling-2_upscale.jpg	2018-03-20 10:53:08.72692+00	11
71	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/39/70/3970b287-6231-4118-b135-e2365b17347b/plano_basico1.png__210x10000_q85_subsampling-2_upscale.png	2018-03-21 15:38:10.757382+00	2
72	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/75/7d/757d7ce6-ef8c-4451-a833-415d0572c7ad/construflix2.png__16x16_q85_crop_subsampling-2_upscale.png	2018-03-21 15:43:26.801535+00	12
73	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/75/7d/757d7ce6-ef8c-4451-a833-415d0572c7ad/construflix2.png__32x32_q85_crop_subsampling-2_upscale.png	2018-03-21 15:43:26.96829+00	12
74	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/75/7d/757d7ce6-ef8c-4451-a833-415d0572c7ad/construflix2.png__48x48_q85_crop_subsampling-2_upscale.png	2018-03-21 15:43:26.982037+00	12
75	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/75/7d/757d7ce6-ef8c-4451-a833-415d0572c7ad/construflix2.png__64x64_q85_crop_subsampling-2_upscale.png	2018-03-21 15:43:26.994169+00	12
76	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/75/7d/757d7ce6-ef8c-4451-a833-415d0572c7ad/construflix2.png__180x180_q85_crop_subsampling-2_upscale.png	2018-03-21 15:43:27.01758+00	12
77	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/f7/83/f7832c7c-849d-4b14-8476-af2a1574c74b/checklist1.png__16x16_q85_crop_subsampling-2_upscale.png	2018-03-22 01:24:37.208584+00	13
78	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/f7/83/f7832c7c-849d-4b14-8476-af2a1574c74b/checklist1.png__32x32_q85_crop_subsampling-2_upscale.png	2018-03-22 01:24:37.21773+00	13
79	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/f7/83/f7832c7c-849d-4b14-8476-af2a1574c74b/checklist1.png__48x48_q85_crop_subsampling-2_upscale.png	2018-03-22 01:24:37.225752+00	13
80	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/f7/83/f7832c7c-849d-4b14-8476-af2a1574c74b/checklist1.png__64x64_q85_crop_subsampling-2_upscale.png	2018-03-22 01:24:37.235461+00	13
81	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/f7/83/f7832c7c-849d-4b14-8476-af2a1574c74b/checklist1.png__180x180_q85_crop_subsampling-2_upscale.png	2018-03-22 01:24:37.278359+00	13
82	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/9d/22/9d227c6f-773d-47b3-addd-b4af9c20e207/comece.jpg__16x16_q85_crop_subsampling-2_upscale.jpg	2018-03-22 03:40:24.883681+00	14
83	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/9d/22/9d227c6f-773d-47b3-addd-b4af9c20e207/comece.jpg__32x32_q85_crop_subsampling-2_upscale.jpg	2018-03-22 03:40:24.892754+00	14
84	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/9d/22/9d227c6f-773d-47b3-addd-b4af9c20e207/comece.jpg__48x48_q85_crop_subsampling-2_upscale.jpg	2018-03-22 03:40:24.901167+00	14
85	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/9d/22/9d227c6f-773d-47b3-addd-b4af9c20e207/comece.jpg__64x64_q85_crop_subsampling-2_upscale.jpg	2018-03-22 03:40:24.909896+00	14
86	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/9d/22/9d227c6f-773d-47b3-addd-b4af9c20e207/comece.jpg__180x180_q85_crop_subsampling-2_upscale.jpg	2018-03-22 03:40:24.924886+00	14
87	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/28/82/2882c2d5-0540-4468-86e2-308d0baa2200/comece2.jpg__16x16_q85_crop_subsampling-2_upscale.jpg	2018-03-22 03:43:33.49836+00	15
88	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/28/82/2882c2d5-0540-4468-86e2-308d0baa2200/comece2.jpg__32x32_q85_crop_subsampling-2_upscale.jpg	2018-03-22 03:43:33.506129+00	15
89	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/28/82/2882c2d5-0540-4468-86e2-308d0baa2200/comece2.jpg__48x48_q85_crop_subsampling-2_upscale.jpg	2018-03-22 03:43:33.513313+00	15
90	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/28/82/2882c2d5-0540-4468-86e2-308d0baa2200/comece2.jpg__64x64_q85_crop_subsampling-2_upscale.jpg	2018-03-22 03:43:33.520888+00	15
91	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/28/82/2882c2d5-0540-4468-86e2-308d0baa2200/comece2.jpg__180x180_q85_crop_subsampling-2_upscale.jpg	2018-03-22 03:43:33.535335+00	15
92	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/44/7e/447e24ab-4469-43f5-bcb3-83fab071ea03/comece2.jpg__16x16_q85_crop_subsampling-2_upscale.jpg	2018-03-22 03:45:13.382196+00	16
93	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/44/7e/447e24ab-4469-43f5-bcb3-83fab071ea03/comece2.jpg__32x32_q85_crop_subsampling-2_upscale.jpg	2018-03-22 03:45:13.38996+00	16
94	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/44/7e/447e24ab-4469-43f5-bcb3-83fab071ea03/comece2.jpg__48x48_q85_crop_subsampling-2_upscale.jpg	2018-03-22 03:45:13.396348+00	16
95	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/44/7e/447e24ab-4469-43f5-bcb3-83fab071ea03/comece2.jpg__64x64_q85_crop_subsampling-2_upscale.jpg	2018-03-22 03:45:13.402946+00	16
96	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/44/7e/447e24ab-4469-43f5-bcb3-83fab071ea03/comece2.jpg__180x180_q85_crop_subsampling-2_upscale.jpg	2018-03-22 03:45:13.415693+00	16
97	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/e9/6f/e96f9f50-cae8-4b21-b1a5-cfd55eab6b2b/comece2.jpg__16x16_q85_crop_subsampling-2_upscale.jpg	2018-03-22 03:46:50.188837+00	17
98	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/e9/6f/e96f9f50-cae8-4b21-b1a5-cfd55eab6b2b/comece2.jpg__32x32_q85_crop_subsampling-2_upscale.jpg	2018-03-22 03:46:50.196146+00	17
99	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/e9/6f/e96f9f50-cae8-4b21-b1a5-cfd55eab6b2b/comece2.jpg__48x48_q85_crop_subsampling-2_upscale.jpg	2018-03-22 03:46:50.202294+00	17
100	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/e9/6f/e96f9f50-cae8-4b21-b1a5-cfd55eab6b2b/comece2.jpg__64x64_q85_crop_subsampling-2_upscale.jpg	2018-03-22 03:46:50.214734+00	17
101	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/e9/6f/e96f9f50-cae8-4b21-b1a5-cfd55eab6b2b/comece2.jpg__180x180_q85_crop_subsampling-2_upscale.jpg	2018-03-22 03:46:50.228315+00	17
102	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/27/bb/27bbea40-5e0a-4608-b4f6-9380a87341ee/plano_basico1.png__16x16_q85_crop_subsampling-2_upscale.png	2018-03-22 04:04:28.643643+00	18
103	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/27/bb/27bbea40-5e0a-4608-b4f6-9380a87341ee/plano_basico1.png__32x32_q85_crop_subsampling-2_upscale.png	2018-03-22 04:04:28.651621+00	18
104	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/27/bb/27bbea40-5e0a-4608-b4f6-9380a87341ee/plano_basico1.png__48x48_q85_crop_subsampling-2_upscale.png	2018-03-22 04:04:28.659368+00	18
105	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/27/bb/27bbea40-5e0a-4608-b4f6-9380a87341ee/plano_basico1.png__64x64_q85_crop_subsampling-2_upscale.png	2018-03-22 04:04:28.66838+00	18
106	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/27/bb/27bbea40-5e0a-4608-b4f6-9380a87341ee/plano_basico1.png__180x180_q85_crop_subsampling-2_upscale.png	2018-03-22 04:04:28.704943+00	18
107	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/27/bb/27bbea40-5e0a-4608-b4f6-9380a87341ee/plano_basico1.png__1170x501_q85_crop_subsampling-2_upscale.png	2018-03-22 04:04:52.661689+00	18
108	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/fa/ae/faaed877-ec1e-4b1a-9dbc-c2db04beb280/plano_basico.jpg__16x16_q85_crop_subsampling-2_upscale.jpg	2018-03-26 10:07:50.700945+00	19
109	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/fa/ae/faaed877-ec1e-4b1a-9dbc-c2db04beb280/plano_basico.jpg__32x32_q85_crop_subsampling-2_upscale.jpg	2018-03-26 10:07:50.7097+00	19
110	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/fa/ae/faaed877-ec1e-4b1a-9dbc-c2db04beb280/plano_basico.jpg__48x48_q85_crop_subsampling-2_upscale.jpg	2018-03-26 10:07:50.717164+00	19
111	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/fa/ae/faaed877-ec1e-4b1a-9dbc-c2db04beb280/plano_basico.jpg__64x64_q85_crop_subsampling-2_upscale.jpg	2018-03-26 10:07:50.724658+00	19
112	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/fa/ae/faaed877-ec1e-4b1a-9dbc-c2db04beb280/plano_basico.jpg__180x180_q85_crop_subsampling-2_upscale.jpg	2018-03-26 10:07:50.738528+00	19
113	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/fa/ae/faaed877-ec1e-4b1a-9dbc-c2db04beb280/plano_basico.jpg__1170x501_q85_crop_subsampling-2_upscale.jpg	2018-03-26 10:07:55.124122+00	19
114	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/05/0b/050bce90-6304-4646-b696-c95b0ed2ea11/topo_home.jpg__16x16_q85_crop_subsampling-2_upscale.jpg	2018-04-03 01:20:13.57633+00	20
115	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/05/0b/050bce90-6304-4646-b696-c95b0ed2ea11/topo_home.jpg__32x32_q85_crop_subsampling-2_upscale.jpg	2018-04-03 01:20:13.60229+00	20
116	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/05/0b/050bce90-6304-4646-b696-c95b0ed2ea11/topo_home.jpg__48x48_q85_crop_subsampling-2_upscale.jpg	2018-04-03 01:20:13.626619+00	20
117	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/05/0b/050bce90-6304-4646-b696-c95b0ed2ea11/topo_home.jpg__64x64_q85_crop_subsampling-2_upscale.jpg	2018-04-03 01:20:13.651266+00	20
118	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/05/0b/050bce90-6304-4646-b696-c95b0ed2ea11/topo_home.jpg__180x180_q85_crop_subsampling-2_upscale.jpg	2018-04-03 01:20:13.686186+00	20
119	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/37/67/3767d6d0-aae0-447e-a23f-27f5e4c5ffdf/clientes-logo.jpg__16x16_q85_crop_subsampling-2_upscale.jpg	2018-04-03 01:30:12.075309+00	21
120	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/37/67/3767d6d0-aae0-447e-a23f-27f5e4c5ffdf/clientes-logo.jpg__32x32_q85_crop_subsampling-2_upscale.jpg	2018-04-03 01:30:12.084622+00	21
121	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/37/67/3767d6d0-aae0-447e-a23f-27f5e4c5ffdf/clientes-logo.jpg__48x48_q85_crop_subsampling-2_upscale.jpg	2018-04-03 01:30:12.092855+00	21
122	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/37/67/3767d6d0-aae0-447e-a23f-27f5e4c5ffdf/clientes-logo.jpg__64x64_q85_crop_subsampling-2_upscale.jpg	2018-04-03 01:30:12.101508+00	21
123	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/37/67/3767d6d0-aae0-447e-a23f-27f5e4c5ffdf/clientes-logo.jpg__180x180_q85_crop_subsampling-2_upscale.jpg	2018-04-03 01:30:12.116354+00	21
124	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/c2/31/c231f7f2-ed3e-4eb1-bd07-33f625fca1c9/guia_de_produtos_e_fornecedores.jpg__16x16_q85_crop_subsampling-2_upscale.jpg	2018-04-03 01:56:27.110612+00	22
125	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/c2/31/c231f7f2-ed3e-4eb1-bd07-33f625fca1c9/guia_de_produtos_e_fornecedores.jpg__32x32_q85_crop_subsampling-2_upscale.jpg	2018-04-03 01:56:27.121793+00	22
126	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/c2/31/c231f7f2-ed3e-4eb1-bd07-33f625fca1c9/guia_de_produtos_e_fornecedores.jpg__48x48_q85_crop_subsampling-2_upscale.jpg	2018-04-03 01:56:27.131521+00	22
127	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/c2/31/c231f7f2-ed3e-4eb1-bd07-33f625fca1c9/guia_de_produtos_e_fornecedores.jpg__64x64_q85_crop_subsampling-2_upscale.jpg	2018-04-03 01:56:27.141448+00	22
128	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/c2/31/c231f7f2-ed3e-4eb1-bd07-33f625fca1c9/guia_de_produtos_e_fornecedores.jpg__180x180_q85_crop_subsampling-2_upscale.jpg	2018-04-03 01:56:27.157662+00	22
129	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/a5/10/a510969e-9632-43c9-a7b3-f4dfa6756109/guia_de_produtos_e_fornecedores.jpg__16x16_q85_crop_subsampling-2_upscale.jpg	2018-04-03 01:56:48.776028+00	23
130	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/a5/10/a510969e-9632-43c9-a7b3-f4dfa6756109/guia_de_produtos_e_fornecedores.jpg__32x32_q85_crop_subsampling-2_upscale.jpg	2018-04-03 01:56:48.787615+00	23
131	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/a5/10/a510969e-9632-43c9-a7b3-f4dfa6756109/guia_de_produtos_e_fornecedores.jpg__48x48_q85_crop_subsampling-2_upscale.jpg	2018-04-03 01:56:48.798282+00	23
132	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/a5/10/a510969e-9632-43c9-a7b3-f4dfa6756109/guia_de_produtos_e_fornecedores.jpg__64x64_q85_crop_subsampling-2_upscale.jpg	2018-04-03 01:56:48.809806+00	23
133	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/a5/10/a510969e-9632-43c9-a7b3-f4dfa6756109/guia_de_produtos_e_fornecedores.jpg__180x180_q85_crop_subsampling-2_upscale.jpg	2018-04-03 01:56:48.82709+00	23
134	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ff/73/ff736e36-c8fd-44a5-9e72-7a0e8b30df39/analise_de_projetos.jpg__16x16_q85_crop_subsampling-2_upscale.jpg	2018-04-03 02:14:41.317745+00	24
135	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ff/73/ff736e36-c8fd-44a5-9e72-7a0e8b30df39/analise_de_projetos.jpg__32x32_q85_crop_subsampling-2_upscale.jpg	2018-04-03 02:14:41.327821+00	24
136	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ff/73/ff736e36-c8fd-44a5-9e72-7a0e8b30df39/analise_de_projetos.jpg__48x48_q85_crop_subsampling-2_upscale.jpg	2018-04-03 02:14:41.337057+00	24
137	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ff/73/ff736e36-c8fd-44a5-9e72-7a0e8b30df39/analise_de_projetos.jpg__64x64_q85_crop_subsampling-2_upscale.jpg	2018-04-03 02:14:41.347104+00	24
138	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ff/73/ff736e36-c8fd-44a5-9e72-7a0e8b30df39/analise_de_projetos.jpg__180x180_q85_crop_subsampling-2_upscale.jpg	2018-04-03 02:14:41.363096+00	24
139	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/e1/27/e12700d3-1b45-4efb-850c-3bb8d89952a5/analise_de_manual_do_proprietario.jpg__16x16_q85_crop_subsampling-2_upscale.jpg	2018-04-03 02:16:36.850988+00	25
140	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/e1/27/e12700d3-1b45-4efb-850c-3bb8d89952a5/analise_de_manual_do_proprietario.jpg__32x32_q85_crop_subsampling-2_upscale.jpg	2018-04-03 02:16:36.861385+00	25
141	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/e1/27/e12700d3-1b45-4efb-850c-3bb8d89952a5/analise_de_manual_do_proprietario.jpg__48x48_q85_crop_subsampling-2_upscale.jpg	2018-04-03 02:16:36.870554+00	25
142	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/e1/27/e12700d3-1b45-4efb-850c-3bb8d89952a5/analise_de_manual_do_proprietario.jpg__64x64_q85_crop_subsampling-2_upscale.jpg	2018-04-03 02:16:36.880077+00	25
143	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/e1/27/e12700d3-1b45-4efb-850c-3bb8d89952a5/analise_de_manual_do_proprietario.jpg__180x180_q85_crop_subsampling-2_upscale.jpg	2018-04-03 02:16:36.896434+00	25
144	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/5b/14/5b14616a-3a56-4464-a7fb-b17d29a5581d/contato_imagem.jpg__16x16_q85_crop_subsampling-2_upscale.jpg	2018-04-03 02:28:24.651264+00	26
145	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/5b/14/5b14616a-3a56-4464-a7fb-b17d29a5581d/contato_imagem.jpg__32x32_q85_crop_subsampling-2_upscale.jpg	2018-04-03 02:28:24.700439+00	26
146	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/5b/14/5b14616a-3a56-4464-a7fb-b17d29a5581d/contato_imagem.jpg__48x48_q85_crop_subsampling-2_upscale.jpg	2018-04-03 02:28:24.749908+00	26
147	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/5b/14/5b14616a-3a56-4464-a7fb-b17d29a5581d/contato_imagem.jpg__64x64_q85_crop_subsampling-2_upscale.jpg	2018-04-03 02:28:24.799283+00	26
148	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/5b/14/5b14616a-3a56-4464-a7fb-b17d29a5581d/contato_imagem.jpg__180x180_q85_crop_subsampling-2_upscale.jpg	2018-04-03 02:28:24.872634+00	26
149	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ff/73/ff736e36-c8fd-44a5-9e72-7a0e8b30df39/analise_de_projetos.jpg__1170x0_q85_subsampling-2_upscale.jpg	2018-04-03 12:29:34.433504+00	24
150	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/2b/96/2b961732-722c-4e1c-84d6-213f09e99c6b/topo_institucional.jpg__16x16_q85_crop_subsampling-2_upscale.jpg	2018-04-03 14:14:49.360241+00	27
151	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/2b/96/2b961732-722c-4e1c-84d6-213f09e99c6b/topo_institucional.jpg__32x32_q85_crop_subsampling-2_upscale.jpg	2018-04-03 14:14:49.385116+00	27
152	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/2b/96/2b961732-722c-4e1c-84d6-213f09e99c6b/topo_institucional.jpg__48x48_q85_crop_subsampling-2_upscale.jpg	2018-04-03 14:14:49.409521+00	27
153	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/2b/96/2b961732-722c-4e1c-84d6-213f09e99c6b/topo_institucional.jpg__64x64_q85_crop_subsampling-2_upscale.jpg	2018-04-03 14:14:49.434346+00	27
154	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/2b/96/2b961732-722c-4e1c-84d6-213f09e99c6b/topo_institucional.jpg__180x180_q85_crop_subsampling-2_upscale.jpg	2018-04-03 14:14:49.469924+00	27
155	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/2b/96/2b961732-722c-4e1c-84d6-213f09e99c6b/topo_institucional.jpg__1170x0_q85_subsampling-2_upscale.jpg	2018-04-03 14:14:58.403186+00	27
156	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ae/de/aede9d94-accb-44d2-8340-c6965c276347/mapa_contato.jpg__16x16_q85_crop_subsampling-2_upscale.jpg	2018-04-04 00:10:00.351686+00	28
157	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ae/de/aede9d94-accb-44d2-8340-c6965c276347/mapa_contato.jpg__32x32_q85_crop_subsampling-2_upscale.jpg	2018-04-04 00:10:00.397573+00	28
158	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ae/de/aede9d94-accb-44d2-8340-c6965c276347/mapa_contato.jpg__48x48_q85_crop_subsampling-2_upscale.jpg	2018-04-04 00:10:00.444309+00	28
159	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ae/de/aede9d94-accb-44d2-8340-c6965c276347/mapa_contato.jpg__64x64_q85_crop_subsampling-2_upscale.jpg	2018-04-04 00:10:00.490758+00	28
160	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ae/de/aede9d94-accb-44d2-8340-c6965c276347/mapa_contato.jpg__180x180_q85_crop_subsampling-2_upscale.jpg	2018-04-04 00:10:00.547238+00	28
161	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ae/de/aede9d94-accb-44d2-8340-c6965c276347/mapa_contato.jpg__1170x0_q85_subsampling-2_upscale.jpg	2018-04-04 00:18:12.929919+00	28
162	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/d1/6c/d16ccf92-c3ed-47e3-a04b-072b2405bf70/fone.png__16x16_q85_crop_subsampling-2_upscale.png	2018-04-04 13:04:31.082615+00	29
163	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/d1/6c/d16ccf92-c3ed-47e3-a04b-072b2405bf70/fone.png__32x32_q85_crop_subsampling-2_upscale.png	2018-04-04 13:04:31.090167+00	29
164	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/d1/6c/d16ccf92-c3ed-47e3-a04b-072b2405bf70/fone.png__48x48_q85_crop_subsampling-2_upscale.png	2018-04-04 13:04:31.096178+00	29
165	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/d1/6c/d16ccf92-c3ed-47e3-a04b-072b2405bf70/fone.png__64x64_q85_crop_subsampling-2_upscale.png	2018-04-04 13:04:31.102137+00	29
166	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/d1/6c/d16ccf92-c3ed-47e3-a04b-072b2405bf70/fone.png__180x180_q85_crop_subsampling-2_upscale.png	2018-04-04 13:04:31.116048+00	29
167	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/d1/6c/d16ccf92-c3ed-47e3-a04b-072b2405bf70/fone.png__1170x0_q85_subsampling-2_upscale.png	2018-04-04 13:04:35.464018+00	29
168	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/fa/ae/faaed877-ec1e-4b1a-9dbc-c2db04beb280/plano_basico.jpg__1170x1170_q85_crop_subsampling-2_upscale.jpg	2018-04-05 02:20:19.954012+00	19
169	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/06/90/06903ff0-bc6a-4b9f-b77a-45cf5f31bfbd/plano-intermediario.jpg__1170x1170_q85_crop_subsampling-2_upscale.jpg	2018-04-05 02:20:20.13663+00	3
170	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/fa/ae/faaed877-ec1e-4b1a-9dbc-c2db04beb280/plano_basico.jpg__1170x877_q85_crop_subsampling-2_upscale.jpg	2018-04-05 02:20:43.946594+00	19
171	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/fa/ae/faaed877-ec1e-4b1a-9dbc-c2db04beb280/plano_basico.jpg__1170x658_q85_crop_subsampling-2_upscale.jpg	2018-04-05 02:20:56.71983+00	19
172	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/06/90/06903ff0-bc6a-4b9f-b77a-45cf5f31bfbd/plano-intermediario.jpg__1170x658_q85_crop_subsampling-2_upscale.jpg	2018-04-05 02:20:56.820679+00	3
173	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/fa/ae/faaed877-ec1e-4b1a-9dbc-c2db04beb280/plano_basico.jpg__1170x2730_q85_crop_subsampling-2_upscale.jpg	2018-04-05 02:21:06.823949+00	19
174	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/06/90/06903ff0-bc6a-4b9f-b77a-45cf5f31bfbd/plano-intermediario.jpg__1170x2730_q85_crop_subsampling-2_upscale.jpg	2018-04-05 02:21:07.325526+00	3
175	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/fa/ae/faaed877-ec1e-4b1a-9dbc-c2db04beb280/plano_basico.jpg__1170x731_q85_crop_subsampling-2_upscale.jpg	2018-04-05 02:21:42.484259+00	19
176	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/06/90/06903ff0-bc6a-4b9f-b77a-45cf5f31bfbd/plano-intermediario.jpg__1170x731_q85_crop_subsampling-2_upscale.jpg	2018-04-05 02:21:42.594397+00	3
177	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ea/3c/ea3cdde2-5ffa-4b26-8b26-5dab0dacb7d0/topo_planos.jpg__16x16_q85_crop_subsampling-2_upscale.jpg	2018-04-05 07:53:16.835794+00	30
178	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ea/3c/ea3cdde2-5ffa-4b26-8b26-5dab0dacb7d0/topo_planos.jpg__32x32_q85_crop_subsampling-2_upscale.jpg	2018-04-05 07:53:16.861641+00	30
179	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ea/3c/ea3cdde2-5ffa-4b26-8b26-5dab0dacb7d0/topo_planos.jpg__48x48_q85_crop_subsampling-2_upscale.jpg	2018-04-05 07:53:16.887159+00	30
180	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ea/3c/ea3cdde2-5ffa-4b26-8b26-5dab0dacb7d0/topo_planos.jpg__64x64_q85_crop_subsampling-2_upscale.jpg	2018-04-05 07:53:16.913216+00	30
181	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ea/3c/ea3cdde2-5ffa-4b26-8b26-5dab0dacb7d0/topo_planos.jpg__180x180_q85_crop_subsampling-2_upscale.jpg	2018-04-05 07:53:16.949898+00	30
182	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ea/3c/ea3cdde2-5ffa-4b26-8b26-5dab0dacb7d0/topo_planos.jpg__1170x0_q85_subsampling-2_upscale.jpg	2018-04-05 07:54:57.494075+00	30
183	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ac/d6/acd6ae08-41a5-4446-8956-a09e57803145/construflix-logomarca-use-full.png__16x16_q85_crop_subsampling-2_upscale.png	2018-04-09 14:44:22.552588+00	31
184	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ac/d6/acd6ae08-41a5-4446-8956-a09e57803145/construflix-logomarca-use-full.png__32x32_q85_crop_subsampling-2_upscale.png	2018-04-09 14:44:22.633624+00	31
185	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ac/d6/acd6ae08-41a5-4446-8956-a09e57803145/construflix-logomarca-use-full.png__48x48_q85_crop_subsampling-2_upscale.png	2018-04-09 14:44:22.733903+00	31
186	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ac/d6/acd6ae08-41a5-4446-8956-a09e57803145/construflix-logomarca-use-full.png__64x64_q85_crop_subsampling-2_upscale.png	2018-04-09 14:44:22.822188+00	31
187	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/ac/d6/acd6ae08-41a5-4446-8956-a09e57803145/construflix-logomarca-use-full.png__180x180_q85_crop_subsampling-2_upscale.png	2018-04-09 14:44:22.929236+00	31
188	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/69/0a/690a178f-6f3f-4e86-9a05-f10e8de3c10c/construflix-logomarca-use.png__16x16_q85_crop_subsampling-2_upscale.png	2018-04-09 14:45:27.590624+00	32
189	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/69/0a/690a178f-6f3f-4e86-9a05-f10e8de3c10c/construflix-logomarca-use.png__32x32_q85_crop_subsampling-2_upscale.png	2018-04-09 14:45:27.598861+00	32
190	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/69/0a/690a178f-6f3f-4e86-9a05-f10e8de3c10c/construflix-logomarca-use.png__48x48_q85_crop_subsampling-2_upscale.png	2018-04-09 14:45:27.605565+00	32
191	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/69/0a/690a178f-6f3f-4e86-9a05-f10e8de3c10c/construflix-logomarca-use.png__64x64_q85_crop_subsampling-2_upscale.png	2018-04-09 14:45:27.613926+00	32
192	f9bde26a1556cd667f742bd34ec7c55e	filer_public_thumbnails/filer_public/69/0a/690a178f-6f3f-4e86-9a05-f10e8de3c10c/construflix-logomarca-use.png__180x180_q85_crop_subsampling-2_upscale.png	2018-04-09 14:45:27.634172+00	32
\.


--
-- Data for Name: easy_thumbnails_thumbnaildimensions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY easy_thumbnails_thumbnaildimensions (id, thumbnail_id, width, height) FROM stdin;
\.


--
-- Data for Name: filer_clipboard; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY filer_clipboard (id, user_id) FROM stdin;
1	1
\.


--
-- Data for Name: filer_clipboarditem; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY filer_clipboarditem (id, clipboard_id, file_id) FROM stdin;
\.


--
-- Data for Name: filer_file; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY filer_file (id, file, _file_size, sha1, has_all_mandatory_data, original_filename, name, description, uploaded_at, modified_at, is_public, folder_id, owner_id, polymorphic_ctype_id) FROM stdin;
1	filer_public/ab/9f/ab9f605e-c3cf-4842-af76-9cd1ad41dd9e/plano_basico.jpg	8031	f3cc3de370a08fefdc9f460807d260bc7ccc49d4	f	plano_basico.jpg		\N	2018-03-20 08:44:57.928002+00	2018-03-20 08:44:57.928024+00	t	\N	1	30
2	filer_public/39/70/3970b287-6231-4118-b135-e2365b17347b/plano_basico1.png	40356	d6b8edab1e318d8b247ddd8400c5bb00425e550c	f	plano_basico1.png		\N	2018-03-20 08:50:24.736205+00	2018-03-20 08:50:24.736225+00	t	\N	1	30
3	filer_public/06/90/06903ff0-bc6a-4b9f-b77a-45cf5f31bfbd/plano-intermediario.jpg	13395	f9d6037cb7ea0b97cd8bafadf6291f7d203b74d2	f	plano-intermediario.jpg		\N	2018-03-20 09:01:05.253257+00	2018-03-20 09:01:05.253279+00	t	\N	1	30
5	filer_public/af/07/af078814-5cbe-4b41-a137-07c136069c34/checklist1.png	34699	2ff22e5b85329cd7f35007f724d0e9dbbfcc9f4d	f	checklist1.png		\N	2018-03-20 09:20:37.954632+00	2018-03-20 09:20:37.954658+00	t	\N	1	30
6	filer_public/3f/e4/3fe4fca1-86d2-4b0c-98f6-47503a51eee7/anlise-de-produtos.jpg	93384	1d7e47011881730be6ee56082ea006f680d59700	f	anlise-de-produtos.jpg		\N	2018-03-20 09:24:48.917909+00	2018-03-20 09:24:48.917933+00	t	\N	1	30
7	filer_public/5e/6a/5e6a8beb-4e32-46be-8e91-b7d1964e4e1f/default.png	27737	aa4d6c44b107e58fb1f0e9ac647d59b1a3324b43	f	default.png		\N	2018-03-20 09:40:11.068554+00	2018-03-20 09:40:11.068576+00	t	\N	1	30
8	filer_public/8c/ab/8cab0ee8-622c-4669-842a-d44c957ee9b8/checklist.jpg	5889	a03d1a9615fc0db5335c7326f369b64edfc98831	f	checklist.jpg		\N	2018-03-20 09:44:34.952108+00	2018-03-20 09:44:34.952129+00	t	\N	1	30
9	filer_public/d5/61/d56196c1-f1bc-499a-bb17-84284651b004/construflix.jpg	21754	8655d10035ff9bd04d00de739134927acfbaedbe	f	construflix.jpg		\N	2018-03-20 09:47:18.111426+00	2018-03-20 09:47:18.111447+00	t	\N	1	30
10	filer_public/ea/f6/eaf609a7-6962-4388-a3fe-e4588f1ab0cc/site_icon_construf.png	748	bff6daab5630a6bdb42b86f2ea3deab63f00edc9	f	site_icon_construf.png		\N	2018-03-20 10:48:53.661644+00	2018-03-20 10:48:53.661675+00	t	\N	1	30
11	filer_public/98/45/98452f1c-0f4c-4778-af81-f92c2f10ed49/site_icon_construf.png	748	bff6daab5630a6bdb42b86f2ea3deab63f00edc9	f	site_icon_construf.png		\N	2018-03-20 10:53:02.565796+00	2018-03-20 10:53:02.565815+00	t	\N	1	30
12	filer_public/75/7d/757d7ce6-ef8c-4451-a833-415d0572c7ad/construflix2.png	18698	8739c33c1e4e698f34c882bc54380264ad5c1c92	f	construflix2.png		\N	2018-03-21 15:43:26.784376+00	2018-03-21 15:43:26.784397+00	t	\N	1	30
13	filer_public/f7/83/f7832c7c-849d-4b14-8476-af2a1574c74b/checklist1.png	34699	2ff22e5b85329cd7f35007f724d0e9dbbfcc9f4d	f	checklist1.png		\N	2018-03-22 01:24:37.19692+00	2018-03-22 01:24:37.196944+00	t	\N	1	30
14	filer_public/9d/22/9d227c6f-773d-47b3-addd-b4af9c20e207/comece.jpg	11684	aec0b7a253d95c9c87ba5e08bf92709bf99181ed	f	comece.jpg		\N	2018-03-22 03:40:24.870092+00	2018-03-22 03:40:24.870122+00	t	\N	1	30
15	filer_public/28/82/2882c2d5-0540-4468-86e2-308d0baa2200/comece2.jpg	8369	7fc4a5a959e8f86ffd77053a2d054b57cc7d60ca	f	comece2.jpg		\N	2018-03-22 03:43:33.486111+00	2018-03-22 03:43:33.486132+00	t	\N	1	30
16	filer_public/44/7e/447e24ab-4469-43f5-bcb3-83fab071ea03/comece2.jpg	8527	3b046674f8b0eb363cd3ed020a5b63318689ce8d	f	comece2.jpg		\N	2018-03-22 03:45:13.349517+00	2018-03-22 03:45:13.349539+00	t	\N	1	30
17	filer_public/e9/6f/e96f9f50-cae8-4b21-b1a5-cfd55eab6b2b/comece2.jpg	6316	86d70745e77a1d65c7e926432bf1261bd0ea8501	f	comece2.jpg		\N	2018-03-22 03:46:50.177813+00	2018-03-22 03:46:50.177836+00	t	\N	1	30
18	filer_public/27/bb/27bbea40-5e0a-4608-b4f6-9380a87341ee/plano_basico1.png	40356	d6b8edab1e318d8b247ddd8400c5bb00425e550c	f	plano_basico1.png		\N	2018-03-22 04:04:28.632034+00	2018-03-22 04:04:28.632056+00	t	\N	1	30
19	filer_public/fa/ae/faaed877-ec1e-4b1a-9dbc-c2db04beb280/plano_basico.jpg	8031	f3cc3de370a08fefdc9f460807d260bc7ccc49d4	f	plano_basico.jpg		\N	2018-03-26 10:07:50.688325+00	2018-03-26 10:07:50.688346+00	t	\N	1	30
20	filer_public/05/0b/050bce90-6304-4646-b696-c95b0ed2ea11/topo_home.jpg	179658	e1b489d71e2b8f411be5c2df8c69e2451fb8148a	f	topo_home.jpg		\N	2018-04-03 01:20:13.357283+00	2018-04-03 01:20:13.357303+00	t	\N	1	30
21	filer_public/37/67/3767d6d0-aae0-447e-a23f-27f5e4c5ffdf/clientes-logo.jpg	26057	529f22dc29d0e15386a0e6eb2abf4eebe5576e60	f	clientes-logo.jpg		\N	2018-04-03 01:30:12.062343+00	2018-04-03 01:30:12.062363+00	t	\N	1	30
22	filer_public/c2/31/c231f7f2-ed3e-4eb1-bd07-33f625fca1c9/guia_de_produtos_e_fornecedores.jpg	42833	c815eb25d2f86b5ae80721497c82f933e1040772	f	guia_de_produtos_e_fornecedores.jpg		\N	2018-04-03 01:56:27.093725+00	2018-04-03 01:56:27.093753+00	t	\N	1	30
23	filer_public/a5/10/a510969e-9632-43c9-a7b3-f4dfa6756109/guia_de_produtos_e_fornecedores.jpg	42833	c815eb25d2f86b5ae80721497c82f933e1040772	f	guia_de_produtos_e_fornecedores.jpg		\N	2018-04-03 01:56:48.760986+00	2018-04-03 01:56:48.761008+00	t	\N	1	30
24	filer_public/ff/73/ff736e36-c8fd-44a5-9e72-7a0e8b30df39/analise_de_projetos.jpg	36105	9c562c495ffbcbb55f4d4f598a2022070ea77a63	f	analise_de_projetos.jpg		\N	2018-04-03 02:14:41.303887+00	2018-04-03 02:14:41.303907+00	t	\N	1	30
25	filer_public/e1/27/e12700d3-1b45-4efb-850c-3bb8d89952a5/analise_de_manual_do_proprietario.jpg	42696	a1708ba75902eb23b2930ccdc596c863ff3954c2	f	analise_de_manual_do_proprietario.jpg		\N	2018-04-03 02:16:36.836068+00	2018-04-03 02:16:36.836089+00	t	\N	1	30
26	filer_public/5b/14/5b14616a-3a56-4464-a7fb-b17d29a5581d/contato_imagem.jpg	333583	0211cc8791c20a1e2b6c0763fe5c076be8f051df	f	contato_imagem.jpg		\N	2018-04-03 02:28:24.597078+00	2018-04-03 02:28:24.597099+00	t	\N	1	30
27	filer_public/2b/96/2b961732-722c-4e1c-84d6-213f09e99c6b/topo_institucional.jpg	175634	4ffbfc7e8bf19d25b4f94a88f34fa485c411c559	f	topo_institucional.jpg		\N	2018-04-03 14:14:49.330297+00	2018-04-03 14:14:49.330318+00	t	\N	1	30
28	filer_public/ae/de/aede9d94-accb-44d2-8340-c6965c276347/mapa_contato.jpg	262265	aa6fdd65a9e7a42f47bd937eb319b54ca31b2a31	f	mapa_contato.jpg		\N	2018-04-04 00:10:00.298728+00	2018-04-04 00:10:00.298751+00	t	\N	1	30
29	filer_public/d1/6c/d16ccf92-c3ed-47e3-a04b-072b2405bf70/fone.png	4379	c5781ea92bbfff1f4b4e609a61815c888ef2973a	f	fone.png		\N	2018-04-04 13:04:30.94462+00	2018-04-04 13:04:30.944642+00	t	\N	1	30
30	filer_public/ea/3c/ea3cdde2-5ffa-4b26-8b26-5dab0dacb7d0/topo_planos.jpg	177767	5220faaf5fca169b6c32b7892b380141fb9e91eb	f	topo_planos.jpg		\N	2018-04-05 07:53:16.80413+00	2018-04-05 07:53:16.804155+00	t	\N	1	30
31	filer_public/ac/d6/acd6ae08-41a5-4446-8956-a09e57803145/construflix-logomarca-use-full.png	41759	b83a654419142f482588a7e67ae1262ef37b8327	f	construflix-logomarca-use-full.png		\N	2018-04-09 14:44:22.461081+00	2018-04-09 14:44:22.46111+00	t	\N	1	30
32	filer_public/69/0a/690a178f-6f3f-4e86-9a05-f10e8de3c10c/construflix-logomarca-use.png	5356	a465d190121ee37c0e8d00797d88e11361110808	f	construflix-logomarca-use.png		\N	2018-04-09 14:45:27.578484+00	2018-04-09 14:45:27.578508+00	t	\N	1	30
\.


--
-- Data for Name: filer_folder; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY filer_folder (id, name, uploaded_at, created_at, modified_at, lft, rght, tree_id, level, owner_id, parent_id) FROM stdin;
\.


--
-- Data for Name: filer_folderpermission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY filer_folderpermission (id, type, everybody, can_edit, can_read, can_add_children, folder_id, group_id, user_id) FROM stdin;
\.


--
-- Data for Name: filer_image; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY filer_image (file_ptr_id, _height, _width, date_taken, default_alt_text, default_caption, author, must_always_publish_author_credit, must_always_publish_copyright, subject_location) FROM stdin;
1	170	297	2018-03-20 08:44:57.923007+00	\N	\N	\N	f	f	
2	102	179	2018-03-20 08:50:24.732699+00	\N	\N	\N	f	f	
3	229	400	2018-03-20 09:01:05.249391+00	\N	\N	\N	f	f	
5	92	138	2018-03-20 09:20:37.94132+00	\N	\N	\N	f	f	
6	479	638	2018-03-20 09:24:48.913604+00	\N	\N	\N	f	f	
7	628	1200	2018-03-20 09:40:11.064405+00	\N	\N	\N	f	f	
8	183	275	2018-03-20 09:44:34.948172+00	\N	\N	\N	f	f	
9	377	720	2018-03-20 09:47:18.10634+00	\N	\N	\N	f	f	
10	16	16	2018-03-20 10:48:53.657719+00	\N	\N	\N	f	f	
11	16	16	2018-03-20 10:53:02.562383+00	\N	\N	\N	f	f	
12	227	432	2018-03-21 15:43:26.778132+00	\N	\N	\N	f	f	
13	92	138	2018-03-22 01:24:37.191333+00	\N	\N	\N	f	f	
14	130	388	2018-03-22 03:40:24.866009+00	\N	\N	\N	f	f	
15	112	448	2018-03-22 03:43:33.482298+00	\N	\N	\N	f	f	
16	68	269	2018-03-22 03:45:13.345786+00	\N	\N	\N	f	f	
17	55	216	2018-03-22 03:46:50.172549+00	\N	\N	\N	f	f	
18	102	179	2018-03-22 04:04:28.628199+00	\N	\N	\N	f	f	
19	170	297	2018-03-26 10:07:50.683441+00	\N	\N	\N	f	f	
20	223	1366	2018-04-03 01:20:13.352114+00	\N	\N	\N	f	f	
21	228	318	2018-04-03 01:30:12.057181+00	\N	\N	\N	f	f	
22	228	318	2018-04-03 01:56:27.086096+00	\N	\N	\N	f	f	
23	228	318	2018-04-03 01:56:48.756672+00	\N	\N	\N	f	f	
24	228	318	2018-04-03 02:14:41.298392+00	\N	\N	\N	f	f	
25	228	318	2018-04-03 02:16:36.830461+00	\N	\N	\N	f	f	
26	613	1366	2018-04-03 02:28:24.592511+00	\N	\N	\N	f	f	
27	223	1366	2018-04-03 14:14:49.3256+00	\N	\N	\N	f	f	
28	606	1366	2018-04-04 00:10:00.291864+00	\N	\N	\N	f	f	
29	55	57	2018-04-04 13:04:30.940071+00	\N	\N	\N	f	f	
30	223	1366	2018-04-05 07:53:16.798708+00	\N	\N	\N	f	f	
31	568	2777	2018-04-09 14:44:22.455082+00	\N	\N	\N	f	f	
32	48	235	2018-04-09 14:45:27.573799+00	\N	\N	\N	f	f	
\.


--
-- Data for Name: filer_thumbnailoption; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY filer_thumbnailoption (id, name, width, height, crop, upscale) FROM stdin;
\.


--
-- Data for Name: menus_cachekey; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY menus_cachekey (id, language, site, key) FROM stdin;
417	pt	1	cms_3.5.1_menu_nodes_pt_1_2_user:public
418	pt	1	cms_3.5.1_menu_nodes_pt_1:public
419	pt	1	cms_3.5.1_menu_nodes_pt_1_1_user:draft
416	pt	1	cms_3.5.1_menu_nodes_pt_1_1_user:public
\.


--
-- Data for Name: polls_choice; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY polls_choice (id, choice_text, votes, poll_id) FROM stdin;
\.


--
-- Data for Name: polls_poll; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY polls_poll (id, question) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bitnami
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, true);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bitnami
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 4, true);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bitnami
--

SELECT pg_catalog.setval('auth_permission_id_seq', 272, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bitnami
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, true);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bitnami
--

SELECT pg_catalog.setval('auth_user_id_seq', 2, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bitnami
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 8, true);


--
-- Name: checklist_avaliacao_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('checklist_avaliacao_id_seq', 8, true);


--
-- Name: checklist_checklist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('checklist_checklist_id_seq', 55, true);


--
-- Name: checklist_empreendimento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('checklist_empreendimento_id_seq', 6, true);


--
-- Name: checklist_etapaprojeto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('checklist_etapaprojeto_id_seq', 2, true);


--
-- Name: checklist_itemavaliacao_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('checklist_itemavaliacao_id_seq', 66, true);


--
-- Name: checklist_itemchecklist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('checklist_itemchecklist_id_seq', 8, true);


--
-- Name: checklist_plano_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('checklist_plano_id_seq', 1, true);


--
-- Name: checklist_produto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('checklist_produto_id_seq', 2, true);


--
-- Name: checklist_produto_planos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('checklist_produto_planos_id_seq', 2, true);


--
-- Name: checklist_projeto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('checklist_projeto_id_seq', 3, true);


--
-- Name: checklist_ramo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('checklist_ramo_id_seq', 1, true);


--
-- Name: cms_cmsplugin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cms_cmsplugin_id_seq', 1203, true);


--
-- Name: cms_globalpagepermission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cms_globalpagepermission_id_seq', 1, false);


--
-- Name: cms_globalpagepermission_sites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cms_globalpagepermission_sites_id_seq', 1, false);


--
-- Name: cms_page_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cms_page_id_seq', 26, true);


--
-- Name: cms_page_placeholders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cms_page_placeholders_id_seq', 38, true);


--
-- Name: cms_pagepermission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cms_pagepermission_id_seq', 1, false);


--
-- Name: cms_placeholder_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cms_placeholder_id_seq', 43, true);


--
-- Name: cms_staticplaceholder_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cms_staticplaceholder_id_seq', 2, true);


--
-- Name: cms_title_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cms_title_id_seq', 26, true);


--
-- Name: cms_treenode_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cms_treenode_id_seq', 14, true);


--
-- Name: cms_urlconfrevision_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cms_urlconfrevision_id_seq', 1, false);


--
-- Name: cms_usersettings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cms_usersettings_id_seq', 1, true);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bitnami
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 403, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bitnami
--

SELECT pg_catalog.setval('django_content_type_id_seq', 91, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bitnami
--

SELECT pg_catalog.setval('django_migrations_id_seq', 151, true);


--
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_site_id_seq', 1, true);


--
-- Name: djangocms_snippet_snippet_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('djangocms_snippet_snippet_id_seq', 2, true);


--
-- Name: easy_thumbnails_source_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('easy_thumbnails_source_id_seq', 32, true);


--
-- Name: easy_thumbnails_thumbnail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('easy_thumbnails_thumbnail_id_seq', 192, true);


--
-- Name: easy_thumbnails_thumbnaildimensions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('easy_thumbnails_thumbnaildimensions_id_seq', 1, false);


--
-- Name: filer_clipboard_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('filer_clipboard_id_seq', 1, true);


--
-- Name: filer_clipboarditem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('filer_clipboarditem_id_seq', 1, false);


--
-- Name: filer_file_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('filer_file_id_seq', 32, true);


--
-- Name: filer_folder_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('filer_folder_id_seq', 1, false);


--
-- Name: filer_folderpermission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('filer_folderpermission_id_seq', 1, false);


--
-- Name: filer_thumbnailoption_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('filer_thumbnailoption_id_seq', 1, false);


--
-- Name: menus_cachekey_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('menus_cachekey_id_seq', 419, true);


--
-- Name: polls_choice_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('polls_choice_id_seq', 1, false);


--
-- Name: polls_poll_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('polls_poll_id_seq', 1, false);


--
-- Name: aldryn_bootstrap3_boostrap3alertplugin aldryn_bootstrap3_boostrap3alertplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3alertplugin
    ADD CONSTRAINT aldryn_bootstrap3_boostrap3alertplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_boostrap3blockquoteplugin aldryn_bootstrap3_boostrap3blockquoteplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3blockquoteplugin
    ADD CONSTRAINT aldryn_bootstrap3_boostrap3blockquoteplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_boostrap3buttonplugin aldryn_bootstrap3_boostrap3buttonplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3buttonplugin
    ADD CONSTRAINT aldryn_bootstrap3_boostrap3buttonplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_boostrap3citeplugin aldryn_bootstrap3_boostrap3citeplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3citeplugin
    ADD CONSTRAINT aldryn_bootstrap3_boostrap3citeplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_boostrap3iconplugin aldryn_bootstrap3_boostrap3iconplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3iconplugin
    ADD CONSTRAINT aldryn_bootstrap3_boostrap3iconplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_boostrap3imageplugin aldryn_bootstrap3_boostrap3imageplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3imageplugin
    ADD CONSTRAINT aldryn_bootstrap3_boostrap3imageplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_boostrap3jumbotronplugin aldryn_bootstrap3_boostrap3jumbotronplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3jumbotronplugin
    ADD CONSTRAINT aldryn_bootstrap3_boostrap3jumbotronplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_boostrap3labelplugin aldryn_bootstrap3_boostrap3labelplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3labelplugin
    ADD CONSTRAINT aldryn_bootstrap3_boostrap3labelplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_boostrap3panelbodyplugin aldryn_bootstrap3_boostrap3panelbodyplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3panelbodyplugin
    ADD CONSTRAINT aldryn_bootstrap3_boostrap3panelbodyplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_boostrap3panelfooterplugin aldryn_bootstrap3_boostrap3panelfooterplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3panelfooterplugin
    ADD CONSTRAINT aldryn_bootstrap3_boostrap3panelfooterplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_boostrap3panelheadingplugin aldryn_bootstrap3_boostrap3panelheadingplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3panelheadingplugin
    ADD CONSTRAINT aldryn_bootstrap3_boostrap3panelheadingplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_boostrap3panelplugin aldryn_bootstrap3_boostrap3panelplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3panelplugin
    ADD CONSTRAINT aldryn_bootstrap3_boostrap3panelplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_boostrap3spacerplugin aldryn_bootstrap3_boostrap3spacerplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3spacerplugin
    ADD CONSTRAINT aldryn_bootstrap3_boostrap3spacerplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_boostrap3wellplugin aldryn_bootstrap3_boostrap3wellplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3wellplugin
    ADD CONSTRAINT aldryn_bootstrap3_boostrap3wellplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_bootstrap3accordionitemplugin aldryn_bootstrap3_bootstrap3accordionitemplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3accordionitemplugin
    ADD CONSTRAINT aldryn_bootstrap3_bootstrap3accordionitemplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_bootstrap3accordionplugin aldryn_bootstrap3_bootstrap3accordionplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3accordionplugin
    ADD CONSTRAINT aldryn_bootstrap3_bootstrap3accordionplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_bootstrap3carouselplugin aldryn_bootstrap3_bootstrap3carouselplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3carouselplugin
    ADD CONSTRAINT aldryn_bootstrap3_bootstrap3carouselplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_bootstrap3carouselslidefolderplugin aldryn_bootstrap3_bootstrap3carouselslidefolderplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3carouselslidefolderplugin
    ADD CONSTRAINT aldryn_bootstrap3_bootstrap3carouselslidefolderplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_bootstrap3carouselslideplugin aldryn_bootstrap3_bootstrap3carouselslideplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3carouselslideplugin
    ADD CONSTRAINT aldryn_bootstrap3_bootstrap3carouselslideplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_bootstrap3codeplugin aldryn_bootstrap3_bootstrap3codeplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3codeplugin
    ADD CONSTRAINT aldryn_bootstrap3_bootstrap3codeplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_bootstrap3columnplugin aldryn_bootstrap3_bootstrap3columnplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3columnplugin
    ADD CONSTRAINT aldryn_bootstrap3_bootstrap3columnplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_bootstrap3fileplugin aldryn_bootstrap3_bootstrap3fileplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3fileplugin
    ADD CONSTRAINT aldryn_bootstrap3_bootstrap3fileplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_bootstrap3listgroupitemplugin aldryn_bootstrap3_bootstrap3listgroupitemplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3listgroupitemplugin
    ADD CONSTRAINT aldryn_bootstrap3_bootstrap3listgroupitemplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_bootstrap3listgroupplugin aldryn_bootstrap3_bootstrap3listgroupplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3listgroupplugin
    ADD CONSTRAINT aldryn_bootstrap3_bootstrap3listgroupplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_bootstrap3responsiveplugin aldryn_bootstrap3_bootstrap3responsiveplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3responsiveplugin
    ADD CONSTRAINT aldryn_bootstrap3_bootstrap3responsiveplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_bootstrap3rowplugin aldryn_bootstrap3_bootstrap3rowplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3rowplugin
    ADD CONSTRAINT aldryn_bootstrap3_bootstrap3rowplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_bootstrap3tabitemplugin aldryn_bootstrap3_bootstrap3tabitemplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3tabitemplugin
    ADD CONSTRAINT aldryn_bootstrap3_bootstrap3tabitemplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: aldryn_bootstrap3_bootstrap3tabplugin aldryn_bootstrap3_bootstrap3tabplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3tabplugin
    ADD CONSTRAINT aldryn_bootstrap3_bootstrap3tabplugin_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: checklist_avaliacao checklist_avaliacao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_avaliacao
    ADD CONSTRAINT checklist_avaliacao_pkey PRIMARY KEY (id);


--
-- Name: checklist_checklist checklist_checklist_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_checklist
    ADD CONSTRAINT checklist_checklist_pkey PRIMARY KEY (id);


--
-- Name: checklist_cms_integration_avaliacaopluginmodel checklist_cms_integration_avaliacaopluginmodel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_cms_integration_avaliacaopluginmodel
    ADD CONSTRAINT checklist_cms_integration_avaliacaopluginmodel_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: checklist_cms_integration_checklistspluginmodel checklist_cms_integration_checklistspluginmodel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_cms_integration_checklistspluginmodel
    ADD CONSTRAINT checklist_cms_integration_checklistspluginmodel_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: checklist_empreendimento checklist_empreendimento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_empreendimento
    ADD CONSTRAINT checklist_empreendimento_pkey PRIMARY KEY (id);


--
-- Name: checklist_etapaprojeto checklist_etapaprojeto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_etapaprojeto
    ADD CONSTRAINT checklist_etapaprojeto_pkey PRIMARY KEY (id);


--
-- Name: checklist_itemavaliacao checklist_itemavaliacao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_itemavaliacao
    ADD CONSTRAINT checklist_itemavaliacao_pkey PRIMARY KEY (id);


--
-- Name: checklist_itemchecklist checklist_itemchecklist_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_itemchecklist
    ADD CONSTRAINT checklist_itemchecklist_pkey PRIMARY KEY (id);


--
-- Name: checklist_plano checklist_plano_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_plano
    ADD CONSTRAINT checklist_plano_pkey PRIMARY KEY (id);


--
-- Name: checklist_produto checklist_produto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_produto
    ADD CONSTRAINT checklist_produto_pkey PRIMARY KEY (id);


--
-- Name: checklist_produto_planos checklist_produto_planos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_produto_planos
    ADD CONSTRAINT checklist_produto_planos_pkey PRIMARY KEY (id);


--
-- Name: checklist_produto_planos checklist_produto_planos_produto_id_plano_id_67072483_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_produto_planos
    ADD CONSTRAINT checklist_produto_planos_produto_id_plano_id_67072483_uniq UNIQUE (produto_id, plano_id);


--
-- Name: checklist_projeto checklist_projeto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_projeto
    ADD CONSTRAINT checklist_projeto_pkey PRIMARY KEY (id);


--
-- Name: checklist_ramo checklist_ramo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_ramo
    ADD CONSTRAINT checklist_ramo_pkey PRIMARY KEY (id);


--
-- Name: cms_aliaspluginmodel cms_aliaspluginmodel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_aliaspluginmodel
    ADD CONSTRAINT cms_aliaspluginmodel_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: cms_cmsplugin cms_cmsplugin_path_4917bb44_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_cmsplugin
    ADD CONSTRAINT cms_cmsplugin_path_4917bb44_uniq UNIQUE (path);


--
-- Name: cms_cmsplugin cms_cmsplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_cmsplugin
    ADD CONSTRAINT cms_cmsplugin_pkey PRIMARY KEY (id);


--
-- Name: cms_globalpagepermission_sites cms_globalpagepermission_globalpagepermission_id__db684f41_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_globalpagepermission_sites
    ADD CONSTRAINT cms_globalpagepermission_globalpagepermission_id__db684f41_uniq UNIQUE (globalpagepermission_id, site_id);


--
-- Name: cms_globalpagepermission cms_globalpagepermission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_globalpagepermission
    ADD CONSTRAINT cms_globalpagepermission_pkey PRIMARY KEY (id);


--
-- Name: cms_globalpagepermission_sites cms_globalpagepermission_sites_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_globalpagepermission_sites
    ADD CONSTRAINT cms_globalpagepermission_sites_pkey PRIMARY KEY (id);


--
-- Name: cms_page cms_page_node_id_publisher_is_draft_c1293d6a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_page
    ADD CONSTRAINT cms_page_node_id_publisher_is_draft_c1293d6a_uniq UNIQUE (node_id, publisher_is_draft);


--
-- Name: cms_page cms_page_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_page
    ADD CONSTRAINT cms_page_pkey PRIMARY KEY (id);


--
-- Name: cms_page_placeholders cms_page_placeholders_page_id_placeholder_id_ab7fbfb8_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_page_placeholders
    ADD CONSTRAINT cms_page_placeholders_page_id_placeholder_id_ab7fbfb8_uniq UNIQUE (page_id, placeholder_id);


--
-- Name: cms_page_placeholders cms_page_placeholders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_page_placeholders
    ADD CONSTRAINT cms_page_placeholders_pkey PRIMARY KEY (id);


--
-- Name: cms_page cms_page_publisher_public_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_page
    ADD CONSTRAINT cms_page_publisher_public_id_key UNIQUE (publisher_public_id);


--
-- Name: cms_pagepermission cms_pagepermission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_pagepermission
    ADD CONSTRAINT cms_pagepermission_pkey PRIMARY KEY (id);


--
-- Name: cms_pageuser cms_pageuser_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_pageuser
    ADD CONSTRAINT cms_pageuser_pkey PRIMARY KEY (user_ptr_id);


--
-- Name: cms_pageusergroup cms_pageusergroup_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_pageusergroup
    ADD CONSTRAINT cms_pageusergroup_pkey PRIMARY KEY (group_ptr_id);


--
-- Name: cms_placeholder cms_placeholder_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_placeholder
    ADD CONSTRAINT cms_placeholder_pkey PRIMARY KEY (id);


--
-- Name: cms_placeholderreference cms_placeholderreference_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_placeholderreference
    ADD CONSTRAINT cms_placeholderreference_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: cms_staticplaceholder cms_staticplaceholder_code_site_id_21ba079c_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_staticplaceholder
    ADD CONSTRAINT cms_staticplaceholder_code_site_id_21ba079c_uniq UNIQUE (code, site_id);


--
-- Name: cms_staticplaceholder cms_staticplaceholder_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_staticplaceholder
    ADD CONSTRAINT cms_staticplaceholder_pkey PRIMARY KEY (id);


--
-- Name: cms_title cms_title_language_page_id_61aaf084_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_title
    ADD CONSTRAINT cms_title_language_page_id_61aaf084_uniq UNIQUE (language, page_id);


--
-- Name: cms_title cms_title_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_title
    ADD CONSTRAINT cms_title_pkey PRIMARY KEY (id);


--
-- Name: cms_title cms_title_publisher_public_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_title
    ADD CONSTRAINT cms_title_publisher_public_id_key UNIQUE (publisher_public_id);


--
-- Name: cms_treenode cms_treenode_path_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_treenode
    ADD CONSTRAINT cms_treenode_path_key UNIQUE (path);


--
-- Name: cms_treenode cms_treenode_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_treenode
    ADD CONSTRAINT cms_treenode_pkey PRIMARY KEY (id);


--
-- Name: cms_urlconfrevision cms_urlconfrevision_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_urlconfrevision
    ADD CONSTRAINT cms_urlconfrevision_pkey PRIMARY KEY (id);


--
-- Name: cms_usersettings cms_usersettings_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_usersettings
    ADD CONSTRAINT cms_usersettings_pkey PRIMARY KEY (id);


--
-- Name: cms_usersettings cms_usersettings_user_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_usersettings
    ADD CONSTRAINT cms_usersettings_user_id_key UNIQUE (user_id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: django_site django_site_domain_a2e37b91_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_site
    ADD CONSTRAINT django_site_domain_a2e37b91_uniq UNIQUE (domain);


--
-- Name: django_site django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);


--
-- Name: djangocms_column_column djangocms_column_column_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_column_column
    ADD CONSTRAINT djangocms_column_column_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: djangocms_column_multicolumns djangocms_column_multicolumns_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_column_multicolumns
    ADD CONSTRAINT djangocms_column_multicolumns_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: djangocms_file_file djangocms_file_file_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_file_file
    ADD CONSTRAINT djangocms_file_file_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: djangocms_file_folder djangocms_file_folder_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_file_folder
    ADD CONSTRAINT djangocms_file_folder_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: djangocms_googlemap_googlemap djangocms_googlemap_googlemap_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_googlemap_googlemap
    ADD CONSTRAINT djangocms_googlemap_googlemap_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: djangocms_googlemap_googlemapmarker djangocms_googlemap_googlemapmarker_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_googlemap_googlemapmarker
    ADD CONSTRAINT djangocms_googlemap_googlemapmarker_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: djangocms_googlemap_googlemaproute djangocms_googlemap_googlemaproute_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_googlemap_googlemaproute
    ADD CONSTRAINT djangocms_googlemap_googlemaproute_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: djangocms_link_link djangocms_link_link_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_link_link
    ADD CONSTRAINT djangocms_link_link_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: djangocms_picture_picture djangocms_picture_picture_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_picture_picture
    ADD CONSTRAINT djangocms_picture_picture_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: djangocms_snippet_snippet djangocms_snippet_snippet_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_snippet_snippet
    ADD CONSTRAINT djangocms_snippet_snippet_name_key UNIQUE (name);


--
-- Name: djangocms_snippet_snippet djangocms_snippet_snippet_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_snippet_snippet
    ADD CONSTRAINT djangocms_snippet_snippet_pkey PRIMARY KEY (id);


--
-- Name: djangocms_snippet_snippet djangocms_snippet_snippet_slug_bd43cd96_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_snippet_snippet
    ADD CONSTRAINT djangocms_snippet_snippet_slug_bd43cd96_uniq UNIQUE (slug);


--
-- Name: djangocms_snippet_snippetptr djangocms_snippet_snippetptr_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_snippet_snippetptr
    ADD CONSTRAINT djangocms_snippet_snippetptr_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: djangocms_style_style djangocms_style_style_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_style_style
    ADD CONSTRAINT djangocms_style_style_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: djangocms_text_ckeditor_text djangocms_text_ckeditor_text_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_text_ckeditor_text
    ADD CONSTRAINT djangocms_text_ckeditor_text_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: djangocms_video_videoplayer djangocms_video_video_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_video_videoplayer
    ADD CONSTRAINT djangocms_video_video_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: djangocms_video_videosource djangocms_video_videosource_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_video_videosource
    ADD CONSTRAINT djangocms_video_videosource_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: djangocms_video_videotrack djangocms_video_videotrack_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_video_videotrack
    ADD CONSTRAINT djangocms_video_videotrack_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: easy_thumbnails_source easy_thumbnails_source_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY easy_thumbnails_source
    ADD CONSTRAINT easy_thumbnails_source_pkey PRIMARY KEY (id);


--
-- Name: easy_thumbnails_source easy_thumbnails_source_storage_hash_name_481ce32d_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY easy_thumbnails_source
    ADD CONSTRAINT easy_thumbnails_source_storage_hash_name_481ce32d_uniq UNIQUE (storage_hash, name);


--
-- Name: easy_thumbnails_thumbnail easy_thumbnails_thumbnai_storage_hash_name_source_fb375270_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY easy_thumbnails_thumbnail
    ADD CONSTRAINT easy_thumbnails_thumbnai_storage_hash_name_source_fb375270_uniq UNIQUE (storage_hash, name, source_id);


--
-- Name: easy_thumbnails_thumbnail easy_thumbnails_thumbnail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY easy_thumbnails_thumbnail
    ADD CONSTRAINT easy_thumbnails_thumbnail_pkey PRIMARY KEY (id);


--
-- Name: easy_thumbnails_thumbnaildimensions easy_thumbnails_thumbnaildimensions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY easy_thumbnails_thumbnaildimensions
    ADD CONSTRAINT easy_thumbnails_thumbnaildimensions_pkey PRIMARY KEY (id);


--
-- Name: easy_thumbnails_thumbnaildimensions easy_thumbnails_thumbnaildimensions_thumbnail_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY easy_thumbnails_thumbnaildimensions
    ADD CONSTRAINT easy_thumbnails_thumbnaildimensions_thumbnail_id_key UNIQUE (thumbnail_id);


--
-- Name: filer_clipboard filer_clipboard_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_clipboard
    ADD CONSTRAINT filer_clipboard_pkey PRIMARY KEY (id);


--
-- Name: filer_clipboarditem filer_clipboarditem_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_clipboarditem
    ADD CONSTRAINT filer_clipboarditem_pkey PRIMARY KEY (id);


--
-- Name: filer_file filer_file_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_file
    ADD CONSTRAINT filer_file_pkey PRIMARY KEY (id);


--
-- Name: filer_folder filer_folder_parent_id_name_bc773258_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_folder
    ADD CONSTRAINT filer_folder_parent_id_name_bc773258_uniq UNIQUE (parent_id, name);


--
-- Name: filer_folder filer_folder_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_folder
    ADD CONSTRAINT filer_folder_pkey PRIMARY KEY (id);


--
-- Name: filer_folderpermission filer_folderpermission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_folderpermission
    ADD CONSTRAINT filer_folderpermission_pkey PRIMARY KEY (id);


--
-- Name: filer_image filer_image_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_image
    ADD CONSTRAINT filer_image_pkey PRIMARY KEY (file_ptr_id);


--
-- Name: filer_thumbnailoption filer_thumbnailoption_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_thumbnailoption
    ADD CONSTRAINT filer_thumbnailoption_pkey PRIMARY KEY (id);


--
-- Name: menus_cachekey menus_cachekey_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY menus_cachekey
    ADD CONSTRAINT menus_cachekey_pkey PRIMARY KEY (id);


--
-- Name: polls_choice polls_choice_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY polls_choice
    ADD CONSTRAINT polls_choice_pkey PRIMARY KEY (id);


--
-- Name: polls_poll polls_poll_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY polls_poll
    ADD CONSTRAINT polls_poll_pkey PRIMARY KEY (id);


--
-- Name: aldryn_bootstrap3_boostrap3buttonplugin_link_file_id_96f04a7b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX aldryn_bootstrap3_boostrap3buttonplugin_link_file_id_96f04a7b ON aldryn_bootstrap3_boostrap3buttonplugin USING btree (link_file_id);


--
-- Name: aldryn_bootstrap3_boostrap3buttonplugin_link_page_id_5cf9d4c7; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX aldryn_bootstrap3_boostrap3buttonplugin_link_page_id_5cf9d4c7 ON aldryn_bootstrap3_boostrap3buttonplugin USING btree (link_page_id);


--
-- Name: aldryn_bootstrap3_boostrap3imageplugin_file_id_67ad88d7; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX aldryn_bootstrap3_boostrap3imageplugin_file_id_67ad88d7 ON aldryn_bootstrap3_boostrap3imageplugin USING btree (file_id);


--
-- Name: aldryn_bootstrap3_bootstra_folder_id_232dba10; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX aldryn_bootstrap3_bootstra_folder_id_232dba10 ON aldryn_bootstrap3_bootstrap3carouselslidefolderplugin USING btree (folder_id);


--
-- Name: aldryn_bootstrap3_bootstra_image_id_d52f4f75; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX aldryn_bootstrap3_bootstra_image_id_d52f4f75 ON aldryn_bootstrap3_bootstrap3carouselslideplugin USING btree (image_id);


--
-- Name: aldryn_bootstrap3_bootstra_link_file_id_1512669d; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX aldryn_bootstrap3_bootstra_link_file_id_1512669d ON aldryn_bootstrap3_bootstrap3carouselslideplugin USING btree (link_file_id);


--
-- Name: aldryn_bootstrap3_bootstra_link_page_id_55b29fcc; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX aldryn_bootstrap3_bootstra_link_page_id_55b29fcc ON aldryn_bootstrap3_bootstrap3carouselslideplugin USING btree (link_page_id);


--
-- Name: aldryn_bootstrap3_bootstrap3columnplugin_tag_a6653c2d; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX aldryn_bootstrap3_bootstrap3columnplugin_tag_a6653c2d ON aldryn_bootstrap3_bootstrap3columnplugin USING btree (tag);


--
-- Name: aldryn_bootstrap3_bootstrap3columnplugin_tag_a6653c2d_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX aldryn_bootstrap3_bootstrap3columnplugin_tag_a6653c2d_like ON aldryn_bootstrap3_bootstrap3columnplugin USING btree (tag varchar_pattern_ops);


--
-- Name: aldryn_bootstrap3_bootstrap3fileplugin_file_id_c83c321d; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX aldryn_bootstrap3_bootstrap3fileplugin_file_id_c83c321d ON aldryn_bootstrap3_bootstrap3fileplugin USING btree (file_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: bitnami
--

CREATE INDEX auth_group_name_a6ea08ec_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: bitnami
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: bitnami
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: bitnami
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: bitnami
--

CREATE INDEX auth_user_groups_group_id_97559544 ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: bitnami
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: bitnami
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: bitnami
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: bitnami
--

CREATE INDEX auth_user_username_6821ab7c_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- Name: checklist_avaliacao_avaliador_id_7c40f0e2; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX checklist_avaliacao_avaliador_id_7c40f0e2 ON checklist_avaliacao USING btree (avaliador_id);


--
-- Name: checklist_avaliacao_empreendimento_id_9ef95bf3; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX checklist_avaliacao_empreendimento_id_9ef95bf3 ON checklist_avaliacao USING btree (empreendimento_id);


--
-- Name: checklist_avaliacao_etapa_projeto_id_ca2ec61e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX checklist_avaliacao_etapa_projeto_id_ca2ec61e ON checklist_avaliacao USING btree (etapa_projeto_id);


--
-- Name: checklist_avaliacao_produto_id_58a09123; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX checklist_avaliacao_produto_id_58a09123 ON checklist_avaliacao USING btree (produto_id);


--
-- Name: checklist_avaliacao_projeto_id_ea20648e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX checklist_avaliacao_projeto_id_ea20648e ON checklist_avaliacao USING btree (projeto_id);


--
-- Name: checklist_checklist_produto_id_158bfe0d; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX checklist_checklist_produto_id_158bfe0d ON checklist_checklist USING btree (produto_id);


--
-- Name: checklist_cms_integration__avaliacao_id_d3b7f5b8; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX checklist_cms_integration__avaliacao_id_d3b7f5b8 ON checklist_cms_integration_avaliacaopluginmodel USING btree (avaliacao_id);


--
-- Name: checklist_cms_integration__checklist_id_313c67b5; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX checklist_cms_integration__checklist_id_313c67b5 ON checklist_cms_integration_checklistspluginmodel USING btree (checklist_id);


--
-- Name: checklist_empreendimento_assinante_id_db825301; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX checklist_empreendimento_assinante_id_db825301 ON checklist_empreendimento USING btree (assinante_id);


--
-- Name: checklist_itemavaliacao_avaliacao_id_30352297; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX checklist_itemavaliacao_avaliacao_id_30352297 ON checklist_itemavaliacao USING btree (avaliacao_id);


--
-- Name: checklist_itemavaliacao_checklist_id_3296c317; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX checklist_itemavaliacao_checklist_id_3296c317 ON checklist_itemavaliacao USING btree (checklist_id);


--
-- Name: checklist_itemchecklist_checklist_id_9b86ccce; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX checklist_itemchecklist_checklist_id_9b86ccce ON checklist_itemchecklist USING btree (checklist_id);


--
-- Name: checklist_produto_planos_plano_id_7fd92a96; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX checklist_produto_planos_plano_id_7fd92a96 ON checklist_produto_planos USING btree (plano_id);


--
-- Name: checklist_produto_planos_produto_id_7456a83a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX checklist_produto_planos_produto_id_7456a83a ON checklist_produto_planos USING btree (produto_id);


--
-- Name: checklist_produto_ramo_id_de36edd0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX checklist_produto_ramo_id_de36edd0 ON checklist_produto USING btree (ramo_id);


--
-- Name: checklist_projeto_ramo_id_13eecc2d; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX checklist_projeto_ramo_id_13eecc2d ON checklist_projeto USING btree (ramo_id);


--
-- Name: cms_aliaspluginmodel_alias_placeholder_id_6d6e0c8a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_aliaspluginmodel_alias_placeholder_id_6d6e0c8a ON cms_aliaspluginmodel USING btree (alias_placeholder_id);


--
-- Name: cms_aliaspluginmodel_plugin_id_9867676e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_aliaspluginmodel_plugin_id_9867676e ON cms_aliaspluginmodel USING btree (plugin_id);


--
-- Name: cms_cmsplugin_language_bbea8a48; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_cmsplugin_language_bbea8a48 ON cms_cmsplugin USING btree (language);


--
-- Name: cms_cmsplugin_language_bbea8a48_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_cmsplugin_language_bbea8a48_like ON cms_cmsplugin USING btree (language varchar_pattern_ops);


--
-- Name: cms_cmsplugin_parent_id_fd3bd9dd; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_cmsplugin_parent_id_fd3bd9dd ON cms_cmsplugin USING btree (parent_id);


--
-- Name: cms_cmsplugin_path_4917bb44_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_cmsplugin_path_4917bb44_like ON cms_cmsplugin USING btree (path varchar_pattern_ops);


--
-- Name: cms_cmsplugin_placeholder_id_0bfa3b26; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_cmsplugin_placeholder_id_0bfa3b26 ON cms_cmsplugin USING btree (placeholder_id);


--
-- Name: cms_cmsplugin_plugin_type_94e96ebf; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_cmsplugin_plugin_type_94e96ebf ON cms_cmsplugin USING btree (plugin_type);


--
-- Name: cms_cmsplugin_plugin_type_94e96ebf_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_cmsplugin_plugin_type_94e96ebf_like ON cms_cmsplugin USING btree (plugin_type varchar_pattern_ops);


--
-- Name: cms_globalpagepermission_group_id_991b4733; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_globalpagepermission_group_id_991b4733 ON cms_globalpagepermission USING btree (group_id);


--
-- Name: cms_globalpagepermission_sites_globalpagepermission_id_46bd2681; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_globalpagepermission_sites_globalpagepermission_id_46bd2681 ON cms_globalpagepermission_sites USING btree (globalpagepermission_id);


--
-- Name: cms_globalpagepermission_sites_site_id_00460b53; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_globalpagepermission_sites_site_id_00460b53 ON cms_globalpagepermission_sites USING btree (site_id);


--
-- Name: cms_globalpagepermission_user_id_a227cee1; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_globalpagepermission_user_id_a227cee1 ON cms_globalpagepermission USING btree (user_id);


--
-- Name: cms_page_application_urls_9ef47497; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_page_application_urls_9ef47497 ON cms_page USING btree (application_urls);


--
-- Name: cms_page_application_urls_9ef47497_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_page_application_urls_9ef47497_like ON cms_page USING btree (application_urls varchar_pattern_ops);


--
-- Name: cms_page_in_navigation_01c24279; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_page_in_navigation_01c24279 ON cms_page USING btree (in_navigation);


--
-- Name: cms_page_is_home_edadca07; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_page_is_home_edadca07 ON cms_page USING btree (is_home);


--
-- Name: cms_page_limit_visibility_in_menu_30db6aa6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_page_limit_visibility_in_menu_30db6aa6 ON cms_page USING btree (limit_visibility_in_menu);


--
-- Name: cms_page_navigation_extenders_c24af8dd; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_page_navigation_extenders_c24af8dd ON cms_page USING btree (navigation_extenders);


--
-- Name: cms_page_navigation_extenders_c24af8dd_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_page_navigation_extenders_c24af8dd_like ON cms_page USING btree (navigation_extenders varchar_pattern_ops);


--
-- Name: cms_page_node_id_c87b85a9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_page_node_id_c87b85a9 ON cms_page USING btree (node_id);


--
-- Name: cms_page_placeholders_page_id_f2ce8dec; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_page_placeholders_page_id_f2ce8dec ON cms_page_placeholders USING btree (page_id);


--
-- Name: cms_page_placeholders_placeholder_id_6b120886; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_page_placeholders_placeholder_id_6b120886 ON cms_page_placeholders USING btree (placeholder_id);


--
-- Name: cms_page_publication_date_684fabf7; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_page_publication_date_684fabf7 ON cms_page USING btree (publication_date);


--
-- Name: cms_page_publication_end_date_12a0c46a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_page_publication_end_date_12a0c46a ON cms_page USING btree (publication_end_date);


--
-- Name: cms_page_publisher_is_draft_141cba60; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_page_publisher_is_draft_141cba60 ON cms_page USING btree (publisher_is_draft);


--
-- Name: cms_page_reverse_id_ffc9ede2; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_page_reverse_id_ffc9ede2 ON cms_page USING btree (reverse_id);


--
-- Name: cms_page_reverse_id_ffc9ede2_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_page_reverse_id_ffc9ede2_like ON cms_page USING btree (reverse_id varchar_pattern_ops);


--
-- Name: cms_page_soft_root_51efccbe; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_page_soft_root_51efccbe ON cms_page USING btree (soft_root);


--
-- Name: cms_pagepermission_group_id_af5af193; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_pagepermission_group_id_af5af193 ON cms_pagepermission USING btree (group_id);


--
-- Name: cms_pagepermission_page_id_0ae9a163; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_pagepermission_page_id_0ae9a163 ON cms_pagepermission USING btree (page_id);


--
-- Name: cms_pagepermission_user_id_0c7d2b3c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_pagepermission_user_id_0c7d2b3c ON cms_pagepermission USING btree (user_id);


--
-- Name: cms_pageuser_created_by_id_8e9fbf83; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_pageuser_created_by_id_8e9fbf83 ON cms_pageuser USING btree (created_by_id);


--
-- Name: cms_pageusergroup_created_by_id_7d57fa39; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_pageusergroup_created_by_id_7d57fa39 ON cms_pageusergroup USING btree (created_by_id);


--
-- Name: cms_placeholder_slot_0bc04d21; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_placeholder_slot_0bc04d21 ON cms_placeholder USING btree (slot);


--
-- Name: cms_placeholder_slot_0bc04d21_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_placeholder_slot_0bc04d21_like ON cms_placeholder USING btree (slot varchar_pattern_ops);


--
-- Name: cms_placeholderreference_placeholder_ref_id_244759b1; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_placeholderreference_placeholder_ref_id_244759b1 ON cms_placeholderreference USING btree (placeholder_ref_id);


--
-- Name: cms_staticplaceholder_draft_id_5aee407b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_staticplaceholder_draft_id_5aee407b ON cms_staticplaceholder USING btree (draft_id);


--
-- Name: cms_staticplaceholder_public_id_876aaa66; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_staticplaceholder_public_id_876aaa66 ON cms_staticplaceholder USING btree (public_id);


--
-- Name: cms_staticplaceholder_site_id_dc6a85b6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_staticplaceholder_site_id_dc6a85b6 ON cms_staticplaceholder USING btree (site_id);


--
-- Name: cms_title_has_url_overwrite_ecf27bb9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_title_has_url_overwrite_ecf27bb9 ON cms_title USING btree (has_url_overwrite);


--
-- Name: cms_title_language_50a0dfa1; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_title_language_50a0dfa1 ON cms_title USING btree (language);


--
-- Name: cms_title_language_50a0dfa1_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_title_language_50a0dfa1_like ON cms_title USING btree (language varchar_pattern_ops);


--
-- Name: cms_title_page_id_5fade2a3; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_title_page_id_5fade2a3 ON cms_title USING btree (page_id);


--
-- Name: cms_title_path_c484314c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_title_path_c484314c ON cms_title USING btree (path);


--
-- Name: cms_title_path_c484314c_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_title_path_c484314c_like ON cms_title USING btree (path varchar_pattern_ops);


--
-- Name: cms_title_publisher_is_draft_95874c88; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_title_publisher_is_draft_95874c88 ON cms_title USING btree (publisher_is_draft);


--
-- Name: cms_title_publisher_state_9a952b0f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_title_publisher_state_9a952b0f ON cms_title USING btree (publisher_state);


--
-- Name: cms_title_slug_4947d146; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_title_slug_4947d146 ON cms_title USING btree (slug);


--
-- Name: cms_title_slug_4947d146_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_title_slug_4947d146_like ON cms_title USING btree (slug varchar_pattern_ops);


--
-- Name: cms_treenode_parent_id_59bb02c4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_treenode_parent_id_59bb02c4 ON cms_treenode USING btree (parent_id);


--
-- Name: cms_treenode_path_6eb22885_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_treenode_path_6eb22885_like ON cms_treenode USING btree (path varchar_pattern_ops);


--
-- Name: cms_treenode_site_id_d3f46985; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_treenode_site_id_d3f46985 ON cms_treenode USING btree (site_id);


--
-- Name: cms_usersettings_clipboard_id_3ae17c19; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_usersettings_clipboard_id_3ae17c19 ON cms_usersettings USING btree (clipboard_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: bitnami
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: bitnami
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: bitnami
--

CREATE INDEX django_session_expire_date_a5c62663 ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: bitnami
--

CREATE INDEX django_session_session_key_c0390e0f_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: django_site_domain_a2e37b91_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_site_domain_a2e37b91_like ON django_site USING btree (domain varchar_pattern_ops);


--
-- Name: djangocms_file_file_file_src_id_74ac14a5; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX djangocms_file_file_file_src_id_74ac14a5 ON djangocms_file_file USING btree (file_src_id);


--
-- Name: djangocms_file_folder_folder_src_id_9558406a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX djangocms_file_folder_folder_src_id_9558406a ON djangocms_file_folder USING btree (folder_src_id);


--
-- Name: djangocms_googlemap_googlemapmarker_icon_id_3b103213; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX djangocms_googlemap_googlemapmarker_icon_id_3b103213 ON djangocms_googlemap_googlemapmarker USING btree (icon_id);


--
-- Name: djangocms_link_link_page_link_id_adba1bc7; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX djangocms_link_link_page_link_id_adba1bc7 ON djangocms_link_link USING btree (internal_link_id);


--
-- Name: djangocms_picture_picture_page_link_id_d5c782e0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX djangocms_picture_picture_page_link_id_d5c782e0 ON djangocms_picture_picture USING btree (link_page_id);


--
-- Name: djangocms_picture_picture_picture_id_f7d6711b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX djangocms_picture_picture_picture_id_f7d6711b ON djangocms_picture_picture USING btree (picture_id);


--
-- Name: djangocms_picture_picture_thumbnail_options_id_59cf80d1; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX djangocms_picture_picture_thumbnail_options_id_59cf80d1 ON djangocms_picture_picture USING btree (thumbnail_options_id);


--
-- Name: djangocms_snippet_snippet_name_260f397b_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX djangocms_snippet_snippet_name_260f397b_like ON djangocms_snippet_snippet USING btree (name varchar_pattern_ops);


--
-- Name: djangocms_snippet_snippet_slug_bd43cd96_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX djangocms_snippet_snippet_slug_bd43cd96_like ON djangocms_snippet_snippet USING btree (slug varchar_pattern_ops);


--
-- Name: djangocms_snippet_snippetptr_snippet_id_56b99614; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX djangocms_snippet_snippetptr_snippet_id_56b99614 ON djangocms_snippet_snippetptr USING btree (snippet_id);


--
-- Name: djangocms_video_videoplayer_poster_id_07790e24; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX djangocms_video_videoplayer_poster_id_07790e24 ON djangocms_video_videoplayer USING btree (poster_id);


--
-- Name: djangocms_video_videosource_source_file_id_16f11167; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX djangocms_video_videosource_source_file_id_16f11167 ON djangocms_video_videosource USING btree (source_file_id);


--
-- Name: djangocms_video_videotrack_src_id_e5a015d8; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX djangocms_video_videotrack_src_id_e5a015d8 ON djangocms_video_videotrack USING btree (src_id);


--
-- Name: easy_thumbnails_source_name_5fe0edc6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX easy_thumbnails_source_name_5fe0edc6 ON easy_thumbnails_source USING btree (name);


--
-- Name: easy_thumbnails_source_name_5fe0edc6_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX easy_thumbnails_source_name_5fe0edc6_like ON easy_thumbnails_source USING btree (name varchar_pattern_ops);


--
-- Name: easy_thumbnails_source_storage_hash_946cbcc9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX easy_thumbnails_source_storage_hash_946cbcc9 ON easy_thumbnails_source USING btree (storage_hash);


--
-- Name: easy_thumbnails_source_storage_hash_946cbcc9_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX easy_thumbnails_source_storage_hash_946cbcc9_like ON easy_thumbnails_source USING btree (storage_hash varchar_pattern_ops);


--
-- Name: easy_thumbnails_thumbnail_name_b5882c31; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX easy_thumbnails_thumbnail_name_b5882c31 ON easy_thumbnails_thumbnail USING btree (name);


--
-- Name: easy_thumbnails_thumbnail_name_b5882c31_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX easy_thumbnails_thumbnail_name_b5882c31_like ON easy_thumbnails_thumbnail USING btree (name varchar_pattern_ops);


--
-- Name: easy_thumbnails_thumbnail_source_id_5b57bc77; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX easy_thumbnails_thumbnail_source_id_5b57bc77 ON easy_thumbnails_thumbnail USING btree (source_id);


--
-- Name: easy_thumbnails_thumbnail_storage_hash_f1435f49; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX easy_thumbnails_thumbnail_storage_hash_f1435f49 ON easy_thumbnails_thumbnail USING btree (storage_hash);


--
-- Name: easy_thumbnails_thumbnail_storage_hash_f1435f49_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX easy_thumbnails_thumbnail_storage_hash_f1435f49_like ON easy_thumbnails_thumbnail USING btree (storage_hash varchar_pattern_ops);


--
-- Name: filer_clipboard_user_id_b52ff0bc; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX filer_clipboard_user_id_b52ff0bc ON filer_clipboard USING btree (user_id);


--
-- Name: filer_clipboarditem_clipboard_id_7a76518b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX filer_clipboarditem_clipboard_id_7a76518b ON filer_clipboarditem USING btree (clipboard_id);


--
-- Name: filer_clipboarditem_file_id_06196f80; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX filer_clipboarditem_file_id_06196f80 ON filer_clipboarditem USING btree (file_id);


--
-- Name: filer_file_folder_id_af803bbb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX filer_file_folder_id_af803bbb ON filer_file USING btree (folder_id);


--
-- Name: filer_file_owner_id_b9e32671; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX filer_file_owner_id_b9e32671 ON filer_file USING btree (owner_id);


--
-- Name: filer_file_polymorphic_ctype_id_f44903c1; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX filer_file_polymorphic_ctype_id_f44903c1 ON filer_file USING btree (polymorphic_ctype_id);


--
-- Name: filer_folder_level_b631d28a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX filer_folder_level_b631d28a ON filer_folder USING btree (level);


--
-- Name: filer_folder_lft_2c2b69f1; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX filer_folder_lft_2c2b69f1 ON filer_folder USING btree (lft);


--
-- Name: filer_folder_owner_id_be530fb4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX filer_folder_owner_id_be530fb4 ON filer_folder USING btree (owner_id);


--
-- Name: filer_folder_parent_id_308aecda; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX filer_folder_parent_id_308aecda ON filer_folder USING btree (parent_id);


--
-- Name: filer_folder_rght_34946267; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX filer_folder_rght_34946267 ON filer_folder USING btree (rght);


--
-- Name: filer_folder_tree_id_b016223c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX filer_folder_tree_id_b016223c ON filer_folder USING btree (tree_id);


--
-- Name: filer_folderpermission_folder_id_5d02f1da; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX filer_folderpermission_folder_id_5d02f1da ON filer_folderpermission USING btree (folder_id);


--
-- Name: filer_folderpermission_group_id_8901bafa; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX filer_folderpermission_group_id_8901bafa ON filer_folderpermission USING btree (group_id);


--
-- Name: filer_folderpermission_user_id_7673d4b6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX filer_folderpermission_user_id_7673d4b6 ON filer_folderpermission USING btree (user_id);


--
-- Name: polls_choice_poll_id_3a553f1a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX polls_choice_poll_id_3a553f1a ON polls_choice USING btree (poll_id);


--
-- Name: aldryn_bootstrap3_bootstrap3listgroupitemplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_01b9ffd2_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3listgroupitemplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_01b9ffd2_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_bootstrap3fileplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_0d372fce_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3fileplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_0d372fce_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_boostrap3panelplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_1f3a5fd6_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3panelplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_1f3a5fd6_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_bootstrap3listgroupplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_224c929a_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3listgroupplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_224c929a_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_boostrap3panelbodyplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_4111e7df_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3panelbodyplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_4111e7df_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_boostrap3wellplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_41136c40_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3wellplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_41136c40_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_bootstrap3carouselslideplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_41eba5f9_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3carouselslideplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_41eba5f9_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_boostrap3buttonplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_4a041852_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3buttonplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_4a041852_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_bootstrap3rowplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_541d9567_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3rowplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_541d9567_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_bootstrap3responsiveplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_58058fe3_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3responsiveplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_58058fe3_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_boostrap3labelplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_5e00e5c7_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3labelplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_5e00e5c7_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_boostrap3panelfooterplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_766b208d_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3panelfooterplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_766b208d_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_bootstrap3codeplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_79f3440c_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3codeplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_79f3440c_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_boostrap3imageplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_853a9c5c_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3imageplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_853a9c5c_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_bootstrap3tabitemplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_9d1cd557_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3tabitemplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_9d1cd557_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_bootstrap3accordionplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_a9d3714d_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3accordionplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_a9d3714d_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_boostrap3citeplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_ac7fb0f8_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3citeplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_ac7fb0f8_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_boostrap3alertplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_b1748a01_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3alertplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_b1748a01_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_boostrap3panelheadingplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_c0c0b6e1_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3panelheadingplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_c0c0b6e1_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_bootstrap3carouselplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_c3abeb84_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3carouselplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_c3abeb84_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_boostrap3blockquoteplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_c4fec007_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3blockquoteplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_c4fec007_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_bootstrap3carouselslidefolderplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_cde052de_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3carouselslidefolderplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_cde052de_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_boostrap3iconplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_cf1150f8_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3iconplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_cf1150f8_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_bootstrap3columnplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_cf3073cd_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3columnplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_cf3073cd_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_bootstrap3accordionitemplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_d977c985_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3accordionitemplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_d977c985_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_bootstrap3tabplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_e14b5130_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3tabplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_e14b5130_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_boostrap3jumbotronplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_e2789ed9_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3jumbotronplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_e2789ed9_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_boostrap3spacerplugin aldryn_bootstrap3_bo_cmsplugin_ptr_id_e2dbcd02_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3spacerplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_cmsplugin_ptr_id_e2dbcd02_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_boostrap3imageplugin aldryn_bootstrap3_bo_file_id_67ad88d7_fk_filer_ima; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3imageplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_file_id_67ad88d7_fk_filer_ima FOREIGN KEY (file_id) REFERENCES filer_image(file_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_bootstrap3fileplugin aldryn_bootstrap3_bo_file_id_c83c321d_fk_filer_fil; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3fileplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_file_id_c83c321d_fk_filer_fil FOREIGN KEY (file_id) REFERENCES filer_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_bootstrap3carouselslidefolderplugin aldryn_bootstrap3_bo_folder_id_232dba10_fk_filer_fol; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3carouselslidefolderplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_folder_id_232dba10_fk_filer_fol FOREIGN KEY (folder_id) REFERENCES filer_folder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_bootstrap3carouselslideplugin aldryn_bootstrap3_bo_image_id_d52f4f75_fk_filer_ima; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3carouselslideplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_image_id_d52f4f75_fk_filer_ima FOREIGN KEY (image_id) REFERENCES filer_image(file_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_bootstrap3carouselslideplugin aldryn_bootstrap3_bo_link_file_id_1512669d_fk_filer_fil; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3carouselslideplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_link_file_id_1512669d_fk_filer_fil FOREIGN KEY (link_file_id) REFERENCES filer_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_boostrap3buttonplugin aldryn_bootstrap3_bo_link_file_id_96f04a7b_fk_filer_fil; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3buttonplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_link_file_id_96f04a7b_fk_filer_fil FOREIGN KEY (link_file_id) REFERENCES filer_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_bootstrap3carouselslideplugin aldryn_bootstrap3_bo_link_page_id_55b29fcc_fk_cms_page_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_bootstrap3carouselslideplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_link_page_id_55b29fcc_fk_cms_page_ FOREIGN KEY (link_page_id) REFERENCES cms_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: aldryn_bootstrap3_boostrap3buttonplugin aldryn_bootstrap3_bo_link_page_id_5cf9d4c7_fk_cms_page_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY aldryn_bootstrap3_boostrap3buttonplugin
    ADD CONSTRAINT aldryn_bootstrap3_bo_link_page_id_5cf9d4c7_fk_cms_page_ FOREIGN KEY (link_page_id) REFERENCES cms_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: checklist_avaliacao checklist_avaliacao_avaliador_id_7c40f0e2_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_avaliacao
    ADD CONSTRAINT checklist_avaliacao_avaliador_id_7c40f0e2_fk_auth_user_id FOREIGN KEY (avaliador_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: checklist_avaliacao checklist_avaliacao_empreendimento_id_9ef95bf3_fk_checklist; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_avaliacao
    ADD CONSTRAINT checklist_avaliacao_empreendimento_id_9ef95bf3_fk_checklist FOREIGN KEY (empreendimento_id) REFERENCES checklist_empreendimento(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: checklist_avaliacao checklist_avaliacao_etapa_projeto_id_ca2ec61e_fk_checklist; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_avaliacao
    ADD CONSTRAINT checklist_avaliacao_etapa_projeto_id_ca2ec61e_fk_checklist FOREIGN KEY (etapa_projeto_id) REFERENCES checklist_etapaprojeto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: checklist_avaliacao checklist_avaliacao_produto_id_58a09123_fk_checklist_produto_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_avaliacao
    ADD CONSTRAINT checklist_avaliacao_produto_id_58a09123_fk_checklist_produto_id FOREIGN KEY (produto_id) REFERENCES checklist_produto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: checklist_avaliacao checklist_avaliacao_projeto_id_ea20648e_fk_checklist_projeto_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_avaliacao
    ADD CONSTRAINT checklist_avaliacao_projeto_id_ea20648e_fk_checklist_projeto_id FOREIGN KEY (projeto_id) REFERENCES checklist_projeto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: checklist_checklist checklist_checklist_produto_id_158bfe0d_fk_checklist_produto_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_checklist
    ADD CONSTRAINT checklist_checklist_produto_id_158bfe0d_fk_checklist_produto_id FOREIGN KEY (produto_id) REFERENCES checklist_produto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: checklist_cms_integration_avaliacaopluginmodel checklist_cms_integr_avaliacao_id_d3b7f5b8_fk_checklist; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_cms_integration_avaliacaopluginmodel
    ADD CONSTRAINT checklist_cms_integr_avaliacao_id_d3b7f5b8_fk_checklist FOREIGN KEY (avaliacao_id) REFERENCES checklist_avaliacao(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: checklist_cms_integration_checklistspluginmodel checklist_cms_integr_checklist_id_313c67b5_fk_checklist; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_cms_integration_checklistspluginmodel
    ADD CONSTRAINT checklist_cms_integr_checklist_id_313c67b5_fk_checklist FOREIGN KEY (checklist_id) REFERENCES checklist_checklist(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: checklist_cms_integration_avaliacaopluginmodel checklist_cms_integr_cmsplugin_ptr_id_46572b99_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_cms_integration_avaliacaopluginmodel
    ADD CONSTRAINT checklist_cms_integr_cmsplugin_ptr_id_46572b99_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: checklist_cms_integration_checklistspluginmodel checklist_cms_integr_cmsplugin_ptr_id_542cd8c3_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_cms_integration_checklistspluginmodel
    ADD CONSTRAINT checklist_cms_integr_cmsplugin_ptr_id_542cd8c3_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: checklist_empreendimento checklist_empreendimento_assinante_id_db825301_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_empreendimento
    ADD CONSTRAINT checklist_empreendimento_assinante_id_db825301_fk_auth_user_id FOREIGN KEY (assinante_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: checklist_itemavaliacao checklist_itemavalia_avaliacao_id_30352297_fk_checklist; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_itemavaliacao
    ADD CONSTRAINT checklist_itemavalia_avaliacao_id_30352297_fk_checklist FOREIGN KEY (avaliacao_id) REFERENCES checklist_avaliacao(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: checklist_itemavaliacao checklist_itemavalia_checklist_id_3296c317_fk_checklist; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_itemavaliacao
    ADD CONSTRAINT checklist_itemavalia_checklist_id_3296c317_fk_checklist FOREIGN KEY (checklist_id) REFERENCES checklist_checklist(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: checklist_itemchecklist checklist_itemcheckl_checklist_id_9b86ccce_fk_checklist; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_itemchecklist
    ADD CONSTRAINT checklist_itemcheckl_checklist_id_9b86ccce_fk_checklist FOREIGN KEY (checklist_id) REFERENCES checklist_checklist(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: checklist_produto_planos checklist_produto_pl_plano_id_7fd92a96_fk_checklist; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_produto_planos
    ADD CONSTRAINT checklist_produto_pl_plano_id_7fd92a96_fk_checklist FOREIGN KEY (plano_id) REFERENCES checklist_plano(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: checklist_produto_planos checklist_produto_pl_produto_id_7456a83a_fk_checklist; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_produto_planos
    ADD CONSTRAINT checklist_produto_pl_produto_id_7456a83a_fk_checklist FOREIGN KEY (produto_id) REFERENCES checklist_produto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: checklist_produto checklist_produto_ramo_id_de36edd0_fk_checklist_ramo_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_produto
    ADD CONSTRAINT checklist_produto_ramo_id_de36edd0_fk_checklist_ramo_id FOREIGN KEY (ramo_id) REFERENCES checklist_ramo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: checklist_projeto checklist_projeto_ramo_id_13eecc2d_fk_checklist_ramo_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY checklist_projeto
    ADD CONSTRAINT checklist_projeto_ramo_id_13eecc2d_fk_checklist_ramo_id FOREIGN KEY (ramo_id) REFERENCES checklist_ramo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_aliaspluginmodel cms_aliaspluginmodel_alias_placeholder_id_6d6e0c8a_fk_cms_place; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_aliaspluginmodel
    ADD CONSTRAINT cms_aliaspluginmodel_alias_placeholder_id_6d6e0c8a_fk_cms_place FOREIGN KEY (alias_placeholder_id) REFERENCES cms_placeholder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_aliaspluginmodel cms_aliaspluginmodel_cmsplugin_ptr_id_f71dfd31_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_aliaspluginmodel
    ADD CONSTRAINT cms_aliaspluginmodel_cmsplugin_ptr_id_f71dfd31_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_aliaspluginmodel cms_aliaspluginmodel_plugin_id_9867676e_fk_cms_cmsplugin_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_aliaspluginmodel
    ADD CONSTRAINT cms_aliaspluginmodel_plugin_id_9867676e_fk_cms_cmsplugin_id FOREIGN KEY (plugin_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_cmsplugin cms_cmsplugin_parent_id_fd3bd9dd_fk_cms_cmsplugin_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_cmsplugin
    ADD CONSTRAINT cms_cmsplugin_parent_id_fd3bd9dd_fk_cms_cmsplugin_id FOREIGN KEY (parent_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_cmsplugin cms_cmsplugin_placeholder_id_0bfa3b26_fk_cms_placeholder_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_cmsplugin
    ADD CONSTRAINT cms_cmsplugin_placeholder_id_0bfa3b26_fk_cms_placeholder_id FOREIGN KEY (placeholder_id) REFERENCES cms_placeholder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_globalpagepermission_sites cms_globalpagepermis_globalpagepermission_46bd2681_fk_cms_globa; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_globalpagepermission_sites
    ADD CONSTRAINT cms_globalpagepermis_globalpagepermission_46bd2681_fk_cms_globa FOREIGN KEY (globalpagepermission_id) REFERENCES cms_globalpagepermission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_globalpagepermission_sites cms_globalpagepermis_site_id_00460b53_fk_django_si; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_globalpagepermission_sites
    ADD CONSTRAINT cms_globalpagepermis_site_id_00460b53_fk_django_si FOREIGN KEY (site_id) REFERENCES django_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_globalpagepermission cms_globalpagepermission_group_id_991b4733_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_globalpagepermission
    ADD CONSTRAINT cms_globalpagepermission_group_id_991b4733_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_globalpagepermission cms_globalpagepermission_user_id_a227cee1_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_globalpagepermission
    ADD CONSTRAINT cms_globalpagepermission_user_id_a227cee1_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_page cms_page_node_id_c87b85a9_fk_cms_treenode_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_page
    ADD CONSTRAINT cms_page_node_id_c87b85a9_fk_cms_treenode_id FOREIGN KEY (node_id) REFERENCES cms_treenode(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_page_placeholders cms_page_placeholder_placeholder_id_6b120886_fk_cms_place; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_page_placeholders
    ADD CONSTRAINT cms_page_placeholder_placeholder_id_6b120886_fk_cms_place FOREIGN KEY (placeholder_id) REFERENCES cms_placeholder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_page_placeholders cms_page_placeholders_page_id_f2ce8dec_fk_cms_page_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_page_placeholders
    ADD CONSTRAINT cms_page_placeholders_page_id_f2ce8dec_fk_cms_page_id FOREIGN KEY (page_id) REFERENCES cms_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_page cms_page_publisher_public_id_d619fca0_fk_cms_page_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_page
    ADD CONSTRAINT cms_page_publisher_public_id_d619fca0_fk_cms_page_id FOREIGN KEY (publisher_public_id) REFERENCES cms_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_pagepermission cms_pagepermission_group_id_af5af193_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_pagepermission
    ADD CONSTRAINT cms_pagepermission_group_id_af5af193_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_pagepermission cms_pagepermission_page_id_0ae9a163_fk_cms_page_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_pagepermission
    ADD CONSTRAINT cms_pagepermission_page_id_0ae9a163_fk_cms_page_id FOREIGN KEY (page_id) REFERENCES cms_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_pagepermission cms_pagepermission_user_id_0c7d2b3c_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_pagepermission
    ADD CONSTRAINT cms_pagepermission_user_id_0c7d2b3c_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_pageuser cms_pageuser_created_by_id_8e9fbf83_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_pageuser
    ADD CONSTRAINT cms_pageuser_created_by_id_8e9fbf83_fk_auth_user_id FOREIGN KEY (created_by_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_pageuser cms_pageuser_user_ptr_id_b3d65592_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_pageuser
    ADD CONSTRAINT cms_pageuser_user_ptr_id_b3d65592_fk_auth_user_id FOREIGN KEY (user_ptr_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_pageusergroup cms_pageusergroup_created_by_id_7d57fa39_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_pageusergroup
    ADD CONSTRAINT cms_pageusergroup_created_by_id_7d57fa39_fk_auth_user_id FOREIGN KEY (created_by_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_pageusergroup cms_pageusergroup_group_ptr_id_34578d93_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_pageusergroup
    ADD CONSTRAINT cms_pageusergroup_group_ptr_id_34578d93_fk_auth_group_id FOREIGN KEY (group_ptr_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_placeholderreference cms_placeholderrefer_cmsplugin_ptr_id_6977ec85_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_placeholderreference
    ADD CONSTRAINT cms_placeholderrefer_cmsplugin_ptr_id_6977ec85_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_placeholderreference cms_placeholderrefer_placeholder_ref_id_244759b1_fk_cms_place; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_placeholderreference
    ADD CONSTRAINT cms_placeholderrefer_placeholder_ref_id_244759b1_fk_cms_place FOREIGN KEY (placeholder_ref_id) REFERENCES cms_placeholder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_staticplaceholder cms_staticplaceholder_draft_id_5aee407b_fk_cms_placeholder_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_staticplaceholder
    ADD CONSTRAINT cms_staticplaceholder_draft_id_5aee407b_fk_cms_placeholder_id FOREIGN KEY (draft_id) REFERENCES cms_placeholder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_staticplaceholder cms_staticplaceholder_public_id_876aaa66_fk_cms_placeholder_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_staticplaceholder
    ADD CONSTRAINT cms_staticplaceholder_public_id_876aaa66_fk_cms_placeholder_id FOREIGN KEY (public_id) REFERENCES cms_placeholder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_staticplaceholder cms_staticplaceholder_site_id_dc6a85b6_fk_django_site_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_staticplaceholder
    ADD CONSTRAINT cms_staticplaceholder_site_id_dc6a85b6_fk_django_site_id FOREIGN KEY (site_id) REFERENCES django_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_title cms_title_page_id_5fade2a3_fk_cms_page_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_title
    ADD CONSTRAINT cms_title_page_id_5fade2a3_fk_cms_page_id FOREIGN KEY (page_id) REFERENCES cms_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_title cms_title_publisher_public_id_003a2702_fk_cms_title_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_title
    ADD CONSTRAINT cms_title_publisher_public_id_003a2702_fk_cms_title_id FOREIGN KEY (publisher_public_id) REFERENCES cms_title(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_treenode cms_treenode_parent_id_59bb02c4_fk_cms_treenode_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_treenode
    ADD CONSTRAINT cms_treenode_parent_id_59bb02c4_fk_cms_treenode_id FOREIGN KEY (parent_id) REFERENCES cms_treenode(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_treenode cms_treenode_site_id_d3f46985_fk_django_site_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_treenode
    ADD CONSTRAINT cms_treenode_site_id_d3f46985_fk_django_site_id FOREIGN KEY (site_id) REFERENCES django_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_usersettings cms_usersettings_clipboard_id_3ae17c19_fk_cms_placeholder_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_usersettings
    ADD CONSTRAINT cms_usersettings_clipboard_id_3ae17c19_fk_cms_placeholder_id FOREIGN KEY (clipboard_id) REFERENCES cms_placeholder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_usersettings cms_usersettings_user_id_09633b2d_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_usersettings
    ADD CONSTRAINT cms_usersettings_user_id_09633b2d_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: bitnami
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_column_column djangocms_column_col_cmsplugin_ptr_id_e7ab9661_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_column_column
    ADD CONSTRAINT djangocms_column_col_cmsplugin_ptr_id_e7ab9661_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_column_multicolumns djangocms_column_mul_cmsplugin_ptr_id_aa54884e_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_column_multicolumns
    ADD CONSTRAINT djangocms_column_mul_cmsplugin_ptr_id_aa54884e_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_file_file djangocms_file_file_cmsplugin_ptr_id_428f5041_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_file_file
    ADD CONSTRAINT djangocms_file_file_cmsplugin_ptr_id_428f5041_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_file_file djangocms_file_file_file_src_id_74ac14a5_fk_filer_file_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_file_file
    ADD CONSTRAINT djangocms_file_file_file_src_id_74ac14a5_fk_filer_file_id FOREIGN KEY (file_src_id) REFERENCES filer_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_file_folder djangocms_file_folde_cmsplugin_ptr_id_b258b166_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_file_folder
    ADD CONSTRAINT djangocms_file_folde_cmsplugin_ptr_id_b258b166_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_file_folder djangocms_file_folder_folder_src_id_9558406a_fk_filer_folder_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_file_folder
    ADD CONSTRAINT djangocms_file_folder_folder_src_id_9558406a_fk_filer_folder_id FOREIGN KEY (folder_src_id) REFERENCES filer_folder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_googlemap_googlemapmarker djangocms_googlemap__cmsplugin_ptr_id_bdd9e018_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_googlemap_googlemapmarker
    ADD CONSTRAINT djangocms_googlemap__cmsplugin_ptr_id_bdd9e018_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_googlemap_googlemap djangocms_googlemap__cmsplugin_ptr_id_c8493888_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_googlemap_googlemap
    ADD CONSTRAINT djangocms_googlemap__cmsplugin_ptr_id_c8493888_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_googlemap_googlemaproute djangocms_googlemap__cmsplugin_ptr_id_f6a135c6_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_googlemap_googlemaproute
    ADD CONSTRAINT djangocms_googlemap__cmsplugin_ptr_id_f6a135c6_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_googlemap_googlemapmarker djangocms_googlemap__icon_id_3b103213_fk_filer_ima; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_googlemap_googlemapmarker
    ADD CONSTRAINT djangocms_googlemap__icon_id_3b103213_fk_filer_ima FOREIGN KEY (icon_id) REFERENCES filer_image(file_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_link_link djangocms_link_link_cmsplugin_ptr_id_10f7b2f2_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_link_link
    ADD CONSTRAINT djangocms_link_link_cmsplugin_ptr_id_10f7b2f2_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_link_link djangocms_link_link_internal_link_id_41549c8e_fk_cms_page_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_link_link
    ADD CONSTRAINT djangocms_link_link_internal_link_id_41549c8e_fk_cms_page_id FOREIGN KEY (internal_link_id) REFERENCES cms_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_picture_picture djangocms_picture_pi_cmsplugin_ptr_id_0e797309_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_picture_picture
    ADD CONSTRAINT djangocms_picture_pi_cmsplugin_ptr_id_0e797309_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_picture_picture djangocms_picture_pi_picture_id_f7d6711b_fk_filer_ima; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_picture_picture
    ADD CONSTRAINT djangocms_picture_pi_picture_id_f7d6711b_fk_filer_ima FOREIGN KEY (picture_id) REFERENCES filer_image(file_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_picture_picture djangocms_picture_pi_thumbnail_options_id_59cf80d1_fk_filer_thu; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_picture_picture
    ADD CONSTRAINT djangocms_picture_pi_thumbnail_options_id_59cf80d1_fk_filer_thu FOREIGN KEY (thumbnail_options_id) REFERENCES filer_thumbnailoption(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_picture_picture djangocms_picture_picture_link_page_id_dbec9beb_fk_cms_page_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_picture_picture
    ADD CONSTRAINT djangocms_picture_picture_link_page_id_dbec9beb_fk_cms_page_id FOREIGN KEY (link_page_id) REFERENCES cms_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_snippet_snippetptr djangocms_snippet_sn_cmsplugin_ptr_id_4cf9435f_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_snippet_snippetptr
    ADD CONSTRAINT djangocms_snippet_sn_cmsplugin_ptr_id_4cf9435f_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_snippet_snippetptr djangocms_snippet_sn_snippet_id_56b99614_fk_djangocms; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_snippet_snippetptr
    ADD CONSTRAINT djangocms_snippet_sn_snippet_id_56b99614_fk_djangocms FOREIGN KEY (snippet_id) REFERENCES djangocms_snippet_snippet(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_style_style djangocms_style_styl_cmsplugin_ptr_id_820d9b8f_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_style_style
    ADD CONSTRAINT djangocms_style_styl_cmsplugin_ptr_id_820d9b8f_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_text_ckeditor_text djangocms_text_ckedi_cmsplugin_ptr_id_946882c1_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_text_ckeditor_text
    ADD CONSTRAINT djangocms_text_ckedi_cmsplugin_ptr_id_946882c1_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_video_videoplayer djangocms_video_vide_cmsplugin_ptr_id_0367e96e_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_video_videoplayer
    ADD CONSTRAINT djangocms_video_vide_cmsplugin_ptr_id_0367e96e_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_video_videosource djangocms_video_vide_cmsplugin_ptr_id_474cebf9_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_video_videosource
    ADD CONSTRAINT djangocms_video_vide_cmsplugin_ptr_id_474cebf9_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_video_videotrack djangocms_video_vide_cmsplugin_ptr_id_edcbdf6c_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_video_videotrack
    ADD CONSTRAINT djangocms_video_vide_cmsplugin_ptr_id_edcbdf6c_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_video_videoplayer djangocms_video_vide_poster_id_07790e24_fk_filer_ima; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_video_videoplayer
    ADD CONSTRAINT djangocms_video_vide_poster_id_07790e24_fk_filer_ima FOREIGN KEY (poster_id) REFERENCES filer_image(file_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_video_videosource djangocms_video_vide_source_file_id_16f11167_fk_filer_fil; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_video_videosource
    ADD CONSTRAINT djangocms_video_vide_source_file_id_16f11167_fk_filer_fil FOREIGN KEY (source_file_id) REFERENCES filer_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_video_videotrack djangocms_video_videotrack_src_id_e5a015d8_fk_filer_file_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY djangocms_video_videotrack
    ADD CONSTRAINT djangocms_video_videotrack_src_id_e5a015d8_fk_filer_file_id FOREIGN KEY (src_id) REFERENCES filer_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: easy_thumbnails_thumbnail easy_thumbnails_thum_source_id_5b57bc77_fk_easy_thum; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY easy_thumbnails_thumbnail
    ADD CONSTRAINT easy_thumbnails_thum_source_id_5b57bc77_fk_easy_thum FOREIGN KEY (source_id) REFERENCES easy_thumbnails_source(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: easy_thumbnails_thumbnaildimensions easy_thumbnails_thum_thumbnail_id_c3a0c549_fk_easy_thum; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY easy_thumbnails_thumbnaildimensions
    ADD CONSTRAINT easy_thumbnails_thum_thumbnail_id_c3a0c549_fk_easy_thum FOREIGN KEY (thumbnail_id) REFERENCES easy_thumbnails_thumbnail(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filer_clipboard filer_clipboard_user_id_b52ff0bc_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_clipboard
    ADD CONSTRAINT filer_clipboard_user_id_b52ff0bc_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filer_clipboarditem filer_clipboarditem_clipboard_id_7a76518b_fk_filer_clipboard_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_clipboarditem
    ADD CONSTRAINT filer_clipboarditem_clipboard_id_7a76518b_fk_filer_clipboard_id FOREIGN KEY (clipboard_id) REFERENCES filer_clipboard(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filer_clipboarditem filer_clipboarditem_file_id_06196f80_fk_filer_file_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_clipboarditem
    ADD CONSTRAINT filer_clipboarditem_file_id_06196f80_fk_filer_file_id FOREIGN KEY (file_id) REFERENCES filer_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filer_file filer_file_folder_id_af803bbb_fk_filer_folder_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_file
    ADD CONSTRAINT filer_file_folder_id_af803bbb_fk_filer_folder_id FOREIGN KEY (folder_id) REFERENCES filer_folder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filer_file filer_file_owner_id_b9e32671_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_file
    ADD CONSTRAINT filer_file_owner_id_b9e32671_fk_auth_user_id FOREIGN KEY (owner_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filer_file filer_file_polymorphic_ctype_id_f44903c1_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_file
    ADD CONSTRAINT filer_file_polymorphic_ctype_id_f44903c1_fk_django_co FOREIGN KEY (polymorphic_ctype_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filer_folder filer_folder_owner_id_be530fb4_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_folder
    ADD CONSTRAINT filer_folder_owner_id_be530fb4_fk_auth_user_id FOREIGN KEY (owner_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filer_folder filer_folder_parent_id_308aecda_fk_filer_folder_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_folder
    ADD CONSTRAINT filer_folder_parent_id_308aecda_fk_filer_folder_id FOREIGN KEY (parent_id) REFERENCES filer_folder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filer_folderpermission filer_folderpermission_folder_id_5d02f1da_fk_filer_folder_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_folderpermission
    ADD CONSTRAINT filer_folderpermission_folder_id_5d02f1da_fk_filer_folder_id FOREIGN KEY (folder_id) REFERENCES filer_folder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filer_folderpermission filer_folderpermission_group_id_8901bafa_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_folderpermission
    ADD CONSTRAINT filer_folderpermission_group_id_8901bafa_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filer_folderpermission filer_folderpermission_user_id_7673d4b6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_folderpermission
    ADD CONSTRAINT filer_folderpermission_user_id_7673d4b6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filer_image filer_image_file_ptr_id_3e21d4f0_fk_filer_file_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filer_image
    ADD CONSTRAINT filer_image_file_ptr_id_3e21d4f0_fk_filer_file_id FOREIGN KEY (file_ptr_id) REFERENCES filer_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: polls_choice polls_choice_poll_id_3a553f1a_fk_polls_poll_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY polls_choice
    ADD CONSTRAINT polls_choice_poll_id_3a553f1a_fk_polls_poll_id FOREIGN KEY (poll_id) REFERENCES polls_poll(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

